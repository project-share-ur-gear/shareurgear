<style>

</style>

@extends('layouts.app')

@section('content')

<?php
    if(!empty($site_configs['stripe_client_id'])){
        $stripeClientId=$site_configs['stripe_client_id'];
        // $redirectUrl= "http://192.168.0.98/shareurgear/payout-setting";
        $redirectUrl= "https://www.webdemoserver.live/shareurgear/payout-setting";
        $stripeUrl 	= "https://connect.stripe.com/express/oauth/authorize?redirect_uri=".$redirectUrl."&client_id=".$stripeClientId;
    }
?>


<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container contact-box common-form">
        <!--  -->
    
        <h2 class="login-title">Payout Settings</h2>
                <div class="border-lines">Click the button below to setup your bank account connection for receiving the payout on the website.</div>
                <br>
                 
                @if($stripe_account=='1')
                    <div class=" account-txt mb-3 font-weight-500">
                        <h6>{{ __('Your account is connected successfully.') }}</h6>
                        <div><span class="">{{ __('general.account_id') }} <b>{{$client_account_id}} </b></span></div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-4">
                            </div>
                            <div class="col-sm-3">
                            <button class="btn  submit-btn my-3 " id="link-button" data-default="Save" type="button">Update Account</button> 
                            </div>
                        </div>
                    </div>
                @else
                    <div class="container">
                        <div class="contact-dots-panels" id="accountBox">    
                            <div class="container">
                                <div class="form-group">
                                    <label for="" class="">Date Of Birth </label>
                                    <input type="text" class="form-control Required" value="" id="stripe_date" name="stripe_date" required="" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <label>SSN <small>(Last 4 digits)</small><span class="needed"></span></label>
                                    <input class="form-control digits required" minlength="4" maxlength="4" name="ssn_number" id="ssn_number" type="text" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-3">
                                <button class="btn  submit-btn my-3 " id="link-button" data-default="Save" type="button">Create Account</button> 
                            </div>
                        </div>
                    </div>
                @endif 
                    <!--  -->
        </div>
</section>


</div>



<link href="{{ asset('public/plugins/bootstrap-datepicker/css/datepicker.css') }}" rel="stylesheet" type="text/css">
<script src="{{ asset('public/plugins/bootstrap-fileinput-master/bootstrap-fileinput.js') }}"></script>
<script src="{{ asset('public/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>


<script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script>

<script src="https://cdn.plaid.com/link/v2/stable/link-initialize.js"></script>
<script type="text/javascript">
(async function() {

  const configs = {
    // Pass the link_token generated in step 2.
    token: '{{$link_token}}',
    onLoad: function() {
      // The Link module finished loading.
    },
    onSuccess: function(public_token, metadata) {
      // The onSuccess function is called when the user has
      // successfully authenticated and selected an account to
      // use.
      //
      // When called, you will send the public_token
      // and the selected account ID, metadata.accounts,
      // to your backend app server.
      //
      stripeconnect(public_token,metadata.accounts[0].id);
    
    },
    onExit: async function(err, metadata) {
      // The user exited the Link flow.
      if (err != null) {
          // The user encountered a Plaid API error
          // prior to exiting.
      }
      // metadata contains information about the institution
      // that the user selected and the most recent
      // API request IDs.
      // Storing this information can be helpful for support.
    },
  };

  var linkHandler = Plaid.create(configs);

  document.getElementById('link-button').onclick = function() {
    @if($stripe_account!='1')
        if($('#stripe_date').val()==''){
            $.notify({
                message: 'Please enter your date of birth, is required.'
                },{
                type: 'danger',
                timer: 1000
            });
            return false;
        }
        if($('#ssn_number').val()==''){
            $.notify({
                message: 'Please enter your last 4 digit of SSN number, is required.'
                },{
                type: 'danger',
                timer: 1000
            });
            return false;
        }
    @endif
    linkHandler.open();
  };
})();

function stripeconnect(plaidPubToken,plaidAccToken){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var dob  = $('#stripe_date').val();
    var ssn  = $('#ssn_number').val();

    @if($stripe_account!='1')
        if(dob==''){
            $.notify({
                message: 'Please enter your date of birth, is required.'
                },{
                type: 'danger',
                timer: 1000
            });
            return false;
        }
        if(ssn==''){
            $.notify({
                message: 'Please enter your last 4 digit of SSN number, is required.'
                },{
                type: 'danger',
                timer: 1000
            });
            return false;
        }
    @endif

    $.blockUI({
        css: {
            backgroundColor: 'transparent',
            border: 'none'
        },
        message: '<div class="spinner"><div class="spinner-border m-5" role="status"><span class="sr-only">Loading...</span></div></div>',
        baseZ: 1500,
        overlayCSS: {
            backgroundColor: '#FFFFFF',
            opacity: 0.7,
            cursor: 'wait'
        }
    });
    
    $.ajax({
        url: "{{route('profile.stripeconnect')}}",
        method: 'POST',
        dataType: 'json',
        data:{
            plaidPubToken : plaidPubToken, plaidAccToken: plaidAccToken,dob:dob,ssnumber:ssn
        },
        success:function(response) {
            if(response.success)
            {     
                $.notify({ message: response.msg },{ type: 'success', timer: 1000 });
                setTimeout(() => { location.reload(); }, 1200);
            }else{
                $.unblockUI();
                $.notify({ message: response.msg },{ type: 'danger', timer: 1000 });
            }
        }
    });
}

</script>



<script type="text/javascript">
    $(document).ready(function() {
        var maxBirthdayDate = new Date();
        maxBirthdayDate.setFullYear( maxBirthdayDate.getFullYear() - 13 );
        $("#stripe_date").datepicker({
            endDate:maxBirthdayDate,
            format:'dd/mm/yyyy',
            startDate:'01/01/1900',
        });
    });

    function changeAccount() {
        $('#accountBox').removeClass('d-none');
        $('#change_account').remove();
        $('.border-lines').removeAttr("style");
        $('.bottom_box').removeClass('d-none');
    }

    $('.save_data_btn').click(function(event) {
        if($('.stripe_connect_form').valid()){
            $('.stripe_connect_form').submit();
        }
    });

    jQuery(document).ready(function($) {
        function init() {
            var input = document.getElementById('address');
            var options = {
            types: ['geocode'],  
            //componentRestrictions: {country: countryCode}
            };
            var autocomplete = new google.maps.places.Autocomplete(input,options);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                    $('.location-help-block').remove();
                    document.getElementById('lat_address').value = place.geometry.location.lat();
                    $('span[for="lat_address"]').remove();
                    document.getElementById('long_address').value = place.geometry.location.lng();
                    $('span[for="long_address"]').remove();

                    $('#client_city').val('');
                        $('#client_state').val('');
                        $('#client_postal_code').val('');
                        $('#client_address2').val('');
                    for (var i = 0; i < place.address_components.length; i++) {
                    
                                    var addressType = place.address_components[i].types[0];	
                            
                            if(addressType=='locality'){
                                var val = place.address_components[i].long_name;
                                $('#client_city').val(val);
                                $('span[for="client_city"]').remove();
                            }
                            if(addressType=='colloquial_area' && $('#client_city').val()==''){
                                var val = place.address_components[i].long_name;
                                alert(val);
                                $('#client_city').val(val);
                                $('span[for="client_city"]').remove();
                            } 
                            if(addressType=='route'){
                                var val = place.address_components[i].long_name;
                                $('#client_address2').val(val);
                                $('span[for=" client_address2"]').remove();
                            }    
                            if(addressType=='administrative_area_level_1'){
                                var val = place.address_components[i].long_name;
                                $('#client_state').val(val);
                                $('span[for="client_state"]').remove();
                            }
                            if(addressType=="postal_code"){
                                var val = place.address_components[i].long_name;
                                $('#client_postal_code').val(val);
                                $('span[for="client_postal_code"]').remove();
                            }	      
                    }
            });
        }
        
        //   $('#phone').inputmask('(999)-999-9999');
        google.maps.event.addDomListener(window, 'load', init);

    });
</script>

@endsection()