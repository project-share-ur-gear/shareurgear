
<style type="text/css">
.modal-header .modal-title {
    font-size: 20px;
    text-align: center;
    display: inline-block;
    width: 100%;
    margin-top: 18px;
    font-weight: bold;
    margin-bottom: 10px;
}
</style>

@extends('layouts.app')

@section('content')
@php
    $profileImage=FRONT_IMG.'/nophoto.png';
    if(Auth::guard('user')->user()->profile_image!='')
        $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.Auth::guard('user')->user()->profile_image);
@endphp



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <form class="profile_form" id="profile_form" action="{{ url('update-profile') }}" method="post">
        @csrf
    <div class="container">
        <!--  -->

        <h2 class="login-title">Account Settings</h2>
        <div class="contact-dots-panels pd-panels">
            <div class="contact-box common-form">
                <!-- user-profile -->
                <div class="user-profile">
                    <div class="profile-user-img">
                        <img src="{{ $profileImage }}" alt="">
                        <div class="user-edit  " onclick="openProfileImageModal()"><img src="{{ FRONT_IMG.'/draw.png' }}"></div>
                    </div>
                </div>
                <!--  -->


                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" value="{{$userInfo->name}}" required name="name" id="name">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control checkemailregister email" value="{{$userInfo->email}}"  id="email" name="email"    >
                </div>
                <div class="form-group" style="display:none;" id="pass_email">
                        <label>Current Password  <small>Required in case of changing email</small></label>
                    <input readonly onfocus="this.removeAttribute('readonly')" type="password" name="email_password" id="email_password" class="form-control required" autocomplete="off">
                </div>


                <div class="form-group">
                    <label for="">Phone Number</label>
                    <input type="text" class="form-control Required" value="{{$userInfo->phone_number}}"   name="phone_number" id="phone_number" Required      >
                </div>
                <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" class="form-control Required" value="{{$userInfo->address}}"  id="address" name="address" Required >
                    <input type="text" class="form-control Required d-none" value="{{$userInfo->latitude}}"  id="lat_address" name="lat_address" Required >
                    <input type="text" class="form-control Required d-none" value="{{$userInfo->longitude}}"  id="long_address" name="long_address" Required >

                </div>
                <!-- Booking Auto Approval  -->
                <div class="change-password-box booking-pay-box">
                    <h2 class="ch-title">Booking Auto Approval</h2>
                    <div class="toggles">
                        <span>No</span>
                        <label class="switch">
                            <input type="checkbox" name="booking_auto_approval" id="booking_auto_approval" value="1" {{ ($userInfo->booking_autoapproval_status=='1')?'checked':'' }}>
                            <span class="slider round"></span>
                        </label>
                        <span>Yes</span>
                    </div>
                    <p class="pay-booking"><span>Yes:</span> Customer payment will be charged instantly and booking will be considered as approved automatically: </p>
                    <p class="pay-booking"><span>No:</span> Customer payment will be authorized and booking will have to be approved manually by you in your bookings section, you need to approve/decline the booking within 24 hours or 12 hours before booking date/time. </p>
                </div>

                <!-- Close  -->
                <div class="check-boxs">
                    <div class="checks custom-checks">
                        <label class="checkbox"> Change Password 
                            <input type="checkbox" name="cpss" value="1" id="cpss">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="change-password-box"  id="cpasswd" style="display: none;" >
                    <h2 class="ch-title">Change Password</h2>
                    <div class="form-group">
                        <label for="">Current Password</label>
                        <input type="password" class="form-control current_password"  id="client_old_password" name="client_old_password"  >
                    </div>
                    <div class="form-group">
                        <label for="">New Password</label>
                        <input type="password" class="form-control passcheck" id="client_password" name="client_password" >
                    </div>
                    <div class="form-group">
                        <label for="">Confirm New Password</label>
                        <input type="password" class="form-control" id="client_rpassword" name="client_rpassword">
                    </div>
                </div>

                <!-- <div class="btn  submit-btn my-3">save</div> -->
                <button class="btn  submit-btn my-3 save_data_btn" data-inprocess="Saving..." data-default="Save" type="button">Save</button> 

            </div>
        </div>
        <!--  -->
    </div>
    </form>
</section>


</div>



<div class="modal upload-img-modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-500" id="modal-title">{{ __('general.Upload Profile Image') }} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/user/saveprofileimage', 'method' => 'post','name' => 'imageForm', 'id' => 'imageForm','enctype'=> 'multipart/form-data']) !!}
                    @method('PATCH')
                    @csrf
                    <div class="d-block">
                        <div class="image-uploading text-center">
                            @php
                                $profileImage=FRONT_IMG.'/nophoto.png';
                                if(Auth::guard('user')->user()->profile_image!='')
                                    $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.Auth::guard('user')->user()->profile_image);
                            @endphp
                            <div class="modalimagebox">
                                <div class="profileImageBox" id="profile_image_preview" style="background-image:url({{$profileImage}})"></div>
                            </div>
                            <div class="d-none hide">{!! Form::file('profile_image',array('id'=>'profile_image','class'=>'required')) !!}</div>
                            <div>
                                <button class="btn btn-primary image-upload-btn mt-3" type="button" onclick="uploadProfileImage()"> {{ __('general.UPLOAD IMAGE') }}</button>
                            </div>
                            <p class="text-dark-grey mt-3">{{ __('general.Allowed file format jpg, png') }}</p>
                        </div><!--image=-uploading-->
                        <div class="btn-box text-center">
                            <button style="display: inline-block;" class="provider-btn register-bg prev-btn btn" data-dismiss="modal">{{ __('general.cancel') }}</button>
                            <button style="display: inline-block;" class="provider-btn register-bg btn" onClick="saveProfileImage()" type="button">{{ __('general.save') }}</button>
                        </div>
                        <div class="Clear"></div>
                    </div>
                {!! Form::close() !!} 
            </div>

        </div>
    </div>
</div>
 
<script src="{{asset('public/plugins/inputmask/js/inputmask.js')}}"></script>
<script src="{{asset('public/plugins/inputmask/js/inputmask.extensions.js')}}"></script>
<script src="{{asset('public/plugins/inputmask/js/inputmask.date.extensions.js')}}"></script>
<script src="{{asset('public/plugins/inputmask/js/jquery.inputmask.js')}}"></script>
<script src="{{asset('public/plugins/inputmask/css/inputmask.css')}}"></script>


<script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript">

    $('#cpss').change(function() {
        if(this.checked == true){
            document.getElementById("cpasswd").style.display = "block";

            $("#client_rpassword").addClass('required');
            $("#client_password").addClass('required');
            $("#client_old_password").addClass('required');
            $('#empform').validate({
                /*ignore:'',*/
                rules:{
                    client_old_password: {
                        minlength: 8
                    },
                    client_password: {
                        minlength: 8,
                        //notDefaultText: true
                    },
                    client_rpassword: {
                        equalTo: '#client_password',
                        minlength: 8,
                    },
                },
            });
        }
        else{
            $("#client_rpassword").removeClass('required');
            $("#client_password").removeClass('required');
            $("#client_old_password").removeClass('required');
            document.getElementById("cpasswd").style.display = "none";
        }
    });

    var currentEmail ="{{$userInfo->email}}";
    jQuery(document).ready(function($) {

        $('#phone_number').inputmask('9[9]-999999999[9]');
        $('#profile_form').validate({
                rules: {
                name:{
                    required: true,
                    specialChar:true,
                    noSpace:true,
                    maxlength:30
                },
                
                email:{
                    required: true,
                },
                
                phone_number:{
                    required: true,
                },
                address:{
                    required: true,
                    maxlength:500    
                },
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element);
                
            }
        });

        $('.save_data_btn').click(function(event) {
            
            /* Act on the event */
            if($('#profile_form').valid()){
                freezeButton('.save_data_btn',$('.save_data_btn').data('inprocess'),'disabled');
                $('#profile_form').submit();
            }else{
                var ele  = "";
                $('.help-block').each(function(index, el) {
                    if($(el).is(':visible')  && ele==""){
                        ele = el;
                    }   
                });
                $('html, body').animate({
                        scrollTop: $(ele).offset().top-150
                }, 500);
            }
        });

        $('#email').keyup(function(event) {
            /* Act on the event */
            var upEmail = $.trim($(this).val());
            if(currentEmail!=upEmail){
                $('#pass_email').fadeIn();
            }else{
                $('#pass_email').fadeOut();
            }
        });

        function init() {
            var input = document.getElementById('address');
            var options = {
            types: ['geocode'],  
            //componentRestrictions: {country: countryCode}
            };
            var autocomplete = new google.maps.places.Autocomplete(input,options);
        
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            
            $('.location-help-block').remove();


            document.getElementById('lat_address').value = place.geometry.location.lat();
            $('span[for="lat_address"]').remove();
            document.getElementById('long_address').value = place.geometry.location.lng();
            $('span[for="long_address"]').remove();


            if(document.getElementById('lat_address').value == '' && document.getElementById('long_address').value== ''){
                $("#file_error1").html("Please enter the location from given suggestions");
                $("#file_error1").css("width","100%");
                $("#file_error1").css("margin-top",".286rem");
                $("#file_error1").css("font-size", "80%");
                $("#file_error1").css("color","#f44336");
                isSubmit=false;
                return false;
            }else{
                $("#file_error1").html("");
                isSubmit=true;
            }
            // console.log(document.getElementById('client_address_lat').value);
            // console.log(document.getElementById('client_address_long').value);
            //to remove error if address is filled
            $("input[name='address']").valid();
            
            });
        }

        google.maps.event.addDomListener(window, 'load', init);
    });

    $(document).on('change','.btn-toggle',function(){
        if($(this).prop("checked") == true){
            //run code
        }else{
            //run code
        }
    });
</script>

@endsection()


