@extends('layouts.app')
@section('content')

@php
$editMode=0;
$categoryId = '';
$serviceId = '';
$pageUrl    = '/save-service'; 
if(!empty($service)){
    $pageUrl    = '/update-service/'.$service->id; 
    $editMode = 1;
    $categoryId = $service->us_category_id;
    $serviceId = $service->us_service_id;
}
$locale = App::getLocale();
@endphp

<section class="account_profile provider-profiles">
    <div class="container">
        <div class="account_pages">

            <!--- Left side Panel --->
            @include('frontend.sidebar')

            <div class="right_account common-form">
                {!! Form::open(['url' => $pageUrl, 'method' => 'post', 'class' => 'profile_form', 'name' => 'profileForm', 'id' => 'profileForm']) !!}

                @if(!empty($service))
                    @method('PATCH')
                @endif

                @csrf
                <div class="personal_data_info">
                    <div class="data-services">
                        <h2 class="title">{{ $pageTitle }}</h2>
                        <a href="{{ SITE_HTTP_URL.'/my-services' }}" class="add-btns">{{ __('general.back') }}</a>
                    </div>
                    <div class="add-services">
                        <div class="form-group"><label>{{ __('general.Select a Category') }}</label>
                            <select name="us_category_id" class="form-control categoryselector required selectpicker " id="us_category_id">
                                <option value="">{{ __('general.choose') }}</option>
                                @foreach($servicecategories as $categories)
                                <option value="{{ $categories->id }}" 
                                    <?php 
                                    if(!empty($service) && $service->us_category_id==$categories->id){ 
                                        echo "selected=selected";
                                    }
                                    else{
                                        if(old('us_category_id') == $categories->id){
                                            echo "selected";
                                        }
                                    }
                                    ?>
                                    >
                                    {{ $locale=='en'?$categories->name_en:$categories->name_fr }}
                                </option>
                                @endforeach()
                            </select>
                            <span for="us_category_id" class="help-block" style="display: none">{{ __('general.required_field') }}</span>
                        </div>
                        <div class="form-group"><label>{{ __('general.Select a Service') }}</label>
                            <select name="us_service_id" class="form-control serviceselector selectpicker required" id="us_service_id">
                                <option value="">{{ __('general.choose') }}</option>
                            </select>
                            <span for="us_service_id" class="help-block" style="display: none">{{ __('general.required_field') }}</span>
                        </div>
                        <div class="form-group">
                            <label>{{ __('general.price') }}</label>
                            {!! Form::text('us_price', !empty($service)?$service->us_price+0:'' ?? old('us_price'),array('class'=>'form-control number required', 'maxlength'=>'8','onkeypress'=>'return  validateFloatKeyPress(this,event)')) !!}
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label>{{ __('general.description') }}</label>
                        {!! Form::textarea('us_desc', $service->us_desc ?? old('us_desc'),array('class'=>'form-control required','rows'=>'4', 'maxlength'=>'1000','placeholder'=>__('general.Tell us more about what you offer'),'id'=>'us_desc')) !!}
                    </div>
                    <div class="form-group">
                        <label>{{ __('general.years_of_exp') }}</label>
                        {!! Form::text('us_exp_year',$service->us_exp_year ?? old('us_exp_year'),array('required'=>'required', 'class'=>'form-control digits required', 'maxlength'=>'2' )) !!}
                    </div>
                    <div class="form-group mb-3">
                        <label>
                        {{ __('general.Maximum distance from your address to provide this service') }}<span class="needed">*</span>
                        </label>
                        <div class="input-group">
                            {!! Form::text('us_max_distance',$service->us_max_distance ?? old('us_max_distance'),array('required'=>'required', 'class'=>'form-control digits required', 'maxlength'=>'2' )) !!}
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2">{{ __('general.in KM') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="bottom_box">
                        <button type="submit" id="payBtn" class="provider-btn register-bg continue-btn">{{ __('general.save') }}</button>
                    </div>
                </div>
                
                {!! Form::close() !!}
            </div>

        </div>
    </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
    var editMode = parseInt('{{ $editMode }}');
    var categoryId = '{{ $categoryId }}';
    var serviceId = '{{ $serviceId  }}';
    if(editMode==1){
        setTimeout(function(){
            getServices(serviceId,categoryId);
        },1000)
    }
})

</script>

@endsection