@extends('layouts.app')
@section('content')

<style>
header{ box-shadow: 0px 2px 10px 0px rgba(170, 170, 170, 0.32);}
.header-section{height:auto !important;}
.bootstrap-select .btn{padding: .84rem .84rem;background-color:#f1f6fb !important;color:#212529;}
</style>

<section class="account_profile provider-profiles">
    <div class="container">
        <div class="account_pages">
            <!--- Left side Panel --->
            @include('frontend.sidebar')

            <div class="right_account common-form profile-navigatorrs">
                <!-- tabbing -->
                <div class="nav nav-tabs nav-fill  d-none d-md-flex"  id="nav-tab" role="tablist">
                    <a onclick="historyPusher('edit','Personal Information')" class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" data-type="edit">{{ __('general.personal_information') }}</a>
                    <a onclick="historyPusher('password','Change Password')" class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false" data-type="password">{{ __('general.change_password') }} </a>
                    @if(Auth::guard('user')->user()->type=='provider')
                    <a onclick="historyPusher('experience','Experience & Certification')" class="nav-item nav-link" id="nav-payout-tab" data-toggle="tab" href="#nav-payout" role="tab" aria-controls="nav-payout" aria-selected="false" data-type="experience">{{ __('general.experience_certification') }}</a>
                    <a onclick="historyPusher('payout','Payout Information')" class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false" data-type="payout">
                    {{ __('general.payout_information') }} 
                    </a>
                    @else
                    <a onclick="historyPusher('payout','Payment Information')" class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false" data-type="payout">
                        {{ __('general.banking_information') }}  
                    </a>
                    @endif
                    <a onclick="historyPusher('background','Background Check')" class="nav-item nav-link" id="nav-about-tab" data-toggle="tab" href="#nav-about" role="tab" aria-controls="nav-about" aria-selected="false" data-type="background">
                    {{ __('general.ID_&_background_check') }} 
                    </a>
                </div>
                <div class="tab-content" id="nav-tabContent">
                    <!--- Edit Profile --->
                    @include('frontend.user.editprofile')

                    <!--- Change Password --->
                    @include('frontend.user.changepassword')

                    @if(Auth::guard('user')->user()->type=='provider')
                    <!--- Experience & Certification --->
                    @include('frontend.user.experience')
                    @endif

                    <!--- Payout Information --->
                    @include('frontend.user.payoutinformation')

                    <!--- Background Check --->
                    @include('frontend.user.backgroundcheck')

                </div>
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
$(function(){
    $(".dobdatepicker").datepicker();
})
</script>

@endsection