@php
$userData = Auth::guard('user')->user();
@endphp

<style type="text/css">
.orliner{position: absolute;top: -13px;right: 0;left: 0;font-size: 20px;display: inline-block;background: #FFF;width: 100px;margin: auto;}
</style>
<div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
    <div class="first_tabing">
        {!! Form::open(['url' => '/user/payout-settings', 'method' => 'post', 'class' => 'paypal_connect_form', 'name' => 'connectPForm', 'id' => 'connectPForm', 'enctype' => 'multipart/form-data']) !!}
        @csrf

        <div class="personal_data_info" style="padding: 25px 30px 0">
            <h2 class="title">
                {{ $userData->type=='client'?__('general.Banking'):__('general.Payout') }} 
                {{ __('general.information') }}
            </h2>
            <p class="payment_desc">{{ __('general.Payment information are secured, Read our') }} <a target="_blank" href="{{ url('terms-and-conditions') }}">{{ __('general.terms_and_conditions') }}</a>, <a target="_blank" href="{{ url('privacy-policy') }}">{{ __('general.privacy_policy') }}</a>, {{ __('general.and') }} <a target="_blank" href="{{ url('faq') }}">FAQ</a> {{ __('general.for more information') }}. </p>

            <div class="form-group">
                <label style="font-weight: bold">{{ __('general.connect_paypal') }}</label>
                <input type="email" name="paypal_email" placeholder="{{ __('general.Enter your PayPal Email') }}" class="form-control required email" value="{{$userData->paypal_email}}">
            </div>
        </div>
        <div class="border-lines"></div>
        <div class="bottom_box">
            <button class="provider-btn register-bg">{{ __('general.save') }}</button>
        </div>
        {!! Form::close() !!}

        {!! Form::open(['url' => '/user/payout-settings', 'method' => 'post', 'class' => 'stripe_connect_form', 'name' => 'connectForm', 'id' => 'connectForm', 'enctype' => 'multipart/form-data']) !!}
            @csrf

            <div class="border-lines" style="text-align: center;position: relative;">
                <div class="orliner">{{ __('general.or') }}</div>
            </div>
            <br/>
            <div class="personal_data_info">
            <div style="font-weight: bold;margin-bottom: 16px;">{{ __('general.connect_stripe') }}</div>
            @if($userData->stripe_account_id!='')
            <div class=" account-txt mb-3 font-weight-500">
                <h6>{{ __('general.Your stripe account is connected successfully.') }}</h6>
                <div><span class="">{{ __('general.account_id') }} <b>{{$userData->stripe_account_id}}</b></span></div>
            </div>
            <div class="border-lines"></div>
            <br>
            <button class="provider-btn register-bg" type="button" onclick="changeAccount()" id="change_account" style="padding: 0 16px;font-size: 16px">
                <i class="fa fa-edit"></i>
                {{ __('general.change_account') }}: 
            </button>
            @endif 
            <div class="{{ $userData->stripe_account_id!=''?'d-none':'' }}" id="accountBox">
                <div class="form-group">
                    <label>{{ __('general.SSN') }} <small>({{ __('general.Last 4 digits') }})</small></label>
                    {!! Form::text('ssn_number',null ?? old('ssn_number'),array('class'=>'form-control digits required', 'minlength'=>'4','maxlength'=>'4' )) !!}
                    @if($errors->has('ssn_number'))
                    <div class="error-message">{{ $errors->first('ssn_number') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('general.account_number') }} </label>
                    {!! Form::text('account_number',null ?? old('account_number'),array('class'=>'form-control required digits', 'minlength'=>'12','maxlength'=>'12' )) !!}
                    @if($errors->has('account_number'))
                    <div class="error-message">{{ $errors->first('account_number') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{ __('general.routing_number') }} </label>
                    {!! Form::text('routing_number',null ?? old('routing_number'),array('class'=>'form-control required digits', 'minlength'=>'9','maxlength'=>'9' )) !!}
                    @if($errors->has('routing_number'))
                    <div class="error-message">{{ $errors->first('routing_number') }}</div>
                    @endif
                </div>
            </div>
            </div>
            <div class="border-lines"></div>
            <div class="bottom_box">
                <button class="provider-btn register-bg">{{ __('general.save') }}</button>
            </div>
            {!! Form::close() !!}
    </div>
</div>

<script type="text/javascript">
$(function(){
    $('.paypal_connect_form').validate();
    $('.stripe_connect_form').validate();
})




</script>