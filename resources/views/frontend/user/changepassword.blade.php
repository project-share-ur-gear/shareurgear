<div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
    {!! Form::open(['url' => 'user/updatepassword', 'method' => 'post', 'class'=>'change_password_form'] ) !!}
    @csrf
    @method('patch')
    <div class="first_tabing">
        <div class="personal_data_info">
            <h2 class="title">{{__('general.change_password') }}</h2>
            <div class="form-group">
                {!! Form::label('current_password',  __('general.old_password')) !!}
                {!! Form::password('current_password',  array('placeholder'=>__('general.enter_here'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'30','id'=>'current_password' )) !!}
            </div> 
            <div class="form-group">
                {!! Form::label('password',  __('general.new_password')) !!}
                {!! Form::password('password',  array('placeholder'=>__('general.enter_here'),'required'=>'required', 'class'=>'form-control passcheck', 'maxlength'=>'32','id'=>'password' )) !!}
            </div>
            <div class="form-group">
                {!! Form::label('password_confirmation',  __('general.confirm_new_password')) !!}
                {!! Form::password('password_confirmation',  array('placeholder'=>__('general.enter_here'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'32','id'=>'password_confirmation' )) !!}
            </div>
        </div>
        <div class="border-lines"></div>
        <div class="bottom_box">
            <button class="provider-btn register-bg">{{__('general.update') }}</button>
        </div>
    </div>
    {!! Form::close() !!}
</div>

<script type="text/javascript">
$(function(){
    $('.change_password_form').validate({
        rules: {
            current_password        : {remote:APPLICATION_URL+"/checkoldpassword"}, 
            password                : {minlength:8 , maxlength:30},
            password_confirmation   : {equalTo:'#password' , minlength:8, maxlength:30},
        },
    })
});
</script>