@if($providerReviews)
@foreach($providerReviews as $reviews)
<div class="review-boxs" data-last="{{ $reviews->ur_id }}">
    <div class="over-all-media">
        <div class="r-rating-per">
            <label class="name-rating">Hygiene Review  </label>
            <ul class="rating-star">
                <li>
                    <input type="hidden" class="rating" value="{{ $reviews->ur_hygiene_rating }}" readonly="readonly">
                </li>
            </ul>
        </div>
        <div class="r-rating-per">
            <label class="name-rating">Communication Review </label>
            <ul class="rating-star">
                <li>
                    <input type="hidden" class="rating" value="{{ $reviews->ur_comm_rating }}" readonly="readonly">
                </li>
            </ul>
        </div>
        <div class="r-rating-per">
            <label class="name-rating">Recommend Review  </label>
            <ul class="rating-star">
                <li>
                    <input type="hidden" class="rating" value="{{ $reviews->ur_recommend_rating }}" readonly="readonly">
                </li>
            </ul>
        </div>
    </div>
    <div class="media">
        <div class="media-img">
            <img src="{{ getUserImage($reviews->profile_image,'thumb') }}">
        </div>
        <div class="media-body">
            <h4 class="user-name">{{ getNameOnTypeBasis($reviews,'full') }}</h4>
            <ul class="rating-star">
                <li>
                    @php
                    $sumOfSingleReview = $reviews->ur_hygiene_rating + $reviews->ur_comm_rating + $reviews->ur_recommend_rating;

                    $overallSingleReview  = round(avg($sumOfSingleReview,3),'2');
                    @endphp
                    <input type="hidden" class="rating" value="{{ $overallSingleReview }}" readonly="readonly">
                </li>
            </ul>
            <p class="description">
                {{ $reviews->ur_review_text }}
            </p>
        </div>
    </div>
</div>
@endforeach
@endif