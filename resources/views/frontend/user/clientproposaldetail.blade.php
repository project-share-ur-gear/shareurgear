<style>
    header{ box-shadow: 0px 2px 10px 0px rgba(170, 170, 170, 0.32);}
    .header-section{height:auto !important;}
</style>

@extends('layouts.app')

@section('content')
<?php 

    $userServices = DB::table('user_services')->leftjoin('services','services.id','=','user_services.us_service_id')->where('user_services.us_user_id',$proposalinfo->ja_provider_id)->get()->all();

    $sumOfExperience = round(array_sum(array_column($userServices, 'us_exp_year')));
    
    if($sumOfExperience > 0){
        $totalExperience = round(array_sum(array_column($userServices, 'us_exp_year'))/count(array_column($userServices, 'us_exp_year')));
    }
    else{
        $totalExperience = 0;
    }

    if($proposalinfo->id_check_status == '1')
    {
        $id_check_status = '<img src="'.FRONT_IMG.'/tick.svg">';
    }
    else
    {
        $id_check_status = '<img src="'.FRONT_IMG.'/cross.svg">';
    }

    if($proposalinfo->ja_status == '1')
    {
        $status = __('general.Waiting');
    }
    else if ($val->ja_status == '2')
    {
        $status = __('general.Active');
    }
    else
    {
        $status = __('general.Reject');
    }
?>

<section class="account_profile job-detail-page">
    <div class="container">
        <div class="account_pages">

            <!--- Left side Panel --->
            @include('frontend.sidebar')

            <div class="right_account common-form provider-jobs">
                <!-- tabing -->
                <div class="nav nav-tabs nav-fill">
                    <h2 class="title1">Details</h2>
                </div>
                <div class="personal_data_info">
                    
                    <div class="row job-details">
                         <div class="col-lg-8 col-sm-7 col-6">
                            <div class="job-detail-media">
                                <div class="media media-obj">
                                    <div class="media-section">
                                        <div class="media-img">
                                            <a href="{{ url('provider-details/'.base64_encode($proposalinfo->id).'/'.urlencode(getNameOnTypeBasis( $proposalinfo,'full'))) }}">
                                                <img src="{{ getUserImage($proposalinfo->profile_image,'thumb') }}" />
                                            </a>
                                        </div>
                                        <div class="env-img">
                                            <a href="{{ url('message/'.base64_encode($proposalinfo->id)) }}">
                                                <img src="{{ FRONT_IMG.'/envelopes.svg' }}" />
                                            </a>
                                        </div>
                                    </div>
                                    <div class="media-body">  
                                        <a href="{{ url('provider-details/'.base64_encode($proposalinfo->id).'/'.urlencode(getNameOnTypeBasis( $proposalinfo,'full'))) }}"><h6 class="user-name">{{ getNameOnTypeBasis( $proposalinfo,'full') }}</h6></a>
                                        <div class="official-id-img"><?=$id_check_status?><span>Official ID Piece</span></div>
                                        {{--  <ul class="rating-star">
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star(1).svg' }}"></a></li>
                                            <li><a href="#" class="star-icon">(259)</a></li>
                                        </ul>  --}}
                                                   
                                        <p class="c-offers"><span>Experience : </span> {{  $totalExperience }} Year</p>
                                        <p class="c-offers"><span>Language  : </span> {{  $proposalinfo->language }} 
                                        {{--  <small class="more-data">(1 more)</small>  --}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-5 col-6">
                            <div class="job-detail-media">
                                <p class="p-codes p-fixed"><span class="p-prices">$ {{ $proposalinfo->ja_offer_amount }}</span></p>
                                <p class="p-codes"><span>Status : </span> <span class="p-prices">{{ $status }}</span>  </p>
                                <p class="p-dates">
                                    Posted : <span>{{ date('F d, Y',strtotime($proposalinfo->job_date)) }}</span>
                                </p>
                            </div>
                        </div>
                    </div>

                    <p class="note-data c-notes-span"><span class="note-span">Cover Letter </span> {{ $proposalinfo->ja_notes }}</p>


                    <div class="button-sections">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#upcomingmodal" class="cancel-offer-btn">Accept</a>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#upcomingmodal" class="cancel-offer-btn">Decline</a>
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#bookModalCenter" class="cancel-offer-btn">Counter</a>
                    </div>   

                    <!-- Explanation (optional)  -->
                    <!-- <div class="e-box">
                        <p class="e-title">Explanation (optional)</p>
                        <p class="c-letter">Cover Letter</p>
                        <p class="d-name">Dear Sir/Madam.</p>
                        <p class="d-letter">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="button-sections">
                        <a href="#" class="cancel-offer-btn">Send</a>
                    </div>  -->
                    <!-- close -->
                </div>
                <!-- close -->
            </div>
        </div>
    </div>
</section>


<!-- Book Modal -->
<div class="modal fade" id="bookModalCenter" tabindex="-1" role="dialog" aria-labelledby="bookModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="book-title title1" id="bookModalLongTitle">Counter</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            {!! Form::open(['url' => SITE_HTTP_URL.'/client-counter/'.base64_encode($proposalinfo->ja_id), 'method' => 'post', 'class' => 'applied_form', 'name' => 'applied_form', 'id' => 'applied_form']) !!}
            @csrf
            

            <div class="Book-modal-box common-form">
                <p class="p-codes p-fixed"><span>Original amount: <span><span class="p-prices">$ {{ $proposalinfo->ja_job_amount }}</span></p>

                <div class="form-group">
                    <label>Offered amount</label>
                    <input type="text" class="form-control required" name="offer_amount" id="offer_amount" placeholder="" min="0" maxlength="8" value="{{ $proposalinfo->ja_offer_amount }}" onkeypress="return validateFloatKeyPress(this,event)">
                </div> 
                <button type="submit" class="btn submit-btn">Submit</button>
            </div>
            {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

@endsection