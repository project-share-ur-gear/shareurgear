<style>
    header{ box-shadow: 0px 2px 10px 0px rgba(170, 170, 170, 0.32);}
    .header-section{height:auto !important;}
</style>

@extends('layouts.app')

@section('content')

<section class="account_profile job-detail-page R-offer-details">
    <div class="container">
        <div class="account_pages">
            <div class="left_account">
                <div class="user-profiles">
                    <div class="user_img_section">
                        <div class="user_position">
                            <div class="demo"></div>
                            <div class="edit_profile edit_profile-bg"><img src="{{ FRONT_IMG.'/camera.svg' }}" /></div>
                            <div class="profile_completion"><div class="edit_profile edit_profile-bg1">50%</div><span>Profile <br> Complete</span></div>
                        </div>
                    </div>
                    <div class="user_data_link">
                        <p class="user_name">Pedro Pascal</p>
                        <ul class="rating-star">
                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star(1).svg' }}"></a></li>
                        </ul>
                        <a href="#" class="rating_review">259 Reviews</a>
                    </div>
                </div>
                <div class="left_profiles_data">
                    <div class="custom_information">
                        <div class="side-items edit_profiles">
                            <a class="accordion-toggle toggle-switch side-link active collapsed" data-toggle="collapse" href="#submenu-2" aria-expanded="false">
                                <span class="sidebar-title">Edit Profile</span>
                                <i class="fas fa-caret-down"></i>
                            </a>
                            <ul id="submenu-2" class="panel-collapse panel-switch collapse" role="menu" style="">
                                <li><a href="#" class="side-sub-link active">Personal Information</a></li>
                                <li><a href="#" class="side-sub-link">Change Password</a></li>
                                <li><a href="#" class="side-sub-link">Experience & Certification</a></li>
                                <li><a href="#" class="side-sub-link">Banking/Payout Information</a></li>
                                <li><a href="#" class="side-sub-link">Background Check </a></li>
                            </ul>
                        </div>
                    </div>
                    <ul class="list_items">
                        <li><a href="#" class="list_data_info">SETTINGS </a></li>
                        <li><a href="#" class="list_data_info">Favorite </a></li>
                        <li><a href="#" class="list_data_info">Received & Sent Offers  </a></li>
                        <li><a href="#" class="list_data_info">Messaging</a></li>
                        <li><a href="#" class="list_data_info">Log Out </a></li>
                    </ul>
                </div>
            </div>
            <div class="right_account common-form provider-jobs">
                <!-- tabing -->
                <div class="nav nav-tabs nav-fill">
                    <h2 class="title1 p-0">Job Details</h2>
                    <a href="#" class="p-view">View Proposal (43)</a>
                </div>
                <div class="personal_data_info">
                    <h2 class="title">Job Title</h2>
                    <div class="row job-details">
                         <div class="col-lg-8 col-sm-7 col-6">
                            <div class="job-detail-media">
                                <div class="media media-obj">
                                    <div class="media-body">  
                                        <h6 class="user-name">Name here</h6>
                                       
                                        <ul class="rating-star">
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></a></li>
                                            <li><a href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star(1).svg' }}"></a></li>
                                            <li><a href="#" class="star-icon">(259)</a></li>
                                        </ul>
                                        <p class="c-offers"><span>Address  : </span> <span class="d-dates">452 FIFTH AVE<br> NEW YORK <br> H4R 7E2</span></p>
                                        <p class="c-offers"> <span class="d-dates">15th February, 2020</span></p>
                                        <p class="c-offers"> <span class="d-dates">9:05 PM</span></p>
                                    </div>
                                </div>
                            </div>
                         </div>
                         <div class="col-lg-4 col-sm-5 col-6 pr-0">
                            <div class="job-detail-media">
                                <!-- change term click edit prices -->
                                <!-- <p class="p-codes p-fixed"><span class="p-prices">$85</span> <span>(Fixed) <img src="{{ FRONT_IMG.'/edit1.svg' }}"></span> </p> -->
                                <!--  -->
                                <p class="p-codes p-fixed"><span class="p-prices">$85</span> <span>(Fixed)</span> </p>
                                <p class="p-codes"><span>Status : </span> <span class="p-prices">Active</span>  </p>
                                <p class="p-dates">Posted : <span>July 12,2020</span></p>
                                
                            </div>
                         </div> 
                    </div>

                    <p class="note-data pl-0"><span class="note-span">Notes </span> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                    <div class="button-sections">
                        <!-- change term click -->
                        <!-- <a href="#" class="cancel-offer-btn">saved</a> -->
                        <!--  -->
                        <a href="#" class="cancel-offer-btn">{{__('general.Change Terms') }}</a>
                        <a href="#" class="cancel-offer-btn">{{__('general.Cancel Offer') }}</a>
                    </div>   

                </div>
                <!-- close -->
            </div>
        </div>
    </div>
</section>

@endsection

