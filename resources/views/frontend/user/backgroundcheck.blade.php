<style type="text/css">
.paypal-btn{margin: 0 10px;}
.wallet-used-display strong {font-weight: bold;border-bottom: 1px solid;padding-bottom: 2px;margin-bottom: 5px;display: inline-block;color: #595959;}
</style>


<div class="tab-pane fade" id="nav-about" role="tabpanel" aria-labelledby="nav-about-tab">
    {!! Form::open(['url' => 'user/upload-id-proof', 'method' => 'post', 'class'=>'upload_id_form','id'=>'upload_id_form','enctype' => 'multipart/form-data' ] ) !!}
    @csrf
    <div class="first_tabing">
        <div class="personal_data_info">
            <h2 class="title">{{ __('general.upload_identification_proof') }} </h2>
            <p class="payment_desc">{{ __('general.upload_your_ID_proof_photo_below') }} </p>
            <div class="row">
                <div class="col-12">
                    <div class="common-input">
                        <label>{{ __('general.upload_ID_proof') }}</label>
                        <div class="input-group">
                            <input type="text" class="form-control required ufilename" placeholder="" aria-label="Phone Number" aria-describedby="basic-addon2" readonly="readonly">
                            <input id="fileproofupload" name="fileproofupload" type="file" style="visibility: hidden;height: 0;width: 0">
                            <div class="input-group-append">
                                <span class="input-group-text input-confirm" id="idproofuploader">
                                {{ __('general.upload') }}
                                </span>
                            </div>
                        </div>
                    </div>
                    @if($user->bg_legal_file)
                    <div style="color: #000 !important">
                        <div>
                            {{ __('general.Current ID') }}:
                            @if(Auth::guard('user')->user()->id_check_status=='1')
                            {{ __('general.verified') }}
                            @elseif(Auth::guard('user')->user()->id_check_status=='2')
                            {{ __('general.Pending verification') }}
                            @elseif(Auth::guard('user')->user()->id_check_status=='3')
                            {{ __('general.Rejected') }}
                            @else
                            {{ __('general.Not sent for verification yet') }}
                            @endif
                        </div>
                        @if(Auth::guard('user')->user()->id_check_status=='3')
                        <div style="margin-bottom: 10px">
                            {{ __('general.Reason for rejection')}}: 
                            {{ Auth::guard('user')->user()->id_check_reject_reason }}
                        </div>
                        @endif
                        <img src="{{STORAGE_IMG_PATH}}/app/public/bgcheck/{{ $user->bg_legal_file }}" style="max-width: 300px; width:100%; border: 5px solid #19c1eb" />
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="bottom_box">
            <button type="submit" id="payBtnss" class="provider-btn register-bg continue-btn">
            {{ __('general.save') }}
            </button>
        </div>
    </div>
    {!! Form::close() !!}

    <div class="border-lines"></div>

    {!! Form::open(['url' => 'user/background-check', 'method' => 'post', 'class'=>'background_check_form','id'=>'background_check_form','enctype' => 'multipart/form-data' ] ) !!}
    @csrf
    <div class="first_tabing">
        <div class="personal_data_info">
            <h2 class="title">{{ __('general.Background Check and Payment') }}</h2>
            @if(Auth::guard('user')->user()->bg_check_status=='4')
            <p class="payment_desc">{{ __('general.We can never be too secure. You can upload your ID proof and also authorize us to do a background check to make your profile verified and secure. Fees apply.') }}</p>
            
            <div class="wallet-used-display d-none">
                <hr>
                <a class="floatwalletcloser" href="javascript:void(0)"><i class="fa fa-times"></i></a>
                <div class="wallet-bg-check-fees">
                    <strong>{{ __('general.background_check_fees') }}</strong>
                    <div></div>
                </div>
                <br>
                <div class="wallet-total-amt">
                    <strong>{{ __('general.total_amount_in_wallet') }}</strong>
                    <div></div>
                </div>
                {{-- <br>
                <div class="wallet-amt-remaining">
                    <strong>{{ __('general.Amount remaining in wallet') }}</strong>
                    <div></div>
                </div> --}}
                <br>
                <div class="wallet-amt-payable">
                    <strong>{{ __('general.total_payable_amount') }}</strong>
                    <div></div>
                </div>
                <input type="hidden" name="wmu" value="">
                <div class="wallet-btn-wrapper hide">
                    <button type="submit" id="proceedbtn" class="provider-btn register-bg continue-btn">{{ __('general.proceed') }} &nbsp; <i class="fa fa-angle-right"></i> </button>
                </div>
                <br>
            </div>

            <p class="total_amt">{{ __('general.amount  ') }} ${{ $site_configs['site_background_check_fees'] }}</p>

            <div class="row justify-content-center mb-4" id="payment-btns-wrapper">
                <a href="javascript:void(0)" class="paypal-btn stripe_btn" id="payBtn">
                    <span>{{ __('general.pay_with') }}</span>
                    <img style="height: 30px" src="{{ FRONT_IMG.'/stripelogo.svg' }}">
                </a>

                <a href="<?=SITE_HTTP_URL?>/user/paypal-check" class="paypal-btn paypal_btn" id="payBtn">
                    <span>{{ __('general.pay_with') }}</span>
                    <img src="{{ FRONT_IMG.'/paypal.png' }}">
                </a>

                @if($myBalance > 0)
                <a href="javascript:void(0)" class="paypal-btn wallet_btn" id="payBtn">
                    <span style="line-height: normal;">{{ __('general.pay_with_wallet') }} </span>
                    <img style="width: auto;margin-left: 20px" src="{{ FRONT_IMG.'/wallet.svg' }}">
                </a>
                @endif
            </div>
            <br>

            <div class="d-none pay_stripe">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{ __('general.legal_first_name') }} </label>
                            {!! Form::text('legal_first_name',Auth::guard('user')->user()->legal_first_name ?? old('legal_first_name'),array('class'=>'form-control lettersonly required', 'maxlength'=>'30')) !!}
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>{{ __('general.legal_last_name') }}</label>
                            {!! Form::text('legal_last_name',Auth::guard('user')->user()->legal_last_name ?? old('legal_last_name'),array('class'=>'form-control lettersonly required', 'maxlength'=>'30')) !!}
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label>{{ __('general.dob') }}</label>
                            {!! Form::text('dob',old('dob'),array('placeholder'=>'MM/DD/YYYY','class'=>'form-control dobdatepicker required', 'maxlength'=>'30')) !!}
                        </div>
                    </div>
                    {{-- <div class="col-12">
                        <div class="common-input">
                            <label>{{ __('general.Upload ID Proof') }}</label>
                            <div class="input-group">
                                <input type="text" class="form-control required ufilename" placeholder="" aria-label="Phone Number" aria-describedby="basic-addon2" readonly="readonly">
                                <input id="fileproofupload" name="fileproofupload" type="file" style="visibility: hidden;height: 0;width: 0">
                                <div class="input-group-append">
                                    <span class="input-group-text input-confirm" id="idproofuploader">
                                    {{ __('general.upload') }}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                </div>

                <div class="payment-form">
                    <div class="form-group">
                        <label>{{ __('general.card_holder_name') }}</label>
                        <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control required" maxlength="100">
                    </div>
                    <div class="form-group mt-4">
                        <label>{{ __('general.card_number') }}</label>
                        <input type="text" placeholder="{{ __('general.card_number') }}" name="card_number" id="card_number" value="{{ old('card_number') }}" class="form-control digits required" aria-required="true" autocomplete="off" maxlength="16" onkeypress="return isNumberKey(event)">
                    </div>
                    <div class="form-group mt-4">
                        <div class="row">
                            <div class="col-md-4">
                                <label>{{ __('general.expiry_month') }}</label>
                                <input type="text" name="card_exp_month" id="card_exp_month" value="{{ old('card_exp_month') }}" class="form-control digits required" aria-required="true" placeholder="e.g. 12"  autocomplete="off" minlength="2" maxlength="2" max="12" onkeypress="return isNumberKey(event)">
                            </div>
                            <div class="col-md-4">
                                <label>{{ __('general.expiry_year') }}</label>
                                <input type="text" name="card_exp_year" id="card_exp_year" value="{{ old('card_exp_year') }}" class="form-control digits required" aria-required="true" placeholder="e.g. 2022" autocomplete="off" minlength="4" maxlength="4"  onkeypress="return isNumberKey(event)">
                            </div>
                            <div class="col-md-4">
                                <label>{{ __('general.cvv') }}</label>
                                <input type="text" name="card_cvc" id="card_cvc" value="{{ old('card_cvc') }}" class="form-control digits required" aria-required="true" autocomplete="off" placeholder="e.g. 123"  maxlength="4"  onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @elseif(Auth::guard('user')->user()->bg_check_status=='2')
            <p class="payment_desc">{{__('general.Your request for background check has been submitted successfully, our staff will look into your request and will inform you as soon as possible.') }}</p>
            
            @elseif(Auth::guard('user')->user()->bg_check_status=='1')
            <p class="payment_desc">{{__('general.Your background check has been completed successfully, our staff has approved you and you are now a approved member.') }}</p>

            @elseif(Auth::guard('user')->user()->bg_check_status=='3')
            <p class="payment_desc">{{__('general.Your background check has been completed successfully, our staff has rejected your request. Please') }} 
            <a href="{{ SITE_HTTP_URL.'/contact-us' }}">{{__('general.contact_us') }}</a> {{__('general.to know more about your rejection.') }}</p>

            @endif
        </div>
        @if(Auth::guard('user')->user()->bg_check_status=='4')
        <div class="d-none pay_stripe">
            <div class="border-lines"></div>
            <div class="bottom_box">
                <button type="submit" id="payBtn" class="provider-btn register-bg continue-btn">
                {{__('general.pay') }}
                </button>
            </div>
        </div>
        @endif
    </div>
    {!! Form::close() !!}
</div>

<script type="text/javascript">
$(function(){
    //

    $('#payBtnss').on("click",function(){
        if($("#upload_id_form").valid()==false){
            $('#payBtnss').removeAttr("disabled");
            return false;
        }
        else{
            $('#payBtnss').attr("disabled", "disabled");
            $("#upload_id_form").submit();
        }
    })

    $("#idproofuploader").on("click",function(){
        $("#fileproofupload").trigger("click");
    })
    $(".dobdatepicker").datepicker({
        endDate     : '-16y',
        autoclose   : true,
        format      : 'mm/dd/yyyy'
    });

    // $('#payBtn').on("click",function(){
    //     if($("#background_check_form").valid()==false){
    //         $('#payBtn').removeAttr("disabled");
    //         return false;
    //     }
    //     else{
    //         $('#payBtn').attr("disabled", "disabled");
    //         $("#background_check_form").submit();
    //     }
    // })

    var walletVal = '';
    $(document).on("click",".wallet_btn",function(){
        $.ajax({
            url     : APPLICATION_URL+'/get-wallet-balance',
            type    : "POST",
            headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success : function(response){
                
                var walletBalance       = parseFloat(response);
                var bgCheckFees         = parseFloat({{ bcdiv($site_configs['site_background_check_fees'],1,2) }});
                var remainingBalance    = 0;
                var amtToBeCharged      = 0;
                var payMode = '';
                
                $(".first_tabing").find(".total_amt").addClass("d-none");
                $(".wallet-used-display").removeClass('d-none');

                
                var paypalurl = $(".paypal_btn").attr('href');
                var newpaypalurl = paypalurl+'?viawallet=wallet';
                $(".paypal_btn").attr('href',newpaypalurl);

                if(walletBalance >= bgCheckFees){
                    remainingBalance = parseFloat(walletBalance) - parseFloat(bgCheckFees);
                    payMode = 'full';
                }
                else{
                    var amtToBeCharged = parseFloat(bgCheckFees) - parseFloat(walletBalance);
                    amtToBeCharged = amtToBeCharged.toFixed(2);
                    
                    $(".wallet_btn").remove();
                    $(".wallet-btn-wrapper").remove();
                }

                $(".wallet-bg-check-fees div").html('$'+bgCheckFees);
                $(".wallet-total-amt div").html('$'+response);
                $(".wallet-amt-remaining div").html('$'+remainingBalance);
                $(".wallet-amt-payable div").html('$'+amtToBeCharged);
                
                $(".first_tabing").find(".total_amt").html("Amount $"+amtToBeCharged);
                
                if(amtToBeCharged == 0){
                    $("#payment-btns-wrapper").remove();
                    $("input[name='wmu']").val(payMode);
                }
                else{
                    $("input[name='wmu']").val(amtToBeCharged);
                }
                
            }
        })
    })


    $("#background_check_form").submit(function(e) {
        if($("#background_check_form").valid()==false){
            return false;
        }

        // Disable the submit button to prevent repeated clicks
        $('#payBtn').attr("disabled", "disabled");

        var partials = $("input[name='wmu']").val();
        if(partials=='full'){
            // $('.background_check_form').validate({
            //     ignore: 'hidden'
            // });
            
            var form$ = $("#background_check_form");
            form$.get(0).submit();

            // Submit from callback
            return false;
        }
        else{
            // Create single-use token to charge the user
            Stripe.createToken({
                number      : $('#card_number').val(),
                exp_month   : $('#card_exp_month').val(),
                exp_year    : $('#card_exp_year').val(),
                cvc         : $('#card_cvc').val()
            }, stripeResponseHandler);

            $('.background_check_form').validate({
                ignore: 'hidden'
            });

            // Submit from Callback
            return false;
        }
    });

});

$("#fileproofupload").on("change",function(){
    var filename = $('#fileproofupload')[0].files[0];
    $(".ufilename").val(filename.name);
    //readURL($(this));
    //$(this).parent(".upload-btn-wrapper").find("span").css("display","block");
});

// $(".continue-btn").on("click",function(){
//     $('.payment-form').removeClass("d-none");
//     $(this).text('Pay');
//     $(".total_amt").css("padding","10px 0");
//     setTimeout(function(){
//         if($(".provider-btn").hasClass("continue-btn")){
//             $('html, body').animate({
//                 scrollTop: $(".payment-form").offset().top
//             }, 500);
//         }
//         // $(".continue-btn").attr("type","submit");
//         $(".provider-btn").removeClass('continue-btn');
//     },500);
// })

$(document).on('click','.stripe_btn',function(){
    $('.pay_stripe').removeClass('d-none');
})

$(document).on('click','.paypal_btn',function(){
    $('.pay_stripe').addClass('d-none');
})


</script>

<!-- Stripe JavaScript library -->
<script src="https://js.stripe.com/v2/"></script>

<script>
// Set your publishable key
Stripe.setPublishableKey('{{ $site_configs['site_stripe_publish_key'] }}');

// Callback to handle the response from stripe
function stripeResponseHandler(status, response) {
    if (response.error) {
        // Enable the submit button
        $('#payBtn').removeAttr("disabled");
        // Display the errors on the form
        showpopupmessage(response.error.message,'danger');
    }
    else {
        var form$ = $("#background_check_form");
        // Get token id
        var token = response.id;
        // Insert the token into the form
        form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        // form to the server
        form$.get(0).submit();
    }
}

</script>