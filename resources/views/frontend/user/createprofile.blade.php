@extends('layouts.app')
@section('content')

<section class="main-form profile-page my-5">
    <div class="container">
        <h4 class="heading">Create My Profile</h4>
        <div class="row">
            <div class="col-lg-8 lg-offset-2 col-md-12 my-0 mx-auto">

                <div class="create-profile-form">
                    <div id="create-profile-carousel" class="carousel slide" data-interval="false" data-ride="carousel">
                        <div class="">
                            <div class="row w-100 carousel-indicators">
                                <div class="col-6 col-md-3 text-center active">
                                    <a href="javascript:void(0);">
                                        <span class="slide-dot" data-target="#create-profile-carousel" data-slide-to="0">1</span>Education</a>
                                </div><!--colmd3-->
                                <div class="col-6 col-md-3 text-center">
                                    <a href="javascript:void(0);" >
                                        <span class="slide-dot"  data-target="#create-profile-carousel" data-slide-to="1">2<br/></span>
                                        Upload Image</a>
                                </div><!--colmd3-->
                                <div class="col-6 col-md-3 text-center">
                                    <a href="javascript:void(0)">
                                        <span class="slide-dot"  data-target="#create-profile-carousel" data-slide-to="2">3</span>
                                        Bank Account</a>
                                </div><!--colmd3-->
                                <div class="col-6 col-md-3 text-center">
                                    <a href="javascript:void(0);"  class="last-form-nav">
                                        <span class="slide-dot" data-target="#create-profile-carousel" data-slide-to="3">4</span>
                                        Invite Friend</a>
                                </div><!--colmd3-->
                            </div><!--row-->
                        </div>
                        {!! Form::open(['url' => '/user/saveprofile', 'method' => 'post', 'class' => 'profile_form', 'name' => 'profileForm', 'id' => 'profileForm','files'=>true,'enctype'=> 'multipart/form-data']) !!}
                         @method('PATCH')
                          @csrf
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-md-12">
                                            <h4 class="sub-heading-txt">Education</h4>
                                            <h5 class="titles">Current Courses</h5>
                                            <div id="add_current_course_div"></div>
                                            <div id="add-current-course" class="mb-3">
                                                 {!! Form::hidden('total_current_courses',0,array('id'=>'total_current_courses')) !!}
                                                <a href="javascript:void(0);" onclick="addCurrentCourse(2)"><i class="fas fa-plus"></i>&nbsp;Add Current Courses</a>
                                            </div><!--add-current-course-->
                                            
                                            <h5 class="titles">Past Courses</h5>
                                            <div id="add_past_course_div"></div>
                                            <div id="add-past-course" class="mb-3">
                                                 {!! Form::hidden('total_past_courses',0,array('id'=>'total_past_courses')) !!}
                                                <a href="javascript:void(0);" onclick="addPastCourse(2)"><i class="fas fa-plus"></i>&nbsp;Add Past Courses</a>
                                            </div><!--add-past-course-->
                                    </div>
                                    <!--colmd12-->
                                </div>
                                <!--row-->
                                
                                <div class="navigation-button justify-content-end">
                                    <button type="button" class="btn  skip-btn bg-light-grey text-dark-grey"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="next" onclick="skipCreateProfile()">SKIP</button>
                                    <button type="button" class="btn find-btn btn-primary" onclick="validateCreateProfile()">NEXT <img src="{{ FRONT_IMG.'/arrow-white.svg' }}" class="searchimg"/> </button>
                                </div>
                            </div>
                            <div class="carousel-item imagebox">
                                <div class="row">
                                    <div class="col-2 col-md-3"></div>
                                    <div class="col-8 col-md-6">
                                        <div class="image-uploading text-center">
                                            <h5 class="image-heading">Upload Profile Image</h5>
                                            @php
                                            	$profileImage=FRONT_IMG.'/image-upload.png';
                                                $fileReq='';
                                            	if($user->profile_image!=''){
                                                	$profileImage=getUserImage($user->profile_image);
                                                    $fileReq='required';
                                                }
                                            @endphp
                                            <div class="profileImageBox" id="profile_image_preview" style="background-image:url({{$profileImage}})"></div>
                                            <div class="d-none hide">{!! Form::file('profile_image',array('id'=>'profile_image','class'=>'{{$fileReq}}')) !!}</div>
                                            <button class="btn btn-primary mt-3 image-upload-btn" type="button" onclick="uploadProfileImage()"> <img src="{{ FRONT_IMG.'/camera.svg' }}" class="searchimg"/></i>&nbsp;UPLOAD IMAGE</button>
                                            <p class="text-dark-grey mt-3">Max file size 2MB, File Formate jpg, png</p>
                                        </div><!--image=-uploading-->
                                    </div>
                                    <!--colmd7-->
                                    <div class="col-2 col-md-3"></div>
                                </div>
                                <!--row-->
                                
                                <div class="navigation-button mt-4">
                                	<div class="left-side-btn"><button type="button" class="btn back-btn bg-light-grey text-dark-grey pull-left"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="prev"><i class="fas fa-arrow-left"></i> BACK</button></div>
                                    <div class="right-side-btn d-flex"><button type="button" class="btn skip-btn  bg-light-grey text-dark-grey"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="next" onclick="skipCreateProfile()">SKIP</button>
                                    <button type="button" class="btn find-btn btn-primary" onclick="validateCreateProfile()">NEXT <img src="{{ FRONT_IMG.'/arrow-white.svg' }}" class="searchimg"/></button></div>
                                </div>
                            </div>
                            <div class="carousel-item form-last-step" id="">
                                <div class="row">
                                    @if($user->account_id!='')
                                    	<div class="col-12">
                                        	 <p></p>
                                        	 <h6 class="mb-3 font-weight-500">Your account is connected successfully.<br /><span class="text-blue">Account Id: {{$user->account_id}}</span></h6>
                                             <button class="btn btn-primary mt-3 small" type="button" onclick="changeAccount()" id="change_account"><i class="fas fa-pencil"></i>&nbsp;CHANGE ACCOUNT</button>
                                        </div>
                                    @endif 
                                    
                                    <div class="col-12 p-0 @if($user->account_id!='') d-none @endif" id="accountBox">   
                                        <div class="col-md-12">
                                            <div class="form-group ui-widget" style="z-index: 1000">
                                                 {!! Form::label('ssn_number', 'SSN Number') !!}
                                                 {!! Form::text('ssn_number',null ?? old('ssn_number'),array('placeholder'=>'SSN Number','class'=>'form-control digits', 'minlength'=>'9','maxlength'=>'9' )) !!}
                                                @if($errors->has('ssn_number'))
                                                  <div class="error-message">{{ $errors->first('ssn_number') }}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                 {!! Form::label('account_number', 'Account Number') !!}
                                                 {!! Form::text('account_number',null ?? old('account_number'),array('placeholder'=>'Account Number','class'=>'form-control digits', 'minlength'=>'12','maxlength'=>'12' )) !!}
                                                @if($errors->has('account_number'))
                                                  <div class="error-message">{{ $errors->first('account_number') }}</div>
                                                @endif
                                            </div>
                                        </div><!--colmd6-->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                 {!! Form::label('routing_number', 'Routing Number') !!}
                                                 {!! Form::text('routing_number',null ?? old('routing_number'),array('placeholder'=>'Routing Number','class'=>'form-control digits', 'minlength'=>'9','maxlength'=>'9' )) !!}
                                                @if($errors->has('routing_number'))
                                                  <div class="error-message">{{ $errors->first('routing_number') }}</div>
                                                @endif
                                            </div>
                                        </div><!--colmd6-->
                                    </div>
                                </div><!--row-->
                                
                                <div class="navigation-button text-right mt-4">
                                	<button type="button" class="btn back-btn skip-btn bg-light-grey text-dark-grey pull-left"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="prev"><i class="fas fa-arrow-left"></i> BACK</button>
                                    <button type="button" class="btn skip-btn bg-light-grey text-dark-grey"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="next" onclick="skipCreateProfile()">SKIP</button>
                                    <button type="button" class="btn find-btn btn-primary" onclick="validateCreateProfile()">NEXT <img src="{{ FRONT_IMG.'/arrow-white.svg' }}" class="searchimg"/></button>
                                </div>
                                
                            </div>
                            <div class="carousel-item form-last-step" id="">
                                <div class="row mt-3">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-6">
                                        <div class="image-uploading text-center">
                                            <h5 class="font-weight-bold mb-3">Invite Friends</h5>
                                            <p>Invite your friends and classmates to get your study on!</p>
                                            <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#send-invitation-modal"><i class="fas fa-paper-plane"></i>&nbsp;SEND INVITATION</button>
                                        </div><!--image=-uploading-->
                                    </div>
                                    <!--colmd7-->
                                    <div class="col-md-3"></div>
                                </div>
                                <!--row-->
                                <div class="navigation-button text-right mt-4">
                                	<button type="button" class="btn  back-btn skip-btn  bg-light-grey text-dark-grey pull-left"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="prev"><i class="fas fa-arrow-left"></i> BACK</button>
                                   <?php /*?> <button type="button" class="btn   bg-light-grey text-dark-grey"  data-target="#create-profile-carousel" href="#carouselExampleControls" role="button" data-slide="next" onclick="skipCreateProfile()">SKIP</button><?php */?>
                                    <button type="button" class="btn find-btn btn-primary" onclick="submitCreateProfile()">SUBMIT <img src="{{ FRONT_IMG.'/arrow-white.svg' }}" class="searchimg"/></button>
                                </div>
                                
                            </div>
                            </form>
                        </div>
                        {!! Form::close() !!}   
                    </div>
                    
                </div>
            </div><!--create-profile-form-->
            <!--colmd12-->
        </div>
        <!--row-->
    </div>
    <!--container-->
</section>



<script>
var univ_info=JSON.parse('<?=$univ_info?>');
var currentCourseData=JSON.parse('<?=$currentCourseData?>');
var pastCourseData=JSON.parse('<?=$pastCourseData?>');
</script>     
   

@endsection