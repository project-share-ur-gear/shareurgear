@php
$uId = Auth::guard('user')->id();
$experiences = DB::table('user_experiences')->where('ue_user_id',Auth::guard('user')->user()->id)->get()->all();

$certificates = DB::table('user_certification')->where('uc_user_id',Auth::guard('user')->user()->id)->get()->all();

@endphp

<style type="text/css">
.cer_remover, .exp_remover{position: absolute;right: 0}
.cer_block{border-top: 1px solid #EEE;padding-top: 20px;padding-bottom: 20px;}
</style>
<div class="tab-pane fade" id="nav-payout" role="tabpanel" aria-labelledby="nav-payout-tab">
	<div class="first_tabing">
        {!! Form::open(['url' => '/user/experience', 'method' => 'post', 'class' => 'exper_form', 'name' => 'profileEForm', 'id' => 'profileEForm','enctype' => 'multipart/form-data']) !!}
        @csrf
		<div class="personal_data_info">
			<h2 class="title">{{__('general.experience') }}</h2>
			<div class="add-exp">
                @if(!empty($experiences))
                    @foreach($experiences as $key => $experience)
                    <div class="exp_block">
                        <div class="form-group" style="position: relative;">
                            @if($key >= 1)
                            <a class="exp_remover"><i class="fas fa-trash"></i></a>
                            @endif
                            <label>{{__('general.describe_label') }}</label>
                            <input name="ue_desc[{{$key}}]" type="text" class="form-control required" required="required" maxlength="200" value="{{ $experience->ue_desc }}" >
                        </div> 
                        <div class="common-input">
                            <label>{{__('general.insert_diploma_label') }}</label>
                            <div class="input-group form-group uexperiencer">
                                <input type="text" readonly="readonly" class="form-control ufilename required" aria-describedby="basic-addon2" required="required" value="{{ $experience->ue_diploma_original }}"  name="ue_diploma_fake_photo[{{$key}}]">
                                <input name="ue_diploma[]" class="ue_diploma" type="file" style="visibility: hidden;height: 0;width: 0" value="{{ $experience->ue_diploma }}">
                                @if($experience->ue_diploma)
                                <input name="ue_diploma_o[]" class="ue_diploma_o" type="hidden" value="{{ $experience->ue_diploma }}">
                                <input name="ue_diploma_ori[]" class="ue_diploma_ori" type="hidden" value="{{ $experience->ue_diploma_original }}">
                                @endif
                                <div class="input-group-append">
                                    <span class="input-group-text input-confirm idproofuploader">{{__('general.update') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @else
                <div class="form-group" style="position: relative;">
                    <label>{{__('general.describe_label') }}</label>
                    <input name="ue_desc[0]" type="text" class="form-control required" required="required" >
                </div> 
                <div class="common-input">
                    <label>{{__('general.insert_certificate_label') }}</label>
                    <div class="input-group uexperiencer">
                        <input type="text" readonly="readonly" class="form-control ufilename required" aria-describedby="basic-addon2" required="required" name="ue_diploma_fake_photo[0]">
                        <input name="ue_diploma[]" class="ue_diploma" type="file" style="visibility: hidden;height: 0;width: 0">
                        <div class="input-group-append">
                            <span class="input-group-text input-confirm idproofuploader">{{__('general.update') }}</span>
                        </div>
                    </div>
                </div>
                @endif

                <div id="experience-adder">
                    
                </div>

                <a href="javascript:void(0)" class="add-btns add-exper">+ {{__('general.add_more') }}</a>
            </div>

            <h2 class="title">{{__('general.certification') }}</h2>
            <div class="add-exp">
                @if(!empty($certificates))
                    @foreach($certificates as $key => $certificate)
                    <div class="cer_block">
                    <div class="form-group" style="position: relative;">
                        @if($key >= 1)
                        <a class="cer_remover"><i class="fas fa-trash"></i></a>
                        @endif
                        <label>{{__('general.certification_name') }}</label>
                        <input name="uc_certificate_title[{{$key}}]" type="text" class="form-control required" value="{{ $certificate->uc_certificate_title }}" required="required" >
                    </div> 
                    <div class="common-input ucertificater">
                        <label>{{__('general.insert_certificate_label') }}</label>
                        <div class="input-group">
                            <input type="text" readonly="readonly" class="form-control ufilename required" aria-describedby="basic-addon2" value="{{ $certificate->uc_certificate_photo_original }}" required="required" name="uc_certificate_fake_photo[{{$key}}]">
                            <input name="uc_certificate_photo[]" class="uc_certificate_photo" type="file" style="visibility: hidden;height: 0;width: 0"  value="{{ $certificate->uc_certificate_photo }}">
                            @if($certificate->uc_certificate_photo)
                            <input name="uc_certificate_photo_o[]" class="uc_certificate_photo_o" type="hidden" value="{{ $certificate->uc_certificate_photo }}">
                            <input name="uc_certificate_photo_ori[]" class="uc_certificate_photo_ori" type="hidden" value="{{ $certificate->uc_certificate_photo_original }}">
                            @endif
                            <div class="input-group-append">
                            <span class="input-group-text input-confirm certproofuploader" id="basic-addon2">{{__('general.Update') }}</span>
                            </div>
                        </div>
                    </div>
                    </div>
                    @endforeach
                @else
                <div class="form-group">
                    <label>{{__('general.certification_name') }}</label>
                    <input name="uc_certificate_title[]" type="text" class="form-control required" >
                </div> 
                <div class="common-input">
                    <label>{{__('general.insert_certificate_label') }}</label>
                    <div class="input-group ucertificater">
                        <input type="text" readonly="readonly" class="form-control ufilename required" aria-describedby="basic-addon2" required="required" name="uc_certificate_fake_photo[0]">
                        <input name="uc_certificate_photo[0]" class="uc_certificate_photo" type="file" style="visibility: hidden;height: 0;width: 0">
                        <div class="input-group-append">
                        <span class="input-group-text input-confirm certproofuploader" id="basic-addon2">{{__('general.update') }}</span>
                        </div>
                    </div>
                </div>
                @endif
                <div id="certificate-adder"></div>

                <a href="javascript:void(0)" class="add-btns add-cer">+ {{__('general.add_more') }}</a>
            </div>
		</div>
		<div class="border-lines"></div>
		<div class="bottom_box">
            <button type="submit" class="provider-btn register-bg">{{__('general.update') }}</a>
        </div>
        {!! Form::close() !!}
	</div>
</div>

<script type="text/javascript">
    $(document).on("click",".idproofuploader",function(){
        $(this).parents(".uexperiencer").find(".ue_diploma").trigger("click");
    })

    $(document).on("click",".certproofuploader",function(){
        $(this).parents(".ucertificater").find(".uc_certificate_photo").trigger("click");
    })

    $(document).on("change",".ue_diploma",function(){
        var filename = $(this).parents(".input-group").find('.ue_diploma')[0].files[0];
        $(this).prev().val(filename.name);
    });

    $(document).on("change",".uc_certificate_photo",function(){
        var filename = $(this).parents(".input-group").find('.uc_certificate_photo')[0].files[0];
        $(this).prev().val(filename.name);
    });

    //$(".exper_form").validate();

    $(document).on("click",".add-exper",function(){
		var experTitle = '{{__("general.describe_label")}}';
		var exprDiploma= '{{__("general.insert_diploma_label")}}';
		var exprUpload = '{{__("general.update")}}';
		
        var ehtml = '';
        ehtml+='<div class="exp_block">';
            ehtml+='<div class="form-group" style="position:relative">';
                ehtml+='<a class="exp_remover"><i class="fas fa-trash"></i></a>';
                ehtml+='<label>'+experTitle+'  <span class="needed">*</span></label>';
                ehtml+='<input name="ue_desc[]" type="text" class="form-control required" required="required" >';
            ehtml+='</div>';
            ehtml+='<div class="common-input">';
                ehtml+='<label>'+exprDiploma+' <span class="needed">*</span></label>';
                ehtml+='<div class="input-group uexperiencer">';
                    ehtml+='<input type="text" class="form-control ufilename required" aria-describedby="basic-addon2" readonly required="required">';
                    ehtml+='<input name="ue_diploma[]" class="ue_diploma" type="file" style="visibility: hidden;height: 0;width: 0">';
                ehtml+='<div class="input-group-append">';
                    ehtml+='<span class="input-group-text input-confirm idproofuploader">'+exprUpload+'</span>';
                ehtml+='</div>';
            ehtml+='</div>';
        ehtml+='</div>';

        $("#experience-adder").append(ehtml);
        $('.exper_form').validate({
            rules:{
                'ue_desc[]' : 'required',
                'uc_certificate_title[]' :'required',
            }
        });
    })

    $(document).on("click",".add-cer",function(){

        var cerTitle  = '{{__("general.certification_name")}}';
        var cerDiploma= '{{__("general.insert_certificate_label")}}';
        var cerUpload = '{{__("general.upload")}}';

        var fhtml = '';
        fhtml+='<div class="cer_block">';
            fhtml+='<div class="form-group" style="position:relative">';
                fhtml+='<a class="cer_remover"><i class="fas fa-trash"></i></a>';
                fhtml+='<label>'+cerTitle+' <span class="needed">*</span></label>';
                fhtml+='<input name="uc_certificate_title[]" type="text" class="form-control required" required="required">';
            fhtml+='</div>';
            fhtml+='<div class="common-input">';
                fhtml+='<label>'+cerDiploma+' <span class="needed">*</span></label>';
                fhtml+='<div class="input-group ucertificater">';
                    fhtml+='<input type="text" class="form-control ufilename required" aria-describedby="basic-addon2" readonly required="required">';
                    fhtml+='<input name="uc_certificate_photo[]" class="uc_certificate_photo" type="file" style="visibility: hidden;height: 0;width: 0">';
                fhtml+='<div class="input-group-append">';
                    fhtml+='<span class="input-group-text input-confirm certproofuploader">'+cerUpload+'</span>';
                fhtml+='</div>';
            fhtml+='</div>';
        fhtml+='</div>';

        $("#certificate-adder").append(fhtml);

    })

$(document).on("click",".exp_remover",function(){
    $(this).parents(".exp_block").remove();
})


$(document).on("click",".cer_remover",function(){
    $(this).parents(".cer_block").remove();
})

function resetFormValidator(formId) {
    $(formId).removeData('validator');
    $(formId).removeData('unobtrusiveValidation');
    $.validator.unobtrusive.parse(formId);
}

$(function(){
    $('.exper_form').validate({
        rules:{
            'ue_desc[]'                     : {required:true, minlength:1 , maxlength:200},
            'ue_diploma_fake_photo[]'       : {required:true},
            'uc_certificate_title[]'        : {required:true, minlength:1 , maxlength:200},
            'uc_certificate_fake_photo[]'   : {required:true},

        }
    });
})

</script>