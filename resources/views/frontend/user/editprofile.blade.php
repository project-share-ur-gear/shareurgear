<?php
$type   = Auth::guard('user')->user()->type;
$subtype= Auth::guard('user')->user()->signup_type;
?>

<style type="text/css">
input[type="time"]::-webkit-calendar-picker-indicator{background: none;}
</style>

@php
$availability = DB::table('user_availability')->where('ua_user_id',Auth::guard('user')->id())->get()->all();

$day = array_column($availability, 'ua_day');


$start_time = array_column($availability,'ua_time_from');
$end_time   = array_column($availability,'ua_time_to');
$locale = App::getLocale();

if($locale=='en'){
	$daysArr = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
}
else{
	$daysArr = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'];
}
$day = $availability;

$startTimeArr = $endTimeArr = [];
foreach($availability as $data){
    $startTimeArr[$data->ua_day] = $data->ua_time_from;
    $endTimeArr[$data->ua_day] = $data->ua_time_to;
}

$o = $d = [];
foreach($daysArr as $k => $value){
    foreach($day as $k=>$val){
        if($val->ua_day == $value){
            $o[$value] = '1';
        }

        if($val != $value){
            $d[$value] = '';
        }
    }
}

$open_day = array_merge($d,$o);
@endphp


<link href="{{ SITE_HTTP_URL.'/public/plugins/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css' }}" rel="stylesheet" type="text/css" />

<div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
    <div class="first_tabing">
        {!! Form::open(['url' => '/user/update', 'method' => 'post', 'class' => 'profile_form', 'name' => 'profileForm', 'id' => 'profileForm']) !!}
        @method('PATCH')
        @csrf
        <div class="personal_data_info">
            <h2 class="title">{{ __('general.personal_information') }}  </h2>

            @if($subtype=='individual')

            @if($type=='provider')
            <div class="form-group">
                {!! Form::label('neq_number', __('general.business_what_is_neq')) !!}
                {!! Form::text('neq_number', Auth::guard('user')->user()->neq_number ?? old('neq_number'),array('placeholder'=>__('general.neq_placeholder'),'class'=>'form-control', 'maxlength'=>'100')) !!}
                {{-- @if($errors->has('neq_number'))
                  <div class="error-message">{{ $errors->first('neq_number') }}</div>
                @endif --}}
                <small class="smalls-one">{{ __('general.govt_id_small_text') }}</small>
            </div>


            <div class="form-group">
                <label>{{ __('general.Are your services taxable?') }}<span class="needed">*</span></label>
                <div class="check-data-tax">
                    <div class="check-boxs">
                        <div class="checks custom-checks">
                            <label class="checkbox">{{ __('general.yes') }}
                                <input type="checkbox" class="required" name="service_taxable" value="1"
                                <?php 
                                if(Auth::guard('user')->user()->service_taxable=='1'){ 
                                    echo "checked=checked";
                                }
                                else{
                                    if(old('service_taxable') == '1'){
                                        echo "checked";
                                    }
                                }
                                ?>
                                >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div class="check-boxs">
                        <div class="checks custom-checks">
                            <label class="checkbox">{{ __('general.no') }}
                                <input type="checkbox" class="required" name="service_taxable" value="2"
                                <?php 
                                if(Auth::guard('user')->user()->service_taxable=='2'){ 
                                    echo "checked=checked";
                                }
                                else{
                                    if(old('service_taxable') == '2'){
                                        echo "checked";
                                    }
                                }
                                ?>
                                >
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <span for="service_taxable" class="help-block" style="display: none">{{ __('general.required_field.') }}</span>
            </div>

            <div class="services_taxable_ornot {{ Auth::guard('user')->user()->service_taxable!='1'?'d-none':'' }} ">
                <div class="form-group">
                    {!! Form::label('tps_price', __('general.tps_field_label')) !!}
                    {!! Form::text('tps_price', Auth::guard('user')->user()->tps_price ?? old('tps_price'),array('placeholder'=>__('general.enter_tps'),'class'=>'form-control', 'maxlength'=>'30' )) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('tvq_price', __('general.tvq_field_label')) !!}
                    {!! Form::text('tvq_price', Auth::guard('user')->user()->tvq_price ?? old('tvq_price'),array('placeholder'=>__('general.enter_tvq'),'class'=>'form-control', 'maxlength'=>'30' )) !!}
                </div>
            </div>
            @endif

            <div class="form-row">
                <div class="col-sm-6 ">
                    <div class="form-group mb-2">
                        {!! Form::label('first_name', __('general.first_name')) !!}
                        {!! Form::text('first_name',Auth::guard('user')->user()->first_name ?? old('first_name'),array('class'=>'form-control lettersonly required', 'maxlength'=>'30')) !!}
                        <small class="smalls-one">{{ __('general.name_match_govt_id_text') }}</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('last_name', __('general.last_name')) !!}
                        {!! Form::text('last_name',Auth::guard('user')->user()->last_name ?? old('last_name'),array('class'=>'form-control lettersonly required', 'maxlength'=>'30')) !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        {!! Form::label('dob', __('general.dob')) !!}
                        {!! Form::text('dob',date('m/d/Y',strtotime(Auth::guard('user')->user()->dob)) ?? old('dob'),array('placeholder'=>'MM/DD/YYYY','class'=>'form-control dobdatepicker required', 'maxlength'=>'30')) !!}
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="dob">{{ __('general.gender') }}</label>
                        {!! Form::select('gender', $gender_info,Auth::guard('user')->user()->gender ?? old('gender'),array('required'=>'required', 'class'=>'form-control selectpicker required', 'maxlength'=>'100' )) !!}
                        <span for="gender" class="help-block" style="display:none">{{ __('general.This field is required.') }}</span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('address', __('general.address')) !!}
                {!! Form::text('address',Auth::guard('user')->user()->address ?? old('address'),array('placeholder'=>__('general.address'),'required'=>'required', 'class'=>'form-control required', 'maxlength'=>'100','id'=>'myaddress' )) !!}
                {{-- @if($errors->has('address'))
                    <div class="error-message">{{ $errors->first('address') }}</div>
                @endif --}}

                <input type="hidden" id="country" name="country" value="{{ Auth::guard('user')->user()->country }}">
                <input type="hidden" id="city" name="city" value="{{ Auth::guard('user')->user()->city }}">
                <input type="hidden" id="state" name="state" value="{{ Auth::guard('user')->user()->state }}">
                <input type="hidden" id="latitude" name="latitude" value="{{ Auth::guard('user')->user()->latitude }}">
                <input type="hidden" id="longitude" name="longitude" value="{{ Auth::guard('user')->user()->longitude }}">
            </div>

            <div class="form-group">
                <label for="zipcode">{{ __('general.postal_code') }}</label>
                    {!! Form::text('zipcode',Auth::guard('user')->user()->zipcode ?? old('zipcode'),array('placeholder'=>__('general.postal_code'),'id'=>'zipcode','class'=>'form-control required','required'=>'required', 'maxlength'=>'100' )) !!}
                    {{-- @if($errors->has('address1'))
                    <div class="error-message">{{ $errors->first('address1') }}</div>
                @endif --}}
            </div>
            
            <div class="form-group">
                <label for="address1">
                    {{ __('general.apt_field_label') }} <small>({{__('general.optional') }})</small>
                </label>
                    {!! Form::text('address1',Auth::guard('user')->user()->address1 ?? old('address1'),array('placeholder'=>__('general.apt_field_label'),'class'=>'form-control', 'maxlength'=>'100' )) !!}
                {{-- @if($errors->has('address1'))
                    <div class="error-message">{{ $errors->first('address1') }}</div>
                @endif --}}
            </div>

            <div class="common-input">
                {!! Form::label('phone', __('general.phoneno_field_label')) !!}
                <div class="input-group">
                    {!! Form::text('phone_number',Auth::guard('user')->user()->phone_number ?? old('phone_number'),array('placeholder'=>__('general.phoneno_field_label'),'required'=>'required', 'class'=>'form-control digits required', 'maxlength'=>'10', 'minlength'=>'10' )) !!}
                    <div class="input-group-append">
                        <span class="input-group-text input-confirm" id="basic-addon2" disabled="disabled" style="cursor: not-allowed;">{{ __('general.confirmed') }}</span>
                    </div>
                </div>
                <small class="pininfo" style="display: none;">{{ __('general.pin_confirm_info_text') }}</small>
            </div>

            <div class="form-group">
                {!! Form::label('email', __('general.email_field_label')) !!}
                {!! Form::text('email',Auth::guard('user')->user()->email ?? old('email'),array('required'=>'required', 'class'=>'form-control required email checkemail','autocomplete'=>'new', 'maxlength'=>'100' )) !!}
            </div>

            @if($type=='provider')
            <!-- new addon design -->
            <div class="form-group">
                {!! Form::label('language', __('general.lang_spoken')) !!}
                {!! Form::text('language',Auth::guard('user')->user()->language ?? old('language'),array('required'=>'required', 'class'=>'form-control required','autocomplete'=>'new', 'maxlength'=>'200' )) !!}
            </div>

            <div class="form-group">
                {!! Form::label('about', __('general.about_me')) !!}
                {!! Form::textarea('about',Auth::guard('user')->user()->about ?? old('about'),array('required'=>'required', 'class'=>'form-control required','autocomplete'=>'new', 'maxlength'=>'1000','rows'=>3 )) !!}
            </div>


            <h2 class="title">{{ __('general.availability') }} </h2>

            @foreach($daysArr as $key => $day)
            <? 
            $startTimeVal = $endTimeVal = '';
            if(isset($startTimeArr[$day])){
                $startTimeVal = date("H:i A",strtotime($startTimeArr[$day]));
            } 
            if(isset($endTimeArr[$day])){
                $endTimeVal = date("H:i A",strtotime($endTimeArr[$day]));
            } 

            $dayOpenCheck = $checkMac =  '';
            if(!empty($open_day)){
                if($open_day[$day]=='1'){
                    $checkMac = 'checked';
                }
            }

            ?>
            <div class="row align-items-center available-data">
                <div class="col-lg-3 col-sm-4">
                    <div class="check-boxs">
                        <div class="checks custom-checks">
                            <label class="checkbox"> {{ $day }}
                                <input type="checkbox" name="selected_day[{{$day}}]" value="1" {{ $checkMac }} class="dayselector">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-7 col-sm-8">
                    <div class="form-row align-items-center">
                        <div class="col-5"><div class="form-group"><input type="text" class="form-control time_from{{$key}}" value="{{ $startTimeVal }}" name="time_from[{{$key}}]" {{ $startTimeVal==""?'readonly="readonly"':'' }} data-starttime="{{$startTimeVal}}" ></div></div>
                        <div class="col-2"><div class="dot-text">-</div></div>
                        <div class="col-5"><div class="form-group"><input type="text" class="form-control time_to{{$key}}" value="{{ $endTimeVal }}" data-endtime="{{$endTimeVal}}" name="time_to[{{$key}}]" {{ $endTimeVal==""?'readonly="readonly"':'' }}></div></div>
                    </div>
                </div>                                               
            </div>

            <script type="text/javascript">
                $(function(){
                    $('.time_from{{$key}}').datetimepicker({
                        format: 'LT',
                        //debug: true,
                        icons: {
                            time: "fas fa-clock-o",
                            up  : "fas fa-arrow-up",
                            down: "fas fa-arrow-down"
                        }
                    });

                    $('.time_to{{$key}}').datetimepicker({
                        format: 'LT',
                        useCurrent: false,
                        icons: {
                            time: "fas fa-clock-o",
                            up  : "fas fa-arrow-up",
                            down: "fas fa-arrow-down"
                        }
                    });

                    $('.time_from{{$key}}').on("dp.change", function (e) {
                        var selectedTime    = moment(e.date).format("H:mm a");
                        var selectedEndTime = $('.time_to{{$key}}').attr('data-endtime');

                        // console.log(selectedTime);

                        // if(selectedTime >= selectedEndTime){
                        //     $('.time_to{{$key}}').val('');
                        // }

                        // $('.time_from{{$key}}').attr("data-starttime",selectedTime);
                        // $('.time_from{{$key}}').data("starttime",selectedTime);

                        $('.time_to{{$key}}').data("DateTimePicker").minDate(e.date);
                    });
                    $('.time_to{{$key}}').on("dp.change", function (e) {
                        // var selectedTime    = moment(e.date).format("H:mm a");
                        // var selectedStartTime = $('.time_from{{$key}}').val();

                        // if(selectedTime < selectedStartTime){
                        //     $('.time_from{{$key}}').val('');
                        // }

                        $('.time_from{{$key}}').data("DateTimePicker").maxDate(e.date);
                    });

                })
            </script>

            @endforeach
            @endif

            @endif

            @if($subtype=='business' || $subtype=='home')
                @if($type=='provider')
                <div class="form-group">
                    {!! Form::label('neq_number', __('general.business_what_is_neq')) !!}
                    {!! Form::text('neq_number', Auth::guard('user')->user()->neq_number ?? old('neq_number'),array('placeholder'=>__('general.neq_placeholder'),'class'=>'form-control', 'maxlength'=>'100')) !!}
                    {{-- @if($errors->has('neq_number'))
                      <div class="error-message">{{ $errors->first('neq_number') }}</div>
                    @endif --}}
                    <small class="smalls-one">{{ __('general.What is your government ID number') }}</small>
                </div>

                <div class="form-group">
                    <label>{{ __('general.Are your services taxable?') }}<span class="needed">*</span></label>
                    <div class="check-data-tax">
                        <div class="check-boxs">
                            <div class="checks custom-checks">
                                <label class="checkbox">{{ __('general.yes') }}
                                    <input type="checkbox" class="required" name="service_taxable" value="1"
                                    <?php 
                                    if(Auth::guard('user')->user()->service_taxable=='1'){ 
                                        echo "checked=checked";
                                    }
                                    else{
                                        if(old('service_taxable') == '1'){
                                            echo "checked";
                                        }
                                    }
                                    ?>
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="check-boxs">
                            <div class="checks custom-checks">
                                <label class="checkbox">{{ __('general.no') }}
                                    <input type="checkbox" class="required" name="service_taxable" value="2"
                                    <?php 
                                    if(Auth::guard('user')->user()->service_taxable=='2'){ 
                                        echo "checked=checked";
                                    }
                                    else{
                                        if(old('service_taxable') == '2'){
                                            echo "checked";
                                        }
                                    }
                                    ?>
                                    >
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <span for="service_taxable" class="help-block" style="display: none">{{ __('general.required_field.') }}</span>
                </div>

                <div class="services_taxable_ornot {{ Auth::guard('user')->user()->service_taxable!='1'?'d-none':'' }} ">
                    <div class="form-group">
                        {!! Form::label('tps_price', __('general.tps_field_label')) !!}
                        {!! Form::text('tps_price', Auth::guard('user')->user()->tps_price ?? old('tps_price'),array('placeholder'=>__('general.enter_tps'),'class'=>'form-control', 'maxlength'=>'100')) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('tvq_price', __('general.tvq_field_label')) !!}
                        {!! Form::text('tvq_price', Auth::guard('user')->user()->tvq_price ?? old('tvq_price'),array('placeholder'=>__('general.enter_tvq'),'class'=>'form-control', 'maxlength'=>'100')) !!}
                    </div>
                </div>
                @else
                @if($subtype=='home')
                <div class="form-group">
                    {!! Form::label('retirement_home', __('general.retirement_name_home')) !!}
                    {!! Form::text('retirement_home', Auth::guard('user')->user()->retirement_home ?? old('retirement_home'),array('placeholder'=>__('general.retirement_name_home'),'class'=>'form-control  required', 'maxlength'=>'100')) !!}
                    @if($errors->has('retirement_home'))
                      <div class="error-message">{{ $errors->first('retirement_home') }}</div>
                    @endif
                </div>
                @else
                <div class="form-group">
                    {!! Form::label('business_name', __('general.name_of_business')) !!}
                    {!! Form::text('business_name', Auth::guard('user')->user()->business_name ??  old('business_name'),array('placeholder'=>__('general.name_of_business'),'class'=>'form-control  required', 'maxlength'=>'100')) !!}
                    {{-- @if($errors->has('business_name'))
                        <div class="error-message">{{ $errors->first('business_name') }}</div>
                    @endif --}}
                </div>
                @endif
                @endif

                <div id="manager-adder">

                    @foreach($managers as $key => $manager)
                    <div class="manager-block">
                        <div class="form-row">
                            <div class="col-sm-12">
                                <h3 class="title">
                                    <span>{{ ordinal($key+1) }}{{ __('general.manager') }}</span>
                                    @if($key >= 1)
                                    <a class="manager_remover"><i class="fas fa-trash"></i></a>
                                    @endif
                                </h3>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('first_name', __('general.first_name')) !!}
                                    {!! Form::text('first_name[]', $manager->first_name ?? old('first_name[]'),array('placeholder'=>__('general.first_name'),'class'=>'form-control lettersonly required', 'maxlength'=>'30')) !!}
                                    {{-- @if($errors->has('first_name[]'))
                                      <div class="error-message">{{ $errors->first('first_name[]') }}</div>
                                    @endif --}}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('last_name[]', __('general.last_name')) !!}
                                    {!! Form::text('last_name[]',$manager->last_name ?? old('last_name[]'),array('placeholder'=>__('general.last_name'),'class'=>'form-control lettersonly required', 'maxlength'=>'30')) !!}
                                    {{-- @if($errors->has('last_name[]'))
                                        <div class="error-message">
                                            {{ $errors->first('last_name[]') }}
                                        </div>
                                    @endif --}}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('dob', __('general.dob')) !!}
                                    {!! Form::text('dob[]',date('m/d/Y',strtotime($manager->dob)) ?? old('dob[]'),array('placeholder'=>__('general.MM/DD/YYYY'),'class'=>'form-control dobdatepicker required', 'maxlength'=>'30')) !!}
                                     {{-- @if($errors->has('dob[]'))
                                      <div class="error-message">{{ $errors->first('dob[]') }}</div>
                                     @endif --}}
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    {!! Form::label('dob', __('general.gender')) !!}
                                    {!! Form::select('gender[]', $gender_info,$manager->gender ?? old('gender[]'),array('required'=>'required', 'class'=>'form-control selectpicker required', 'maxlength'=>'100' )) !!}
                                    <span for="gender[]" class="help-block" style="display:none">{{ __('general.This field is required.') }}</span>
                                    {{-- @if($errors->has('gender[]'))
                                    <div class="error-message">{{ $errors->first('gender[]') }}</div>
                                    @endif --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <a class="add-btns">+ {{ __('general.addmanager') }}</a>

                <div class="form-group">
                    {!! Form::label('address', __('general.address')) !!}
                    {!! Form::text('address',Auth::guard('user')->user()->address ?? old('address'),array('placeholder'=>__('general.Address'),'required'=>'required', 'class'=>'form-control required', 'maxlength'=>'100','id'=>'myaddress' )) !!}
                    {{-- @if($errors->has('address'))
                        <div class="error-message">{{ $errors->first('address') }}</div>
                    @endif --}}

                    <input type="hidden" id="country" name="country" value="{{ Auth::guard('user')->user()->country }}">
                    <input type="hidden" id="city" name="city" value="{{ Auth::guard('user')->user()->city }}">
                    <input type="hidden" id="state" name="state" value="{{ Auth::guard('user')->user()->state }}">
                    <input type="hidden" id="latitude" name="latitude" value="{{ Auth::guard('user')->user()->latitude }}">
                    <input type="hidden" id="longitude" name="longitude" value="{{ Auth::guard('user')->user()->longitude }}">
                </div>

                <div class="form-group">
                    <label for="zipcode">{{ __('general.postal_code') }}</label>
                        {!! Form::text('zipcode',Auth::guard('user')->user()->zipcode ?? old('zipcode'),array('placeholder'=>__('general.Enter postal code'),'id'=>'zipcode','class'=>'form-control required','required'=>'required', 'maxlength'=>'100' )) !!}
                        {{-- @if($errors->has('address1'))
                        <div class="error-message">{{ $errors->first('address1') }}</div>
                    @endif --}}
                </div>
                
                <div class="form-group">
                    <label for="address1">
                        {{ __('general.apt_field_label') }} <small>({{__('general.optional') }})</small>
                    </label>
                        {!! Form::text('address1',Auth::guard('user')->user()->address1 ?? old('address1'),array('placeholder'=>__('general.Apt., Suite'),'class'=>'form-control', 'maxlength'=>'100' )) !!}
                    {{-- @if($errors->has('address1'))
                        <div class="error-message">{{ $errors->first('address1') }}</div>
                    @endif --}}
                </div>

                <div class="common-input">
                    {!! Form::label('phone', __('general.phoneno_field_label')) !!}
                    <div class="input-group">
                        {!! Form::text('phone_number',Auth::guard('user')->user()->phone_number ?? old('phone_number'),array('placeholder'=>__('general.Phone Number'),'required'=>'required', 'class'=>'form-control digits required','id'=>'phone_number', 'maxlength'=>'10', 'minlength'=>'10' )) !!}
                        <div class="input-group-append">
                            <span class="input-group-text input-confirm" id="basic-addon2" disabled="disabled" style="cursor: not-allowed;" >{{__('general.confirmed') }}</span>
                        </div>
                    </div>
                    <small class="pininfo" style="display: none;">{{__('general.You will receive a Pin to confirm your number') }}</small>
                </div>

                <div class="form-group">
                    {!! Form::label('email', __('general.email_field_label')) !!}
                    {!! Form::text('email',Auth::guard('user')->user()->email ?? old('email'),array('required'=>'required', 'class'=>'form-control required email checkemail','autocomplete'=>'new', 'maxlength'=>'100' )) !!}
                </div>

                @if($type=='provider')
                <!-- new addon design -->
                <div class="form-group">
                    {!! Form::label('language', __('general.Language spoken')) !!}
                    {!! Form::text('language',Auth::guard('user')->user()->language ?? old('language'),array('required'=>'required', 'class'=>'form-control required','autocomplete'=>'new', 'maxlength'=>'200' )) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('about',  __('general.about_me')) !!}
                    {!! Form::textarea('about',Auth::guard('user')->user()->about ?? old('about'),array('required'=>'required', 'class'=>'form-control required','autocomplete'=>'new', 'maxlength'=>'1000','rows'=>3 )) !!}
                </div>


                <h2 class="title">{{__('general.availability') }} </h2>

                @foreach($daysArr as $key => $day)
                <? 
                $startTimeVal = $endTimeVal = '';
                if(isset($startTimeArr[$day])){
                    $startTimeVal = date("H:i A",strtotime($startTimeArr[$day]));
                } 
                if(isset($endTimeArr[$day])){
                    $endTimeVal = date("H:i A",strtotime($endTimeArr[$day]));
                } 

                $dayOpenCheck = $checkMac =  '';
                if(!empty($open_day)){
                    if($open_day[$day]=='1'){
                        $checkMac = 'checked';
                    }
                }
                ?>
                <div class="row align-items-center available-data">
                    <div class="col-lg-3 col-sm-4">
                        <div class="check-boxs">
                            <div class="checks custom-checks">
                                <label class="checkbox"> {{ $day }}
                                    <input type="checkbox" name="selected_day[{{$day}}]" value="1" {{ $checkMac }} class="dayselector">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div> 
                    <div class="col-lg-7 col-sm-8">
                        <div class="form-row align-items-center">
                            <div class="col-5"><div class="form-group"><input type="text" class="form-control time_from{{$key}}" value="{{ $startTimeVal }}" name="time_from[{{$key}}]" {{ $startTimeVal==""?'readonly="readonly"':'' }} data-starttime="{{$startTimeVal}}" ></div></div>
                            <div class="col-2"><div class="dot-text">-</div></div>
                            <div class="col-5"><div class="form-group"><input type="text" class="form-control time_to{{$key}}" value="{{ $endTimeVal }}" name="time_to[{{$key}}]" {{ $endTimeVal==""?'readonly="readonly"':'' }} data-endtime="{{$endTimeVal}}"></div></div>
                        </div>
                    </div>                                               
                </div>

                <script type="text/javascript">
                    $(function(){
                        $('.time_from{{$key}}').datetimepicker({
                            format: 'LT',
                            icons: {
                                time: "fas fa-clock-o",
                                up  : "fas fa-arrow-up",
                                down: "fas fa-arrow-down"
                            }
                        });

                        $('.time_to{{$key}}').datetimepicker({
                            format: 'LT',
                            useCurrent: false,
                            icons: {
                                time: "fas fa-clock-o",
                                up  : "fas fa-arrow-up",
                                down: "fas fa-arrow-down"
                            }
                        });

                        $('.time_from{{$key}}').on("dp.change", function (e) {
                            $('.time_to{{$key}}').data("DateTimePicker").minDate(e.date);
                        });
                        $('.time_to{{$key}}').on("dp.change", function (e) {
                            $('.time_from{{$key}}').data("DateTimePicker").maxDate(e.date);
                        });

                    })
                </script>

                @endforeach
                @endif
            @endif
        </div>
        <div class="border-lines"></div>
        <div class="bottom_box">
            <button class="provider-btn register-bg">{{__('general.update') }}</button>
        </div>
        {!! Form::close() !!}
    </div>
</div>


<script src="{{ SITE_HTTP_URL.'/public/js/moment.js' }}"></script>
<script src="{{ SITE_HTTP_URL.'/public/plugins/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js' }}"></script>

<script type="text/javascript">
$(function(){
    $("#phone_number").on("keypress",function(){
        var existingPhone = '{{ Auth::guard('user')->user()->phone_number }}';
        if(existingPhone == $(this).val()){
            return false;
        }
        $(".input-confirm").addClass("confirm-phone-twilio");
        $(".input-confirm").text("Confirm");
        $(".input-confirm").removeAttr("style");
        $(".pininfo").removeAttr("style");
    })
})

$(document).on("change",".dayselector",function(){
    var currentState = $(this).prop("checked");
    if(currentState==false){
        $(this).parents(".available-data").find("input[type='text']").attr("readonly","readonly");
        $(this).parents(".available-data").find("input[type='text']").val("");
    }
    else{
        $(this).parents(".available-data").find("input[type='text']").removeAttr("readonly");
    }
})

$(document).on("click",".add-btns",function(){
    var titleText   = $(".manager-block").last().find(".title span").text();
    var newTitleText= parseInt(titleText)+1;
    var suffixedVal = ordinal_suffix_of(newTitleText)+' manager';
    
    var genderText = '{{ __("general.gender") }}';
    var gender_male = '{{ __("general.male") }}';
    var gender_female = '{{ __("general.female") }}';
    var other = '{{ __("general.other") }}';
    var dob = '{{ __("general.dob") }}';
    var firstName = '{{ __("general.first_name") }}';
    var lastName = '{{ __("general.last_name") }}';

    var managerBlock = '';
    managerBlock+='<div class="manager-block" id="managernumber'+newTitleText+'">';
        managerBlock+='<div class="form-row">';
            managerBlock+='<div class="col-sm-12">';
                managerBlock+='<h3 class="title"><span>'+suffixedVal+'</span> <a class="manager_remover"><i class="fas fa-trash"></i></a></h3>';
            managerBlock+='</div>';
            managerBlock+='<div class="col-sm-6">';
            managerBlock+='<div class="form-group">';
            managerBlock+='<label for="first_name" class="active">'+firstName+'<span class="needed"> *</span></label>';
            managerBlock+='<input placeholder="'+firstName+'" class="form-control lettersonly required" maxlength="30" name="first_name['+newTitleText+']" type="text" aria-required="true" autocomplete="off">';
            managerBlock+='</div>';
            managerBlock+='</div>';
            managerBlock+='<div class="col-sm-6">';
            managerBlock+='<div class="form-group">';
            managerBlock+='<label for="last_name" class="active">'+lastName+'<span class="needed"> *</span></label>';
            managerBlock+='<input placeholder="'+lastName+'" class="form-control lettersonly required" maxlength="30" name="last_name['+newTitleText+']" type="text" aria-required="true" autocomplete="off">';
            managerBlock+='</div>';
            managerBlock+='</div>';
            managerBlock+='<div class="col-sm-6">';
            managerBlock+='<div class="form-group">';
            managerBlock+='<label for="dob" class="active">'+dob+'<span class="needed"> *</span></label>';
            managerBlock+='<input placeholder="MM/DD/YYYY" class="form-control dobdatepicker required" maxlength="30" name="dob['+newTitleText+']" type="text" aria-required="true" autocomplete="off">';
            managerBlock+='</div>';
            managerBlock+='</div>';
            managerBlock+='<div class="col-sm-6">';
            managerBlock+='<div class="form-group">';
            managerBlock+='<label for="dob">'+genderText+'<span class="needed"> *</span></label>';
            managerBlock+='<select required="required" class="form-control selectpicker required" maxlength="100" name="gender['+newTitleText+']" aria-required="true"><option value="" selected="selected">Choose</option><option value="male">'+gender_male+'</option><option value="female">'+gender_female+'</option><option value="other">'+other+'</option></select>';
            managerBlock+='<span for="gender['+newTitleText+']" class="help-block" style="display:none">'+fieldRequired+'</span>';
            managerBlock+='</div>';
            managerBlock+='</div>';
        managerBlock+='</div>';
    managerBlock+='</div>';

    $("#manager-adder").append(managerBlock);

    $(".selectpicker").selectpicker();

    $(".dobdatepicker").datepicker({
        endDate     : '-16y',
        autoclose   : true,
        format      : 'mm/dd/yyyy'
    });
})

$(document).on("click",".manager_remover",function(){
    $(this).parents(".manager-block").remove();
    $(".manager-block").each(function(index,element){
        var numberVal = (index)+1;
        var arrangeSuffixedVal  = ordinal_suffix_of(numberVal)+' manager';
        $(element).find(".title span").text(arrangeSuffixedVal);
    })
})

</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqVOKulpEhFwPZd7R5h_zqW70Ch19DVOI&libraries=places&sensor=false&callback=initMap" defer async></script>

<script type="text/javascript">
function initMap(){
    var input = document.getElementById('myaddress');
    var autocomplete = new google.maps.places.Autocomplete(input);

    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        var lat = place.geometry.location.lat();
        var lng = place.geometry.location.lng();
        
        var country=state=city=zipcode='';
        
        $('#country').val('');
        $('#state').val('');
        $('#city').val('');
        $('#zipcode').val('');

        var addr1 = addr2 = '';
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i]['long_name'];

            console.log(addressType);
			
            if(addressType == 'country'){
                country = val;
            }
			
            if(addressType == 'administrative_area_level_1'){
                state = val;
            }

            if(addressType == 'locality'){
            	city = val;
            }
			
            if(addressType == 'postal_code'){
            	zipcode = val;
            }
        }
		
        if(city==''){
            city = state;
        }

        $('#country').val(country);
        $('#state').val(state);
        $('#city').val(city);
        $('#zipcode').val(zipcode);

        $('#latitude').val(lat);
        $('#longitude').val(lng);

    });

    $(document).on("change",'input[name="service_taxable"]',function(){
        var group = "input:checkbox[name='"+$(this).prop("name")+"']";
        $(group).prop("checked",false);
        $(this).prop("checked",true);
    })
}


$('input[name="service_taxable"]').on("change",function(){
    var selectedVal = $(this).val();
    if(selectedVal=='2'){
        $('.services_taxable_ornot').find("input").removeClass("required");
        $('.services_taxable_ornot').addClass("d-none");
    }
    else{
        $('.services_taxable_ornot').find("input").addClass("required");
        $('.services_taxable_ornot').removeClass("d-none");
    }
})
</script>