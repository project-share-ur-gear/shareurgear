@extends('layouts.app')
@section('content')

@php
$locale=App::getLocale();
if(Auth::guard('user')->user()->type=='client'){
    $userSettingsArr = [
        'id_approved'           => __('general.Your ID Proof is approved'),
        'bg_check_approved'     => __('general.Your Background check is approved'),
        //'msg_received'        => 'You receive a message in your mailbox',
        'proposal_received'     => __('general.You receive a proposal on your posted job'),
        'counter_offer_received'=> __('general.You receive a counter-offer'),
        'offer_rejected'        => __('general.Your offer is rejected'),
        'offer_accepted'        => __('general.Your offer is accepted by a provider and that it is time for you to pay'),
        'job_booked'            => __('general.A job is booked and your money is frozen on the platform'),
        'booking_cancelled'     => __('general.A booking is cancelled'),
        //'pin_received'          => 'The PIN of the booking is ready to be sent to you',
        'booking_completed_prov'=> __('general.A booking is confirmed as ‘Completed’ by the provider. You receive your review and you can send provider review and tip'),
        //'amount_billed_to_acct' => 'The amount of the service rendered is billed to your account',
        'calendar_reminder'     => __('general.You receive a reminder from your calendar about a booking or a note.'),
        'ref_money_deposited'   => __('general.Your referral coupon has been used and your referral money is deposit in your wallet')
    ];
}
else{
    $userSettingsArr = [
        'id_approved'           => __('general.Your ID Proof is approved'),
        'bg_check_approved'     => __('general.Your Background check is approved'),
        //'msg_received'        => 'You receive a message in your mailbox',
        //'job_posted'          => 'A new job matching your availability and distance criteria is posted by a client',
        'counter_offer_received'=> __('general.You receive a counter-offer'),
        'offer_rejected'        => __('general.Your offer is rejected'),
        //'job_booked'          => 'A job is booked',
        'booking_cancelled'     => __('general.A booking is cancelled'),
        //'amt_deposited_wallet'  => 'An amount has been deposited in your wallet after a completed booking',
        'review_received'       => __('general.You receive a review and a tip after a complete booking'),
        'calendar_reminder'     => __('general.You receive a reminder from your calendar about a booking or a note.'),
        'ref_money_deposited'   => __('general.Your referral coupon has been used and your referral money is deposit in your wallet')
    ];
}
@endphp


<section class="account_profile client-setting">
	<div class="container">
		<div class="account_pages">
			<!--- Left side Panel --->
            @include('frontend.sidebar')
			<div class="right_account common-form">
				<div class="personal_data_info">
					<h2 class="title">{{__('general.notifications_&_preferences_settings') }}</h2>
                    {!! Form::open(['url' => '/user/savesettings','method' => 'post', 'class' => 'notification_form', 'name' => 'connectForm', 'id' => 'settingsForm']) !!}
                    @csrf
                    <div class="message_boxs">
                        <div class="message_ques">
                            <h2 class="question">{{__('general.Where_would_you_like_to_get_text_messages?') }}</h2>
                            <p class="ques_desc">{{__('general.answer') }}</p>
                        </div>
                        <div class="form-group"><input readonly="readonly" type="text" class="form-control" value="+1 {{ preg_replace('~[+\d-](?=[\d-]{4})~', '*', Auth::guard('user')->user()->phone_number) }}" maxlength="13"></div>
                    </div>
                    <div class="message_boxs">
                        <div class="message_ques" style="width: 63%">
                            <h2 class="question">{{__('general.Where_would_you_like_to_get_email_notifications') }}</h2>
                            <p class="ques_desc">{{__('general.All notification emails will be sent to this email, to update email go to your') }} <a href="{{url('user-profile?type=edit')}}" style="color:#19c1eb">{{__('general.profile_settings')}}</a>.</p>
                        </div>
                        <div class="form-group"><input readonly="readonly" type="text" class="form-control" value="{{ Auth::guard('user')->user()->email}}" maxlength="13" style="width:225px;line-height: 40px;"></div>
                    </div>
                    <div class="setting-page">
                    	<div class="email-data-field">
                            {{-- <p class="call-name">Call </p> --}}
                            <p class="call-name">{{__('general.email') }} </p>
                            <p class="call-name">{{__('general.sms') }} </p>
                        </div>
                        @foreach($userSettingsArr as $key => $setting)
                        @php
                        $populateBackSettings = [
                            'email' => '',
                            'msg'   => ''
                        ];
                        if(isset($newArr[$key])){
                            $populateBackSettings = ($newArr[$key]);
                        }
                        @endphp
                        <div class="data-remindar">
                            <div class="data-remainder-left">
                                <h2 class="question">{{ $setting }}</h2>
                            </div>
                            <div class="email-data-field">
                                <div class="check-boxs">
                                    <div class="checks custom-checks">
                                        <label class="checkbox">
                                            <input type="checkbox" name="{{$key}}[email]" value="1" {{ $populateBackSettings['email']=='active'?'checked="checked"':'' }}>
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="check-boxs">
                                    <div class="checks custom-checks">
                                        <label class="checkbox">
                                            <input type="checkbox" name="{{$key}}[msg]" value="1" {{ $populateBackSettings['msg']=='active'?'checked="checked"':'' }} >
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if($setting!==end($userSettingsArr))
                        <div class="border-line-2"></div>
                        @endif
                        @endforeach
                        <br>
                    </div>
                    <div class="border-line-2"></div>
                    <div class="bottom_box">
                        <button class="provider-btn register-bg">{{__('general.save') }}</button>
                    </div>
                    {!! Form::close() !!} 
				</div>
			</div>
		</div>
	</div>
</section>

@endsection