@extends('layouts.app')
@section('content')
@include('frontend.breadcrumb')

<section class="search-results-body my-4">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h4 class="text-blue mb-3">My Profile</h4>
            </div><!--colmd6-->

        </div><!--row-->
        <div class="row">
            <div class="col-lg-3 col-md-4 p-0 mb-md-3">
                <!--user-profile-info-->
                 @include('frontend.sidebar')
            </div><!--colmd3-->
            <div class="col-lg-9 col-md-8">
                <div class="personal-info-card mt-3">
                    <div class="personal-info-header">
                        <h5 class="mb-3">Education</h5>
                        <p class="mb-2">Current Courses</p>
                    </div><!--personal-info-header-->

                    <div class="personal-info-body">
                    {!! Form::open(['url' => '/user/update', 'method' => 'post', 'class' => 'profile_form', 'name' => 'profileForm', 'id' => 'profileForm']) !!}
                      
                          @method('PATCH')
                          @csrf
            
            			<div id="add_current_course_div"></div>
                        <div id="add-current-course" class="mt-3">
                             {!! Form::hidden('total_current_courses',0,array('id'=>'total_current_courses')) !!}
                            <a href="javascript:void(0);" onclick="addCurrentCourse(2)"><i class="fas fa-plus"></i>&nbsp;Add Current Courses</a>
                        </div><!--add-current-course-->
                        <br />
                        <p class="mb-2">Past Courses</p>
                        <div id="add_past_course_div"></div>
                        <div id="add-past-course" class="mt-3">
                             {!! Form::hidden('total_past_courses',0,array('id'=>'total_past_courses')) !!}
                            <a href="javascript:void(0);" onclick="addPastCourse(2)"><i class="fas fa-plus"></i>&nbsp;Add Past Courses</a>
                        </div><!--add-past-course-->

                        <h5 class="mt-3">Social Media</h5>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                     {!! Form::label('facebook_link', 'Facebook link') !!}
                                     {!! Form::text('facebook_link',Auth::guard('user')->user()->facebook_link ?? old('facebook_link'),array('placeholder'=>'https://www.facebook.com/settings?_faceboo','class'=>'form-control url checkfacebooklink', 'maxlength'=>'100' )) !!}
                                    @if($errors->has('facebook_link'))
                                      <div class="error-message">{{ $errors->first('facebook_link') }}</div>
                                    @endif
                                </div>
                            </div><!--colmd6-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('twitter_link', 'Twitter Link') !!}
                                     {!! Form::text('twitter_link',Auth::guard('user')->user()->twitter_link ?? old('twitter_link'),array('placeholder'=>'https://www.twitter.com/abc','class'=>'form-control url checktwitterlink', 'maxlength'=>'100' )) !!}
                                    @if($errors->has('twitter_link'))
                                      <div class="error-message">{{ $errors->first('twitter_link') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div><!--row-->
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('linkedin_link', 'LinkedIn Link') !!}
                                     {!! Form::text('linkedin_link',Auth::guard('user')->user()->linkedin_link ?? old('linkedin_link'),array('placeholder'=>'https://in.linkedin.com/company/linkedin','class'=>'form-control url checklinkedinlink', 'maxlength'=>'100' )) !!}
                                    @if($errors->has('linkedin_link'))
                                      <div class="error-message">{{ $errors->first('linkedin_link') }}</div>
                                    @endif
                                </div>
                            </div><!--colmd6-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('youtube_link', 'YouTube Link') !!}
                                     {!! Form::text('youtube_link',Auth::guard('user')->user()->youtube_link ?? old('youtube_link'),array('placeholder'=>'https://www.youtube.com/user','class'=>'form-control url checkyoutubelink', 'maxlength'=>'100' )) !!}
                                    @if($errors->has('youtube_link'))
                                      <div class="error-message">{{ $errors->first('youtube_link') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div><!--row-->
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::label('instagram_link', 'Instagram Link*') !!}
                                     {!! Form::text('instagram_link',Auth::guard('user')->user()->instagram_link ?? old('instagram_link'),array('placeholder'=>'https://in.instagram.com/company/Instagram','class'=>'form-control url checkinstagramlink', 'maxlength'=>'100' )) !!}
                                    @if($errors->has('instagram_link'))
                                      <div class="error-message">{{ $errors->first('instagram_link') }}</div>
                                    @endif
                                </div>
                            </div><!--colmd6-->

                        </div><!--row-->
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <h5><b>Delete Account</b><br/></h5>
                                <p class="font-size-15 text-dark-grey"><b class="font-weight-500">Are you sure you want to delete your account?</b><br/>Once you delete your account, all the information will be lost forever. We will not be able to restore your account.</p>
                                <button type="button" class="btn btn-primary btn-md" onclick="deleteAccount(this)" data-message="Are you sure you want to delete your account?</b><br/>Once you delete your account, all the information will be lost forever. We will not be able to restore your account." data-value="" data-action="{{ url('/user/deleteaccount') }}"><i class="fas fa-trash"></i> &nbsp;DELETE ACCOUNT</button>
                            </div>

                        </div><!--row-->
                        <div class="d-block float-right">
                            <button class="btn btn-secondary bg-light-grey text-dark-grey btn-md" type="button" onclick="window.location.href='{{SITE_HTTP_URL}}/user/dashboard'">CANCEL</button>
                            <button class="btn btn-primary btn-md">SAVE</button>
                        </div>
                     {!! Form::close() !!}   
                    </div><!--personal-info-body-->
                </div><!--personal-info-card-->
            </div>

        </div><!--container-->
    </div>    
</section><!--search-results-body-->




<script>
var univ_info=JSON.parse('<?=$univ_info?>');
var currentCourseData=JSON.parse('<?=$currentCourseData?>');
var pastCourseData=JSON.parse('<?=$pastCourseData?>');
</script>      

@endsection