@extends('layouts.app')
@section('content')

<section class="account_profile provider-profiles">
    <div class="container">
        <div class="account_pages">

            <!--- Left side Panel --->
            @include('frontend.sidebar')

            <div class="right_account common-form">
                <div class="personal_data_info">
                    <div class="data-services">
                        <h2 class="title">{{ __('general.My Services') }} </h2>
                        <a href="{{ SITE_HTTP_URL.'/add-service' }}" class="add-btns">{{ __('general.add_service') }}</a>
                    </div>
                    <div class="table_data">
                    	<div class="table-responsive">
                    		<table class="table table-striped">
                    			<thead>
                                    <tr>
                                    <th scope="col">{{ __('general.category') }}</th>
                                    <th scope="col">{{ __('general.service') }}</th>
                                    <th scope="col">{{ __('general.price') }}</th>
                                    <th scope="col"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	@if(!empty($services))
                                	@foreach($services as $service)
                                    @php
                                    $encrypted = Crypt::encryptString($service->id);
                                    @endphp
                                    <tr class="main">
                                    <td>{{ $service->servicecategory }}</td>
                                    <td>{{ $service->servicename }} </td>
                                    <td>${{ $service->us_price+0 }}</td>
                                    <td>
                                        <div class="edit-btns-box">
                                            <a href="{{ SITE_HTTP_URL.'/edit-service/'.base64_encode($service->id)}}"class="edit-btns edit-btn-bg">
                                                <img src="{{ FRONT_IMG.'/edit.svg' }}">
                                            </a>
                                            <a href="javascript:void(0)" class="edit-btns edit-btn-bg1" onclick="deleteService(this)" data-value="{{$encrypted}}" data-action="{{ url('/service/destroy') }}" data-message="{{ __('general.Are you sure you want to delete this service') }}">

                                                <img src="{{ FRONT_IMG.'/delete.svg' }}">
                                            </a>
                                        </div>
                                    </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                    	<td colspan="4">{{ __('general.No services found') }}</td>
                                    </tr>
                                    @endif
                                </tbody>
                    		</table>
                    	</div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@endsection