@extends('layouts.app')
@section('content')


<style>

.common-section-top{background: url({{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->sign_up_image}})no-repeat;}

</style>



<?php
$title='meta_title_'.$locale;
$description='meta_desc_'.$locale;
$keywords='meta_keywords_'.$locale;
$referralCode = '';
if(isset($_GET['referral'])){
    $referralCode = $_GET['referral'];
}



?>



<meta name="description" content="{{ $pageData->$description }}">
<meta name="keywords" content="{{ $pageData->$keywords }}">
<title>{{ config('app.name', $site_configs['site_name_'.$locale]) }} | {{ $pageData->$title }}</title>


<section class="common-section-top login-common-bg">
    <div class="container">
        <!-- <h2 class="heading">Contact Us</h2> -->
    </div>
</section>



<section class="login-page signup-page" id="scrollmouse">
    <div class="left-badges lb-common slide" style="background: rgb(138, 113, 178);"></div>
    <div class="right-badges lb-common slide" style="background:#fff36e;"></div>

    <div class="container">
        <div class="login-section">

            <h2 class="login-title">{{ $pageData->cms->sign_up_text }}</h2>
            <form class="form-wrapper registerform loginForm" method="post" action="{{ url('user/saveregister') }}" id="loginForm" name="loginForm">
                @csrf
                <div class="contact-dots-panels">
                        <div class="dots dotscroll cd-dots1"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>
                        <div class="dots dotscroll cd-dots2"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>
                    <div class="contact-box common-form">
                        <div class="form-group">
                            <label for="">Name</label>
                            <input type="text" class="form-control required nameUS" name="name" id="name">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control required"  name="email" id="email"  >
                        </div>
                        <div class="form-group">
                            <label for="">Phone Number</label>
                            <input type="text" class="form-control required noalpha" name="phone_number" id="phone_number" >
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" class="form-control required" name="address" id="address"   >
                            <input type="hidden" class="form-control " name="lat_address" id="lat_address"   >
                            <input type="hidden" class="form-control " name="long_address" id="long_address"   >
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password"  name="password" id="password" class="form-control required" placeholder="Password">
                        </div>
                            
                        <div class="check-boxs">
                            <div class="checks custom-checks">
                                <!-- <label class="checkbox "> Agree <a href="#">Terms & Conditions</a>
                                    <input type="checkbox " name="agree_terms" class="custom-control-input required" id="customCheck1">
                                    <span class="checkmark"></span>
                                </label> -->

                            <label class="checkbox"> Agree <a href="{{ url('terms-and-conditions') }}"  target="_blank" >Terms and Condition</a>.
                                <input type="checkbox" name="agree_terms" value="" id="customCheck1"  class="required">
                                <span class="checkmark"></span>
                            </label>


                            </div>
                        </div>
                      
                        
                        <button type="submit" name="submit-msg-btn" id="submit-msg-btn" class="btn  submit-btn">Sign Up</button>
                        <p class="copy-go-login text-center">Already User <a href="{{ url('login') }}"> Log In </a></p>
                    </div>
                </div>

            </form>
            
        </div>
    </div>
</section>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>


<script src="https://www.google.com/recaptcha/api.js?render={{$site_configs['recaptcha_sitekey']}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script>



<script>


  function init() {
    var input = document.getElementById('address');
    var options = {
      types: ['geocode'],  
      //componentRestrictions: {country: countryCode}
    };
    var autocomplete = new google.maps.places.Autocomplete(input,options);
   
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      var place = autocomplete.getPlace();
     
    $('.location-help-block').remove();


      document.getElementById('lat_address').value = place.geometry.location.lat();
    $('span[for="lat_address"]').remove();
      document.getElementById('long_address').value = place.geometry.location.lng();
      $('span[for="long_address"]').remove();


      if(document.getElementById('lat_address').value == '' && document.getElementById('long_address').value== ''){
   $("#file_error1").html("Please enter the location from given suggestions");
   $("#file_error1").css("width","100%");
   $("#file_error1").css("margin-top",".286rem");
   $("#file_error1").css("font-size", "80%");
   $("#file_error1").css("color","#f44336");
   isSubmit=false;
   return false;
      }else{
        $("#file_error1").html("");
         isSubmit=true;
      }
      // console.log(document.getElementById('client_address_lat').value);
      // console.log(document.getElementById('client_address_long').value);
      //to remove error if address is filled
      $("input[name='address']").valid();
      
       });
  }
  $(document).ready(function(){

    //   $('#phone').inputmask('(999)-999-9999');
    google.maps.event.addDomListener(window, 'load', init);



grecaptcha.ready(function() {
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $('#loginForm').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
    });
});
$(document).on("keyup","input, textarea",function(){
    $("form").find("button").removeAttr("disabled");
})


  $('#loginForm').validate({
			ignore: '',
			rules: {
				name: {
					required: true,
                    specialChar:true,
                    noSpace:true,
                    maxlength:30
				},
				email: {
					required: true,
				},
				password: {
                    required: true,
                    passcheck:true,
                    minlength: 8, 
                    maxlength: 30,
                },
                phone_number: 
                {
                    minlength:10, maxlength:10
                },
			},
			messages: {
                phone_number:{
    		    minlength:'Please enter your valid 10 digit phone number'
    	        },
				hiddenGRecaptchaResponse: {
					required: "Please check captcha",
				}
			},
			errorPlacement: function(error, element) {
                if(element.attr('name')=="agree_terms"){
                    error.insertAfter($(element).parent('.custom-control '));
                }else{
				    error.insertAfter(element);
                }
			},

	});








$('#submit-msg-btn').on('click',function(){
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $("#hiddenGRecaptchaResponse").remove();
        $('#loginForm').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
        if($('#loginForm').valid()){
		    $(this).attr("disabled","disabled");
		$("#loginForm").submit();
	    }
    
        // $("#loginForm").submit();
        
    });
});

$(document).ready(function(){
<?
if(isset($type) && !empty($type)){
    if($type=='provider'){
    ?>
    $(".meproviderhu").find("a").trigger("click");
    setTimeout(function(){
        $(".firstclickerbt").trigger("click");
    },200);
    <?
    }
    else{
    ?>
    $(".meclienthu").find("a").trigger("click");
    setTimeout(function(){
        $(".firstclickerbt").trigger("click");
    },200);
    <?
    }
}
?>
})


  // mouse-animation
var rect = $('#scrollmouse')[0].getBoundingClientRect();
var mouse = {x: 0, y: 0, moved: false};

$("#scrollmouse").mousemove(function(e) {
  mouse.moved = true;
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
});
 
// Ticker event will be called on every frame
TweenLite.ticker.addEventListener('tick', function(){
  if (mouse.moved){    
    parallaxIt(".slide", -100);
    parallaxIt(".dotscroll", -50);
  }
  mouse.moved = false;
});

function parallaxIt(target, movement) {
  TweenMax.to(target, 0.3, {
    x: (mouse.x - rect.width / 2) / rect.width * movement,
    y: (mouse.y - rect.height / 2) / rect.height * movement
  });
}

$(window).on('resize scroll', function(){
  rect = $('#scrollmouse')[0].getBoundingClientRect();
});

});

</script>

@endsection