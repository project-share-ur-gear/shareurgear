@extends('layouts.app')

@section('content')
<?php 


$title='meta_title_'.$locale;
$description='meta_desc_'.$locale;
$keywords='meta_keywords_'.$locale;
?>
                    
<meta name="description" content="{{ $pageData->$description }}">
<meta name="keywords" content="{{ $pageData->$keywords }}">
<title>{{ config('app.name', $site_configs['site_name_'.$locale]) }} | {{ $pageData->$title }}</title>



<section class="common-section-top login-common-bg">
    <div class="container">
        <!-- <h2 class="heading">Contact Us</h2> -->
    </div>
</section>



<section class="login-page forgot_page" id="scrollmouse">

    <div class="left-badges lb-common slide" style="background: rgb(138, 113, 178);"></div>
    <div class="right-badges lb-common slide" style="background:#fff36e;"></div>

    <div class="container">
        <div class="login-section">
            
                <h2 class="login-title">{!! $page_info['reset_title'] !!}</h2>

                <form class="form-wrapper profile_form" role="form" method="POST" action="{{ url('/user/password/resetpassword/'.$token) }}" id="resetPassword">
                    @csrf()
                    <div class="forgot-data contact-dots-panels">
                        <div class="dots dotscroll cd-dots1"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>
                        <div class="dots dotscroll cd-dots2"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>

                        <div class="contact-box common-form">
                            
                            <div class="form-group">
                                {!! Form::label('password', __('general.reset_newpass') ) !!}
                                {!! Form::password('password',array('placeholder'=>'','class'=>'form-control passcheck required', 'maxlength'=>'30','autocomplete'=>'new-password','id'=>'password' )) !!}
                                @if($errors->has('password'))
                                <div class="error-message">{{ $errors->first('password') }}</div>
                                @endif
                            </div>
                                
                            <div class="form-group">
                                {!! Form::label('password_confirmation', __('general.reset_confirmpass')) !!}
                                {!! Form::password('password_confirmation',array('placeholder'=>'','class'=>'form-control required', 'maxlength'=>'30','autocomplete'=>'new-password' )) !!}
                                {!! Form::hidden('token',$token,array('id'=>'token' )) !!}
                                @if($errors->has('password_confirmation'))
                                    <div class="error-message">
                                        {{ $errors->first('password_confirmation') }}
                                    </div>
                                @endif
                            </div>
                                
                            <button type="submit" name="submit-msg-btn" id="submit-msg-btn" class="btn  submit-btn">
                                {{ __('general.forgot_submit') }}
                            </button>
                            <!-- <p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p> -->
                        </div>
                    </div>
                </form>
            
            
        </div>
    </div>
</section>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>

<script src="https://www.google.com/recaptcha/api.js?render={{$site_configs['recaptcha_sitekey']}}"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $('#resetPassword').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
    });
});
$(document).on("keyup","input, textarea",function(){
    $("form").find("button").removeAttr("disabled");
})

$('#submit-msg-btn').on('click',function(){
    $(this).attr('disabled','disabled');
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $("#hiddenGRecaptchaResponse").remove();
        $('#resetPassword').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
        $("#resetPassword").submit();
    });
});



  // mouse-animation
  var rect = $('#scrollmouse')[0].getBoundingClientRect();
var mouse = {x: 0, y: 0, moved: false};

$("#scrollmouse").mousemove(function(e) {
  mouse.moved = true;
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
});
 
// Ticker event will be called on every frame
TweenLite.ticker.addEventListener('tick', function(){
  if (mouse.moved){    
    parallaxIt(".slide", -100);
    parallaxIt(".dotscroll", -50);
  }
  mouse.moved = false;
});

function parallaxIt(target, movement) {
  TweenMax.to(target, 0.3, {
    x: (mouse.x - rect.width / 2) / rect.width * movement,
    y: (mouse.y - rect.height / 2) / rect.height * movement
  });
}

$(window).on('resize scroll', function(){
  rect = $('#scrollmouse')[0].getBoundingClientRect();
});

</script>
@endsection()