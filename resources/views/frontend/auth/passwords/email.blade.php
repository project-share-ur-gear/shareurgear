@extends('layouts.app')

@section('content')
<?php 

$title='meta_title_'.$locale;
$description='meta_desc_'.$locale;
$keywords='meta_keywords_'.$locale;
?>
                    
<meta name="description" content="{{ $pageData->$description }}">
<meta name="keywords" content="{{ $pageData->$keywords }}">
<title>{{ config('app.name', $site_configs['site_name_'.$locale]) }} | {{ $pageData->$title }}</title>

<section class="common-section-top login-common-bg">
    <div class="container">
        <!-- <h2 class="heading">Contact Us</h2> -->
    </div>
</section>


<section class="login-page forgot_page" id="scrollmouse">
    <div class="left-badges lb-common slide" style="background: rgb(138, 113, 178);"></div>
    <div class="right-badges lb-common slide" style="background:#fff36e;"></div>

    <div class="container">
        <div class="login-section">
           
           
            <h2 class="login-title">{{$page_info->forgot_password_title}}</span></h2>
           
                <form class="form-wrapper profile_form forgot-password-form" role="form" method="POST" action="{{ url('/user/password/email') }}" id="forgotPassForm">
                    @csrf()
                    <div class="forgot-data contact-dots-panels">
                        
                        <div class="dots dotscroll cd-dots1"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>
                        <div class="dots dotscroll cd-dots2"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>

                        <div class="contact-box common-form">
                            <p class="copy-go-login description">{{$page_info->forgot_desc}}</p>

                            <div class="form-group">
                                <label>Email</label>
                                <!-- {!! Form::label('email', __('general.forgot_text2')) !!} -->
                                <input type="text" name="email" placeholder="Email Address" class="form-control required email" onchange="return trim(this)"  maxlength="255">
                            </div>
                            <button type="submit" name="submit-msg-btn" id="submit-msg-btn" class="btn  submit-btn">
                                {{ __('general.forgot_submit') }}
                            </button>
                            <p class="copy-go-login text-center">Back to<a href="{{ url('login') }}"> Log In </a></p>
                        </div>
                    </div>
                </form>
           
            
        </div>
    </div>
</section>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>


<script src="https://www.google.com/recaptcha/api.js?render={{$site_configs['recaptcha_sitekey']}}"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $('#forgotPassForm').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
    });
});
$(document).on("keyup","input, textarea",function(){
    $("form").find("button").removeAttr("disabled");
})

$('#submit-msg-btn').on('click',function(){
    $(this).attr('disabled','disabled');
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $("#hiddenGRecaptchaResponse").remove();
        $('#forgotPassForm').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
        $("#forgotPassForm").submit();
    });
});


  // mouse-animation
  var rect = $('#scrollmouse')[0].getBoundingClientRect();
var mouse = {x: 0, y: 0, moved: false};

$("#scrollmouse").mousemove(function(e) {
  mouse.moved = true;
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
});
 
// Ticker event will be called on every frame
TweenLite.ticker.addEventListener('tick', function(){
  if (mouse.moved){    
    parallaxIt(".slide", -100);
    parallaxIt(".dotscroll", -50);
  }
  mouse.moved = false;
});

function parallaxIt(target, movement) {
  TweenMax.to(target, 0.3, {
    x: (mouse.x - rect.width / 2) / rect.width * movement,
    y: (mouse.y - rect.height / 2) / rect.height * movement
  });
}

$(window).on('resize scroll', function(){
  rect = $('#scrollmouse')[0].getBoundingClientRect();
});


 function trim(el) {
    el.value = el.value.
    replace(/(^\s*)|(\s*$)/gi, ""). // removes leading and trailing spaces
    replace(/[ ]{2,}/gi, " "). // replaces multiple spaces with one space 
    replace(/\n +/, "\n"); // Removes spaces after newlines
    return;
  }

</script>
@endsection()