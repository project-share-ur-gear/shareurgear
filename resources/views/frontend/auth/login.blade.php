@extends('layouts.app')

@section('content')
<style>

.common-section-top{background: url({{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->login_image}})no-repeat;}

</style>


<?php 


$title='meta_title_'.$locale;
$description='meta_desc_'.$locale;
$keywords='meta_keywords_'.$locale;
$url = 'user/authenticate';
if(!empty($_REQUEST['url'])){
    $url = 'user/authenticate?url='.$_REQUEST['url'];
}
?>
                    
<meta name="description" content="{{ $pageData->$description }}">
<meta name="keywords" content="{{ $pageData->$keywords }}">
<title>{{ config('app.name', $site_configs['site_name_'.$locale]) }} | {{ $pageData->$title }}</title>



<section class="common-section-top login-common-bg">
    <div class="container">
        <!-- <h2 class="heading">Contact Us</h2> -->
    </div>
</section>


<section class="login-page" id="scrollmouse">

    <div class="left-badges lb-common slide" style="background: rgb(138, 113, 178);"></div>
    <div class="right-badges lb-common slide" style="background:#fff36e;"></div>

    <div class="container">
        <div class="login-section">
            
            <h2 class="login-title"> {{$pageData->cms->log_in_title}} <!-- {{ __('general.login') }} --></h2>

                <form class="form-wrapper profile_form" method="POST" action="{{ url($url) }}" id="loginForm">
                @csrf
                <div class="contact-dots-panels">
                        <div class="dots dotscroll cd-dots1"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>
                        <div class="dots dotscroll cd-dots2"><img src="{{ FRONT_IMG.'/l-dots.png' }}" alt=""></div>
                    <div class="contact-box common-form">
                        <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="">Email</label>
                            <!-- {!! Form::label('email', __('general.login_email') ) !!} -->
                            {!! Form::text('email',old('email'),array('placeholder'=>__('general.login_email'),'class'=>'form-control required email', 'maxlength'=>'255' )) !!}
                            @if($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                            
                        <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                            <!-- {!! Form::label('password', __('general.login_password')) !!} -->
                            <label for="">Password</label>
                            {!! Form::password('password',array('placeholder'=>__('general.login_password'),'class'=>'form-control required', 'maxlength'=>'255','autocomplete'=>'new-password' )) !!}
                            @if($errors->has('password'))
                            <div class="error-message">{{ $errors->first('password') }}</div>
                            @endif
                        </div>
                            
                        <h6 class="copy-go-login"><a href="{{ url('forgot-password') }}">Forgot Password?</a></h6>
                        <button type="submit" name="submit-msg-btn" id="submit-msg-btn" class="btn  submit-btn">{{__('general.login_login')}}</button>
                        <p class="copy-go-login text-center">Looking for an Account? <a href="{{ url('signup') }}"> Sign Up </a></p>
                    </div>
                </div>
            </form>
            
        </div>
    </div>
</section>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>

<script src="https://www.google.com/recaptcha/api.js?render={{$site_configs['recaptcha_sitekey']}}"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $('#loginForm').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
    });
});
$(document).on("keyup","input, textarea",function(){
    $("form").find("button").removeAttr("disabled");
})

$('#submit-msg-btn').on('click',function(){
    $(this).attr('disabled','disabled');
    grecaptcha.execute('{{$site_configs['recaptcha_sitekey']}}', {action: 'contact'}).then(function(token) {
        $("#hiddenGRecaptchaResponse").remove();
        $('#loginForm').prepend('<input type="hidden" id="hiddenGRecaptchaResponse" name="g-recaptcha-response" value="' + token + '">');
        $("#loginForm").submit();
    });
});




  // mouse-animation
  var rect = $('#scrollmouse')[0].getBoundingClientRect();
var mouse = {x: 0, y: 0, moved: false};

$("#scrollmouse").mousemove(function(e) {
  mouse.moved = true;
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
});
 
// Ticker event will be called on every frame
TweenLite.ticker.addEventListener('tick', function(){
  if (mouse.moved){    
    parallaxIt(".slide", -100);
    parallaxIt(".dotscroll", -50);
  }
  mouse.moved = false;
});

function parallaxIt(target, movement) {
  TweenMax.to(target, 0.3, {
    x: (mouse.x - rect.width / 2) / rect.width * movement,
    y: (mouse.y - rect.height / 2) / rect.height * movement
  });
}

$(window).on('resize scroll', function(){
  rect = $('#scrollmouse')[0].getBoundingClientRect();
});


</script>

@endsection