<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/star-rating.css" rel="stylesheet" type="text/css">
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/theme.css" rel="stylesheet" type="text/css">

<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/star-rating.js"></script>
<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/theme.js"></script>

@if(!empty($product_data))
    @foreach($product_data as $key => $value)
        <div class="col-xl-6 col-sm-6 col-6 wish_length_box">
            <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$value->product_id }}">
                <div class="card-br">
                    <div class="media card-media">
                        <div class="media-img"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $value->primary_image }}" alt=""></div>
                        <div class="media-body">
                            <h2 class="title">{{$value->product_title}}</h2>
                            <div class="m-flex">
                                <p class="cat-data">{{$value->productcategorytitle}},{{$value->subcategorytitle}}</p>
                                <ul class="rating-star">
                                    <input type="number" id="viwrating" name="starRating" class="rating required" style="width: 5px;" min=0 max=5 step=1 data-size="sm" data-ltr="true" value="{{ bcdiv($value->pro_avg_rating,1,2) }}" readonly>
                                    <li><span href="#" class="star-span"> ({{ $value->pro_total_rating!=''?$value->pro_total_rating:'0' }})</span></li>
                                </ul>
                            </div>
                            <div class="media-data">
                                <div class="price-d">${{$value->price_per_day   }}<sub>/Day</sub></div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    @endforeach  
@endif            
@php 
    exit();
@endphp