@extends('layouts.app')


@section('content')

<style>

.common-section-top{background: url({{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->cnt_image}})no-repeat;}

</style>



<section class="common-section-top">
    <div class="container">
        <h2 class="heading">{{	$pageData->cms->cnt_heading}}</h2>
    </div>
</section>


<div class="over-all-home" id="scrollmouse">

    <section class="about-us contact-us">
        <div class="text-gear">Message</div>
        <div class="container">
            <div class="our-about">
                <div class="row"> 
                    <div class="col-xl-5">
                        <div class="FG-data-featured"><h2 class="title"> {!! $pageData->cms->contact_us_first_title !!}</h2></div>
                        <p class="description d-xl-none d-block">{{$pageData->cms->cnt_description}} </p>

                        <div class="contact-orders">
                            <div class="contact-data-detail">
                                <div class="cd-details">
                                  <a href="tel:{{$pageData->cms->cnt_whatsapp_no}} " class="ns-cd">{{$pageData->cms->cnt_whatsapp_no}} </a><br>
                                  <a href="mailto:{{$pageData->cms->cnt_email}}" class="ns-cd">{{$pageData->cms->cnt_email}}</a>
                                </div>
                                <div class="cd-details">
                                  <h6 class="title-ad">Address</h6>
                                  <a href="#" class="ns-cd">{{$pageData->cms->cnt_address}}  </a>
                                </div>
                            </div>
                            <div class="contact-data-img">
                                <div class="A-about-right">
                                    <img src="{{ FRONT_IMG.'/c-img.jpg' }}">
                                
                                    <div class="bottom-rect">
                                        <div class="B-rect1 dotscroll"></div>
                                        <div class="B-rect dotscroll"></div>
                                        <div class="rect"></div>
                                        <div class="rect rect1"></div>
                                        <div class="rect"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               
                    <div class="col-xl-7">
                    <form class="contact_form" id="contact_form" action="{{url('contact-us/store') }}" method="post">
                     
                        <p class="description d-xl-block d-none">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
                          <div class="contact-dots-panels">
                              <div class="dots dotscroll cd-dots1"><img src="{{ FRONT_IMG.'/dot1.png' }}" alt=""></div>
                              <div class="dots dotscroll cd-dots2"><img src="{{ FRONT_IMG.'/dot1.png' }}" alt=""></div> 
                            <div class="contact-box common-form">
                                <div class="form-group">
									<label>Name</label>
									<input type="text" class="form-control required" name="user_name" id="user_name" required >
                                </div>
                                <div class="form-group">
                                	<label>Email</label>
                                	<input type="email" class="form-control required" name="user_email" id="user_email" required></div>
                                <div class="form-group"><label>Phone Number</label><input type="text" class="form-control required" name="phone_number" id="phone_number"  required></div>
                                <div class="form-group"><label>Message</label><textarea class="form-control required" rows="5" type="text" name="user_message" id="user_message" required></textarea></div>
                                <!-- <div class="btn submit-btn">Send  A Message</div> -->
                                <!-- <button class="btn  submit-btn my-3 save_data_btn"  type="button">Save</button>  -->
                                <button class="btn  submit-btn my-3 save_data_btn" data-inprocess="Saving..." data-default="Save" type="button">Save</button> 
                            </div>
                          </div> 
                    </form>
                      </div>
               

                </div>
            </div>
            
        </div>


    </section>
    
    <div class="map">
      <div id="googleMap" class="google_map" style="width:100%;height:100%;"></div>
    </div>

</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&callback=initMap&libraries=&v=weekly" async></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script> -->


<script>
var var_lat={{$pageData->cms->cnt_address_lat}};
var var_long={{$pageData->cms->cnt_address_long}};
    $(document).ready(function(e) {
        if ($(document).width() > 1024) {
            AOS.init({ once: true });
        } else {
            $('[data-aos]').removeAttr('data-aos');
        }
    });

// mouse-animation
var rect = $('#scrollmouse')[0].getBoundingClientRect();
var mouse = {x: 0, y: 0, moved: false};

$("#scrollmouse").mousemove(function(e) {
  mouse.moved = true;
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
});
 
// Ticker event will be called on every frame
TweenLite.ticker.addEventListener('tick', function(){
  if (mouse.moved){    
    parallaxIt(".slide", -100);
    parallaxIt(".dotscroll", -50);
  }
  mouse.moved = false;
});

function parallaxIt(target, movement) {
  TweenMax.to(target, 0.3, {
    x: (mouse.x - rect.width / 2) / rect.width * movement,
    y: (mouse.y - rect.height / 2) / rect.height * movement
  });
}

$(window).on('resize scroll', function(){
  rect = $('#scrollmouse')[0].getBoundingClientRect();
});

// google map
let map;

function initMap() {
  map = new google.maps.Map(document.getElementById("googleMap"), {
    center: { lat: var_lat, lng:var_long },
    zoom: 8,
  });
}



        $('.save_data_btn').click(function(event) {
               /* Act on the event */
                if($('#contact_form').valid()){
                    freezeButton('.contact_form',$('.contact_form').data('inprocess'),'disabled');
                    $('#contact_form').submit();
                }else{
                    var ele  = "";
                    $('.help-block').each(function(index, el) {
                        if($(el).is(':visible')  && ele==""){
                            ele = el;
                        }   
                    });
                    $('html, body').animate({
                            scrollTop: $(ele).offset().top-150
                    }, 500);
                }
        });
</script>
@endsection