

@extends('layouts.app')

@section('content')

<style>

   .common-section-top{background: url({{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->section_faq_image}})no-repeat;}

</style>

 

    
<section class="common-section-top">
    <div class="container">
        <h2 class="heading">{{$pageData->cms->section_one_faq_title}}</h2>
    </div>
</section>


<div class="over-all-home">

<!-- FAQ-section  -->
<section class="faq-home">
    <div class="container">
        <div class="faq-section row">
            <div class="col-lg-5">
                <div class="left-faqImg d-lg-block d-none">
                
                <img src="{{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->faq_image_first}}" class="" width="">
  
                
                
                </div>
            </div>
            <div class="col-lg-7">
                <div class="about-contain">
                
                    @if(!empty($allFaqs))
                    <div class="panel-group" id="accordion">
                        @php $i=1; @endphp
                        @foreach($allFaqs as $key=>$value) 
                        <!--  -->
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingFaqOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse"  href=#collapseFaqOne{{$i}} aria-expanded="false" aria-controls="collapseFaqOne">
                                    {!! $value->question_en !!} </a>
                                </h4>
                            </div>
                            <div id="collapseFaqOne{{$i}}" class="panel-collapse collapse {{ $i==1?"in":""}}" role="tabpanel" data-parent="#accordion" aria-labelledby="headingFaqOne">
                                <div class="panel-body">
                                    <div class="description">
                                        {!! $value->answer_en !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        @php $i++; @endphp
                        @endforeach

                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>


</div>
 

<script type="text/javascript">
    $(function(){
        $("#accordion").find('.panel:first').find('.panel-collapse').addClass("show");
        $("#accordion").find('.panel:first').find("a").attr("aria-expanded","true");
        $("#accordion").find('.panel:first').addClass("panel-bg");
    })

    $("#accordion").on('shown.bs.collapse', function (e) {
        var id = $(e.target).attr('id');
        $("#"+id).parent().addClass("panel-bg");
    })

    $("#accordion").on('hidden.bs.collapse', function (e) {
        var id = $(e.target).attr('id');
        $("#"+id).parent().removeClass("panel-bg");
    })
    $(document).ready(function(){

        $('.about-contain').find('a:first').removeClass('collapsed');

    });

</script>

@endsection()