@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->

        <h2 class="login-title">Account Settings</h2>
        <div class="contact-dots-panels pd-panels">
            <div class="contact-box common-form">
                <!-- user-profile -->
                <div class="user-profile">
                    <div class="profile-user-img">
                        <img src="{{ FRONT_IMG.'/user-img.jpg' }}" alt="">
                        <div class="user-edit"><img src="{{ FRONT_IMG.'/draw.png' }}"></div>
                    </div>
                </div>
                <!--  -->


                <div class="form-group">
                    <label for="">Name</label>
                    <input type="text" class="form-control" value="Mos Sukjaroenkraisri">
                </div>
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" value="mos87sukjaroenkraisri@gmail.com" >
                </div>
                <div class="form-group">
                    <label for="">Phone Number</label>
                    <input type="text" class="form-control" value="+01 12345 67890">
                </div>
                <div class="form-group">
                    <label for="">Address</label>
                    <input type="text" class="form-control" value="99 Boonah Qld, CRESSBROOK, Queensland, 4313">
                </div>
                
                <div class="check-boxs">
                    <div class="checks custom-checks">
                        <label class="checkbox"> Change Password 
                            <input type="checkbox" name="" value="">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>

                <div class="change-password-box">
                    <h2 class="ch-title">Change Password</h2>
                    <div class="form-group">
                        <label for="">Current Password</label>
                        <input type="password" class="form-control" >
                    </div>
                    <div class="form-group">
                        <label for="">New Password</label>
                        <input type="password" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Confirm New Password</label>
                        <input type="password" class="form-control">
                    </div>
                </div>

                <div class="btn  submit-btn my-3">save</div>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 


@endsection()