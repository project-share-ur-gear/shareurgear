    @if(!empty($question_Data))
        @foreach($question_Data as $key => $value)
            <div class="ques-faqs">
                <div class="ques-header" id="heading{{$value->question_id}}">
                    <div class="ques-media">
                        <div class="media media-ques">
                            @php $profileImage=FRONT_IMG.'/nophoto.png';  @endphp
                            @if(!empty($value->profile_image))
                                    @php $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value->profile_image); @endphp
                            @endif
                            <img src="{{ $profileImage }}" alt="" class="user-imgs">
                            <div class="media-body">
                                <h2 class="user-name">{{$value->name}}</h2>
                                @php  $test=$value->question_date @endphp
                                <p class="time">Asked on :  {{$test}} </p>
                                
                            </div>
                        </div>
                        <div class="plus-faq collapsed" data-toggle="collapse" data-target="#collapse{{$value->question_id}}" aria-expanded="true" aria-controls="collapse{{$value->question_id}}">
                            <div class="circles"></div>
                        </div>
                    </div>
                    <h2 class="Q-title">Q.) {{$value->question_answer}}</h2>
                </div>
                <div id="collapse{{$value->question_id}}" class="collapse" aria-labelledby="heading{{$value->question_id}}" data-parent="#accordion">
                    <div class="ques-ans "style="word-break: break-all;" >
                        A.) {{$value->answered_by_owner}}
                    </div>
                </div>
            </div>

        @endforeach
    @endif
   


@php 
    exit();
@endphp
                               