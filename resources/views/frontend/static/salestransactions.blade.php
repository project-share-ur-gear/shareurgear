@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">My Bookings</h2>
        <div class="dashboard-sales">
          <div class="row">
            <div class="col-xl-7 col-lg-6">
              <div class="booking-dash">
                <h2 class="D-title">Bookings</h2>
                <div class="D-book-box">
                  <div class="data-book">Last Week <span> 55</span> </div>
                  <div class="data-book">Last Month <span> 125 </span> </div>
                  <div class="data-book">Total <span> 180</span> </div>
                </div>
              </div>
            </div>
            <div class="col-xl-5 col-lg-6">
              <div class="booking-dash">
                <h2 class="D-title">Earning</h2>
                <div class="D-book-box">
                  <div class="data-book">Last Week <span> $122</span> </div>
                  <div class="data-book">Last Month <span> $180 </span> </div>
                  <div class="data-book">Total <span> $302</span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="text-right">
            <div class="filter-search">
              <h2 class="Filter-title">Filters</h2>
              <div class="filter-flex">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="Booking Date" aria-label="Booking Date " aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <span class="input-group-text" id="basic-addon2"><img src="{{ FRONT_IMG.'/calendar.png' }}" alt=""></span>
                  </div>
                </div>
                <div class="all-data">
                    <select class="form-control selectpicker">
                        <option>Booking Status </option>
                        <option>Booking Status </option>
                        <option>Booking Status </option>
                        <option>Booking Status </option>
                    </select>
                </div>
                <div class="search-btn btn">Search</div>
                <div class="reset">Reset</div>
              </div>
            </div>
        </div>

        <div class="manage-product-table booking-page">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col"></th>
                            <th scope="col">Booked By</th>
                            <th scope="col">Date Booked </th>
                            <th scope="col">Booking Start Date </th>
                            <th scope="col">Booking End Date </th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i<4; $i++){?>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>
                                <div class="media media-object">
                                    <img class="media-img" src="{{ FRONT_IMG.'/user-img.jpg' }}" alt="">
                                    <div class="review-user">
                                      <div class="user-name">Nick Jones</div>
                                      <div class="read-R" data-toggle="modal" data-target="#readreviewModalCenter">Read Review</div>
                                    </div>
                                </div>
                            </td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>$xxx</td>
                        </tr>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>
                                <div class="media media-object">
                                    <img class="media-img" src="{{ FRONT_IMG.'/user-img.jpg' }}" alt="">
                                    <div class="user-name">Nick Jones</div>
                                </div>
                            </td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>$xxx <span class="cancelled-booked" data-toggle="modal" data-target="#bookingModalCenter">Booking Cancelled</span></td>
                        </tr>
                        <? } ?>
                        <tr class="border-0">
                            <td colspan="7">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link active" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    </ul>
                                </nav>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 


<!--Read Review Modal -->
<div class="modal fade " id="readreviewModalCenter" tabindex="-1" role="dialog" aria-labelledby="readreviewModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="readreviewModalLongTitle">Read Your Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <ul class="rating-star">
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li>
            </ul>
            <div class="review-desc">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
           <div class="text-center"> <div class="btn submit-btn">Submit</div></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Booking Cancelled Modal -->
<div class="modal fade " id="bookingModalCenter" tabindex="-1" role="dialog" aria-labelledby="bookingModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="bookingModalLongTitle">Reason of Cancellation </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <div class="review-desc">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection()