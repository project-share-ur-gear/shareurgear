<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/star-rating.css" rel="stylesheet" type="text/css">
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/theme.css" rel="stylesheet" type="text/css">

<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/star-rating.js"></script>
<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/theme.js"></script>

<style>
/* -- rating */
.rating-container .caption { display:none !important; }
.rating-container .clear-rating { display:none !important; }
.rating-container .rating-stars:focus { outline: unset !important; }
.theme-krajee-svg .empty-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star2.svg" }}') !important; }
.theme-krajee-svg .filled-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star.svg" }}') !important; }
</style>

@if(!empty($product_Data))
        @foreach($product_Data as $key => $value)
            <?php // for($i=0; $i<15; $i++){?>
            <div class="col-xl-6 col-lg-12 col-sm-6 col-6">
                <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$value->product_id }}">
                    <div class="card-br chat_length_box">
                        <div class="media card-media">
                        <div class="media-img"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $value->primary_image }}" alt=""></div>
                        <div class="media-body">
                            <h2 class="title">@if(!empty($value->product_title)){{$value->product_title}} @endif</h2>
                            <div class="m-flex">
                                <p class="cat-data">@if(!empty($value->productcategorytitle)){{ $value->productcategorytitle}}@endif, @if(!empty($value->subcategorytitle)){{$value->subcategorytitle}}@endif</p>
                                <ul class="rating-star">
                                    <input type="number" id="viwrating" name="starRating" class="rating required" style="width: 5px;" min=0 max=5 step=1 data-size="sm" data-ltr="true" value="{{ bcdiv($value->pro_avg_rating,1,2) }}" readonly>
                                    <li><span href="#" class="star-span"> ({{ $value->pro_total_rating!=''?$value->pro_total_rating:'0' }})</span></li>
                                </ul>
                            </div>
                            <div class="m-flex">
                            <p class="cat-data">@if(!empty($value->location)){{ $value->location}}@endif</p>
                            </div>
                            <div class="media-data">
                                <div class="media">
                                        @php
                                            $profileImage=FRONT_IMG.'/nophoto.png';
                                            if($value->profile_image!='')
                                            $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value->profile_image);
                                        @endphp
                                    <img class="media-imgs" src="{{ $profileImage }}" alt="">
                                    <div class="media-body">
                                        <p class="m-title">@if(!empty($value->name)){{$value->name}}@endif</p>
                                    </div>
                                </div>
                                <div class="price-d">@if(!empty($value->price_per_day))${{$value->price_per_day}}@endif<sub>/Day</sub></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </a>
            </div>
            <? // } ?>
        @endforeach 
@endif


<script>
    jQuery(document).ready(function($) {
        if(totalRecords==$('.chat_length_box').length){
            $('#loadmore_icon').hide();
        }
    });
</script>
@php 
    exit();
@endphp