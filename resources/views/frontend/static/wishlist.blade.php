@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">My Wish List</h2>
        <div class="wish-list-page">
            <div class="browser-content">
                <div class="row">
                    <?php for($i=0; $i<8; $i++){?>
                    <div class="col-xl-6 col-sm-6 col-6">
                        <a href="#">
                            <div class="card-br">
                                <div class="media card-media">
                                    <div class="media-img"><img src="{{ FRONT_IMG.'/f-img.jpg' }}" alt=""></div>
                                    <div class="media-body">
                                        <h2 class="title">Product Title</h2>
                                        <div class="m-flex">
                                            <p class="cat-data">Category, Sub-Category</p>
                                            <ul class="rating-star">
                                                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li>
                                                <li><span href="#" class="star-span"> (5)</span></li>
                                            </ul>
                                        </div>
                                        <div class="media-data">
                                            <div class="media">
                                                <img class="media-imgs" src="{{ FRONT_IMG.'/user-img.jpg' }}" alt="">
                                                <div class="media-body">
                                                    <p class="m-title">Name</p>
                                                </div>
                                            </div>
                                            <div class="price-d">$175<sub>/Day</sub></div>
                                        </div>
                                    </div>
                                    <div class="remove-btn"><span>Remove</span></div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <? } ?>
                </div>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 


@endsection()