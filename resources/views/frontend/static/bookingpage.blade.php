@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">My Bookings</h2>
        <div class="manage-product-table booking-page">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col"></th>
                            <th scope="col">Date Booked </th>
                            <th scope="col">Booking Start Date </th>
                            <th scope="col">Booking End Date </th>
                            <th scope="col">Price</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i<4; $i++){?>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>$xxx</td>
                            <td>
                                <div class="A-flex">
                                    <div class="mq-data">
                                        <a href="#" class="manage-avial" data-toggle="modal" data-target="#leaveModalCenter">Leave Review</a>
                                        <span class="questions" data-toggle="modal" data-target="#cancelModalCenter">Cancel Booking </span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img2.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>DD-MM-YYYY</td>
                            <td>$xxx</td>
                            <td>
                                <div class="read-review-table">
                                    <a href="#" class="read-review" data-toggle="modal" data-target="#readreviewModalCenter">Read Your Review</a>
                                </div>
                            </td>
                        </tr>
                        <? } ?>
                        <tr class="border-0">
                            <td colspan="7">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link active" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    </ul>
                                </nav>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 
<!--leave Modal -->
<div class="modal fade " id="leaveModalCenter" tabindex="-1" role="dialog" aria-labelledby="leaveModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="leaveModalLongTitle">Write Your Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <ul class="rating-star">
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li>
            </ul>
            <textarea class="form-control" rows="7" placeholder="Write your review here...."></textarea>
           <div class="text-center"> <div class="btn submit-btn">Submit</div></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!--Read Review Modal -->
<div class="modal fade " id="readreviewModalCenter" tabindex="-1" role="dialog" aria-labelledby="readreviewModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="readreviewModalLongTitle">Read Your Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <ul class="rating-star">
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li>
            </ul>
            <div class="review-desc">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
           <div class="text-center"> <div class="btn submit-btn">Submit</div></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Cancel-Booking -->
<div class="modal fade " id="cancelModalCenter" tabindex="-1" role="dialog" aria-labelledby="cancelModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cancelModalLongTitle">Cancel Booking </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <textarea class="form-control" rows="6" placeholder="Please write the reason for cancellation of the booking...."></textarea>
           <div class="text-center"> <div class="btn submit-btn">Submit</div></div>
        </div>
      </div>
    </div>
  </div>
</div>



@endsection()