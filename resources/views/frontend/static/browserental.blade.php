

<link href="{{ asset('public/plugins/customscroolbar/jquery.mCustomScrollbar.min.css') }}" rel="stylesheet" type="text/css">

@extends('layouts.app')

@section('content')
@php
 use Illuminate\Support\Facades\Crypt;
@endphp


<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/star-rating.css" rel="stylesheet" type="text/css">
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/theme.css" rel="stylesheet" type="text/css">

<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/star-rating.js"></script>
<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/theme.js"></script>

<style>
footer{ display:none; }
body { overflow :hidden}
	#marker-tooltip {
    display: none;
    position:absolute;    margin: 15px;
}

/* -- rating */
.rating-container .caption { display:none !important; }
.rating-container .clear-rating { display:none !important; }
.rating-container .rating-stars:focus { outline: unset !important; }
.theme-krajee-svg .empty-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star2.svg" }}') !important; }
.theme-krajee-svg .filled-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star.svg" }}') !important; }

</style>

<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>

 
        <div class="filter-data-mobile d-lg-none d-flex">
            <div class="filter"><i class="fas fa-funnel-dollar"></i></div>

            <div class="filter-top-search">
                <span class="closebtn-filter">x</span>
                <div class="resert-f reset_button"><span><i class="fas fa-funnel-dollar"></i> Reset Filter</span></div>
                @include('mobile-search')
            </div>


            <ul class="nav nav-tabs " id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fas fa-stream"></i></a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false"><i class="fas fa-map-marked-alt"></i></a>
            </li>
            </ul>
        </div>

        <!-- Tab panes -->
    
        <section class="browser-rental-page">
        <div class="browser-data-flx tab-content">
            <!-- left data -->
            <div class="tab-pane  browser-data-items show" id="home" role="tabpanel" aria-labelledby="home-tab">
                <div class="">
                <!-- Begin::Filter -->
                <div class="filter-search d-lg-block d-none">
                    @include('filter-search')
                </div>
                <!-- Close::Filter -->
                    <div class="browser-content">
                    <!-- data-scrollbar -->
                        <div class="scroll-bar-data search-business-location" >
                            <div class="append-data-height" >
                                <div class="form-row append_data" >
                                    
                                @if(empty($product_Data))
                                    <div class="not-found-box">
                                        <div class="not-found-img"> <img src="{{FRONT_IMG}}/not-found.png"> </div>
                                        <p class="not-found-txt"> No Product Found in Selected criteria </p>
                                    </div>
                                @endif
                                </div>
                                <div class="loader " id="loadmore_icon"  style="display:none" ><img src="{{ FRONT_IMG.'/Gear-0.3s-98px.gif' }}" alt=""></div>
                            </div>                            

                        </div>
                    </div>
                </div>
            </div>
            <!-- right- Map -->
            <div class="tab-pane google-map active "  id="profile" role="tabpanel" aria-labelledby="profile-tab" style="position:relative;">
                
                <div id="googleMap" class="google_map" style="width:100%;height:100%;"></div>
                  <div id="marker-tooltip" class="basic-info-forms"></div>
                
            </div>
            <!-- close -->
        </div>
        </section>
                            

</div>


<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OverlappingMarkerSpiderfier/1.0.3/oms.min.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script> -->
<script src="{{ asset('public/js/smooth-scrollbar.js') }}"></script>


<script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&libraries=places"></script>


<script src="{{ asset('public/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>



<script>
    var tot;
    $(document).ready(function(){
         $(".custom-select").select2();
        var mainHeight = $('body').innerHeight();
        var hearder=$('.common-section-top').outerHeight();
		hearder=hearder+30;
	    // var fullheight  = $('.browser-data-items').innerHeight();
        var mainhight1=mainHeight-hearder;
        var subheader  = $('.filter-search').outerHeight();
        var subheader1  = $('.filter-data-mobile').outerHeight();
        var fullsubheader=subheader+subheader1;
        tot = mainhight1-fullsubheader-20;

    
        $('.scroll-bar-data').css('height',tot+'px');
        $('.scroll-bar-data').mCustomScrollbar({axis:"Y",  theme:"dark-3", mouseWheelPixels: 150 });
        // $('.scroll-bar-data').slimscroll({
        //     height		: tot,
        //     railColor 	: 'red',
        //     color        : '#ff8c00',
        //     opacity      : 1,        //600px
        //     alwaysVisible: false,
        // });
        $('.filter').on('click', function() {
            $('.filter-top-search').toggle(function() {
                $(this).animate({             
                }, 500);
            });
            $('body').toggleClass('navOpen');
        }); 
        $('.closebtn-filter').on('click', function() {
            $('.filter-top-search').toggle(function() {
                $(this).animate({              
                }, 500);
            });
            $('body').removeClass('navOpen');
        });

        $(document).on("change","#sub_cat",function(){
            var pro_cat  =$('#category').val();
            $('#pro_category').val(pro_cat);
            $('#pro_category1').val(pro_cat);
            $("form").submit();

        });
        $(document).on("change","#sub_cat1",function(){
            var pro_cat  =$("#category").val();
            $('#pro_category').val(pro_cat);
            $('#pro_category1').val(pro_cat);
            $("#search_product1").submit();

        });

        



        $(document).on("change","#price_details",function(){
            $("form").submit();
        });
        $(document).on("change","#price_details1",function(){
            $("#search_product1").submit();
        });
        
        // $(document).on("change","#location",function(){
           
        // });
        $("form").bind("location", function(e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
    });



</script>
<script>
$(".reset_button").click(function(){
    window.location.href =APPLICATION_URL+'/browse-rental';
});

var totalRecords;
var loc_lat ="{{$loc_lat}}";
var loc_long ="{{$loc_long}}";
var centerLat ="{{$center_lat}}";
var centerLng ="{{$center_lng}}";
var urlZoom = "{{$zoom}}";
var autocomplete;

   jQuery(document).ready(function($){


        @if(old('category') !='' || !empty($category))    
            setTimeout(function(e){
              $('#category').change(); 
            },200);
        @endif

        var input = document.getElementById('location');
        google.maps.event.addDomListener(input, 'keydown', function(event) { 
            if (event.keyCode === 13) { 
                event.preventDefault(); 
            }
        }); 



         autocomplete = new google.maps.places.Autocomplete((document.getElementById('location')), {
            types: ['geocode'],
        });

        autocomplete2 = new google.maps.places.Autocomplete((document.getElementById('location1')), {
                types: ['geocode'],
        });
        
    
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
          pointOnLocation(autocomplete);
        });
        google.maps.event.addListener(autocomplete2, 'place_changed', function () {
        pointOnLocation1(autocomplete2);
        });
    });


<?php if (!empty($product_Data)) { ?>
    var map_default_lat='40.741895';
    var map_default_long='-73.989308';

	var data = [
		<?php foreach ($latlngdata as $key => $val) { ?>
			[<?= $val ?>],
		<?php 
} ?> 
	];

    var locations = [];
	for (i = 0; i < data.length; i++) {
    	locations[i] = {lat: parseFloat(data[i][0]),lng:parseFloat(data[i][1]),p_id:data[i][2]}
    }


    var infowindow = new google.maps.InfoWindow({disableAutoPan:true});
    var bounds = new google.maps.LatLngBounds(); 
    var activeInfoWindow; 
    
    var zoomlevel= urlZoom!=""?parseInt(urlZoom):parseInt(4);
    if(loc_lat!="" && loc_long!=""){

        map_default_lat = loc_lat;
        map_default_long = loc_long;
    }else{
        if(centerLat!="" && centerLng!=""){
            //zoomlevel = parseInt(urlZoom);
            map_default_lat = centerLat;
            map_default_long = centerLng;    
        }
    } 
   
    var map = new google.maps.Map(document.getElementById('googleMap'), {
      //zoom: zoomlevel,
      //disableDefaultUI: true,
      gestureHandling: "greedy",
      minZoom:4,
      zoom:zoomlevel,
      maxZoom: 15,
      zoomControl: true,
      mapTypeControl: false,
      fullscreenControl: false,
      
      streetViewControl: false,
 
      center: new google.maps.LatLng(map_default_lat, map_default_long),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles:	[
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels",
                "stylers": [
                {
                    "visibility": "on"
                }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "poi.business",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "road",
                "stylers": [
                {
                    "visibility": "simplified"
                }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                {
                    "visibility": "off"
                }
                ]
            }
        ]
    });
    var oms = new OverlappingMarkerSpiderfier(map, { 
        markersWontMove: false,  
        markersWontHide: true,   
        basicFormatEvents: true,
        keepSpiderfied : true, 
        circleSpiralSwitchover: 5
    });
    
    var mcOptions = {
        ignoreHidden: true,            	
        gridSize: 30,
        maxZoom: 14,
        cssClass:'cluster-custum-img',
        styles: [ 
            {	

                url: "{{FRONT_IMG.'/cluster_green.png' }}?r="+Math.random(),
                width: 40,
                height:40,
                fontFamily:"comic sans ms",
                textSize:20,
                textColor:"black",
            }
        ] 
    };


    var label = [
		<?php foreach ($product_Data as $key => $val) { ?>
            @php $profileImage=FRONT_IMG.'/nophoto.png';  @endphp
                @if($val->profile_image!='')
                   @php $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$val->profile_image); @endphp
                @endif
                 ['<div class="map-box"><div class="card-br" ><div class="media card-media" style="display:block;" ><div class="media-img" style="width:100%"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $val->primary_image }}" alt=""></div><div class="media-body"><a href="{{ SITE_HTTP_URL }}/rental-detail-page/{{ $val->product_id }}"><h2 class="title"> @if(!empty($val->product_title)){{$val->product_title}} @endif</h2></a><div class="m-flex"><p class="cat-data">@if(!empty($val->productcategorytitle)){{ $val->productcategorytitle}}@endif, @if(!empty($val->subcategorytitle)){{$val->subcategorytitle}}@endif</p><ul class="rating-star"><li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star3.svg' }}"></span></li><li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star3.svg' }}"></span></li><li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star3.svg' }}"></span></li><li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star3.svg' }}"></span></li><li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li><li><span href="#" class="star-span"> (5)</span></li></ul></div><div class="media-data"><div class="media"><img class="media-imgs" src="{{ $profileImage }}" alt=""><div class="media-body"><p class="m-title">@if(!empty($val->name)){{$val->name}}@endif</p></div></div><div class="price-d">@if(!empty($val->price_per_day))${{$val->price_per_day}}@endif<sub>/Day</sub></div></div></div></div></div></div>'],

        <?php 
    } ?> 
    ];

    
      // overlays marker add
      var labels = label;
    var markers = [];
 

    var localLatLng = [];
	locations.forEach(function(location,i) {
        
            if(jQuery.inArray(location.lat+','+location.lng, localLatLng) !== -1){
            location.lat = location.lat + (Math.random() -.5) / 1500;// * (Math.random() * (max - min) + min);
            location.lng = location.lng + (Math.random() -.5) / 1500;// * (Math.random() * (max - min) + min);
        }
     var marker = new google.maps.Marker({
            position: location,
            icon: '{{ FRONT_IMG.'/cluster_xxxx.png' }}',
            // url: "{{ FRONT_IMG.'/cluster_xxxx.png' }}?r="+Math.random(),
        });	
      	
        var flag = 0;
        google.maps.event.addListener(marker, 'mouseover', (function(marker,i ){ 
             return function() {

                // infowindow.setContent(labels[i][0]);
                 //infowindow.open(map, marker);
				    var mapPos = $("#googleMap").offset();
                    var mapW = $("#googleMap").width();
                    var mapH = $("#googleMap").height();
                    var hH = 335;
                    var hW = 332;
			var s=document.getElementsByClassName('card-section')[0];		
		 $('#marker-tooltip').html(labels[i][0]);
		
		if($(document.getElementById('marker-tooltip')).width()>50){
			 hW = $(document.getElementById('marker-tooltip')).width();
		}
		if($(document.getElementById('marker-tooltip')).height()>50){
			 hH = $(document.getElementById('marker-tooltip')).height();
		}
                    
		
					var point = fromLatLngToPoint(marker.getPosition(), map);
		
		
					if(point.x+5<hW){
							if(point.y+5<hH){								
								 $('#marker-tooltip').css({ left: (point.x-10), right: 'auto',top:point.y-20,bottom:'unset'});
							}else{								
								$('#marker-tooltip').css({ left: (point.x-10), right: 'auto',bottom:mapH-point.y-10,top:'unset'});
							}
						 
					}else{
						 
						if(point.y+5<hH){							
								$('#marker-tooltip').css({ left: 'auto', right: (mapW-point.x-10),top:point.y-20,bottom:'unset'});
							}else{								
								$('#marker-tooltip').css({ left: 'auto', right: (mapW-point.x-10),bottom:mapH-point.y-10,top:'unset'});
							}
					}
                    $('#marker-tooltip').html(labels[i][0]).show();
				 
				 
             }
           
        })(marker, i));


           google.maps.event.addListener(marker, 'click', (function(marker,i ){ 
          
             return function() {
                
                var mapPos = $("#googleMap").offset();
                    var mapW = $("#googleMap").width();
                    var mapH = $("#googleMap").height();
                    var hH = 335;
                    var hW = 332;
		
			var s=document.getElementsByClassName('card-section')[0];		
		 $('#marker-tooltip').html(labels[i][0]);
		
		if($(document.getElementById('marker-tooltip')).width()>50){
			 hW = $(document.getElementById('marker-tooltip')).width();
		}
		if($(document.getElementById('marker-tooltip')).height()>50){
			 hH = $(document.getElementById('marker-tooltip')).height();
		}
                    
		
					var point = fromLatLngToPoint(marker.getPosition(), map);
		
		
					if(point.x+5<hW){
							if(point.y+5<hH){								
								 $('#marker-tooltip').css({ left: (point.x-10), right: 'auto',top:point.y-20,bottom:'unset'});
							}else{								
								$('#marker-tooltip').css({ left: (point.x-10), right: 'auto',bottom:mapH-point.y-10,top:'unset'});
							}
						 
					}else{
						 
						if(point.y+5<hH){							
								$('#marker-tooltip').css({ left: 'auto', right: (mapW-point.x-10),top:point.y-20,bottom:'unset'});
							}else{								
								$('#marker-tooltip').css({ left: 'auto', right: (mapW-point.x-10),bottom:mapH-point.y-10,top:'unset'});
							}
                    }
                    if ($(window).width() < 1024) {
                        $('#marker-tooltip').html(labels[i][0]).show();
                    }else{
                        
                      
                        window.open(APPLICATION_URL+'/rental-detail-page/'+location.p_id+'');
                        
                    }



                    
             }
           
        })(marker, i));


        
        google.maps.event.addListener(marker, 'mouseout', function () {
            // infowindow.close();
        if ($(window).width() < 1024) {
            //mobile, do nothing
        }else{
                $('#marker-tooltip').hide();
        }
        });

        map.addListener("click", (e) => {
            $('#marker-tooltip').hide();	
        });
        oms.addMarker(marker);
        markers.push(marker);
        localLatLng.push(location.lat+','+location.lng);

    });
    // end overlays marker


   markerCluster = new MarkerClusterer(map, markers, mcOptions);
    if (typeof google === 'object' && typeof google.maps === 'object') {
        google.maps.event.addListener(map, 'idle', function(e) {
    	    var latlngbounds = map.getBounds();
            var status2='complete';
            var maxX = latlngbounds.getNorthEast().lng();
            var maxY = latlngbounds.getNorthEast().lat();
            var minX = latlngbounds.getSouthWest().lng();
            var minY = latlngbounds.getSouthWest().lat();
            min_lng=minX;max_lng=maxX;min_lat=minY;max_lat=maxY;
            var lnk = window.location.href;
            
    
            var center_lat =  map.getCenter().lat();
            var center_lng =  map.getCenter().lng();
               
            var getZoom =map.getZoom();
            
            var currentUrl = lnk;
            var url = new URL(currentUrl);
            if(loc_lat==""){
                url.searchParams.set("location","");  
                url.searchParams.set("loc_lat", "");
                url.searchParams.set("loc_long","");   
                url.searchParams.set("center_lat",center_lat);   
                url.searchParams.set("center_lng",center_lng);   
                url.searchParams.set("zoom",getZoom);   
                
                $('input[name="center_lat"]').val(center_lat);
                
                $('input[name="center_lng"]').val(center_lng);
                
                $('input[name="zoom_mp"]').val(getZoom);
            }else{

                 var locationnn = $('input[name="location"]').val();
                 if(locationnn=='')
                 {
                    
                    $('input[name="location"]').val('');                    
                 }
            } 
           
            var newUrl = url.href; 
            window.history.pushState({path:newUrl},'',newUrl);
            var params = lnk.split('?')[1];
            //   ajax  open
                    $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                    });
                    $.ajax({
                    type: 'POST',
                    url  : APPLICATION_URL+'/getbounddata?'+params,
                    data:{
                            min_lng:min_lng,
                            max_lng:max_lng,
                            min_lat:min_lat,
                            max_lat:max_lat,
                    },
                    success: function(response) {
                            $(".scroll-bar-data").mCustomScrollbar('destroy');
                            $(".append_data").html(response);
                        $('.scroll-bar-data').mCustomScrollbar({axis:"Y",  theme:"dark-3", mouseWheelPixels: 170,
                            callbacks:{
                                onTotalScroll:function(){
                                
                                    var totalRecordsLoaded = $('.chat_length_box').length;
                                    if(totalRecords==totalRecordsLoaded){
                                        $('#loadmore_icon').hide();
                                        return;
                                    }  
                                    if(totalRecords > totalRecordsLoaded){
                                        $('#loadmore_icon').show();
                                        loadMoreproduct();
                                    }
                                    
                                }
    					    }

    			        });		

                    }
                });

            //close ajax
        });
    }
<?php 
} else { ?>


	var map;
	var sch_latitude = '40.785091';
	var sch_longitude = '-73.968285';
	var myCenter = new google.maps.LatLng(sch_latitude, sch_longitude);

	function initialize() {
		var mapProp = {
			center: myCenter,
			zoom: 10,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false,
			styles: {
				saturation: 1
			}
		};
		var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
		var positionLoc = new google.maps.LatLng(sch_latitude, sch_longitude);
		// var position = new google.maps.Marker({
		// 	position: positionLoc,
		
		// });
		position.setMap(map);
	}
    google.maps.event.addDomListener(window, 'load', initialize);
    

    
    <?php 
} ?>



 var status='complete';
function loadMoreproduct()
{   
    var startLimit = $('.chat_length_box').length;
	var totalRecordsLoaded = totalRecords;
	var skipLimit = totalRecordsLoaded;

	if(status=='complete'){
        var latlngbounds = map.getBounds();
        var lnk = window.location.href;
    	var params2 = lnk.split('?')[1];
        status='running';
        var maxX = latlngbounds.getNorthEast().lng();
        var maxY = latlngbounds.getNorthEast().lat();
        var minX = latlngbounds.getSouthWest().lng();
        var minY = latlngbounds.getSouthWest().lat();
        min_lng=minX;max_lng=maxX;min_lat=minY;max_lat=maxY;
        $.ajax({
            type: 'POST',
            url  : APPLICATION_URL+'/getmoreproduct?'+params2,
            data:{min_lng:min_lng,max_lng:max_lng,min_lat:min_lat,max_lat:max_lat,startLimit:startLimit},
            success: function(data){
			    status='complete';
                  $(".append_data").append(data);
               
            }
        });
    }

}


    $(document).on("click","#search_button",function(){ 
        //  $('#keyword2').val(center_lat);
        var keyword = $('#keyword').val();
        $('#keyword2').val(keyword);
        var pro_cat  =$("#category").val();
        $('#pro_category').val(pro_cat);
        $('#pro_category1').val(pro_cat);
        
        if($(window).width() < 768){
            $("#search_product1").submit();
             
        }else
        {
            $("form").submit();
        }
    });






function pointOnLocation1(autocomplete){
    var near_place = autocomplete.getPlace();
    $('#loc_lat1').val(near_place.geometry.location.lat());
    $('#loc_long1').val(near_place.geometry.location.lng());

    if(near_place.types[0]=="country"){
        $('#zoom_mp1').val(4);
    }
    if(near_place.types[0]=="administrative_area_level_1"){
        $('#zoom_mp1').val(7);   

    }
    if(near_place.types[0]=="locality"){
      
        $('#zoom_mp1').val(12);   
    }
    if(near_place.types[0]=="route" || near_place.types[0]=="street_address" ){
       
        $('#zoom_mp1').val(15);      
    }
    
    $('#location').val(near_place.formatted_address);
    $("#search_product1").submit(); 
}



function pointOnLocation(autocomplete){
    var near_place = autocomplete.getPlace();
    $('#loc_lat').val(near_place.geometry.location.lat());

    $('#loc_long').val(near_place.geometry.location.lng());

    if(near_place.types[0]=="country"){
        $('#zoom_mp').val(4);
    }
    if(near_place.types[0]=="administrative_area_level_1"){
        $('#zoom_mp').val(7);   

    }
    if(near_place.types[0]=="locality"){
      
        $('#zoom_mp').val(12);   
    }
    if(near_place.types[0]=="route" || near_place.types[0]=="street_address" ){
       
        $('#zoom_mp').val(15);      
    }
    
    $('#location').val(near_place.formatted_address);
    $("form").submit(); 
}


    $('#category').change(function(e){ 
       var pro_cat  =$(this).val();
        chnagecategory(pro_cat);
    });

    $('#Rating').change(function(e){ 
        $('.nextMilestoneButton').click();
    });
    $('#Rating1').change(function(e){ 
        $('.nextMilestoneButton').click();
    });
    
    function chnagecategory(pro_cat){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
                });
        $.ajax({
                type: 'POST',
                url  :   APPLICATION_URL+'/sub-category',
                data : ({pro_cat : pro_cat}),
                // dataType: 'JSON',
                success: function(response) {
                            var substr = response;
                            if(substr.sub_product_categories == "product_cat_not_found" || substr.sub_product_categories == "failed" ){
                                    $("#sub_cat").html('');
                                    $("#sub_cat").append("<option value='0'> Please Select Category </option>"); 
                                    $("#sub_cat1").html('');
                                    $("#sub_cat1").append("<option value='0'> Please Select Category </option>");

                            }else{

                                    $("#sub_cat").html('');
                                    $("#sub_cat").append("<option value='0'> Sub Category </option>");
                                    $("#sub_cat1").html('');
                                    $("#sub_cat1").append("<option value='0'> Sub Category </option>");
                                    var sub_category = "";


                                    @if(!empty($sub_category)) 
                                        sub_category = "{{$sub_category}}";
                                    @elseif(old('sub_category') !='') 
                                        sub_category = "{{old('sub_category')}}";
                                    @endif
                                    jQuery.each(substr.sub_product_categories, function( i, val ) {
                                        if(sub_category != "" && i == sub_category){
                                            $("#sub_cat").append("<option value="+i+" selected >"+val+"</option>");
                                            $("#sub_cat1").append("<option value="+i+" selected >"+val+"</option>")
                                        }else{
                                            $("#sub_cat").append("<option value="+i+" >"+val+"</option>");
                                            $("#sub_cat1").append("<option value="+i+" >"+val+"</option>");
                                        }
                                        
                                        
                                    });
                            }

                }
        });
    }


    $('.check_cat').on('click', function() {
       var pro_cat  =$("#category").val();
          if(pro_cat=='0')
           {
                $.notify({
                    message: "Please select Category First"
                    },{
                    type: 'danger',
                    timer: 1000
                });

           }    
    });

function fromLatLngToPoint(latLng, map) {
    var topRight = map.getProjection().fromLatLngToPoint(map.getBounds().getNorthEast());
    var bottomLeft = map.getProjection().fromLatLngToPoint(map.getBounds().getSouthWest());
    var scale = Math.pow(2, map.getZoom());
    var worldPoint = map.getProjection().fromLatLngToPoint(latLng);
    return new google.maps.Point((worldPoint.x - bottomLeft.x) * scale, (worldPoint.y - topRight.y) * scale);
}

</script>

@endsection()