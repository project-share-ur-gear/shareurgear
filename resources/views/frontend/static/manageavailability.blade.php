@extends('layouts.app')

@section('content')


<link href="{{FRONT_CSS}}/main.min.css" rel="stylesheet" type="text/css">


<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">Manage Availability</h2>
        <div class="manage-product-table">
            <div class="manage-data-avail">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img.jpg'}}" alt=""></div></td>
                                <td>Product Name</td>
                                <td>Product Category</td>
                                <td>$xxx</td>
                                <td><span style="color:#8a71b2">Approved</span></td>
                                <td>
                                    <div class="A-flex justify-content-end">
                                        <a href="#" class="edit-icon"><img src="{{ FRONT_IMG.'/draw1.png'}}" alt=""></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="calendar-box">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 


<script src="{{ asset('public/js/main.min.js') }}"></script>


<script>
    
  document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    initialView: 'dayGridMonth',
    initialDate: '2020-09-07',
    headerToolbar: {
      left: 'prev,next',
      center: 'title',
      right: ''
    },
    dragScroll: true,
    eventLimitText: 'more',
    eventLimitClick: 'popover',
    unselectAuto: true,
    events: [
      {
        title: 'All Day Event',
        start: '2020-09-01'
      },
      {
        title: 'Long Event',
        start: '2020-09-07',
        end: '2020-09-10'
      },
      
      {
        title: 'Conference',
        start: '2020-09-11',
        end: '2020-09-13'
      },
      
      {
        title: 'Lunch',
        start: '2020-09-12T12:00:00'
      },
      
       {
        title: 'Birthday Party',
        start: '2020-09-13T07:00:00'
      },
      {
        title: 'Click for Google',
        url: 'http://google.com/',
        start: '2020-09-28'
      }
    ]
  });

  calendar.render();
});
</script>


@endsection()