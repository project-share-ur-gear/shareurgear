@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">Messaging</h2>
        <div class="group-chat">
            <div class="online-users"  data-scrollbar>
                <?php for($i=0; $i<8; $i++){?>
                    <div class="user-section">
                        <div class="media media_user align-items-center">
                            <div class="img-user data_object_status"> <img src="{{ FRONT_IMG.'/user-img.jpg' }}"/> </div>
                            <div class="media-body">
                                <p class="user-name">Adrien Delforge </p>
                            </div>
                        </div>
                    </div>
                    <!-- user-section -->
                    <div class="user-section active">
                        <div class="media media_user align-items-center">
                            <div class="img-user data_object_status"> <img src="{{ FRONT_IMG.'/user-img2.jpg' }}"/> </div>
                            <div class="media-body">
                                <p class="user-name">Amr Taha </p>
                            </div>
                        </div>
                    </div>
                    <!-- user-section -->
                    <div class="user-section">
                        <div class="media media_user align-items-center">
                            <div class="img-user data_object_status"> <img src="{{ FRONT_IMG.'/user-img.jpg' }}"/> </div>
                            <div class="media-body">
                                <p class="user-name">Jonas Leupe </p>
                            </div>
                        </div>
                        <div class="mr-number"> 2 </div>
                    </div>
                    <!-- user-section -->
                <? } ?>
            </div>
            <!----------------- chat user -------------------->
            <div class="chat-box">
                <div class="d-lg-none d-block massage-top">
                  <a href="javascript:void(0)" class="back-btn btn close_chat_window"> <i class="fas fa-arrow-circle-left"></i> Back </a>
                  <!-- <div class="count new_msg_indi" style="display: none;"></div> -->
                </div>
                <div class="message-window" data-scrollbar>
                    <?php for($i=0; $i<2; $i++){?>
                    <div class="message-time"> <span>Yesterday</span> </div>
                    <div class="right-box">
                            <div class="text_wrapper">
                                <div class="text">Hey, Thank for the explanation. </div>
                                <div class="text">Any updates so far?</div>
                                <p class="date_day">Yesterday at 1:32 pm</p>
                            </div>
                            <div class="avatar"><img src="{{ FRONT_IMG.'/user-img.jpg' }}"></div>
                    </div>
                    <div class="message-time"> <span>Today</span> </div>
                    <div class="left-box">
                            <div class="avatar"><img src="{{ FRONT_IMG.'/user-img2.jpg' }}"></div>
                            <div class="text_wrapper">
                                <div class="text">Hey Jon, will update today. Not sure when exactly. But i’ll keep you posted.</div>
                                <p class="date_day">1:30 pm</p>
                            </div>
                    </div>
                    <div class="right-box">
                            <div class="text_wrapper">
                                <div class="text">Hey, Thank for the explanation. </div>
                                <p class="date_day"> 1:32 pm</p>
                            </div>
                            <div class="avatar"><img src="{{ FRONT_IMG.'/user-img.jpg' }}"></div>
                    </div>
                    <div class="left-box">
                            <div class="avatar"><img src="{{ FRONT_IMG.'/user-img2.jpg' }}"></div>
                            <div class="text_wrapper">
                                <div class="text">Hey Jon, will update today. Not sure when exactly. But i’ll keep you posted.</div>
                                <p class="date_day">1:33 pm</p>
                            </div>
                    </div>
                    <?}?>
                </div>

                <div class="message-type-window">
                    <form>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Type a message">
                            <div class="input-group-append">
                                <span class="input-group-text"> Send </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--------------------- chat box -------------------------------------->
        </div>
        <!---------------------------- message box --------------------->
    </div>
</section>


</div>
 
<script src="{{ asset('public/js/smooth-scrollbar.js') }}"></script>

<script>

    $(document).ready(function () {
        var mainHeight = $(".chat-box").height();
        var mwindow = $(".message-type-window").height();
        var tot = mainHeight - mwindow;
        $(".message-window").css("height", tot + "px");
        

        Scrollbar.initAll();

        $('.user-section').click(function(){
            $('.chat-box').addClass('chtFixedLayout');
            $('body').addClass('nav-open');
        });


        $('.massage-top').click(function(){
            $('.chat-box').removeClass('chtFixedLayout');
            $('body').removeClass('nav-open');
        });

    });

</script>


@endsection()