@extends('layouts.app')


@section('content')
<?php $date=date('d-m-Y',strtotime($getPage->updated_at )); ?>

<style>

.common-section-top{background: url({{STORAGE_IMG_PATH.'/app/public/static/'.$getPage->cms->tearms_image}})no-repeat;}

</style>

<section class="common-section-top">

   
    <div class="container">
    
  
        <h2 class="heading"> {!! $getPage->cms->terms_title !!}</h2>
    </div>
</section>

<div class="over-all-home">

<!-- common-header-section -->

<section class="privacy-data">
    <div class="container">
        <div class="privacy-terms">
        <p><b>Last Modification:</b> {{ $date }}  </p>
            {!! $getPage->cms->description_en !!}
        </div>
    </div>
</section>




</div>
 

@endsection