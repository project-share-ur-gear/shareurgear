
@extends('layouts.app')

@php

    $profileImage=FRONT_IMG.'/nophoto.png';
    if($user_data->profile_image!='')
    $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$user_data->profile_image);

    $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
@endphp

@section('share.meta')

    <meta name="image_url" content="{{ $profileImage }}" />
    <meta name="image" content="{{ $profileImage }}" />

    <meta property="og:image" content="{{ $profileImage }}" />
    <meta property="og:title" content="{{$user_data->name}}" />
    <meta property="og:description" content="{{$user_data->address}}" />
    <meta property="og:url" content="{{ $actual_link }}" />
    <meta property="og:type" content="website" />

    <meta itemprop="name"  content="{{$user_data->name}}" />
    <meta itemprop="description" content="{{$user_data->address}}"  />
    <meta itemprop="image" content="{{ $profileImage }}"/>

    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:title" content="{{$user_data->name}}" />
    <meta name="twitter:description" content="{{$user_data->address}}"/>
    <meta name="twitter:image" content="{{ $profileImage }}"/>
@endsection

@section('content')
<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/star-rating.css" rel="stylesheet" type="text/css">
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/theme.css" rel="stylesheet" type="text/css">

<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/star-rating.js"></script>
<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/theme.js"></script>

<style>
.a2a_full_footer{display:none !important;}
/* -- rating */
.rating-container .caption { display:none !important; }
.rating-container .clear-rating { display:none !important; }
.rating-container .rating-stars:focus { outline: unset !important; }
.theme-krajee-svg .empty-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star2.svg" }}') !important; }
.theme-krajee-svg .filled-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star.svg" }}') !important; }
</style>

<div class="over-all-home">
    <section class="user-profile-page">
        <div class="container">
            <!--  -->
            <input type="hidden" name="Count_data" id="Count_data" value="{{$Count_data}}">
            <div class="user-details">
                <div class="form-row">
                    <div class="col-xl-3 col-lg-3 col-sm-4">
                        <div class="user-img-left"><img src="{{  $profileImage }}" alt=""></div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-sm-8">
                        <div class="user-right-content">
                            <h2 class="title">{{$user_data->name}}</h2>
                            <div class="row">
                                <div class="col-6">
                                    <ul class="rating-star">
                                        <input type="number" id="viwrating" name="starRating" class="rating required" min=0 max=5 step=1 data-size="sm" data-ltr="true" value="{{ bcdiv($user_data->avg_rating,1,2) }}" readonly>
                                    </ul>
                                </div>
                                <div class="col-6"><p class="location">Date of Joining: <?php if(!empty($product_data->created_at)) { ?> <?=date('d-m-Y',strtotime($product_data->created_at))?> <? } ?></p></div>
                            </div>
                            <!-- <p class="location">{{$user_data->address}} </p> -->
                            <div class="p-received">
                                <span>Product Listed {{ $Count_data }}</span>
                                <span>Bookings Received {{  $totalOrders }}</span>
                            </div>
                            <div class="M-data-blog">

                                @if($logged_user_id!='0')
                                    @if($logged_user_id!=$product_data[0]->id)
                                        <a href="{{ SITE_HTTP_URL.'/messaging/'.Crypt::encrypt($product_data[0]->id) }}" class="btn msg-btn">Message </a>
                                    @endif
                                @endif
                            

                                <div class="share-icon" id="share_it"> 
                                    <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                        <a class="a2a_dd" href="https://www.addtoany.com/share">
                                            Share <i class="fas fa-share-alt" ></i>
                                        </a>
                                    </div>
                                </div>                            
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3">
                        <div class="map-location" id="map"></div>
                    </div>
                </div>
            </div>
            <div class="browser-content">
                <div class="row append_Data">
                    @if(!empty($product_data))
                        @foreach($product_data as $key => $value)
                            <div class="col-xl-6 col-sm-6 col-6 wish_length_box">
                                <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$value->product_id }}">
                                    <div class="card-br">
                                        <div class="media card-media">
                                            <div class="media-img"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $value->primary_image }}" alt=""></div>
                                            <div class="media-body">
                                                <h2 class="title">{{$value->product_title}}</h2>
                                                <div class="m-flex">
                                                    <p class="cat-data">{{$value->productcategorytitle}},{{$value->subcategorytitle}}</p>
                                                    <ul class="rating-star">
                                                        <input type="number" id="viwrating" name="starRating" class="rating required" style="width: 5px;" min=0 max=5 step=1 data-size="sm" data-ltr="true" value="{{ bcdiv($value->pro_avg_rating,1,2) }}" readonly>
                                                        <li><span href="#" class="star-span"> ({{ $value->pro_total_rating!=''?$value->pro_total_rating:'0' }})</span></li>
                                                    </ul>
                                                </div>
                                                <div class="media-data">
                                                    <div class="price-d">${{$value->price_per_day   }}<sub>/Day</sub></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach  
                    @endif            

                </div>
            </div>

            <div class="loader " id="loadmore_icon"  style="flex: auto; text-align:center;display:none" ><img src="{{ FRONT_IMG.'/Gear-0.3s-98px.gif' }}" alt=""></div>
            <!--  -->
        </div>
    </section>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key={{ $getConfigs['site_google_key'] }}&callback=initMap" async ></script>
 
<script>

    let map;

    // Initialize and add the map
    function initMap() {

        // The location of Uluru
        
        @if(!empty($user_data->latitude) && !empty($user_data->longitude))
            var uluru = { lat: {{  $user_data->latitude }}, lng: {{ $user_data->longitude }} };
        @else
            var uluru = { lat: -34.397, lng: 150.644 };
        @endif
        
        // The map, centered at Uluru
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 4,
            center: uluru,
            disableDefaultUI: true,
        });

        @if(!empty($user_data->latitude) && !empty($user_data->longitude))
            // The marker, positioned at Uluru
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
            });
        @endif
    }

    var status='complete';
    function loadMoreproduct(){ 
        var startLimit = $('.wish_length_box').length;
            if(status=='complete'){ 
                var user_id= '{{$user_id}}';
                status='running';   
                $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                });
                $.ajax({
                    type: 'post',
                    url  : APPLICATION_URL+'/getmoreuserprofilelist',
                    data:{offset:startLimit,user_id:user_id},
                    success: function(data){
                        status='complete';
                        // alert(data);
                            $(".append_Data").append(data);
                            $('#loadmore_icon').css("display","none");
                        
                    }
                }); 
            }
    }
    $(window).scroll(function(e){
        if(($(window).scrollTop()+$(window).innerHeight())>=$('footer').offset().top){
            var totalRecordsLoaded = $('.wish_length_box').length;
            var totalRecords = $('#Count_data').val();
            if(totalRecords > totalRecordsLoaded){
                $('#loadmore_icon').css("display","block");
                loadMoreproduct();
            } 
        }
    });
</script>
@endsection()