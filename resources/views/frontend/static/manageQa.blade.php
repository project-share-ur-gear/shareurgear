@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <div class="mdata-product">
            <h2 class="login-title">Manage Q&A</h2>
            <div class="all-data">
                <select class="form-control selectpicker">
                    <option>All</option>
                    <option>All</option>
                    <option>All</option>
                    <option>All</option>
                </select>
            </div>
        </div>

        <div class="manage-QA-page manage-product-table">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col"></th>
                            <th scope="col">Question by</th>
                            <th scope="col">Date of Posted</th>
                            <th scope="col">Question</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i<4; $i++){?>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>
                                <div class="media media-object">
                                    <img class="media-img" src="{{ FRONT_IMG.'/user-img.jpg' }}" alt="">
                                    <div class="user-name">Nick Jones</div>
                                </div>
                            </td>
                            <td>DD-MM-YYYY</td>
                            <td>
                                <div class="mq-data">
                                    <span>Question display here. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</span>
                                    <div class="manage-avial btn" data-toggle="modal" data-target="#answerModalCenter">Answer</div>
                                </div>
                            </td>
                        </tr>
                       
                        <? } ?>
                        <tr class="border-0">
                            <td colspan="7">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link active" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    </ul>
                                </nav>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 
<!--Answer Modal -->
<div class="modal fade " id="answerModalCenter" tabindex="-1" role="dialog" aria-labelledby="answerModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="answerModalLongTitle">Question display here Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <textarea class="form-control" rows="10" placeholder="Enter answer here...."></textarea>
           <div class="text-center"> <div class="btn submit-btn">Submit</div></div>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection()