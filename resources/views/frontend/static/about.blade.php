@extends('layouts.app')


@section('content')

<style>
    .common-section-top{background: url({{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->section_background_image}})no-repeat;
}
</style>
<section class="common-section-top">
    <div class="container">
        <h2 class="heading">{{$pageData->cms->about_banner_text}}</h2>
    </div>
</section>


<div class="over-all-home" id="scrollmouse">

    <section class="about-us">
        <div class="text-gear">Mission</div>
        <div class="container">
            <div class="our-about">
                <div class="row">
                    <div class="col-xl-5 col-sm-4">
                        <div class="FG-data-featured"><h2 class="title"><span>{{$pageData->cms->section_one_title_p1}}</span>{{$pageData->cms->section_two_title_p1}}</h2></div>
                    </div>
                    <div class="col-xl-7 col-sm-8">
                        <p class="description">{!!$pageData->cms->section_one_desc_p1!!} </p>
                    </div>
                </div>
            </div>

            <div class="middle-section">
                <div class="first-about-b">
                    <div class="About-img">  <img src="{{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->section_one_image}}" >   </div>
                    <p class="description">{!! $pageData->cms->section_two_desc !!} </p>
                </div>
                <div class="second-about-b">
                        {!! $pageData->cms->section_third_desc!!}
                    <div class="About-img"><div class="dots dotscroll"></div><img src="{{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->section_two_image}}" ></div>
                </div>
            </div>
            <div class="last-about">
                <div class="row align-items-center">
                    <div class="col-xl-7 col-lg-5 order-2">
                        <div class="A-about-last">
                            <h2 class="title">{{$pageData->cms->section_fourth_title}}</h2>
                            {!!$pageData->cms->section_fourth_desc!!}          
                            
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-7 order-lg-2">
                        <div class="A-about-right">
                        <img src="{{STORAGE_IMG_PATH.'/app/public/static/'.$pageData->cms->section_last_image}}" >
                            
                            <div class="bottom-rect">
                                <div class="B-rect1 dotscroll"></div>
                                <div class="B-rect dotscroll"></div>
                                <div class="rect"></div>
                                <div class="rect rect1"></div>
                                <div class="rect"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>


<script>
    $(document).ready(function(e) {
        if ($(document).width() > 1024) {
            AOS.init({ once: true });
        } else {
            $('[data-aos]').removeAttr('data-aos');
        }
    });

  // mouse-animation
  var rect = $('#scrollmouse')[0].getBoundingClientRect();
var mouse = {x: 0, y: 0, moved: false};

$("#scrollmouse").mousemove(function(e) {
  mouse.moved = true;
  mouse.x = e.clientX - rect.left;
  mouse.y = e.clientY - rect.top;
});
 
// Ticker event will be called on every frame
TweenLite.ticker.addEventListener('tick', function(){
  if (mouse.moved){    
    parallaxIt(".slide", -100);
    parallaxIt(".dotscroll", -50);
  }
  mouse.moved = false;
});

function parallaxIt(target, movement) {
  TweenMax.to(target, 0.3, {
    x: (mouse.x - rect.width / 2) / rect.width * movement,
    y: (mouse.y - rect.height / 2) / rect.height * movement
  });
}

$(window).on('resize scroll', function(){
  rect = $('#scrollmouse')[0].getBoundingClientRect();
});



</script>



@endsection