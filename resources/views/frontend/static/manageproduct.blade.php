@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <div class="mdata-product">
            <h2 class="login-title">Manage Products</h2>
            <div class="search-product">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><img src="{{ FRONT_IMG.'/loupe1.png' }}" alt=""></span>
                    </div>
                </div>
            </div>
            <div class="all-data">
                <select class="form-control selectpicker">
                    <option>Price</option>
                    <option>Price</option>
                    <option>Price</option>
                    <option>Price</option>
                </select>
            </div>
        </div>
        <div class="manage-product-table">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Image</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Price</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=0; $i<4; $i++){?>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>Product Category</td>
                            <td>$xxx</td>
                            <td><span style="color:#8a71b2">Approved</span></td>
                            <td>
                                <div class="A-flex">
                                    <div class="mq-data">
                                        <a href="#" class="manage-avial">Manage Availability</a>
                                        <span class="questions">7 Questions</span>
                                    </div>
                                    <a href="#" class="edit-icon"><img src="{{ FRONT_IMG.'/draw1.png'}}" alt=""></a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td><div class="p-image"><img src="{{ FRONT_IMG.'/p-img2.jpg'}}" alt=""></div></td>
                            <td>Product Name</td>
                            <td>Product Category</td>
                            <td>$xxx</td>
                            <td>Approval <br> Waiting</td>
                            <td>
                                <div class="A-flex">
                                    <div class="mq-data">
                                        <a href="#" class="manage-avial">Manage Availability</a>
                                    </div>
                                    <a href="#" class="edit-icon"><img src="{{ FRONT_IMG.'/draw1.png'}}" alt=""></a>
                                </div>
                            </td>
                        </tr>
                        <? } ?>
                        <tr class="border-0">
                            <td colspan="7">
                                <nav aria-label="Page navigation">
                                    <ul class="pagination">
                                        <li class="page-item"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link active" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    </ul>
                                </nav>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 


@endsection()