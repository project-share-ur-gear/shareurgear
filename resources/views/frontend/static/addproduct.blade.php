@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->

        <h2 class="login-title">Add Product</h2>
        <div class="contact-dots-panels">
            <div class="contact-box common-form">
                
                <div class="form-group">
                    <label for="">Product Name</label>
                    <input type="text" class="form-control" placeholder="Product Name">
                </div>
                <div class="form-group">
                    <label for="">Product Category</label>
                    <div class="pro-cateOption type-select">
                        <select class="form-control selectpicker">
                            <option>Choose Category </option>
                            <option>Choose Category </option>
                            <option>Choose Category </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Sub-Category</label>
                    <div class="pro-cateOption type-select">
                        <select class="form-control selectpicker">
                            <option>Choose Sub-Category </option>
                            <option>Choose Sub-Category </option>
                            <option>Choose Sub-Category </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Tags</label>
                    <input type="text" value="Tag 1" id="shop-label" data-role="tagsinput" placeholder="Enter tag">
                </div>
                <div class="form-group">
                    <label for="">Price Per Day</label>
                    <input type="text" class="form-control" placeholder="$">
                </div>
                <div class="form-group">
                    <label for="">Location</label>
                    <input type="text" class="form-control" placeholder="Location">
                </div>
                <div class="form-group">
                    <label for="">Product Image </label>
                    <div class="uploader-img">
                        <div class="img-upload"><img class="icon-img" src="{{ FRONT_IMG.'/image-file.png' }}" alt=""><span>Upload Image</span></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Description</label>
                   <textarea rows="6" class="form-control" placeholder="Write description here..."></textarea>
                </div>
                
                <div class="btn  submit-btn my-3">Add Product</div>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 
<script>
    $(document).ready(function(){
        $("#shop-label").tagsinput('items');
    });
</script>


@endsection()