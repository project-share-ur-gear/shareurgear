@extends('layouts.app')

@section('content')



<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->

        <h2 class="login-title">Payout Settings</h2>
        <div class="contact-dots-panels">
            <div class="contact-box common-form">
                <div class="stripe-connects">
                    <img src="{{ FRONT_IMG.'/stripe.png' }}" alt="">
                    <span>Connect to Stripe Account</span>
                </div>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>


@endsection()