@extends('layouts.app')

@section('content')

<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/star-rating.css" rel="stylesheet" type="text/css">
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/theme.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<style>
  .rating-container .caption { display:none; }
  .rating-container .clear-rating { display:none; }
  .rating-container .rating-stars:focus { outline: unset; }
  .theme-krajee-svg .empty-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star2.svg" }}'); }
  .theme-krajee-svg .filled-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star.svg" }}'); }
  .help-block {color:red;}
  .datepicker-days .table-condensed .day{display: table-cell;float: unset;padding: 10px;}
  .datepicker table tr td.active.active, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled.disabled, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled:hover.disabled, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active.disabled:hover[disabled], .datepicker table tr td.active.disabled[disabled], .datepicker table tr td.active:active, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover.active, .datepicker table tr td.active:hover.disabled, .datepicker table tr td.active:hover:active, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active:hover[disabled], .datepicker table tr td.active[disabled]{background: #8a71b2 !important;}
</style>

<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>

<div class="over-all-home">
  @include('sidebar')

  <section class="profile-page login-page">
    <div class="container">
      <!--  -->
      <h2 class="login-title">My Bookings</h2>
      <div class="dashboard-sales">
        <div class="row">
          <div class="col-xl-7 col-lg-6">
            <div class="booking-dash">
              <h2 class="D-title">Bookings</h2>
              <div class="D-book-box">
                <div class="data-book">Last Week <span> {{ $lastWeekTotalSales }}</span> </div>
                <div class="data-book">Last Month <span> {{ $lastMonthTotalSales }} </span> </div>
                <div class="data-book">Total <span> {{ $totalOrders }}</span> </div>
              </div>
            </div>
          </div>
          <div class="col-xl-5 col-lg-6">
            <div class="booking-dash">
              <h2 class="D-title">Earning</h2>
              <div class="D-book-box">
                <div class="data-book">Last Week <span> ${{ bcdiv($getLastWeekSales,1,2) }}</span> </div>
                <div class="data-book">Last Month <span> ${{ bcdiv($getLastMonthSales,1,2) }} </span> </div>
                <div class="data-book">Total <span> ${{ bcdiv($getTotalSales,1,2) }}</span> </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="text-right">
          <div class="filter-search">
            <h2 class="Filter-title">Filters</h2>
            <div class="filter-flex">
              <div class="input-group">
                <input type="text" class="form-control" name="date_filter" id="date_filter" placeholder="Booking Date" aria-label="Booking Date " aria-describedby="basic-addon2">
                <div class="input-group-append">
                  <span class="input-group-text" id="basic-addon2"><img src="{{ FRONT_IMG.'/calendar.png' }}" alt=""></span>
                </div>
              </div>
              <div class="all-data">
                  <select class="form-control selectpicker" name="status_filter" id="status_filter">
                      <option value="">Select</option>
                      <option value="confirm">Confirm</option>
                      <option value="cancel">Cancelled</option>
                      <option value="approved">Approved</option>
                      <option value="decline">Decline</option>
                  </select>
              </div>
              <div class="search-btn btn" onclick="pagination(`1`,`0`)">Search</div>
              <div class="reset" onclick="location.reload();">Reset</div>
            </div>
          </div>
      </div>

      <div class="manage-product-table booking-page">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Product</th>
                <th scope="col"></th>
                <th scope="col">Booked By</th>
                <th scope="col">Date Booked </th>
                <th scope="col">Booking Start Date </th>
                <th scope="col">Booking End Date </th>
                <th scope="col">Price</th>
              </tr>
            </thead>
            <tbody id="bookingData">
                @if(!empty($getBookings))
                  @foreach($getBookings as $key => $value)
                    <tr>
                        <td><div class="p-image"><img src="{{ HTTP_UPLOADED_IMAGES_PATH.'/'.$value['primary_image'] }}" alt=""></div></td>
                        <td>
                          <a href="{{ SITE_HTTP_URL }}/rental-detail-page/{{ $value['booking_product_id'] }}" target="_blank">
                            {{ $value['product_title'] }}
                          </a>
                        </td>
                        <td>
                          @php
                            $profileImage=FRONT_IMG.'/nophoto.png';
                            if($value['profile_image']!='')
                                $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value['profile_image']);
                          @endphp
                            <div class="media media-object">
                                <img class="media-img" src="{{ $profileImage }}" alt="">
                                <div class="review-user">
                                  <div class="user-name">{{ $value['name'] }}</div>
                                  @if(!empty($value['review_id']))
                                    {{--  data-toggle="modal" data-target="#readreviewModalCenter"  --}}
                                    <div class="read-R"  onclick="readReview(`{{ ($value['review_rating']*20) }}`,`{{ nl2br($value['review_messsage']) }}`)">Read Review</div>
                                  @endif
                                </div>
                            </div>
                        </td>
                        <td>{{ date('d-m-Y',strtotime($value['booking_created'])) }}</td>
                        <td>{{ date('d-m-Y',strtotime($value['booking_start_date'])) }}</td>
                        <td>{{ date('d-m-Y',strtotime($value['booking_end_date'])) }}</td>
                        <td>
                          ${{ number_format($value['booking_total_price']) }}
                          <div class="A-flex" style="text-align: end;">
                            <div class="mq-data bkdiv-{{ $value['booking_id'] }}">
                              @if($value['booking_approve_status']=='0')
                                <a href="Javascript:void(0)" class="manage-avial mb-2" onclick="bookingApproveStatus(`{{ encrypt($value['booking_id']) }}`,'approve')">Approve</a>
                                <a href="Javascript:void(0)" class="manage-avial btn btn-danger" onclick="bookingApproveStatus(`{{ encrypt($value['booking_id']) }}`,'decline')">Decline</a>
                              @elseif($value['booking_approve_status']=='1' && $value['booking_status']=='1')

                                <a href="Javascript:void(0)" class="manage-avial mb-2" onclick="openCheckInOutModal(`{{ encrypt($value['booking_id']) }}`,'checkin')">Check-In</a>
                                <a href="Javascript:void(0)" class="manage-avial btn btn-danger" onclick="openCheckInOutModal(`{{ encrypt($value['booking_id']) }}`,'checkout')">Check-Out</a>
                                @if($value['checkin_status']=='0')
                                  <span class="cancelled-booked" onclick="cancelBooking(`{{ encrypt($value['booking_id']) }}`)" >Cancel Booking </span>
                                @endif
                              @elseif($value['booking_approve_status']=='2')
                                <span class="cancelled-booked">Booking Decline</span>
                              @endif

                              @if($value['booking_status']=='2' && $value['booking_approve_status']=='1')
                                <span class="cancelled-booked" onclick="viewCancelb(`{{ $value['booking_cancelled_reason'] }}`)">Booking Cancelled</span>
                              @endif
                              
                              <span class="cancelled-booked" onclick="openReportModal(`{{ encrypt($value['booking_id']) }}`)" >Report Issued</span>

                              
                            </div>
                          </div>
                        </td>
                    </tr>
                  @endforeach
                @else
                  <tr class="data-ex-items" style="text-align: center;">
                    <td colspan="7">No Booking Found...</td>
                  </tr>
                @endif

                @if(!empty($getPages) && !empty($getBookings))
                  <tr class="border-0">
                    <td colspan="7">
                      <nav aria-label="Page navigation">
                        <ul class="pagination">
                          @foreach($getPages as $page => $rec)
                            <li class="page-item"><a class="page-link {{ ($page=='1')?'active':'' }}"  onclick="pagination(`{{ $page }}`,`{{ $rec }}`)" data-rec="{{ $rec }}" href="Javascript:void(0)">{{ $page }}</a></li>
                          @endforeach
                        </ul>
                      </nav>
                    </td>
                  </tr>
                @endif
            </tbody>
          </table>
        </div>
      </div>
      <!--  -->
    </div>
  </section>
</div>
 
<!--Read Review Modal -->
<div class="modal fade " id="readreviewModalCenter" tabindex="-1" role="dialog" aria-labelledby="readreviewModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="readreviewModalLongTitle">Read Your Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <ul class="rating-star v-rev">
                <input type="number" id="viwrating" name="starRating" class="rating required" min=0 max=5 step=1 data-size="md" data-ltr="true" readonly>
            </ul>
            <div class="review-desc">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
            </div>
           {{-- <div class="text-center"> <div class="btn submit-btn">Submit</div></div> --}}
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Cancel-Booking -->
<div class="modal fade " id="cancelModalCenter" tabindex="-1" role="dialog" aria-labelledby="cancelModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cancelModalLongTitle">Cancel Booking </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <form id="cancel_form" class="cancel_form" method="post">
              <textarea class="form-control required" name="cancel_reason" id="razlog_preklica" maxlength="1000" rows="6" placeholder="Please write the reason for cancellation of the booking...."></textarea>
              <div class="text-center"> <div class="btn submit-btn ">Submit</div></div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Report-Modal -->
<div class="modal fade " id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="reportModalLongTitle">Reoprt Issue</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
            <form id="report_form" class="report_form" method="post">
              <textarea class="form-control required" name="aithisg_reason" id="aithisg_reason" maxlength="1000" rows="6" placeholder="Please write the reason for report of the booking...."></textarea>
              <div class="text-center"> <div class="btn submit-btn ">Submit</div></div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Checkinout-Modal -->
<div class="modal fade " id="checkInOutModal" tabindex="-1" role="dialog" aria-labelledby="checkInOutModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="checkInOutModalTitle">Check In - Check Out</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
          <form id="check_inout_form" class="check_inout_form" method="post" enctype='multipart/form-data'>
            <div class="contact-box common-form">
              <div class="form-row itemlist">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Image</label>
                    <input type="file" name="check_in_out_image" id="check_in_out_image" class="form-control required" style="margin-bottom: 0px;" value="" autocomplete="off">
                    <span for="check_in_out_image" class="help-block"></span>
                  </div>
                  <div class="form-group">
                    <label for="" class="">Date</label>
                    <input type="text" name="check_in_out_date" id="check_in_out_date" class="form-control required" style="margin-bottom: 0px;" value="" maxlength="100" autocomplete="off" placeholder="Date" readonly>
                    <span for="check_in_out_date" class="help-block"></span>
                  </div>
                  <div class="form-group">
                    <label for="" class="">Description</label>
                    <textarea type="text" name="check_in_out_description" id="check_in_out_description" class="form-control required" style="margin-bottom: 0px;" value="" maxlength="500" autocomplete="off"></textarea>
                    <span for="check_in_out_description" class="help-block"></span>
                  </div>
                </div>
              </div>
              <div class="text-center"> <div class="btn submit-btn ">Submit</div></div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade " id="checkInOutViewModal" tabindex="-1" role="dialog" aria-labelledby="checkInOutModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="checkInOutModalTitle">Check In - Check Out</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
          <form id="check_inout_form" class="check_inout_form" method="post" enctype='multipart/form-data'>
            <div class="contact-box common-form">
              <div class="form-row itemlist">
                <div class="col-sm-12">
                  <div class="form-group">
                    <label for="">Image</label>
                    <div style="width:200px;height: 200px;"><img src="" id="ckinimage" width="100%"/></div>
                  </div>
                  <div class="form-group">
                    <label for="" class="">Date</label> 
                    <label for="" id="CheckIndate"></label> 
                  </div>
                  <div class="form-group">
                    <label for="" class="">Description</label>
                     <label for="" id="CheckInDesc"></label> 
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- View Cancel-Booking -->
<div class="modal fade " id="viewCancelModal" tabindex="-1" role="dialog" aria-labelledby="viewCancelModal" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cancelModalLongTitle">Cancel Booking </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box cancel-reason">
        </div>
      </div>
    </div>
  </div>
</div>


<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/star-rating.js"></script>
<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/theme.js"></script>

<script>
  $(document).ready(function(){
    $('#date_filter').datepicker({
      format:'dd-mm-yyyy',
      autoclose: true,
    });

    $('#check_in_out_date').datepicker({
      format:'dd-mm-yyyy',
      autoclose: true,
    });
  });

  function readReview(rate,summ){
    $('.v-rev').find('.filled-stars').css('width',rate+'%');
    $('.review-desc').html(summ);
    $('#readreviewModalCenter').modal('show');
  }

  function pagination(page,rec){
    $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
    var searchDate = $('#date_filter').val(); 
    var searchStatus = $('#status_filter').val(); 

    $.ajax({
      type: "post",
      url: "{{ route('booking.getmoresharer') }}",
      data: {active:page,onpage:rec,date:searchDate,status:searchStatus},
      success: function (response) {
          var isJson = IsJsonString(response);
          var response = (isJson) ? JSON.parse(response):response;
          if(isJson)
          {
            if(!response.status){
              $.notify({ message: response.msg },{ type: 'danger', timer: 1000 });
            }
          }
          else{
            var body = $("html, body");
            body.stop().animate({scrollTop:200}, 1000, 'swing', function() { 
              $('#bookingData').html(response);
            });
          }
          return;
      }
    });
  }

  function IsJsonString(str) {
    try {
        var obj = JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
  }

  function cancelBooking(bkid){
    $('#cancel_form').find('.submit-btn').attr('onclick','submitCancelRequest(`'+bkid+'`)');
    $('#cancelModalCenter').modal('show');
  }

  function submitCancelRequest(bkid){
    if($('#cancel_form').valid()){

      blockUI();

      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      });
      var razlog_preklica = $('#razlog_preklica').val();

      $.ajax({
        type: "post",
        url: "{{ route('booking.cancelbooking') }}",
        data: {'id':bkid,'cuiseanna':btoa(razlog_preklica),'tip':btoa('sharer')},
        success: function (response) {
            var isJson = IsJsonString(response);
            var response = (isJson) ? JSON.parse(response):response;
            if(isJson)
            {
              if(!response.status){
                $.notify({ message: response.msg },{ type: 'danger', timer: 1000 });
              }else{
                $.notify({ message: 'Your booking cancelled successfully.' },{ type: 'success', timer: 1000 });
                $('#cancelModalCenter').modal('hide');
                setTimeout(function(){
                  location.reload();
                },500);
              }

              $.unblockUI();
            }
            return;
        }
      });
    }
  }

  function openReportModal(bkid){
    $('#aithisg_reason').val('');
    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $.ajax({
      type: "post",
      url: "{{ route('booking.reportissue') }}",
      data: {'id':bkid,'tip':btoa('sharer'),'action':'checked'},
      success: function (response) {
        var isJson = IsJsonString(response);
        var response = (isJson) ? JSON.parse(response):response;
        if(isJson)
        {
          if(!response.status){
            $('#report_form').find('.submit-btn').show();
            $('#report_form').find('.submit-btn').attr('onclick','submitReport(`'+bkid+'`)');
            $('#reportModal').modal('show');
          }else{
            $('#report_form').find('.submit-btn').hide();
            $('#aithisg_reason').val(response.summary);
            $('#reportModal').modal('show');
          }
        }
        return;
      }
    });
  }

  function submitReport(bkid){
    if($('#report_form').valid()){
      blockUI();
      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      });
      var aithisg_reason = $('#aithisg_reason').val();

      $.ajax({
        type: "post",
        url: "{{ route('booking.reportissue') }}",
        data: {'id':bkid,'aithisg':btoa(aithisg_reason),'tip':btoa('sharer')},
        success: function (response) {
            var isJson = IsJsonString(response);
            var response = (isJson) ? JSON.parse(response):response;
            if(isJson)
            {
              if(!response.status){
                $.notify({ message: response.msg },{ type: 'danger', timer: 1000 });
              }else{
                $.notify({ message: 'Your report submitted successfully.' },{ type: 'success', timer: 1000 });
                $('#reportModal').modal('hide');
                $('#aithisg_reason').val('');
              }

              $.unblockUI();
            }
            return;
        }
      });
    }
  }

  function bookingApproveStatus(bid,action){

    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });

    $.ajax({
      type: "post",
      url: "{{ route('booking.approvestatus') }}",
      data: {bid:bid,action:action,type:'sharer'},
      success: function (response) {
          var isJson = IsJsonString(response);
          var response = (isJson) ? JSON.parse(response):response;
          if(isJson)
          {
            if(!response.status){
              $.notify({ message: response.msg },{ type: 'danger', timer: 1000 });
            }else{

              $.notify({ message: response.msg },{ type: 'success', timer: 1000 });

              if(action=='approve'){
                $('.bkdiv-'+bid).html('<span class="questions" style="color:#8a71b2;">Booking Approved</span>');
              }else{
                $('.bkdiv-'+bid).html('<span class="questions" style="color:#8a71b2;">Booking Declined</span>');
              }
              
              location.reload();
            }

            $.unblockUI();
          }
          return;
      }
    });
  }

  function openCheckInOutModal(bkid,type){
    $('#check_in_out_image').val('');
    $('#check_in_out_date').val('');
    $('#check_in_out_description').val('');

    $.ajaxSetup({
      headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
    });
 
    $.ajax({
      type: "post",
      url: "{{ route('booking.checkinout') }}",
      data: {'id':bkid,'type':type,'tip':btoa('sharer'),'action':'checked'},
      success: function (response) {
        var isJson = IsJsonString(response);
        var response = (isJson) ? JSON.parse(response):response;
        if(isJson)
        {
          if(!response.status){
            $('#check_inout_form').find('.submit-btn').attr('onclick','submitCheckInOut(`'+bkid+'`,`'+type+'`)');
            $('#checkInOutModal').modal('show');
          }else{
            $('#ckinimage').attr('src',response.images);
            $('#CheckIndate').text(response.datetime);
            $('#CheckInDesc').text(response.summary);
            $('#checkInOutViewModal').modal('show');
          }
        }
        return;
      }
    });
  }

  function submitCheckInOut(bkid,type){
    if($('#check_inout_form').valid()){
      
      blockUI();

      $.ajaxSetup({
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
      });
      
      var formData = $('#check_inout_form').serialize();
      var Formdata = new FormData($('#check_inout_form')[0]);
      Formdata.append('id',bkid);
      Formdata.append('type',type);
      Formdata.append('tip',btoa('sharer'));
      Formdata.append('action','submit');

      $.ajax({
        type: "post",
        url: "{{ route('booking.checkinout') }}",
        data: Formdata,//{'id':bkid,'type':type,'tip':btoa('sharer'),'action':'submit'},
        enctype: 'multipart/form-data',
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
          var isJson = IsJsonString(response);
          var response = (isJson) ? JSON.parse(response):response;
          if(isJson)
          {
            if(!response.status){
              $.notify({ message: response.msg },{ type: 'danger', timer: 1000 });
            }else{
              $.notify({ message: response.msg },{ type: 'success', timer: 1000 });
              $('#checkInOutModal').modal('hide');
            }

            $.unblockUI();
          }
          return;
        }
      });
    }
  }

  function viewCancelb(res){
    $('.cancel-reason').text(res);
    $('#viewCancelModal').modal('show');
  }

  function blockUI() {
    $.blockUI({
        css: {
            backgroundColor: 'transparent',
            border: 'none'
        },
        message: '<div class="spinner"><div class="spinner-border m-5" role="status"><span class="sr-only">Loading...</span></div></div>',
        baseZ: 1500,
        overlayCSS: {
            backgroundColor: '#FFFFFF',
            opacity: 0.7,
            cursor: 'wait'
        }
    });
  } //end Blockui   
</script>
@endsection()