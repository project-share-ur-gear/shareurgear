 @if(!empty($getBookings))
  @foreach($getBookings as $key => $value)
    <tr class="data-ex-items">
        <td><div class="p-image"><img src="{{ HTTP_UPLOADED_IMAGES_PATH.'/'.$value['primary_image'] }}" alt=""></div></td>
        <td>
          <a href="{{ SITE_HTTP_URL }}/rental-detail-page/{{ $value['booking_product_id'] }}" target="_blank">
            {{ $value['product_title'] }}
          </a>
        </td>
        <td>{{ date('d-m-Y',strtotime($value['booking_created'])) }}</td>
        <td>{{ date('d-m-Y',strtotime($value['booking_start_date'])) }}</td>
        <td>{{ date('d-m-Y',strtotime($value['booking_end_date'])) }}</td>
        <td>${{ number_format($value['booking_price']) }}</td>
        <td>
            <div class="A-flex">
              <div class="mq-data bkdiv-{{ encrypt($value['booking_id']) }}">
                @if($value['booking_approve_status']=='0' || $value['booking_approve_status']=='2')
                  <span class="questions" style="color:red;">{{ ($value['booking_approve_status']=='0')?'Booking Approve Pending':'Booking Decline' }} </span>
                @else
                  @if($value['booking_status']=='1')
                    @if(date('Y-m-d') > $value['booking_end_date'] && $value['checkout_status']=='1')
                      {{--  data-toggle="modal" data-target="#leaveModalCenter"  --}}
                      @if(!empty($value['review_id']))
                        <a href="Javascript:void(0)" class="manage-avial bka-{{ $value['booking_id'] }}" onclick="readReview(`{{ ($value['review_rating']*20) }}`,`{{ nl2br($value['review_messsage']) }}`)">Read Review</a>
                      @else
                        <a href="Javascript:void(0)" class="manage-avial bka-{{ $value['booking_id'] }}" onclick="leaveReview(`{{ encrypt($value['booking_id']) }}`,`{{ $value['booking_id'] }}`)">Leave Review</a>
                      @endif
                    @else 
                      <a href="Javascript:void(0)" class="manage-avial mb-2" onclick="openCheckInOutModal(`{{ encrypt($value['booking_id']) }}`,'checkin')">Check-In</a>
                      <a href="Javascript:void(0)" class="manage-avial btn btn-danger" onclick="openCheckInOutModal(`{{ encrypt($value['booking_id']) }}`,'checkout')">Check-Out</a>
                      @if($value['checkin_status']=='0')
                        <span class="questions" onclick="cancelBooking(`{{ encrypt($value['booking_id']) }}`)" >Cancel Booking </span>
                      @endif
                    @endif
                  @else 
                    <span class="cancelled-booked" onclick="viewCancelb(`{{ $value['booking_cancelled_reason'] }}`)">Booking Cancelled</span>
                  @endif
                @endif
                
                <span class="cancelled-booked" onclick="openReportModal(`{{ encrypt($value['booking_id']) }}`)" >Report Issued</span>
              </div>
            </div>
        </td>
    </tr>
    <tr>
      <td colspan="7" class="pt-0">
        <div class="booking-page-items">
          @if(!empty($value['booking_extra_items']))
            <!-- left-items -->
            <div class="booking-ex-item">
              <h2 class="title-ex">Extra Items ({{ count($getExtraItems[$value['booking_id']]) }})</h2>
              <div class="scoll-bar-items">
                  <div class="row">
                    @foreach($getExtraItems[$value['booking_id']] as $i => $item)
                      <div class="col-sm-6 col-6">
                        <div class="ex-items media align-items-center">
                          <div class="media-img"><img src="{{ HTTP_UPLOADED_IMAGES_PATH.'/'.$item['item_image'] }}"></div>
                          <div class="media-body">
                            <div class="ex-flex">
                              <h2 class="title">{{ $item['item_name'] }}</h2>
                              <p class="desc">${{ $item['item_price'] }}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    @endforeach
                  </div>
              </div>
            </div>
          @endif
          <!-- right-pay -->
          <div class="right-pay-data">
              <p class="p-data">Pickup & delivery location : <span class="d-block">{{ $value['booking_location'] }}</span></p>
              <p class="p-data">Booking price : <span>${{ number_format($value['booking_price']) }} </span></p>
              <p class="p-data">Pickup & delivery price : <span>${{ number_format($value['booking_distance_price']) }}</span></p>
              <p class="p-data">Extra price : <span>${{ number_format($value['booking_extra_item_price']) }}</span></p>
              <p class="p-data">Service fee : <span>${{ bcdiv($value['booking_site_price'],1,2) }}</span></p>
              <p class="p-data">Total amount : <span>${{ bcdiv($value['booking_total_price'],1,2) }} </span></p>
          </div>
        </div>
      </td>
    </tr>
  @endforeach
@endif
@if(!empty($getPages) && !empty($getBookings))
  <tr class="border-0">
    <td colspan="7">
      <nav aria-label="Page navigation">
        <ul class="pagination">
          @foreach($getPages as $page => $rec)
            <li class="page-item"><a class="page-link {{ ($page==$activePage)?'active':'' }}"  onclick="pagination(`{{ $page }}`,`{{ $rec }}`)" data-rec="{{ $rec }}" href="Javascript:void(0)">{{ $page }}</a></li>
          @endforeach
        </ul>
      </nav>
    </td>
  </tr>
@endif
@php exit(); @endphp