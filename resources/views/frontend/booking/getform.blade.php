@php
    $productData = $getProduct['productData'];
    $deliveryLocation = $getProduct['deliveryLocation'];
    $deliveryToLocation = $getProduct['deliveryToLocation'];
    $rangeOfdates = $getProduct['rangeOfdates'];
    $getExtraItem = $getProduct['getExtraItem'];
    $stripe_cust_account = $loggedUser['stripe_cust_account_id'];
@endphp
<style>
.datepicker-days .table-condensed .day{display: table-cell;float: unset;padding: 10px;}
.datepicker table tr td.active.active, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled.disabled, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled:hover.disabled, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active.disabled:hover[disabled], .datepicker table tr td.active.disabled[disabled], .datepicker table tr td.active:active, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover.active, .datepicker table tr td.active:hover.disabled, .datepicker table tr td.active:hover:active, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active:hover[disabled], .datepicker table tr td.active[disabled]{background: #8a71b2 !important;}
.booked{background: #8a71b2 !important;color: #fff!important;}
.pac-container{z-index:999999999;}
</style>

<script src="{{ asset('public/plugins/customscroolbar/jquery.mCustomScrollbar.min.js') }}"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js" integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $getConfig['site_google_key'] }}&libraries=places"></script>
<!--  -->

<div class="rent-item-modal ">
    <form method="post" id="bookingForm" class="bookingForm"   enctype="multipart/form-data">
        <p class="per-day">$ {{ $productData['price_per_day'] }} <sub>/Per Day</sub></p>
        <div class="common-form rent-middle-content">
                <input type="hidden" name="product" id="product" value="{{ encrypt($productData['product_id']) }}">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Select Start Date</label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker required" name="start_date" id="start_date" placeholder="DD-MM-YYYY" readonly>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><img src="{{ FRONT_IMG.'/calendar.png' }}" alt=""></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Select End Date</label>
                            <div class="input-group">
                                <input type="text" class="form-control datepicker required" name="end_date" id="end_date" placeholder="DD-MM-YYYY" readonly>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><img  src="{{ FRONT_IMG.'/calendar.png' }}" alt=""></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="Pickup-box">
                    <div class="pick-flex">
                        <h6 class="title">Pickup</h6>
                        <p class="piz">$ <span id="locPriceCal">0.00</span></p>
                    </div>
                    <p class="descriptions">Home Location</p>
                    <div class="form-group">
                        <input type="radio" class="value_data_check pickup_location required" name="pickup_location" id="pickup_location" value="home" data-lat="{{ $productData['home_lat'] }}" data-lang="{{  $productData['home_long'] }}">
                        <div class="input-group">
                            <div class="form-control">{{ $productData['home_location'] }}</div>
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon2"><span class="locPrice">Free</span></span>
                            </div>
                        </div>
                        <span for="pickup_location" class="help-block"></span>
                    </div>

                    @if(!empty($deliveryLocation))
                        <div class="locations">
                            <p class="descriptions">Delivery Location </p>
                            @foreach($deliveryLocation as $key => $value)
                                <i class="fa fa-info-circle" aria-hidden="true" data-container="body" data-toggle="popover" data-placement="top" data-content="{{ $value['d_instruction'] }}"></i>
                                <div class="form-group">
                                    <input type="radio" class="value_data_check pickup_location required" name="pickup_location" id="pickup_location" value="{{ $value['d_id'] }}" data-lat="{{ $value['d_lat'] }}" data-lang="{{  $value['d_long'] }}">
                                    <div class="input-group">
                                        <div class="form-control">{{ $value['d_location'] }}</div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">$<span class="locPrice">{{ $value['d_price'] }} </span></span>
                                        </div>
                                    </div>
                                </div>
                                
                                
                            @endforeach
                        </div>
                    @endif

                    <input type="hidden" name="pickup_lat" id="pickup_lat" value="">
                    <input type="hidden" name="pickup_lang" id="pickup_lang" value="">

                    @if(!empty($deliveryToLocation))
                        <div class="locations">
                            <div class="pick-flex">
                                <h6 class="title">Delivered to you</h6>
                            </div>
                            <p class="descriptions">Enter a delivery address</p>
                            <div class="form-group">
                                <input type="text" class="form-control" name="delivered_to_you" id="delivered_to_you" placeholder="Enter a delivery address">
                            </div>
                        </div>
                        <input type="hidden" name="delivered_address_lat" id="delivered_address_lat" value="">
                        <input type="hidden" name="delivered_address_long" id="delivered_address_long" value="">
                    @endif
                </div>
                @if(!empty($getExtraItem))
                    <!-- extra-item -->
                    <div class="pick-flex">
                        <h6 class="title">Extra Items </h6>
                    </div>

                    <div class="extra-items-data">
                        @foreach($getExtraItem as $key => $extraItem)
                            <div class="item-extra media align-items-center">
                                <div class="item-img"><img src="{{ HTTP_UPLOADED_IMAGES_PATH.'/'.$extraItem['item_image'] }}" alt=""></div>
                                <div class="media-body">
                                    <div class="h-items">
                                        <h2 class="title">{{  $extraItem['item_name']  }}</h2>
                                        <div class="h-prices d-flex">
                                            <p class="prices">${{  $extraItem['item_price']  }}</p>
                                            <div class="added_checked">
                                                <input type="checkbox" value="{{ $extraItem['item_id'] }}" name="extra_item[]" id="extra_item{{ $key }}" class="value_data_check extraitem_checked">
                                                <div class="added-btn btn">Add</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <!--  -->
                @endif

        </div>

        <!-- <p class="final-price">Final Booking Price : $ xxx</p> -->
        <div class="text-center answer-box">
            <div class="btn submit-btn" onclick="payModal()">Continue</div>
        </div>
    </form>
</div>


<script>

    var dateRange = [
        @if(!empty($rangeOfdates))
            @foreach($rangeOfdates as $key => $value)
               '{{ $value }}',
            @endforeach
        @endif
    ];
    var deliveryToLocation = [
        @foreach($deliveryToLocation as $key => $value)
            {'distance':'{{ $value['d_to_miles'] }}','price' : '{{ $value['d_to_price'] }}'},
        @endforeach
    ];

    $(document).ready(function(){
        setTimeout(function(){
            $('[data-toggle="popover"]').popover()
        },1000)


        $('#bookingForm').validate();
        updateDatepiker(dateRange);

        $('.extra-items-data').mCustomScrollbar({theme:"dark-3"});
    });

    $(document).ready(function() {
        setTimeout(function(){
            var input1 = document.getElementById('delivered_to_you');
            var autocomplete1 = new google.maps.places.Autocomplete(input1);
            var geocoder = new google.maps.Geocoder();
            google.maps.event.addListener(autocomplete1, 'place_changed', function() {
                var place = autocomplete1.getPlace();
                var lat = place.geometry.location.lat();
                var long = place.geometry.location.lng();
                for (var i = 0; i < place.address_components.length; i++) {
                    for (var j = 0; j < place.address_components[i].types.length; j++) {
                        if (place.address_components[i].types[j] == "postal_code") {
                            var postal_code = place.address_components[i].long_name;
                        }
                    }
                }

                $('#delivered_address_lat').val(lat);
                $('#delivered_address_long').val(long);

                var getLat = `{{ $productData['home_lat'] }}`;
                var getLang = `{{  $productData['home_long'] }}`;
                if(getLat!='' && getLang!=''){
                    var loc1 = {lat: getLat, lng: getLang};
                    var loc2 = {lat: lat, lng: long};
                    var getDistance = haversine_distance(loc1,loc2).toFixed(2);
                    console.log(getDistance);
                    var disPrice = "";
                    $(deliveryToLocation).each(function(key,value){
                        if(parseFloat(getDistance) <= parseFloat(value.distance) && getDistance > 0){
                            disPrice = value.price;
                        }
                    });
                    if(disPrice!=''){
                        $('#locPriceCal').text(disPrice);
                        $('.pickup_location').prop('checked',false);
                        $('.pickup_location').removeClass('required');

                    }else{
                        $('#locPriceCal').text('0.00');
                        $('#delivered_to_you').val('');
                        $('.pickup_location').addClass('required');
                        $.notify({  message: 'Please select another address., because this address not in our zone.' },{ type: 'danger', timer: 100000, z_index:99999999999999999999999999999 });
                        return;
                    }
                }
            });
        },1000);
    });

    function haversine_distance(mk1, mk2) {
        var R = 3958.8; // Radius of the Earth in miles
        var rlat1 = mk1.lat * (Math.PI/180); // Convert degrees to radians
        var rlat2 = mk2.lat * (Math.PI/180); // Convert degrees to radians
        var difflat = rlat2-rlat1; // Radian difference (latitudes)
        var difflon = (mk2.lng - mk1.lng) * (Math.PI/180); // Radian difference (longitudes)

        var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat/2)*Math.sin(difflat/2)+Math.cos(rlat1)*Math.cos(rlat2)*Math.sin(difflon/2)*Math.sin(difflon/2)));
        return d;
    }

    $(document).on('change','.pickup_location',function(){
        if($(this).is(':checked')){
            var getLat = $(this).data('lat');
            var getLang = $(this).data('lang');
            $('#pickup_lat').val(getLat);
            $('#pickup_lang').val(getLang);
        }
    });

    $(document).on('change','.pickup_location',function(){
        if($(this).is(':checked')){
            $('#delivered_to_you').val('');
            $('#delivered_address_lat').val('');
            $('#delivered_address_long').val('');

            var getPrice = $(this).parent('div').find('.locPrice').text();
            var getLat = $(this).data('lat');
            var getLang = $(this).data('lang');
            var price = getPrice=='Free'? '0.00':getPrice;

            $('#locPriceCal').text(price);
            $('#pickup_lat').val(getLat);
            $('#pickup_lang').val(getLang);
        }
    });

    async function payModal(){
        if($('#bookingForm').valid()){
            var form = $('#bookingForm').serialize();
            await $.ajax({
                type: "post",
                url: "{{ route('booking.getdata') }}",
                data: form,
                success: function (response) {
                    var isJson = IsJsonString(response);
                    var response = (isJson) ? JSON.parse(response):response;
                    if(isJson)
                    {
                        if(!response.status){
                            $.notify({  message: response.msg },{ type: 'danger', timer: 1000 });
                        }else{
                            $('#perDayPrice').text(response.perDayPrice);
                            $('#startDate').text(response.startDate);
                            $('#endDate').text(response.endDate);
                            $('#bookingPrice').text(response.bookingPrice);
                            $('#distancePrice').text(response.distancePrice);
                            $('#extraItem').text(response.extraItem);
                            $('#serviceFee').text(response.siteFee);
                            $('#totalBookingPrice').text(response.totalPrice);
                            $('#strip_key').val(response.stripe_public_key);
                            
                            
                            $('#payModalCenter').modal('show');
                        }
                    }
                    return;
                }
            });
        }
    }

    function convertDatetoJs(checkDate){

        var dateName=parseInt(new Date(checkDate).getDate());
        if(dateName<10){
        dateName="0"+dateName;
        }
        var monthName=parseInt(new Date(checkDate).getMonth());

        monthName =parseInt(monthName)+1;
        if(monthName<10){
        var newMonth="0"+monthName;
        } else {
        var newMonth = monthName;
        }

        var curr_day = dateName;
        var curr_month = newMonth;
        var curr_year = new Date(checkDate).getUTCFullYear();
        var curr_date=curr_year+'-'+curr_month+'-'+curr_day;
        return curr_date;
    }

    function updateDatepiker(obj){
        var dates = obj;
        $('#start_date').datepicker({
            startDate: '-0d',
            format:'mm-dd-yyyy',
            autoclose: true,
            beforeShowDay	: function(checkDate){
                var search = convertDatetoJs(checkDate);
                if(dates.indexOf(search) > -1)
                {
                    return {classes: 'disabled booked'};
                }
                else
                {
                    return {classes: 'available'};
                }
            }
        }).on('changeDate',function() {
            // `e` here contains the extra attributes
            $('#end_date').val('');
            $('#end_date').datepicker('destroy');
            var selectedDate = $(this).val();
            splitDate = selectedDate.split('-');
            selectedDate = splitDate[2]+'-'+splitDate[0]+'-'+splitDate[1];
            selectedDate = convertDatetoJs(selectedDate);
    
            $('#end_date').datepicker({
                format:'mm-dd-yyyy',
                startDate: new Date(selectedDate),
                autoclose: true,
                beforeShowDay	: function(checkDate){
                    var search = convertDatetoJs(checkDate);
                    if(dates.indexOf(search) > -1)
                    {
                        return {classes: 'disabled booked'};
                    }
                    else
                    {
                        return {classes: 'available'};
                    }
                }
            });
        });
    }

    $(document).on('click','.extraitem_checked',function(){
        if($(this).is(':checked')){
            $(this).next().text('Added');
        }else{
            $(this).next().text('Add');
        }
    });

</script>
@php exit; @endphp
