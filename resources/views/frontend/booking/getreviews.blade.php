@if(!empty($reviews))
    @foreach($reviews as $key => $value)
        @php
            
            $profileImage=FRONT_IMG.'/nophoto.png';
            if($value['profile_image']!='')
                $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value['profile_image']);
        @endphp
        <div class="media media-review" data-records="{{ $totalreviews }}">
     
            <div class="media-user"><img src="{{ $profileImage }}" alt=""></div>
            <div class="media-body">
                <div class="M-flex">
                 
                    <h2 class="title"><span> {{  $value['name'] }} </span>{{  date('d-m-Y',strtotime($value['review_dated_on'])) }}</h2>
                    <ul class="rating-star">
                        @for($i = 1; $i <= 5; $i++)
                            <li><span href="Javascript:void(0);" class="star-icon"><img src="{{ FRONT_IMG }}/{{($i <= $value['review_rating'])?'star.svg':'star2.svg' }}"></span></li>
                        @endfor
                    </ul>
                </div>
            
                <div class="description">{!! nl2br($value['review_messsage']) !!}</div>
            </div>
        </div>  
    @endforeach
@endif

@php  exit(); @endphp