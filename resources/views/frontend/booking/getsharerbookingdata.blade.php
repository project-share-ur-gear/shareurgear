@if(!empty($getBookings))
  @foreach($getBookings as $key => $value)
    <tr>
        <td><div class="p-image"><img src="{{ HTTP_UPLOADED_IMAGES_PATH.'/'.$value['primary_image'] }}" alt=""></div></td>
        <td>
          <a href="{{ SITE_HTTP_URL }}/rental-detail-page/{{ $value['booking_product_id'] }}" target="_blank">
            {{ $value['product_title'] }}
          </a>
        </td>
        <td>
          @php
            $profileImage=FRONT_IMG.'/nophoto.png';
            if($value['profile_image']!='')
                $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value['profile_image']);
          @endphp
            <div class="media media-object">
                <img class="media-img" src="{{ $profileImage }}" alt="">
                <div class="review-user">
                  <div class="user-name">{{ $value['name'] }}</div>
                  @if(!empty($value['review_id']))
                    {{--  data-toggle="modal" data-target="#readreviewModalCenter"  --}}
                    <div class="read-R"  onclick="readReview(`{{ ($value['review_rating']*20) }}`,`{{ nl2br($value['review_messsage']) }}`)">Read Review</div>
                  @endif
                </div>
            </div>
        </td>
        <td>{{ date('d-m-Y',strtotime($value['booking_created'])) }}</td>
        <td>{{ date('d-m-Y',strtotime($value['booking_start_date'])) }}</td>
        <td>{{ date('d-m-Y',strtotime($value['booking_end_date'])) }}</td>
        <td>
          ${{ number_format($value['booking_total_price']) }}
          <div class="A-flex" style="text-align: end;">
            <div class="mq-data bkdiv-{{ $value['booking_id'] }}">
              @if($value['booking_approve_status']=='0')
                <a href="Javascript:void(0)" class="manage-avial mb-2" onclick="bookingApproveStatus(`{{ encrypt($value['booking_id']) }}`,'approve')">Approve</a>
                <a href="Javascript:void(0)" class="manage-avial btn btn-danger" onclick="bookingApproveStatus(`{{ encrypt($value['booking_id']) }}`,'decline')">Decline</a>
              @elseif($value['booking_approve_status']=='1' && $value['booking_status']=='1')

                <a href="Javascript:void(0)" class="manage-avial mb-2" onclick="openCheckInOutModal(`{{ encrypt($value['booking_id']) }}`,'checkin')">Check-In</a>
                <a href="Javascript:void(0)" class="manage-avial btn btn-danger" onclick="openCheckInOutModal(`{{ encrypt($value['booking_id']) }}`,'checkout')">Check-Out</a>
                
                @if($value['checkin_status']=='0')
                  <span class="cancelled-booked" onclick="cancelBooking(`{{ encrypt($value['booking_id']) }}`)" >Cancel Booking </span>
                @endif
                
              @elseif($value['booking_approve_status']=='2')
                <span class="cancelled-booked">Booking Decline</span>
              @endif

              @if($value['booking_status']=='2' && $value['booking_approve_status']=='1')
                <span class="cancelled-booked" onclick="viewCancelb(`{{ $value['booking_cancelled_reason'] }}`)">Booking Cancelled</span>
              @endif

              <span class="cancelled-booked" onclick="openReportModal(`{{ encrypt($value['booking_id']) }}`)" >Report Issued</span>
            </div>
          </div>
        </td>
    </tr>
  @endforeach
  @if(!empty($getPages))
    <tr class="border-0">
      <td colspan="7">
        <nav aria-label="Page navigation">
          <ul class="pagination">
            @foreach($getPages as $page => $rec)
              <li class="page-item"><a class="page-link {{ ($page==$activePage)?'active':'' }}"  onclick="pagination(`{{ $page }}`,`{{ $rec }}`)" data-rec="{{ $rec }}" href="Javascript:void(0)">{{ $page }}</a></li>
            @endforeach
          </ul>
        </nav>
      </td>
    </tr>
  @endif
@endif



@php exit(); @endphp;