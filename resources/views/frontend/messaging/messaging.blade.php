
@php 
    if($get_userid==''){
        $get_userid='';
    }
    $loggedInData_ID=$loggedInUserData->id;
@endphp

@extends('layouts.app')
@section('content')

<style>
    footer{ display: none;}
    .main {
        top: 50%;
        left: 50%;
        display: flex;
        transform: translate(-50%, -50%);
        position: absolute;
    } 

    .main div {
        margin: 10px;
        width: 50px;
        height: 50px;
        background-color: #f44336;
        border-radius: 50%;
        animation-duration: 0.8s;
        animation-iteration-count: infinite;
    }

    .one {
        animation-name: load-one;
    }
    .blockOverlay{ background-color: rgb(0 0 0 / 29%) !important;}

    @keyframes load-one {
        30% {
            transform: translateY(-50%);
        }
    }

    .two {
        animation-name: load-two;
    }

    @keyframes load-two {
        50% {
            transform: translateY(-50%);
        }
    }

    .three {
        animation-name: load-three;
    }

    @keyframes load-three {
        70% {
            transform: translateY(-50%);
        }
    }

    .message-window .right-box .text_wrapper .text{
        word-break: break-all;
    }
    .message-window .left-box .text_wrapper .text{
        word-break: break-all;
    }
    .user-profile-block{z-index:9999999999;}
</style>


<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">
    @include('sidebar')
    <section class="profile-page login-page">
        <div class="container">
            <!--  -->
            <h2 class="login-title">Messaging</h2>
        
            <div class="group-chat">
                <div class=" massage-top mobchatback" id="mobchatback" style="display:none"   >
                    <a href="javascript:void(0)" class="back-btn btn close_chat_window "  > <i class="fas fa-arrow-circle-left"></i> Back </a>
                </div>
                
                <div class="online-users"  data-scrollbar>
                    <div class="userhide">
                        <div class="onlnusers">
                            <div id="contacts-list">
                            
                            </div>
                        </div>    
                    </div>
                </div>

                <!----------------- chat user -------------------->
                <div class="chat-box">    
                    <div class="message-window" id="messageBody">
                        <div class="msgwindow" >
                            <div class="chat-paddings">
                            </div>
                        </div>
                    </div>

                    <div class="message-type-window">
                        <form>
                            <div class="input-group">
                                <textarea rows="3" name="chatMessage" id="chatMessage" class="form-control" placeholder="write the text"></textarea>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="send_msg"> Send </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!--------------------- chat box -------------------------------------->
            </div>
            <!---------------------------- message box --------------------->
        </div>
    </section>
</div>
 
<script src="{{ asset('public/js/smooth-scrollbar.js') }}"></script>
<script src="{{ asset('public/plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.js" integrity="sha512-QSb5le+VXUEVEQbfljCv8vPnfSbVoBF/iE+c6MqDDqvmzqnr4KL04qdQMCm0fJvC3gCWMpoYhmvKBFqm1Z4c9A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
var activeUserId = "{{$get_userid}}";//{{$get_userid}}
var topMessageId = '';    //lower message id (SET)
var bottomMessageId = '';   //higher message id (SET)
var loggedInClientId = "{{$loggedInData_ID}}";
var loadActiveContactFlag = false;
var hasTriggerredFirstUserForMsg = false;
var before_sh = "";
var lastCheckUserId= "";
var lastJQueryTS = 0;

$(document).ready(function() {
    $.fn.getMessageContactsList();	
});

// get contacts list for left panel
$.fn.getMessageContactsList = function(){ 
    var getUserList = $('.user-profile-block').length;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: APPLICATION_URL+'/messagecontactslist/'+'{{$loggedInData_ID}}',
        method: 'POST',
        dataType: 'json',
        data:{
            activeUserId: activeUserId,
            getUserList:getUserList
        },
        success:function(response) {
            if(response!=null)
            {
                if(response.status==200){
                    if(response.data.length > 0){
                        var userProfileBlock = '';
                        for(var u=0; u < response.data.length; u++){
                            var classActive = '';
                            if(activeUserId==response.data[u].id){
                                classActive = 'active';
                            }
                            userProfileBlock += '<div class="user-section user_click_ user-profile-block user-'+response.data[u].id+'  '+classActive+'" userid="'+response.data[u].id+'" lastMsgID="'+response.data[u].lastMessage+'" >' ;
                            userProfileBlock += '<div class="media media_user align-items-center">';
                            userProfileBlock += '<div class="img-user data_object_status">';
                            userProfileBlock += '<img src="'+response.data[u].profile_image+'"/></div>';
                            userProfileBlock += '<div class="media-body">';
                            userProfileBlock += '<p class="user-name">'+response.data[u].name+'</p>';
                            userProfileBlock += '</div>';
                            userProfileBlock += '</div>';
                            if(parseInt(response.data[u].UnreadMessages) > 0){
                                var UnreadMessages = parseInt(response.data[u].UnreadMessages);
                                if(UnreadMessages > 99){
                                    UnreadMessages = '99+';
                                }
                                userProfileBlock += '<div class="mr-number">'+UnreadMessages+' </div>';
                            }
                            userProfileBlock += '</div>';
                        }

                        $('#contacts-list').html(userProfileBlock);

                        selectUser(activeUserId);
                        
                    }else{
                        $('#contacts-list').html('');
                    }
                                    
                }else if(response.status==404){
                    $.notify({
                        message: "Invalid Request"
                        },{
                        type: 'danger',
                        timer: 1000
                    });
                    window.location.href = APPLICATION_URL;
                }  else{
                    showAppAlert('Information', response.message, 'info');
                }
            }
        }, 
    });
}

var selectedUser=true;
function selectUser(uid){
    setTimeout(function(){  
        if(selectedUser && uid!=''){
            $('.user-'+uid).trigger('click');
            selectedUser = false;
        }
    }, 1000);
}

setInterval(function(){
    $.fn.getMessageContactsList();
}, 10000);
	
// fetch NEW messages to prepend by interval (RECEIVED FROM OTHER USERS)

setInterval(function(){
    if(activeUserId!='' && activeUserId!='0' && bottomMessageId!=''){ // (SET)
        var st = $('.message-window').scrollTop();
        var ih = $('.message-window').innerHeight();
        var sh = $('.message-window')[0].scrollHeight;
        if (st + ih >=  sh) {
            $.fn.getUserMessages('1');
        }
    }
    $.fn.refreshTime(); //refresh messages time
}, 10000);

// get messages of selected user
$.fn.getUserMessages = function(chk_new_msg){

    var send = true;
    if (typeof(event) == 'object'){
        if (event.timeStamp - lastJQueryTS < 500){
            send = false;
        }
        lastJQueryTS = event.timeStamp;
    }

    if(send)
    { 
        $.ajax({
            url:APPLICATION_URL+'/getusermesssage/'+'{{$loggedInData_ID}}',
            method: 'POST',
            dataType: 'json',
            async: false,
            data:{
                activeUserId: activeUserId,
                topMessageId: topMessageId,
                bottomMessageId: bottomMessageId,
                checkNewMessages: chk_new_msg,
            },
            success:function(response) {
                if(response.status==200){
                    unBlockUi();
                            
                    if(response.data.length > 0){
                        var msg_data = '';
                        for(var m=(response.data.length-1); m >= 0; m--){

                            // set latest / lastest message id (SET)
                            if(bottomMessageId==''){
                                bottomMessageId = response.data[m].message_id;
                            }

                            if((response.data.length-1)==m){
                                topMessageId = response.data[m].message_id; // (SET)
                            }

                            // no need to add message in message list
                            // when user posts a new message and at the same time 
                            // system fetches that new message then same message
                            // should not be displayed in message board.
                            var msgid = response.data[m].message_id;
                            var msg_id_length = $('.msg_box[msgid="'+msgid+'"]').length;

                            if(msg_id_length == 0){
                                if(response.data[m].user_id != loggedInClientId){
                                    // receiver is logged in user
                                    msg_data += '<div class="left-box msg_box" time="'+response.data[m].message_time+'" id="msg'+response.data[m].message_id+'" msgid="'+response.data[m].message_id+'">';
                                    msg_data += '<div class="avatar"><img src="'+response.data[m].profile_image+'"></div>';
                                    msg_data += '<div class="text_wrapper">';
                                    msg_data += '<div class="text">'+response.data[m].message+'</div>';
                                    msg_data += '<p class="date_day dates chat_time">&nbsp;</p>';
                                    msg_data += '</div>';
                                    msg_data += '</div>';
                                    
                                } else {
                                
                                    // receiver is not logged in user
                                    msg_data += '<div class="right-box msg_box" time="'+response.data[m].message_time+'" id="msg'+response.data[m].message_id+'" msgid="'+response.data[m].message_id+'">';
                                    msg_data += '<div class="text_wrapper">';
                                    msg_data += '<div class="text">'+response.data[m].message+'</div>';
                                    msg_data += '<p class="date_day dates chat_time">&nbsp;</p>';
                                    msg_data += '</div>';
                                    msg_data += '<div class="avatar"><img src="'+response.data[m].profile_image+'"></div>';
                                    msg_data += '</div>';
                                }
                            }
                        }

                        if(chk_new_msg=='1'){
                            // new messages will be appended
                            bottomMessageId = response.data[0].message_id; // (SET)
                            var st = $('.message-window').scrollTop();
                            var ih = $('.message-window').innerHeight();
                            var sh = $('.message-window')[0].scrollHeight;
                            if (st + ih >=  sh) {
                                $('.chat-paddings').append(msg_data);  // (SET)
                                setTimeout(function(){
                                    var scrollTo_val = $('.message-window').prop('scrollHeight') + 'px';
                                    $('.message-window').slimScroll({ scrollTo : scrollTo_val });
                                }, 1000);
                            }

                        } else {
                            // first load and old messages will appended
                            $('.chat-paddings').prepend(msg_data); // (SET) 
                            if(chk_new_msg){
                                if(response.unreadcount!=''){
                                    if(response.unreadcount.count > '0'){
                                        setTimeout(function(){
                                            $('.message-window').slimScroll({ start: 'top' });
                                        }, 1000);
                                        lastCheckUserId = activeUserId;
                                    }
                                }else{
                                    
                                    if(lastCheckUserId != activeUserId){
                                        setTimeout(function(){
                                            var scrollTo_val = $('.message-window').prop('scrollHeight') + 'px';
                                            $('.message-window').slimScroll({ scrollTo : scrollTo_val });
                                        }, 1000);
                                        lastCheckUserId = activeUserId;
                                    }
                                }   
                            }  
                                       
                        }
                        if(before_sh==''){
                            before_sh = $('.message-window')[0].scrollHeight;
                        }
                    }

                } else if(response.status==404){
                    window.location.href = APPLICATION_URL;
                } else if(response.status==440){
                    // session time out
                    window.location.href = APPLICATION_URL+'/account-setting';
                } else {
                    showAppAlert('Information', response.message, 'info');
                }

                $.fn.refreshTime();
            },
            error:function(response){
                //window.location.href = APPLICATION_URL;
            }
        });
    }
}


// refresh time     
$.fn.refreshTime = function() {
    $('.msg_box').each(function(){
        var element_datetime = $(this).attr('time');
        var dateFuture = new Date(element_datetime);
        var dateNow = new Date();
        var seconds = Math.abs(Math.floor((dateFuture - (dateNow))/1000));
        var minutes = Math.floor(seconds/60);
        var hours = Math.floor(minutes/60);
        var days = Math.floor(hours/24);
        hours = hours-(days*24);
        minutes = minutes-(days*24*60)-(hours*60);
        seconds = (seconds-(days*24*60*60)-(hours*60*60)-(minutes*60));

        const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        const weekdayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

        var datetimesuffix = '';
        var xdiff = '';
        if(days > 0){
            if(days > 1){
                var old_date = weekdayNames[dateFuture.getDay()] + ', ' + monthNames[dateFuture.getMonth()] + ' ' + dateFuture.getDate() + ', ' + dateFuture.getFullYear();
                xdiff = old_date;
            } else {
                xdiff = 'Yesterday';
                // xdiff = '1 day ago';
            }
        } else if(hours > 0){
            if(hours > 1){
                xdiff = hours + ' hours ago';
            } else {
                xdiff = '1 hour ago';
            }
        } else if(minutes > 0){
            if(minutes > 1){
                xdiff = minutes + ' mins. ago';
            } else {
                xdiff = '1 min. ago';
            }
        } else if(seconds < 60){
            xdiff = 'a moment ago';
        }
        // var xdiff = days + ' days ' + hours + ' hours ' + minutes + ' minutes ' + seconds + ' seconds ';
        $(this).find('.chat_time').html(xdiff);
    });
};

// contact list
$('.online-users').slimscroll({
    height		: '515px',
    railColor 	: 'red',
    color        : '#8a71b2',
    opacity      : 1,        //600px
    alwaysVisible: false,
});

// messages window
$('.message-window').slimscroll({
    height		: '450px',
    start		: 'bottom',
    railColor 	: 'red',
    color        : '#8a71b2',
    opacity      : 1,        //600px
    alwaysVisible: true
});

//slim scroll end
$('.message-window').on('scroll', function() { 
    if ($(this).scrollTop() < 10) {
        $.fn.getUserMessages(2);
        var after_sh = $('.message-window')[0].scrollHeight;
        var height = $('.msg_box').height();
        var new_top = after_sh - before_sh - height * parseInt(5);
        $('.message-window').slimScroll({ scrollTo : new_top + 'px' });
        if(before_sh!=after_sh){
            before_sh = after_sh;
        }
    } 
});

$.fn.postNewMessage = function(){ 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // --------------- special case ---------------------------------------------
    // when user has scrolled messages, received new messages from other user
    // at the same time without scrolling up logged in user posted a new message
    // then fetch other users sent messages first then post logged in user message
    
    if(activeUserId!='' && activeUserId!='0' && bottomMessageId!=''){ 
        // (SET)
        // set scroll position to top in this case
        // $('.message-window').slimScroll({ scrollTo : '0px' }); 
        // auto to bottom
        var scrollTo_val = $('.message-window').prop('scrollHeight') + 'px';
        $('.message-window').slimScroll({ scrollTo : scrollTo_val });
    }
    //------------------
    var x = $('#chatMessage').val();
    var y = x.split(/\s/).join('|||||');
    x = x.trimRight('|||||');
    $('#chatMessage').val(x);

    var message = $('#chatMessage').val();

    $('#chatMessage').val('');  // blank chat message element value and ready to post 

    $.ajax({
        url: APPLICATION_URL+'/postnewmsg/'+'{{$loggedInData_ID}}',
        method: 'POST',
        dataType: 'json',
        async: false,
        data:{
            message: $.trim(message),
            activeClientId: activeUserId, //receiver id
            
            // attachmentFileNameOriginal: attachment_file_name_original,
            // newAttachmentFileName: new_attachment_file_name,
        },
        success:function(response) {
            
            if(response.status==200){
                var msg_data = '';
                if(response.data.message_id){
                    // set latest / lastest message id
                    bottomMessageId = response.data.message_id;     // (SET)
                    // no need to add message in message list
                    // when user posts a new message and at the same time 
                    // system fetches that new message then same message
                    // should not be displayed in message board.
                    var msgid = response.data.message_id;
                    var msg_id_length = $('.msg_box[msgid="'+msgid+'"]').length;

                    if(msg_id_length == 0){
                        
                        var msg_data = '';
                        // receiver is not logged in user
                        msg_data += '<div class="right-box msg_box" time="'+response.data.message_time+'" msgid="'+response.data.message_id+'">';
                        msg_data += '<div class="text_wrapper">';
                        msg_data += '<div class="text">'+response.data.message+'</div>';
                        msg_data += '<p class="date_day dates chat_time">&nbsp;</p>';
                        msg_data += '</div>';
                        msg_data += '<div class="avatar"><img src="'+response.data.profile_image+'"></div>';
                        msg_data += '</div>';
                        $('.chat-paddings').append(msg_data);  // (SET)
                    }
                    $('#chatMessage').val('');
                    // auto to bottom
                    var scrollTo_val = $('.message-window').prop('scrollHeight') + 'px';
                    $('.message-window').slimScroll({ scrollTo : scrollTo_val });
                }   
            }

            var temp_enc_get_artistid = loggedInClientId;
            if(temp_enc_get_artistid!='' && temp_enc_get_artistid!=activeUserId){
                loadActiveContactFlag = true;
            }
            else if(response.status==404){
                window.location.href = APPLICATION_URL;
            } else if(response.status==440){
                showAppAlert('Information', response.message, 'info',false);
                window.location.href = response.data.redirect_url;
            } else if(response.status==441){
                // requested user not exists
                window.location.href = response.data.redirect_url;
            } else {
                showAppAlert('Information', response.message, 'info');
                $('#chatMessage').val(message);
            }
        },
    

    });
}

$('#send_msg').on('click', function(){
    if($.fn.checkMessage()){
        $.fn.postNewMessage();
    }
});

// check messsage 
$.fn.checkMessage = function(){
    if(activeUserId == ''){
        showAppAlert('Error', 'Please select receiver', 'error');
        return false;
    }
    // check message when user does not upload a file and also not entered chat message
    var msg = $('#chatMessage').val();
    msg = $.trim(msg);
    if(msg==''){
        showAppAlert('Error', 'please enter your message', 'error');
        $('#chatMessage').val('');
        $('#chatMessage').focus();
        return false;
    } else {
        if(msg.length > 20000){
            showAppAlert('Error', 'maximum 20000 char allow in message', 'error');
            $('#chatMessage').focus();
            return false;
        }
    }

    return true;
}

$('#chatMessage').on('keypress', function(e){
    if($(window).width() < 768){
    }else{
        var key = e.keyCode;
        var skey = e.shiftKey;
        if (key == 13 && !e.shiftKey) {	
            if($.fn.checkMessage()){
                // console.log(skey);
                // console.log('submitted');
                $.fn.postNewMessage();
            }
        }else{
            //new line
        }
    }
});

if($(window).width() < 991){
    $('#mobchatback').hide();
    $('.chat-box').hide();
        
    $('#mobchatback').on('click', function(e){
        $('.user-profile-block').removeClass('active');
        $('.chat-box').hide();
        $('#mobchatback').hide();
        $('.online-users').parent('div').show();
        activeUserId = "";
    });

    $(document).on('click touchstart',".user-profile-block", function (){
        blockUi();
        // when new user selected from left panel
        before_sh="";
        topMessageId = '';    //higher message id
        bottomMessageId = '';   //lower message id
        $('.user-profile-block').removeClass('active'); // remove from all
        $('.chat-paddings').html('');
        $(this).addClass('active');
        activeUserId = $(this).attr('userid');
        $('.online-users').parent('div').hide();
        $('.chat-box').show();
        $('#mobchatback').show();
        $.fn.getUserMessages(); // call first user messages by default

        // auto to top
        // auto to bottom
        setTimeout(function(){
            var scrollTo_int =  $('.message-window').prop('scrollHeight') + 'px';
            $('.message-window').slimScroll({ scrollTo : scrollTo_int, start: 'bottom' });
            $('html','body').animate({scrollTop: $(".message-type-window").position().top}, 800, 'swing');
        },500);
    });

}else{
    // on click contact list
    $(document).on('click', '.user-profile-block', function(e){
        blockUi();
        before_sh="";
        topMessageId = '';    //higher message id
        bottomMessageId = '';   //lower message id
        $('.user-profile-block').removeClass('active'); // remove from all
        $('.chat-paddings').html('');
        $(this).addClass('active');
        activeUserId = $(this).attr('userid');
        $.fn.getUserMessages(); // call first user messages by default

        /*
        // desktop
        var scrollTo_val = $('.message-window').prop('scrollHeight') + 'px';
        $('.message-window').slimScroll({ scrollTo : scrollTo_val });*/

        if($(window).width() < 768){
            // mobile devices
            var posX = $('#chatMessage').offset().left;
            var posY = $('#chatMessage').offset().top - 50;
            $('html, body').animate({
                scrollTop: posY
            }, 2000, function(){
                $('#chatMessage').focus();
            });                
        } else {
            // desktop
            $('#chatMessage').focus();
        }
    });
}

/*
$(window).resize(function() {
    if($(window).width() < 991){
        $('#mobchatback').hide();
        $('.chat-box').hide();
            
        $('#mobchatback').on('click', function(e){
            $('.user-profile-block').removeClass('active');
            $('.chat-box').hide();
            $('#mobchatback').hide();
            $('.online-users').parent('div').show();
            activeUserId = "";
        });

        $(document).on('click touch',".user-profile-block", function (){
            blockUi();
            // when new user selected from left panel
            topMessageId = '';    //higher message id
            bottomMessageId = '';   //lower message id
            $('.user-profile-block').removeClass('active'); // remove from all
            $('.chat-paddings').html('');
            $(this).addClass('active');
            activeUserId = $(this).attr('userid');
            $('.online-users').parent('div').hide();
            $('.chat-box').show();
            $('#mobchatback').show();
            $.fn.getUserMessages(); // call first user messages by default

            // auto to top
            // auto to bottom            
        });

    }else{
        $('#mobchatback').hide();
        $('.chat-box').show();
        $('.online-users').parent('div').show();
        // on click contact list
        $(document).on('click', '.user-profile-block', function(e){
            blockUi();
            topMessageId = '';    //higher message id
            bottomMessageId = '';   //lower message id
            $('.user-profile-block').removeClass('active'); // remove from all
            $('.chat-paddings').html('');
            $(this).addClass('active');
            activeUserId = $(this).attr('userid');
            $.fn.getUserMessages(); // call first user messages by default
            if($(window).width() < 768){
                // mobile devices
                var posX = $('#chatMessage').offset().left;
                var posY = $('#chatMessage').offset().top - 50;
                $('html, body').animate({
                    scrollTop: posY
                }, 2000, function(){
                    $('#chatMessage').focus();
                });                
            } else {
                // desktop
                $('#chatMessage').focus();
            }
        });
    }
});*/

function blockUi(){
    $.blockUI({
        css: {
            border: 'none',
            padding: '5px',
            width: '24%',
            left: '39%',
            backgroundColor: '#ca000100',
            opacity: .9,
            color: '#ffffff',
        },
        message: `<div class='Loader'><div class="spinner-border text-dark" role="status" style="width: 3rem;height: 3rem;color: #71119b!important;"><span class="sr-only">Loading...</span></div></div>`,
    });
}

function unBlockUi(){
    $.unblockUI();
}
</script>
@endsection()