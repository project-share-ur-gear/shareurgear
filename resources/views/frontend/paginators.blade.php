@if ($paginator->hasPages())
    <ul class="pagination-custom text-center">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            
        @else
            <li class="pag-link previous">
                <a href="{{ $paginator->previousPageUrl() }}" rel="prev">
                    <img src="{{ FRONT_IMG.'/arrow-down-sign-to-navigate.svg' }}">
                </a>
            </li>
        @endif


        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="pag-link disabled"><span>{{ $element }}</span></li>
            @endif


            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="pag-link active"><a>{{ $page }}</a></li>
                    @else
                        <li class="pag-link"><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach


        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="pag-link next">
                <a class="text-dark-grey" href="{{ $paginator->nextPageUrl() }}" rel="next">
                    <img src="{{ FRONT_IMG.'/arrow-down-sign-to-navigate.svg' }}">
                </a>
            </li>
        @else
            <li class="pag-link next disabled">
                <a>
                    <img src="{{ FRONT_IMG.'/arrow-down-sign-to-navigate.svg' }}">
                </a>
            </li>
        @endif
    </ul>
@endif