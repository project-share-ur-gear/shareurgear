<style type="text/css">
.modal-header .modal-title {
    font-size: 20px;
    text-align: center;
    display: inline-block;
    width: 100%;
    margin-top: 18px;
    font-weight: bold;
    margin-bottom: 10px;
}
.review-lefty{position: absolute;bottom: -20px;width: 100%;padding-top: 18px;}
@if(Auth::guard('user')->user())
.imgProgress-img{background-image:url('{{ getUserImage(Auth::guard('user')->user()->profile_image,'thumb') }}' ) !important;background-size: cover !important;background-color: #FFF !important; }
@endif
</style>

@php
$requestPath = explode('/',Request::path());
$requestPath = $requestPath[0];
$fullPath = Request::path();
$uId = Auth::guard('user')->id();
$userData = Auth::guard('user')->user();
$activeD = '';
$locale=App::getLocale();
if($userData->type=='provider'){
    $services = DB::table('user_services')
                ->where('us_user_id',$userData->id)->get()->all();

    $experience = DB::table('user_experiences')
                ->where('ue_user_id',$userData->id)->get()->all();
    $certification = DB::table('user_certification')
                ->where('uc_user_id',$userData->id)->get()->all();
    if($experience && $certification){
        $userData['experience'] = true;
    }
    if($services){
        $userData['services'] = true;
    }
}

//$ratings = DB::table('');

$userProfilePercentager = profilePercentageCalculator($userData);

//prd($site_configs['percentage_modal_text_'.$locale]);
//prd($userProfilePercentager);
@endphp


<div class="left_account">
    <div class="user-profiles">
        <div class="user_img_section">
            <div class="user_position">
                <div class="demo"></div>
                <div class="edit_profile edit_profile-bg" onclick="openProfileImageModal()">
                    <img src="{{ FRONT_IMG.'/camera.svg' }}" />
                </div>
                <div class="profile_completion" data-toggle="modal" data-target="#profileModalCenter">
                    <div class="edit_profile edit_profile-bg1">{{ $userProfilePercentager['percentage'] }}%</div>
                    <span>{{ __('general.profile') }} <br> {{ __('general.complete') }}</span>
                </div>
            </div>
        </div>
        <div class="user_data_link">
            <p class="user_name">
                {{ getNameOnTypeBasis(Auth::guard('user')->user(),'full') }}
            </p>
            @php
            $starRating = userAvgCalculator($userData->id,'full');
            @endphp
            <ul class="rating-star">
                <li>
                    <input type="hidden" class="rating" value="{{ $starRating['overall'] }}">
                </li>
            </ul>
            <a data-jobu='{{ base64_encode(Auth::guard('user')->user()->id) }}' href="javascript:void(0)" class="rating_review review-lefty reviewpopper" style="cursor: pointer;text-decoration: none;">{{ $starRating['count'] }} {{ __('general.Reviews') }}</a>
        </div>
    </div>
    <div class="left_profiles_data">
        <div class="custom_information">
            <div class="side-items edit_profiles">
                <a class="accordion-toggle toggle-switch side-link {{ $requestPath!='user-profile'?'collapsed':'active' }}" data-toggle="collapse" href="#submenu-2" aria-expanded="false">
                    <span class="sidebar-title">{{ __('general.edit_profile') }}</span>
                    <i class="fas fa-caret-down"></i>
                </a>
                <ul id="submenu-2" class="panel-collapse panel-switch collapse {{ $requestPath=='user-profile'?'show':'' }}" role="menu" style="">
                    <li><a href="{{ SITE_HTTP_URL.'/user-profile?type=edit' }}" class="side-sub-link active" data-id="edit">{{ __('general.personal_information') }}</a></li>
                    <li><a href="{{ SITE_HTTP_URL.'/user-profile?type=password' }}" class="side-sub-link" data-id="password">{{ __('general.change_password') }}</a></li>
                    @if(Auth::guard('user')->user()->type=='provider')

                    <li><a href="{{ SITE_HTTP_URL.'/user-profile?type=experience' }}" class="side-sub-link" data-id="experience">{{ __('general.experience_certification') }}</a></li>
                    @endif
                    <li><a href="{{ SITE_HTTP_URL.'/user-profile?type=payout' }}" class="side-sub-link" data-id="payout">{{ __('general.banking_payout_information') }}</a></li>
                    <li><a href="{{ SITE_HTTP_URL.'/user-profile?type=background' }}" class="side-sub-link" data-id="background">{{ __('general.background_check') }} </a></li>

                    <li>
                        <a href="javascript:void(0)" class="side-sub-link" onclick="deleteAccount(this)" data-message="{{ __('general.Are you sure you want to delete your account?')}}</b><br/>{{ __('general.Once you delete your account, all the information will be lost forever.')}}" data-value="" data-action="{{ url('/user/deleteaccount') }}">
                        {{ __('general.deleteacct') }} 
                        </a>
                    </li>

                </ul>
            </div>
        </div>
        <ul class="list_items">
            <li>
                <a href="{{ SITE_HTTP_URL.'/user/settings' }}" class="list_data_info {{ $fullPath=='user/settings'?'active':'' }}">
                {{ __('general.settings') }}  
                </a>
            </li>
            @if(Auth::guard('user')->user()->type=='provider')
                <li>
                    <a href="{{ SITE_HTTP_URL.'/my-services' }}" class="list_data_info {{ $fullPath=='my-services'?'active':'' }}"> {{ __('general.My Services') }} </a>
                </li>
                <li>
                    <a href="{{ SITE_HTTP_URL.'/provider/jobs' }}" class="list_data_info {{ $fullPath=='provider/jobs'?'active':'' }}">
                    {{ __('general.My Jobs') }} 
                    </a>
                </li>
                <li>
                    <?php
                    if($fullPath=='my-bookings' || $requestPath=='booking-details'){
                        $activeD = 'active';
                    }
                    ?>
                    <a href="{{ SITE_HTTP_URL.'/my-bookings' }}" class="list_data_info {{ $activeD }} ">{{ __('general.mybookings') }}  </a>
                </li>
                <li>
                    <a href="{{ SITE_HTTP_URL.'/my-schedule' }}" class="list_data_info {{ $fullPath=='my-schedule'?'active':'' }}">
                    {{ __('general.mycalendar') }} 
                    </a>
                </li>
                <li>
                    <a href="{{ SITE_HTTP_URL }}/message" class="list_data_info {{ $fullPath=='message'?'active':'' }}">
                    {{ __('general.Messaging') }} 
                    </a>
                </li>
            @else
                <li>
                    <a href="{{ SITE_HTTP_URL.'/my-favourites' }}" class="list_data_info {{ $fullPath=='my-favourites'?'active':'' }}">
                    {{ __('general.favorite') }}  
                    </a>
                </li>
                <li>
                    <a href="{{ SITE_HTTP_URL.'/my-jobs' }}" class="list_data_info {{ $fullPath=='my-jobs'?'active':'' }}">
                    {{ __('general.My Jobs') }}  
                    </a>
                </li>
                <li>
                    <?php
                    if($fullPath=='my-bookings' || $requestPath=='booking-details'){
                        $activeD = 'active';
                    }
                    ?>
                    <a href="{{ SITE_HTTP_URL.'/my-bookings' }}" class="list_data_info {{ $activeD }}">
                    {{ __('general.mybookings') }}  
                    </a>
                </li>

                <li>
                    <a href="{{ SITE_HTTP_URL.'/my-schedule' }}" class="list_data_info {{ $fullPath=='my-schedule'?'active':'' }}">
                    {{ __('general.mycalendar') }}   
                    </a>
                </li>
                <li>
                    <a href="{{ SITE_HTTP_URL }}/message" class="list_data_info {{ $fullPath=='message'?'active':'' }}">
                    {{ __('general.Messaging') }}   
                    </a>
                </li>
            @endif
            {{-- <li><a href="#" class="list_data_info">Link 1 </a></li>
            <li><a href="#" class="list_data_info">Link 2 </a></li> --}}

            <li>
                <?php
                $activeX = '';
                if($fullPath=='tickets' || $requestPath=='view-ticket-detail'){
                    $activeX = 'active';
                }
                ?>
                <a href="{{ SITE_HTTP_URL.'/tickets' }}" class="list_data_info {{ $activeX }}">
                {{ __('general.tickets') }}  
                </a>
            </li>

            <li>
                <a href="{{ SITE_HTTP_URL }}/my-wallet" class="list_data_info {{ $fullPath=='my-wallet'?'active':'' }}">
                {{ __('general.mywallet') }}  
                </a>
            </li>

            <li>
                <a href="{{ SITE_HTTP_URL }}/my-referrals" class="list_data_info {{ $fullPath=='my-referrals'?'active':'' }}">
                {{ __('general.referalandearnings') }} 
                </a>
            </li>
            
            <li><a href="javascript:void(0)" class="list_data_info" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('general.header_logout') }}  </a></li>
        </ul>
    </div>
</div>

<div class="modal upload-img-modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-500" id="modal-title">{{ __('general.Upload Profile Image') }} </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/user/saveprofileimage', 'method' => 'post','name' => 'imageForm', 'id' => 'imageForm','enctype'=> 'multipart/form-data']) !!}
                    @method('PATCH')
                    @csrf
                    <div class="d-block">
                        <div class="image-uploading text-center">
                            @php
                                $profileImage=FRONT_IMG.'/image-upload.png';
                                if(Auth::guard('user')->user()->profile_image!='')
                                    $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.Auth::guard('user')->user()->profile_image);
                            @endphp
                            <div class="modalimagebox">
                                <div class="profileImageBox" id="profile_image_preview" style="background-image:url({{$profileImage}})"></div>
                            </div>
                            <div class="d-none hide">{!! Form::file('profile_image',array('id'=>'profile_image','class'=>'required')) !!}</div>
                            <div>
                                <button class="btn btn-primary image-upload-btn mt-3" type="button" onclick="uploadProfileImage()"><img style="max-width: 20px" src="{{ FRONT_IMG.'/camera.svg' }}" class="searchimg"/> {{ __('general.UPLOAD IMAGE') }}</button>
                            </div>
                            <p class="text-dark-grey mt-3">{{ __('general.Allowed file format jpg, png') }}</p>
                        </div><!--image=-uploading-->
                        <div class="btn-box text-center">
                            <button style="display: inline-block;" class="provider-btn register-bg prev-btn" data-dismiss="modal">{{ __('general.cancel') }}</button>
                            <button style="display: inline-block;" class="provider-btn register-bg" onClick="saveProfileImage()" type="button">{{ __('general.save') }}</button>
                        </div>
                        <div class="Clear"></div>
                    </div>
                {!! Form::close() !!} 
            </div>

        </div>
    </div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-500" id="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '', 'method' => 'post','name' => 'deleteForm', 'id' => 'deleteForm']) !!}
                    <div class="btn-box text-center">
                        <input type="hidden" name="value" id="value" />
                        <button class="btn  cancel-btn skip-btn" data-dismiss="modal">{{ __('general.cancel') }}</button>
                        <button class="btn btn-primary find-btn" onClick="deleteRecords()" type="button">{{ __('general.ok') }}</button>
                    </div>
                {!! Form::close() !!} 
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="profileModalCenter" tabindex="-1" role="dialog" aria-labelledby="profileModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <h5 class="modal-title" id="profileModalLongTitle">Profile Complete 50%</h5> -->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="profile_data">
            <h5 class="title">{{ __('general.Profile complete') }} {{ $userProfilePercentager['percentage'] }}%</h5>
            <p class="description">{{ $site_configs['percentage_modal_text_'.$locale] }}</p>
            <ul class="listing-profiles">
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['id_proof']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.ID proof') }} </p>
                    <a href="#" class="complete-btn active" disabled="disabled" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.ID proof') }} </p>
                    <a href="{{ SITE_HTTP_URL.'/user-profile?type=background' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['bg_check_status']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.bgcheck') }} </p>
                    <a href="javascript:void(0)" class="complete-btn active" disabled="disabled" style="cursor: not-allowed;">
                    {{ __('general.completed') }}
                    </a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.bgcheck') }}  </p>
                    <a href="{{ SITE_HTTP_URL.'/user-profile?type=background' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['email_verified']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.email') }}   </p>
                    <a href="javascript:void(0)" class="complete-btn active" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.email') }}  </p>
                    <a href="{{ SITE_HTTP_URL.'/user-profile?type=edit' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['phone_number']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.phoneno_field_label') }}   </p>
                    <a href="javascript:void(0)" class="complete-btn active" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.phoneno_field_label') }} </p>
                    <a href="{{ SITE_HTTP_URL.'/user-profile?type=edit' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['profile_image']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.photo') }}  </p>
                    <a href="javascript:void(0)" class="complete-btn active" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.photo') }} </p>
                    <a href="javascript:void(0)" onclick="openProfileImageModal()" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                @if(Auth::guard('user')->user()->type=="provider")
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['stripe_account_id']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.payout_info') }}  </p>
                    <a href="javascript:void(0)" class="complete-btn active" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.payout_info') }} </p>
                    <a href="{{ SITE_HTTP_URL.'/user-profile?type=payout' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['experience']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.experience_certification') }}  </p>
                    <a href="javascript:void(0)" class="complete-btn active" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.experience_certification') }}  </p>
                    <a href="{{ SITE_HTTP_URL.'/user-profile?type=experience' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                <li class="lisying-items">
                    @if(!empty($userProfilePercentager['services']))
                    <div class="complete-circle"></div>
                    <p class="pro-items">{{ __('general.services') }}  </p>
                    <a href="javascript:void(0)" class="complete-btn active" style="cursor: not-allowed;">{{ __('general.completed') }}</a>
                    @else
                    <div class="empty-circle"></div>
                    <p class="pro-items">{{ __('general.services') }} </p>
                    <a href="{{ SITE_HTTP_URL.'/add-service' }}" class="complete-btn">{{ __('general.complete') }}</a>
                    @endif
                </li>
                @endif
            </ul>
        </div>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
$(function(){
    $(".demo").imgProgress({
        size: 200,
        barSize: 12,
        percent: '{{ $userProfilePercentager['percentage'] }}'
    });

    $(".rating").rating({
        size        : 'xs',
        theme       : 'krajee-fas',
        showClear   : false,
        showCaption : false,
        disabled    : true,
    });

    // $("#star-val").rating({
    //     step:'0.1',
    //     theme:'krajee-svg',
    //     size: 'xs',
    //     filledStar: '<span class="krajee-icon krajee-icon-star"></span>',
    //     emptyStar: '<span class="krajee-icon krajee-icon-star"></span>',
    // });
})
</script>