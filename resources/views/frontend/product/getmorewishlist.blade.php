        @if(!empty($wishlist_data))
            @foreach($wishlist_data as $key =>$value)
                <div class="col-xl-6 col-sm-6 col-6 wish_length_box" id="box-{{$value->product_id}}" data-id="{{$value->product_id}}"> 
                        <div class="card-br">
                            <div class="media card-media">
                                <div class="media-img"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $value->primary_image }}" alt=""></div>
                                    <div class="media-body">
                                        <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$value->product_id }}">
                                            <h2 class="title">{{$value->product_title}}</h2>
                                            <div class="m-flex">
                                                <p class="cat-data">{{$value->productcategorytitle}},{{$value->subcategorytitle}}</p>
                                                <ul class="rating-star">
                                                    <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                    <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                    <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                    <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                    <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li>
                                                    <li><span href="#" class="star-span"> (5)</span></li>
                                                </ul>
                                            </div>
                                            <div class="media-data">
                                                <div class="media">
                                                    @php $profileImage=FRONT_IMG.'/nophoto.png';  @endphp
                                                    @if($value->profile_image!='')
                                                            @php $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value->profile_image); @endphp
                                                    @endif
                                                    <img class="media-imgs" src="{{ $profileImage}}" alt="">
                                                    <div class="media-body">
                                                        <p class="m-title">{{$value->name}}</p>
                                                    </div>
                                                </div>
                                                <div class="price-d">${{$value->price_per_day}}<sub>/Day</sub></div>
                                            </div>
                                        </a>
                                    </div>
                                
                                <div class="remove-btn" id="remove_button" data-id="{{$value->product_id}}"><span>Remove</span></div>
                            </div>
                        </div>
                </div>
            @endforeach
        @endif

@php 
    exit();
@endphp


    