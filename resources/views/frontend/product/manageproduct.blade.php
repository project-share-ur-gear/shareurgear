@extends('layouts.app')

@section('content')
@php
 use Illuminate\Support\Str;
 use Illuminate\Support\Facades\Crypt;
 use Illuminate\Pagination\Paginator;
@endphp
<?php $curl_key=CURL_KEY; 
$url = SITE_HTTP_URL_API.'manage-product/';


?>
<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>

<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
     
            <div class="mdata-product">
                <h2 class="login-title">Manage Products</h2>
                <input type="hidden" name="Count_data" id="Count_data" value="{{$countdata}}">
                <div class="search-product">
                    <div class="input-group">
                        <input type="text" class="form-control keyword  " placeholder="Search" aria-label="Search" aria-describedby="basic-addon2" id="keyword" >
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><img src="{{ FRONT_IMG.'/loupe1.png' }}" alt=""></span>
                        </div>
                    </div>
                </div>
                <div class="all-data">
                    <select class="form-control selectpicker" id="price_details" name="price_details" >
                        <option value="0">Price</option>
                        <option value="h_to_l">High To Low</option>
                        <option value="l_to_h">Low To High</option>
                    </select>
                </div>
            </div>
        <div class="manage-product-table">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Image</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Price</th>
                            <th scope="col">Status</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody  class="append_data" >
                    @if(empty($manageproduct))
                        <tr class="border-0" id="search_educator" >
                                <td colspan="7">
                                    No Product Added
                                </td>
                            </tr> 
                    @endif
                    @if(!empty($manageproduct))
                        @foreach($manageproduct as $key => $value)
                            <tr  class="chat_length_box" >
                                <td><div class="p-image"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/{{ $value->primary_image }}" alt=""></div></td>
                                <td> {{ Str::limit($value->product_title, 20) }} </td>
                                <td>{{ $value->productcategorytitle }}</td>
                                <td>{{ bcdiv($value->price_per_day,1,2) }}</td>
                                <td><span style="color:#8a71b2"> @if ($value->product_request_status == 1) Approved @elseif ($value->product_request_status == 2) Decliened @else Approvel Waiting @endif </span>  <span id="reason"  data-id="{{$value->product_id}}" style="color:#8a71b2; cursor: pointer;"> @if ($value->product_request_status == 2) Reason  @endif </span></td>
                                <td>
                                    <div class="A-flex">
                                        <div class="mq-data">
                                            @if(!empty($value->product_request_status==1))
                                        <a href='{{url("manage-availability")}}/{{$value->product_id}}' class="manage-avial">Manage Availability</a>
                                        <span class="questions"><a href='{{url("manage-Q&a")}}?product_id={{$value->product_id}}' class="">{{$value->supercount}} Questions</a></span>
                                            @endif 
                                            
                                        </div>
                                        @if(!empty($value->product_request_status!=2))
                                             <a href="{{ SITE_HTTP_URL.'/edit-product/'.Crypt::encrypt($value->product_id) }}" class="edit-icon"><img src="{{ FRONT_IMG.'/draw1.png'}}" alt=""></a>
                                        @endif 
                                        <a href="javascript:void(0);" id="delete_data" data-id="{{$value->product_id}}"   onclick="delete_product({{$value->product_id}})" class="edit-icon"><img src="{{ FRONT_IMG.'/trash.png'}}" alt=""></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                    @endif
                        @if($countdata>5)
                            <tr class="border-0" id="search_educator" >
                                <td colspan="7" style="text-align: center;">
                            
                                    <nav aria-label="Page navigation">
                                            <ul class="pagination">
                                                <a href='javascript:void(0)' class="manage-avial page-link active" id="loadmore" onclick="morefunctioncall()" >Load More</a>
                                            </ul>
                                    </nav>
                                </td>
                            </tr>
                        @endif
                        
                    </tbody>
                </table>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>

<div class="modal common-modals fade add" id="manage1ModalCenter"  data-backdrop="false" role="dialog" aria-labelledby="manage1ModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="manage1ModalLongTitle"> Decline Reason </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                
                        <div id="Text_reason">


                        </div>

                </div>                                  
                </div>
            </div>
    </div>

<script>
 $(document).on("click","#reason",function(){
    var dataId = $(this).attr("data-id");
    $.ajax({ 
             url:APPLICATION_URL+'/get-reason/'+dataId,
             async:true, 
             type: "get",
             success: function(data) {
                $('#manage1ModalCenter').modal({
                    show: true,
                    keyboard:true,
                });
                $("#Text_reason").html(data);
                
             }
    });


 });


    function morefunctioncall(){ 
        var totalRecordsLoaded = $('.chat_length_box').length;
        var totalRecords = $('#Count_data').val();
        if(totalRecords > totalRecordsLoaded){
            loadMoreproduct();
        } 
    }
    function loadMoreproduct(){ 
        var startLimit = $('.chat_length_box').length;
        var lnk = window.location.href;
    	var params2 = lnk.split('?')[1];
        
        var keyword = '' ;
        var pricedetails = '' ;
        keyword = $(".keyword").val();
        if($('#price_details').val()!='' && $('#price_details').val()!='0'){
            pricedetails = $('#price_details').val();
        }
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });
        $.ajax({
            type: 'post',
            url  : APPLICATION_URL+'/getmoremanageproduct?'+params2,
            data:{startLimit:startLimit,pricedetails:pricedetails,keyword:keyword},
            success: function(data){
			    status='complete';
                // $(".append_data").append(data);
                $(".append_data tr:last").before(data);
                var totalRecordsLoaded = $('.chat_length_box').length;
                var totalRecords = $('#Count_data').val();
                if(totalRecords <= totalRecordsLoaded){
                    $('#search_educator').css("display","none");
                } 
            }
        }); 
    }
    function delete_product(id){
            swal({   
                title:'',   
                text:'Are you want to sure delete product',   
                type:"warning",   
                showCancelButton:true,   
                confirmButtonText:'Yes',
                cancelButtonText:'Cancel',     
                closeOnConfirm:true,   
                closeOnCancel:true 
                },function(isConfirm){   
                if(isConfirm){     
                    window.location.href = APPLICATION_URL+'/deleteproduct/'+id;
                } 
            }); 
    }

    $(document).on("keyup","#keyword",function(){

        var keyword = '' ;
        var pricedetails = '' ;
        keyword = $(".keyword").val();
        if($('#price_details').val()!='' && $('#price_details').val()!='0'){
            pricedetails = $('#price_details').val();
        }
          manageproductsearching(keyword,pricedetails);
    });

    $('#price_details').change(function(e){ 
        var keyword1 = '' ;
        var pricedetails = '' ;
        keyword1 = $(".keyword").val();
        if($('#price_details').val()!='' && $('#price_details').val()!='0'){
            pricedetails = $('#price_details').val();
        }
        manageproductsearching(keyword1,pricedetails);
    });


    function manageproductsearching(keyword,pricedetails){
        $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
        });
        $.ajax({ 
             url:APPLICATION_URL+'/manage-searching',
             async:true, 
             type: "POST",
             data: {keyword:keyword,price_details:pricedetails},
             success: function(data) {
                $(".append_data").html(data);
             }
        });
    }
    

</script>

@endsection()