@extends('layouts.app')

@section('content')

<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">My Wish List</h2>
        <div class="wish-list-page">
            <div class="browser-content">
                <input type="hidden" name="Count_data" id="Count_data" value="{{$count_Data}}">
                <div class="row append_Data" id="">
                    @if(empty($wishlist_data)) 
                        <h5>   No Wishlist Added </h5>    
                    @endif

                    @if(!empty($wishlist_data))
                        @foreach($wishlist_data as $key =>$value)
                            <div class="col-xl-6 col-sm-6 col-6 wish_length_box" id="box-{{$value->product_id}}" data-id="{{$value->product_id}}"> 
                                <div class="card-br">
                                    <div class="media card-media">
                                        <div class="media-img"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $value->primary_image }}" alt=""></div>
                                            <div class="media-body">
                                                <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$value->product_id }}">
                                                    <h2 class="title">{{$value->product_title}}</h2>
                                                    <div class="m-flex">
                                                        <p class="cat-data">{{$value->productcategorytitle}},{{$value->subcategorytitle}}</p>
                                                        <ul class="rating-star">
                                                            <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                            <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                            <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                            <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star.svg' }}"></span></li>
                                                            <li><span href="#" class="star-icon"><img src="{{ FRONT_IMG.'/star2.svg' }}"></span></li>
                                                            <li><span href="#" class="star-span"> (5)</span></li>
                                                        </ul>
                                                    </div>
                                                    <div class="media-data">
                                                        <div class="media">
                                                            @php $profileImage=FRONT_IMG.'/nophoto.png';  @endphp
                                                            @if($value->profile_image!='')
                                                                @php $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value->profile_image); @endphp
                                                            @endif
                                                            <img class="media-imgs" src="{{ $profileImage}}" alt="">
                                                            <div class="media-body">
                                                                <p class="m-title">{{$value->name}}</p>
                                                            </div>
                                                        </div>
                                                        <div class="price-d">${{$value->price_per_day}}<sub>/Day</sub></div>
                                                    </div>
                                                </a>
                                            </div>
                                        
                                        <div class="remove-btn" id="remove_button" data-id="{{$value->product_id}}"><span>Remove</span></div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                {{--  <div class="loader" id="loadmore_icon" style="flex: auto; text-align:center; display:none" ><img src="{{ FRONT_IMG.'/Gear-0.3s-98px.gif' }}" alt=""></div>
                  --}}
                  
                <?php if($count_Data > count($wishlist_data)) { ?>
                    <div class="text-center">
                        <a href="Javascript:void(0);" onclick="loadMoreproduct()" class="btn view-all waves-effect waves-light" id="loadmore_icon">Load More</a>
                    </div>
                <? } ?>
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 <script>
    $( document ).ready(function() {
        $(document).on("click","#remove_button",function() {
            var dataId = $(this).attr("data-id");  
            $.ajax({ 
                    url:APPLICATION_URL+'/remove-wishlist/'+dataId,
                    async:true, 
                    type: "get",
                    success: function(data) {

                        if(data=='invalid_request')
                        {
                            $.notify({
                                message: "Invalid Request"
                                },{
                                type: 'danger',
                                timer: 1000
                            });

                        }else if(data=='remove_sucessfully'){
                            $("#box-"+dataId).remove();
                            $.notify({
                                message: "Wishlist Remove sucessfully"
                                },{
                                type: 'success',
                                timer: 1000
                            });
                        }
                    
                        
                    }
            });
        });

        /*
        $(window).scroll(function(e){
            if(($(window).scrollTop()+$(window).innerHeight())>=$('footer').offset().top){
                var totalRecordsLoaded = $('.wish_length_box').length;
                var totalRecords = $('#Count_data').val();
                if(totalRecords > totalRecordsLoaded){
                    
                    loadMoreproduct();
                } 
            }
        });*/
    });

    var status='complete';
    function loadMoreproduct(){ 
        var startLimit = $('.wish_length_box').length;

         var totalRecordsLoaded = $('.wish_length_box').length;
        var totalRecords = $('#Count_data').val();
        if(status=='complete'){ 
            status='running';   
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'post',
                url  : APPLICATION_URL+'/getmorewishlist',
                data:{offset:startLimit},
                success: function(data){
                    status='complete';
                    $(".append_Data").append(data);

                    var totalRecordsLoaded = $('.wish_length_box').length;
                    
                    if(totalRecords == totalRecordsLoaded){
                        $('#loadmore_icon').css("display","none");
                    }
                    
                }
            }); 
        }
    }
 </script>


@endsection()