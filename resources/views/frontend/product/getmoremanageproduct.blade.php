
@php
 use Illuminate\Support\Str;
 use Illuminate\Support\Facades\Crypt;
 use Illuminate\Pagination\Paginator;
@endphp
        @if(!empty($manageproduct))
            @foreach($manageproduct as $key => $value)
                <tr  class="chat_length_box" >
                    <td><div class="p-image"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/{{ $value->primary_image }}" alt=""></div></td>
                    <td> {{ Str::limit($value->product_title, 20) }} </td>
                    <td>{{ $value->productcategorytitle }}</td>
                    <td>{{ bcdiv($value->price_per_day,1,2) }}</td>
                    <td><span style="color:#8a71b2"> @if ($value->product_request_status == 1) Approved @elseif ($value->product_request_status == 2) Decliened @else Approvel Waiting @endif </span>  @if ($value->product_request_status == 2) <span id="reason" data-id="{{$value->product_id}}" style="color:#8a71b2;cursor: pointer;">  Reason   </span> @endif</td>
                    <td>
                        <div class="A-flex">
                            <div class="mq-data">
                                @if(!empty($value->product_request_status==1))
                            <a href='{{url("manage-availability")}}/{{$value->product_id}}' class="manage-avial">Manage Availability</a>
                            <span class="questions"><a href='{{url("manage-Q&a")}}?product_id={{$value->product_id}}' class="">{{$value->supercount}} Questions</a></span>
                                @endif 
                               
                            </div>
                            @if(!empty($value->product_request_status!=2))
                                <a href="{{ SITE_HTTP_URL.'/edit-product/'.Crypt::encrypt($value->product_id) }}" class="edit-icon"><img src="{{ FRONT_IMG.'/draw1.png'}}" alt=""></a>
                            @endif 
                            <a href="javascript:void(0);" id="delete_data" data-id="{{$value->product_id}}"   onclick="delete_product({{$value->product_id}})" class="edit-icon"><img src="{{ FRONT_IMG.'/trash.png'}}" alt=""></a>
                        </div>
                    </td>
                </tr>
            @endforeach
        @endif

@php 
    exit();
@endphp
