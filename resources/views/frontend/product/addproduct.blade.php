
@extends('layouts.app')

@section('content')
@php
 use Illuminate\Support\Facades\Crypt;
@endphp

<style>
.modal-backdrop.show {opacity: .0; }
.pac-container { z-index: 999999999; }
</style>

<link href="{{ asset('public/plugins/dropzone/dropzone.min.css') }}" rel="stylesheet" type="text/css">

<section class="common-section-top login-common-bg">
    <div class="container"></div>
</section>

<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->

        <h2 class="login-title">@if(!empty($products)) Edit @else Add @endif Product</h2>
        <div class="contact-dots-panels mw-100">
        {!! Form::open(['url' => $formurl, 'method' => 'patch','name' => 'productform', 'id' => 'productform','enctype'=> 'multipart/form-data']) !!}

            @csrf
            <div class="contact-box common-form">
                <div class="form-row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Product Name</label>
                            <input type="text" name="product_title" id="product_title" class="form-control Required" value="@if(!empty($products)){{$products['product_title']}}@elseif(old('product_title')!=''){{old('product_title')}}@endif" placeholder="Product Name" maxlength="255" Required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Product Category</label>
                            <div class="pro-cateOption type-select">
                                <select class="form-control custom-select Required" name="product_category" id="product_category" required aria-required="true">
                                <option value="" >Choose Product-Category</option>
                                    @foreach($product_categories as $key => $value)
                                        <option value="{{$key}}" @if(!empty($products) && $products['product_category'] == $key ) selected @elseif(old('product_category')!='' &&  old('product_category') ==$key ) selected @endif   >{{$value}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <span for="product_category" class="help-block" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Sub-Category</label>
                            <div class="pro-cateOption type-select">
                                <select class="form-control custom-select Required" name="sub_category" id="sub_category" required aria-required="true">
                                    <option value="">Choose Sub-Category </option>

                                </select>
                            </div>
                            <span for="sub_category" class="help-block" style="display:none"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Tags</label>
                            <input type="text" name="tags" id="tags" class="Required" maxlength="255" value="@if(!empty($products)) {{$products['tags']}} @elseif(old('tags')!='') {{old('tags')}} @endif" id="shop-label" data-role="tagsinput" placeholder="Enter tag" required aria-required="true">
                            <span for="" class="help-block"></span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Price Per Day</label>
                            <input type="text" name="price_per_day" id="price_per_day" value="@if(!empty($products)) {{$products['price_per_day']}} @elseif(old('price_per_day')!='') {{old('price_per_day')}} @endif"  class="form-control amount Required " placeholder="$" Required>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="">Location</label>
                            <input type="text" name="location" id="location" value="@if(!empty($products)) {{$products['location']}} @elseif(old('location')!='') {{old('location')}} @endif" class="form-control Required" maxlength="400" placeholder="Location" Required>
                            <input type="hidden" name="p_latitude" id="p_latitude" value="@if(!empty($products) && $products['p_latitude']!='' ) {{$products['p_latitude']}} @elseif(old('p_latitude')!='' && empty($products) ) {{old('p_latitude')}} @endif">
                            <input type="hidden" name="p_longitude" id="p_longitude" value="@if(!empty($products) && $products['p_longitude']!='' ) {{$products['p_longitude']}} @elseif(old('p_longitude')!='' && empty($products) ) {{old('p_longitude')}} @endif">
                            <input type="hidden" name="city" id="city" value="@if(!empty($products) && $products['city']!='') {{$products['city']}} @elseif(old('city')!='' && empty($products) ) {{old('city')}} @endif">
                            <input type="hidden" name="state" id="state" value="@if(!empty($products) && $products['state']!='') {{$products['state']}} @elseif(old('state')!='' && empty($products) ) {{old('state')}} @endif">
                            <input type="hidden" name="postal" id="postal" value="@if(!empty($products) && $products['postal']!='') {{$products['postal']}} @elseif(old('postal')!='' && empty($products) ) {{old('postal')}} @endif">
                            <input type="hidden" name="country" id="country"  value="@if(!empty($products) && $products['country']!='') {{$products['country']}} @elseif(old('country')!='' && empty($products) ) {{old('country')}} @endif">
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="form-group">
                                    <label for="">Home Location</label>
                                    <input type="text" name="home_location" id="home_location"  class="form-control Required" placeholder="Home Location" value="@if(!empty($products) && !empty($products['home_location'])) {{$products['home_location']}} @elseif(old('home_location')!='' && empty($products) ) {{old('home_location')}} @endif" Required >
                                    <input type="hidden" name="home_lat" id="home_lat"  class="form-control Required" placeholder="Home Location" value="@if(!empty($products) && $products['home_lat']!='') {{$products['home_lat']}} @elseif(old('home_lat')!='' && empty($products) ) {{old('home_lat')}} @endif" >
                                    <input type="hidden" name="home_long" id="home_long"  class="form-control Required" placeholder="Home Location"  value="@if(!empty($products) && $products['home_long']!='') {{$products['home_long']}} @elseif(old('home_long')!='' && empty($products) ) {{old('home_long')}} @endif">
                                </div>
                                
                                <div class="delivery-location">
                                    <div class="dl-header">
                                        <h2 class="titles">Delivery Location </h2>
                                        <!-- data-target="#payModalCenter" -->
                                        <div class="btn add-more-btn" data-toggle="modal"  id="add_more"   >Add More Location</div>
                                    </div>
                                    <p class="description">{{ $getPageData['delivery_location_text'] }}</p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody class="add_delivery_location">
                                                @if(!empty($delevery_location))
                                                    @foreach($delevery_location as $key => $value)
                                                    <tr class="more_location" id="more_location_{{$key}}">
                                                        <td><input type="hidden" name="delivery_latitude[]" id="delivery_latitude{{$key}}" value="{{ $value->d_lat}}">
                                                            <input type="hidden" name="delivery_longitude[]" id="delivery_longitude{{$key}}" value="{{ $value->d_long}}">
                                                            <input type="hidden" name="delivery_location[]" id="delivery_location{{$key}}" value="{{ $value->d_location}}">
                                                            <input type="hidden" name="delivery_instrucation[]" id="delivery_instrucation{{$key}}" value="{{ $value->d_instruction}}">

                                                            <input type="hidden" name="location_delivery_price[]" id="location_delivery_price{{$key}}" value="{{ $value->d_price}}">
                                                            {{ $value->d_location}}
                                                        </td>
                                                        <td>${{ $value->d_price}}</td>
                                                        <td>
                                                            <div class="edit-icons">
                                                                <a href="javascript:void(0)" class="edit-btn editbtn" data-id="{{$key}}"><img src="{{ FRONT_IMG.'/draw1.png' }}"   alt=""></a>
                                                                <a href="javascript:void(0)" class="edit-btn removebtn" data-id="{{$key}}"><img src="{{ FRONT_IMG.'/trash1.png' }}" ></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="delivery-location">
                                    <div class="dl-header">
                                        <h2 class="titles">Delivered to you </h2>
                                        <!-- data-target="#locationModalCenter" -->
                                        <div class="btn add-more-btn" data-toggle="modal" id="deleverty_to" >Add More Location</div>
                                    </div>
                                    <p class="description">{{ $getPageData['delivery_to_you_text'] }}</p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody  class="add_delivery_to_you">
                                            
                                                @if(!empty($delevery_to_location))  
                                                    @foreach($delevery_to_location as $key => $value)
                                                    <tr  class="more_deliver_to"  id="more_deliver_to_{{$key}}">
                                                    <td>
                                                            <input type="hidden" name="delivery_mile[]" id="delivery_mile{{$key}}" value="{{$value->d_to_miles}}">
                                                            <input type="hidden" name="delivered_price[]" id="delivered_price{{$key}}" value="{{$value->d_to_price}}">
                                                            Up to {{$value->d_to_miles}} miles 
                                                        
                                                    </td>
                                                        
                                                        @if($value->d_to_price=='0')
                                                            <td>free</td>
                                                        @else
                                                            <td>{{$value->d_to_price}}</td>
                                                        @endif
                                                        <td>
                                                        
                                                            <div class="edit-icons">
                                                                <a href="javascript:void(0)" class="edit-btn editdelivery_btn" data-id="{{$key}}"><img src="{{ FRONT_IMG.'/draw1.png' }}" alt=""></a>
                                                                <a href="javascript:void(0)" class="edit-btn  removedelivery_btn" data-id="{{$key}}"><img src="{{ FRONT_IMG.'/trash1.png' }}" alt=""></a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="form-group" >
                                    <label for="">Product Image </label>
                                    <div class="uploader-img" >
                                    <div class="dropzone needsclick dz-clickable">
                                        <div id="documentUploader" class="imagestorage needsclick dz-clickable">
                                        <div class="dz-message needsclick dz-clickable">
                                            <div class="img-upload"><img class="icon-img" src="{{ FRONT_IMG.'/image-file.png' }}" alt=""><span>Upload Image</span></div>
                                        </div>
                                        </div>
                                        <div id="documentPreview" class="">
                                            @if(!empty($products))
                                                @foreach($products['product_images'] as $k => $val) 
                                                <div class="dz-preview dz-processing dz-image-preview dz-complete" id="{{ $k }}"> 
                                                        <div class="dz-image">
                                                            <img data-dz-thumbnail=""  src="{{HTTP_UPLOADED_IMAGES_PATH}}/160X160/{{ $k}}">
                                                        </div>
                                                        <div class="dz-details">  <div class="dz-filename"><span data-dz-name="">{{ $val }}</span></div>  </div>
                                                        <a class="dz-remove" href="javascript:void(0)" onclick="removeFileEdit('{{ $k }}','{{ Crypt::encrypt($products['product_id']) }}');">Remove file</a>
                                                        <div class="check-boxs">
                                                            <div class="checks custom-checks"> 
                                                                <label class="checkbox"> Make it Primary
                                                                    <input type="radio" name="primary_image" class="add" required value="{{ $k }}" @if($k == $products['primary_image']) checked="checked"  @endif > 
                                                                    <span class="checkmark" data-id="{{$k}}"></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        </div>
                                        
                                    </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="">Description</label>
                                <textarea rows="6"  name="product_desp" id="product_desp" class="form-control Required" placeholder="Write description here..." maxlength="1500" Required> @if(!empty($products)){{$products['product_desp']}}@elseif(old('product_desp')!=''){{old('product_desp')}}@endif</textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Extra items -->
                    <div class="col-12">
                        <div class="delivery-location">
                            <div class="dl-header"><h2 class="titles">Extra Item</h2></div>
                            <p class="description"></p>
                        </div>
                        <div id="extraItemSection">
                            <?php if(empty($extraItems)) { ?>
                                <div class="form-row itemlist">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Product Image</label>
                                            <input type="file" name="extra_product_image0" id="extra_product_image"  class="form-control" value="" >
                                            <span for="extra_product_image" class="help-block"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Product Title</label>
                                            <input type="text"  name="extra_product[0][name]" id="extra_product_name" class="form-control" value="" placeholder="Product Title" maxlength="30">
                                            <span for="extra_product_name" class="help-block"></span>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Product Price</label> 
                                            <input type="text" name="extra_product[0][price]"  id="extra_product_price" class="form-control  money_charm numbersOnly" value="" placeholder="$">
                                            <span for="extra_product_price" class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            <? } else { ?>
                                @foreach($extraItems as $key => $item)
                                    <div class="form-row itemlist">
                                        <div class="col-sm-6">
                                            <div class="removeItem" style="text-align: end;"><a href="Javascript:void();"><i style="color: red;" class="fas fa-times"></i></a></div>
                                            <div class="form-group">
                                                <label for="">Product Image</label>
                                                <input type="file" name="extra_product_image<?=$key?>" id="extra_product_image<?=$key?>"  class="form-control">
                                                <span for="extra_product_image<?=$key?>" class="help-block"></span>
                                            </div>

                                            @if(!empty($item->item_name))
                                                <div class="form-group" >
                                                    <label for="">Product Image Preview</label>
                                                    <div class="uploader-img" >
                                                        <div class="img-upload"><img class="icon-img" src="{{HTTP_UPLOADED_IMAGES_PATH}}/160X160/{{ $item->item_image }}" alt=""><span></span></div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                <label for="">Product Title</label>
                                                <input type="text"  name="extra_product[<?=$key?>][name]" id="extra_product_name<?=$key?>" class="form-control " value="{{ $item->item_name }}" placeholder="Product Title" maxlength="30">
                                                <span for="extra_product_name<?=$key?>" class="help-block"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="">Product Price</label> 
                                                <input type="text" name="extra_product[<?=$key?>][price]"  id="extra_product_price<?=$key?>" class="form-control  money_charm numbersOnly" value="{{ $item->item_price }}" placeholder="$">
                                                <span for="extra_product_price<?=$key?>" class="help-block"></span>
                                            </div>
                                            
                                            <input type="hidden" name="extra_product[<?=$key?>][itemId]" id="itemId<?=$key?>" value="{{ $item->item_id }}">
                                        </div>
                                    </div>
                                @endforeach
                            <? } ?>
                        </div>

                        <div class="btn add-more-btn" onclick="addMoreItem()">Add More item</div>
                    </div>
                
                    <div class="col-sm-6 offset-3"><button type="button" id="startedbtn" data-inprocess="Product Saving..." data-default="@if(!empty($products)) Edit @else Add @endif Product" class="btn  submit-btn my-3">@if(!empty($products)) Edit @else Add @endif Product</button></div>
                </div>
            </div>
            {!! Form::close() !!} 
        </div>
        <!--  -->
    </div>
</section>


</div>

<!-- Delivery Location  -->
<div class="modal fade " id="payModalCenter" tabindex="-1" role="dialog" aria-labelledby="payModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form class="delevery_location" id="delevery_location">
                    <div class="modal-header">
                        <h5 class="modal-title" id="payModalLongTitle">Delivery Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="rent-item-modal ">
                            <div class="common-form rent-middle-content">


                                <input type="hidden" name="edited_id" id="edited_id" value="" >
                                <div class="form-group"><label> Address </label> <input type="text" class="form-control required" placeholder="Address" id="delivery_location" required>
                                <input type="hidden" name="delivery_latitude" id="delivery_latitude" value="">
                                <input type="hidden" name="delivery_longitude" id="delivery_longitude" value="">
                                </div>
                                <div class="form-group"><label> price </label>
                                    <div class="form-select"> 
                                        <select class="form-control custom-select Required" id="locationdelivery_price" Required >
                                            <option value="">price</option>
                                            <option value="0">Free</option>
                                            <option value="1">$1</option>
                                            <option value="2">$2</option>
                                            <option value="3">$3</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group"><label> Instruction </label>   <textarea class="form-control Required" rows="5" id="delivery_instrucation"  placeholder="Ask here...." Required></textarea>
                            </div>
                                
                            </div>
                            <div class="text-center answer-box">
                                <div class="btn submit-btn" id="delivery_submit" class="delivery_submit">Submit</div>
                            </div>
                        </div>
                        <!--  -->
                    </div>
                </form>

            </div>
    </div>
</div>

<!-- Delivered to you -->
<div class="modal fade " id="locationModalCenter" tabindex="-1" role="dialog" aria-labelledby="locationModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
       
            <form class="delevery_to_location" id="delevery_to_location">
                <div class="modal-header">
                    <h5 class="modal-title" id="payModalLongTitle">Delivered to you</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="rent-item-modal ">
                        <div class="common-form rent-middle-content">
                            <div class="form-group"><label> Fee </label>
                            <input type="hidden" name="delivery_to_edited_id" id="delivery_to_edited_id" value="" >
                                <div class="form-select"> 
                                    <select class="form-control custom-select" id="delivered_to_price" Required >
                                        <option value='0'>Free</option>
                                        <option value='10' >10$</option>
                                        <option value='20'>20$</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group"><label> Distance from Home Location </label>
                                <div class="form-select"> 
                                    <select class="form-control custom-select" id="delivered_to_mile">
                                        <option value="10">Up to 10 miles</option>
                                        <option value="20">Up to 20 miles</option>
                                        <option value="30">Up to 30 miles</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                            
                        </div>
                        <div class="text-center answer-box">
                            <div class="btn submit-btn"   id="delivered_to_submit" >Submit</div>
                        </div>
                    </div>
                    <!--  -->
                </div>
            </form>
        
        </div>
    </div>
</div>

<script src="{{asset('public/plugins/dropzone/dropzone.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script>

<script>
    var itemList = $('.itemlist').length - parseInt(1);
    function addMoreItem(){

        itemList++;
        var html='';
        html+=`<div class="form-row itemlist">`;
        html+=`<div class="col-sm-6">`;
        html+=`<div class="removeItem" style="text-align: end;"><a href="Javascript:void();"><i style="color: red;" class="fas fa-times"></i></a></div>`;
        html+=`<div class="form-group">`;
        html+=`<label for="">Product Image</label>`;
        html+=`<input type="file" name="extra_product_image`+itemList+`" id="extra_product_image`+itemList+`"  class="form-control" value="" >`;
        html+=`<span for="extra_product_image`+itemList+`" class="help-block"></span>`;
        html+=`</div>`;
        html+=`<div class="form-group">`;
        html+=`<label for="">Product Title</label>`;
        html+=`<input type="text"  name="extra_product[`+itemList+`][name]" id="extra_product_name`+itemList+`" class="form-control" value="" placeholder="Product Title" maxlength="30">`;
        html+=`<span for="extra_product_name`+itemList+`" class="help-block"></span>`;
        html+=`</div>`;
        html+=`<div class="form-group">`;
        html+=`<label for="">Product Price</label>`;
        html+=`<input type="text" name="extra_product[`+itemList+`][price]" id="extra_product_price`+itemList+`" class="form-control  money_charm numbersOnly" value="" placeholder="$">`;
        html+=`<span for="extra_product_price`+itemList+`" class="help-block"></span>`;
        html+=`</div>`;
        html+=`</div>`;
        html+=`</div>`;

        $('#extraItemSection').append(html);
    }

    $(document).on('click','.removeItem',function(){
        $(this).parent('div').find('.form-group').remove();
        $(this).remove();
    });

    $(document).on('keyup','.money_charm',function(e){
        var inputVal = $(this).val();
        var inputVal = $(this).val();
        var inputLen = $(this).val().length;
        var dott = inputVal.toString().indexOf(".");
        var allowDi = parseFloat(inputLen) - parseFloat(dott);
        if(dott != -1){var allowDi = parseFloat(inputLen) - parseFloat(dott);}
        else{var allowDi = '0';}
        if((inputLen > 5 && allowDi == 0) || (inputLen > 8 || allowDi >3)){
            $(this).val(tempInput);
        }else{
            tempInput = inputVal;
        }
    });

    $(document).on('keyup','.numbersOnly',function () { 
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });

</script>

<script>
    $(document).ready(function(){
        $("#shop-label").tagsinput('items');
        $(".custom-select").select2();
        $('#productform').validate({
            messages: {
            primary_image: "Make it primary key field must required.",
    
            },
            errorPlacement: function(error, element) {
                if (element.attr("class") == "add" ){  error.insertAfter("#documentPreview"); }
                else{ error.insertAfter(element); }
            }
        });

        @if(!empty($products)) 
            multiUploader("{{$products['primary_image']}}");
        @else  
            var test = "";
            multiUploader(test); 
        @endif
        @if(old('product_category') !='' || !empty($products))    
            setTimeout(function(e){ $('#product_category').change(); },200);
        @endif
    });

    $('#product_category').change(function(e){ 
        var pro_cat  =$(this).val();
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
        $.ajax({
            type: 'POST',
            url  :   APPLICATION_URL+'/sub-category',
            data : ({pro_cat : pro_cat}),
            dataType: 'JSON',
            success: function(response) {
                var substr = response;
                if(substr.sub_product_categories == "product_cat_not_found" || substr.sub_product_categories == "failed" ){
                    $.notify({
                            message: "something is going wrong."
                            },{
                            type: 'danger',
                            timer: 1000
                        });
                        $("#sub_category").html('');
                        $("#sub_category").append("<option>Choose Sub-Category </option>"); 

                }else{
                        $("#sub_category").html('');
                        $("#sub_category").append("<option>Choose Sub-Category </option>");
                        var sub_category = "";
                        @if(!empty($products)) 
                        sub_category = "{{$products['sub_category']}}";
                        @elseif(old('sub_category') !='') 
                        sub_category = "{{old('sub_category')}}";
                        @endif
                        jQuery.each(substr.sub_product_categories, function( i, val ) {
                            if(sub_category != "" && i == sub_category){
                                $("#sub_category").append("<option value="+i+" selected >"+val+"</option>");
                            }else{
                                $("#sub_category").append("<option value="+i+" >"+val+"</option>");
                            }
                            
                            
                        });
                }

            }
        });
    });

    /** for address */
    $(document).ready(function(){ 
        
        function init() {
            var input = document.getElementById('location');
            var options = {
                types: ['geocode'],  
                //componentRestrictions: {country: countryCode}
            };
            var autocomplete = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();
                document.getElementById('p_latitude').value = place.geometry.location.lat();
                document.getElementById('p_longitude').value = place.geometry.location.lng();
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];	
                    //console.log(addressType + ":" + place.address_components[i].long_name);
                    if(addressType=='locality'){
                        var val = place.address_components[i].long_name;
                        $('#city').val(val);
                    }
                    if(addressType=='administrative_area_level_1'){
                        var val2 = place.address_components[i].long_name;
                        $('#state').val(val2);
                    }
                    if(addressType=="country"){
                        var val = place.address_components[i].long_name;
                        $('#country').val(val);
                    }	
                    if(addressType=="postal_code"){
                        var val = place.address_components[i].long_name;
                        $('#postal').val(val);
                    }	      
                }

            });
        }

        google.maps.event.addDomListener(window, 'load', init);

        function init2() {
            var input = document.getElementById('delivery_location');
            var options = {
                types: ['geocode'],  
                //componentRestrictions: {country: countryCode}
            };
            var autocomplete1 = new google.maps.places.Autocomplete(input,options);
            google.maps.event.addListener(autocomplete1, 'place_changed', function () {
                var place = autocomplete1.getPlace();
                document.getElementById('delivery_latitude').value = place.geometry.location.lat();
                document.getElementById('delivery_longitude').value = place.geometry.location.lng();
            });
        }

        google.maps.event.addDomListener(window, 'load', init2);
        
        function init3() {
            var input = document.getElementById('home_location');
            var options = {
                types: ['geocode'],  
                //componentRestrictions: {country: countryCode}
            };
            var autocomplete = new google.maps.places.Autocomplete(input,options);
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    document.getElementById('home_lat').value = place.geometry.location.lat();
                    document.getElementById('home_long').value = place.geometry.location.lng();
                });
        }
        google.maps.event.addDomListener(window, 'load', init3);
    });
        
    /** for address end */
    $("#price_per_day").keyup(function(){   // 1st
        var price_per_day = $("#price_per_day").val();
        if (isNaN(price_per_day) || parseFloat(price_per_day) < 0) {
            $("#price_per_day").val('');
        }
    });

    $('#startedbtn').click(function(event) {
        $('#productform').validate();
        if($('#productform').valid()){
            var morelocation=$('.more_location').length;
            var more_deliver_to=$('.more_deliver_to').length;
            if(morelocation=='0'){
                showAppAlert("Please Select Delivery Location", "warning");
                return false;
            }

            if(more_deliver_to=='0'){
                showAppAlert("Please Select Delivery To Location", "warning");
                return false;
            }

            if(isEmpty($('#documentPreview'))){
                showAppAlert("Please Select File For Product Images", "warning");
                return false;
            }else{
                
                var blockuicss = { border: 'none',padding: '5px',width: '24%',left: '39%',backgroundColor: '#262b3e',border: '#262b3e',opacity: .9,color: '#ffffff',};
                var blockuimsg = "<div class='Loader'><div class='Text' style='letter-spacing: 2px;'>Just a moment</div></div>";
                $.blockUI({ 
                    css: blockuicss,message: blockuimsg,
                    timeout: 200,
                });

                freezeButton('#startedbtn',$('#startedbtn').data('inprocess'),'disabled');
                $('#productform').submit();
            }


        }else{
            $('html, body').animate({
                scrollTop: $(".help-block:visible").offset().top
            }, 1000);
        }
    });

    $('#delivery_submit').click(function(event){
        var t1= document.getElementById("delivery_location").value;
        var t2= document.getElementById("locationdelivery_price").value;
        var t3= document.getElementById("delivery_instrucation").value;

        if(t1=='' || t2=='' || t3==''  ){

            $.notify({
                message: "Please fill all fields, all fields are required."
                },{
                type: 'danger',
                timer: 1000
            });

        }else{
            $('#delevery_location').validate();
            if($('#delevery_location').valid()){
                var morelocation=$('.more_location').length;
                var edited_id = document.getElementById("edited_id").value;
                if(edited_id==''){
                    if(morelocation==''){
                        var id='1';
                    }else{
                        var id=morelocation+1;
                    }
                }else{
                    var  id= edited_id;
                }
                var delivery_location = document.getElementById("delivery_location").value;
                var delivery_price = document.getElementById("locationdelivery_price").value;
                var delivery_instrucation = document.getElementById("delivery_instrucation").value;
                var delivery_longitude = document.getElementById("delivery_longitude").value;
                var delivery_latitude = document.getElementById("delivery_latitude").value;

                
                var edit_image="<?php echo FRONT_IMG ?>/draw1.png";
                var remove_image="<?php echo FRONT_IMG ?>/trash1.png";
                var appenddata='';
                var  inputFeild1='<input type="hidden" name="delivery_location[]" id="delivery_location'+id+'" value="'+delivery_location+'" >';
                var inputFeild2='<input type="hidden" name="delivery_instrucation[]" id="delivery_instrucation'+id+'" value="'+delivery_instrucation+'">';

                var inputFeild5='<input type="hidden" name="location_delivery_price[]" id="location_delivery_price'+id+'" value="'+delivery_price+'">';
                
                var inputFeild3='<input type="hidden" name="delivery_latitude[]" id="delivery_latitude'+id+'" value="'+delivery_latitude+'">';
                var inputFeild4='<input type="hidden" name="delivery_longitude[]" id="delivery_longitude'+id+'" value="'+delivery_longitude+'">';
                if(edited_id==''){
                    appenddata+='<tr class="more_location" id="more_location_'+id+'">';
                }   
                appenddata+='<td>'+inputFeild5+inputFeild3+inputFeild4+inputFeild1+inputFeild2+delivery_location+'</td><td>$'+delivery_price+'</td>';
                appenddata+='<td><div class="edit-icons"><a href="javascript:void(0)" class="edit-btn editbtn" data-id="'+id+'"><img src="'+edit_image+'" alt=""></a>';
                appenddata+='<a href="javascript:void(0)" class="edit-btn removebtn" data-id="'+id+'"><img src="'+remove_image+'" alt=""  ></a></div></td>';
                if(edited_id==''){
                    appenddata+='</tr>';
                    $(".add_delivery_location").append(appenddata);
                }else{
                    $("#more_location_"+edited_id).html(appenddata);
                }
                document.getElementById('delivery_location').value = '';
                document.getElementById('delivery_instrucation').value = '';
                $('#locationdelivery_price').val('').trigger('change');
                $("#payModalCenter").modal("hide");
            }
        }
    });

    $('#add_more').click(function(event){
        $('#edited_id').val('');
        document.getElementById('delivery_location').value = '';
        document.getElementById('delivery_instrucation').value = '';
        $("#payModalCenter").modal("show");
    });

    $(document).on("click",".editbtn",function(){
        var dataId=$(this).attr("data-id");
        var editlocation  =$("#delivery_location"+dataId).val();
        var editinstrucation  =$("#delivery_instrucation"+dataId).val();
        var location_deliveryprice  =$("#location_delivery_price"+dataId).val();
        document.getElementById('edited_id').value = dataId;
        document.getElementById('delivery_location').value = editlocation;
        document.getElementById('delivery_instrucation').value = editinstrucation;
        $('#locationdelivery_price').val(''+location_deliveryprice+'').trigger('change');
        $("#payModalCenter").modal("show");
    });

    $(document).on("click",".removebtn",function(){
        var dataId=$(this).attr("data-id");
        $("#more_location_"+dataId).remove();
    });

    $(document).on("click","#deleverty_to",function(){
        $('#delivery_to_edited_id').val('');
        $("#locationModalCenter").modal("show");
    });

    $(document).on("click","#delivered_to_submit",function(){
        var price  =$("#delivered_to_price").val();
        var miles  =$("#delivered_to_mile").val();
       
        if(price!='' && miles!=''){   

            var more_deliver_to=$('.more_deliver_to').length;
            var to_edited_id = document.getElementById("delivery_to_edited_id").value;
            
            if(to_edited_id=='')
            {
                if(to_edited_id=='')
                {
                    var id='1';
                }else{
                    var id=more_deliver_to+1;
                }
            }else{
                var  id= to_edited_id;
            }

            var edit_image="<?php echo FRONT_IMG ?>/draw1.png";
            var remove_image="<?php echo FRONT_IMG ?>/trash1.png";
            var appenddata='';
            var  inputFeild1='<input type="hidden" name="delivery_mile[]" id="delivery_mile'+id+'" value="'+miles+'" >';
            var inputFeild2='<input type="hidden" name="delivered_price[]" id="delivered_price'+id+'" value="'+price+'">';
            if(to_edited_id==''){
                appenddata+='<tr class="more_deliver_to" id="more_deliver_to_'+id+'">';
            }   
            if(price=='0')
            {
                price ='free';
            }
            appenddata+='<td>'+inputFeild1+inputFeild2+'Up to'+miles+' miles</td><td>$'+price+'</td>';
            
            if(to_edited_id==''){
                appenddata+='<td><div class="edit-icons"><a href="javascript:void(0)" class="edit-btn editdelivery_btn" data-id="'+id+'"><img src="'+edit_image+'" alt=""></a>';
                appenddata+='<a href="javascript:void(0)" class="edit-btn removedelivery_btn" data-id="'+id+'"><img src="'+remove_image+'" alt=""  ></a></div></td>';

                $(".add_delivery_to_you").append(appenddata);
            }else{
                appenddata+='<td><div class="edit-icons"><a href="javascript:void(0)" class="edit-btn editdelivery_btn" data-id="'+to_edited_id+'"><img src="'+edit_image+'" alt=""></a>';
                appenddata+='<a href="javascript:void(0)" class="edit-btn removedelivery_btn" data-id="'+to_edited_id+'"><img src="'+remove_image+'" alt=""  ></a></div></td>';
                $("#more_deliver_to_"+to_edited_id).html(appenddata);
            }
            appenddata+='</tr>';
            
            $('#delivered_to_price').val('0').trigger('change');
            $('#delivered_to_mile').val('10').trigger('change');
            $("#locationModalCenter").modal("hide");
        }else{
            if(price==''){
                var msg = 'Please enter your price is required.';
            }

            if(miles==''){
                var msg = 'Please select your miles is required.';
            }

            $.notify({
                message: msg
                },{
                type: 'danger',
                timer: 1000
            });
        }
    });

    $(document).on("click",".editdelivery_btn",function(){
        var dataId=$(this).attr("data-id");
        $('#delivery_to_edited_id').val(dataId);
        var price  =$("#delivered_price"+dataId).val();
        var miles  =$("#delivery_mile"+dataId).val();
        $('#delivered_to_price').val(''+price+'').trigger('change');
        $('#delivered_to_mile').val(''+miles+'').trigger('change');
        $("#locationModalCenter").modal("show");
    });
    
    $(document).on("click",".removedelivery_btn",function(){
        var dataId=$(this).attr("data-id");
        $("#more_deliver_to_"+dataId).remove();
    });
</script>

@endsection()