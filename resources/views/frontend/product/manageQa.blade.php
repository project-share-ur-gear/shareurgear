@extends('layouts.app')

@section('content')


<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>


<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <input type="hidden" name="Count_data" id="Count_data" value="{{$count_Data}}">
        <div class="mdata-product">
            <h2 class="login-title">Manage Q&A</h2>
            <div class="all-data">
            @if(!empty($product_Data))
                <select class="form-control selectpicker" id="p_title" name="p_title" >
                    <option value="0">All</option>
                    @foreach($product_Data as $key => $value)
                    <option data-id="{{$value->product_id}}" value="{{$value->product_id}}" @if($product_id == $value->product_id) selected @endif  >{{$value->product_title}}</option>
                    @endforeach
                </select>
            @endif  

            </div>
        </div>

        <div class="manage-QA-page manage-product-table">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Product</th>
                            <th scope="col"></th>
                            <th scope="col">Question by</th>
                            <th scope="col">Date of Posted</th>
                            <th scope="col">Question</th>
                        </tr>
                    </thead>
                    <tbody class="append_data">

                        @if(!empty($question_Data))
                            @foreach($question_Data as $key => $value)
                                <tr class="qa_box">
                                    @php $profileImage=FRONT_IMG.'/nophoto.png';  @endphp
                                    @if(!empty($value->profile_image))
                                        @php $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value->profile_image); @endphp
                                    @endif
                                    <td ><div class="p-image"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/{{ $value->primary_image }}" alt=""></div></td>
                                    <td> {{ $value->product_title }}</td>
                                    <td>
                                        <div class="media media-object">
                                            <img class="media-img" src="{{ $profileImage }}" alt="">
                                            <div class="user-name">{{ $value->name }}</div>
                                        </div>
                                    </td>
                                    <td>{{ $value->question_date }}</td>
                                    <td>
                                        <div class="mq-data">
                                            <span>{{ mb_strimwidth($value->question_answer,0,200,'...') }}</span>
                                            @if($value->answered_status=='0')
                                                <div class="manage-avial btn" data-toggle="modal" data-id="{{ $value->question_id }}" id="answer" >Answer</div>
                                            @endif
                                            @if($value->answered_status=='1')
                                                <div class="manage-avial btn" data-toggle="modal" data-id="{{ $value->question_id }}" id="answered" >Answered</div>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td colspan="7" style="text-align: center;">No Qa Added </td>  
                        @endif
                        <tr class="border-0">
                            <td colspan="7">
                                <nav aria-label="Page navigation">
                                    
                                </nav>
                            </td>
                        </tr>
                    </tbody>
                </table>
                @if($count_Data > count($question_Data))
                    <div class="text-center">
                        <a href="Javascript:void(0);" onclick="loadMoreproduct()" class="btn view-all waves-effect waves-light" id="loadmore_icon">Load More</a>
                    </div>
                @endif
            </div>
        </div>
        <!--  -->
    </div>
</section>


</div>
 
<!--Answer Modal -->
<div class="modal fade " id="answerModalCenter" tabindex="-1" role="dialog" aria-labelledby="answerModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" >Ques. <span id="answerModalLongTitle" style="font-family: 'robotoregular';"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">
        <input type="hidden" class="form-control" id="question_id" name="question_id" placeholder="">
            <textarea class="form-control" rows="10" placeholder="Enter answer here...." id="answer_data" name="answer_data"></textarea>
           <div class="text-center"> <div class="btn submit-btn" id="add_answer" >Submit</div></div>
        </div>
      </div>
    </div>

  </div>
</div>




<!-- answered model -->
<div class="modal fade " id="answerModalCenter1" tabindex="-1" role="dialog" aria-labelledby="answerModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">

    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ques. <span id="answerModalLongTitle1" style="font-family: 'robotoregular';"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="answer-box">    
        <p class="user-name"><h5 class="modal-title" style="text-align:center" >Your Answered</h5></p>
        <h5 class="modal-title"><span id="answerModalLong1" style="word-break:break-all;font-family: 'robotoregular';"></span> </h5>
        </div>
      </div>
    </div>

  </div>
</div>
<!-- end answered model -->
<script>

    $( document ).ready(function() {
        $("#p_title").change();
    });

    //  answered click
    var lastJQueryTS = 0;
    $(document).on("click","#answered",function() {

        var send = true;
        if (typeof(event) == 'object'){
            if (event.timeStamp - lastJQueryTS < 300){
                send = false;
            }
            lastJQueryTS = event.timeStamp;
        }
        if(send)
        { 
            var dataId = $(this).attr("data-id");
            $("#question_id").val(dataId);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url  : APPLICATION_URL+'/getquestion/'+dataId,
                success: function(response) {
                    $("#answerModalLongTitle1").html(response.question);
                    $("#answerModalLong1").html(response.answer);
                    $("#answerModalCenter1").modal("show");
                },
                
            });
        }
    });
    // end answered click

    // answer click
    $(document).on("click","#answer",function() {
        var send = true;
        if (typeof(event) == 'object'){
            if (event.timeStamp - lastJQueryTS < 300){
                send = false;
            }
            lastJQueryTS = event.timeStamp;
        }
        if(send)
        { 
            var dataId = $(this).attr("data-id");
            $("#question_id").val(dataId);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'get',
                url  : APPLICATION_URL+'/getquestion/'+dataId,
                success: function(response) {
                    
                    $("#answerModalLongTitle").html(response.question);
                },
                
            });
            $("#answerModalCenter").modal("show");
        }
    });
    // end answer click

    // add answer 
    $(document).on("click","#add_answer",function() {
        var question_id=document.getElementById("question_id").value;
        var answer_data=document.getElementById("answer_data").value;
        if(answer_data=='')
        {
            $.notify({
                message: "Please add answer"
                },{
                type: 'danger',
                timer: 1000
            });

        }else{
            var send = true;
            if (typeof(event) == 'object'){
                if (event.timeStamp - lastJQueryTS < 300){
                    send = false;
                }
                lastJQueryTS = event.timeStamp;
            }
            if(send)
            { 
                $.ajaxSetup({      
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',   
                    data:{answer_data:answer_data,question_id:question_id},
                    url  : APPLICATION_URL+'/addanswer',
                    success: function(response) {
                        $.notify({
                            message: "Answer Submited Sucessfully"
                            },{
                            type: 'success',
                            timer: 1000
                        });
                        $("#answerModalCenter").modal("hide");
                        setTimeout(function(){ 
                            location.reload();    
                        }, 1000);

                        
                    },
                });
            }
        }
    });
    // end add answer

    //  change searching
    $('#p_title').change(function(e){ 
        if($('#p_title').val()!=''){

            var send = true;
            if (typeof(event) == 'object'){
                if (event.timeStamp - lastJQueryTS < 300){
                    send = false;
                }
                lastJQueryTS = event.timeStamp;
            }
            if(send)
            { 
                p_title = $('#p_title').val();
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                });
                $.ajax({ 
                    url:APPLICATION_URL+'/sorting-qa',
                    async:true, 
                    type: "POST",
                    data: {p_title:p_title},
                    success: function(data) {
                        $(".append_data").html(data);
                    }
                });
            }
        }
            
    });
    // end change searching

    // load more product
    var status='complete';
    function loadMoreproduct(){
        var send = true;
        if (typeof(event) == 'object'){
            if (event.timeStamp - lastJQueryTS < 300){
                send = false;
            }
            lastJQueryTS = event.timeStamp;
        }
        if(send)
        { 
            var startLimit = $('.qa_box').length;
            p_title = $('#p_title').val();
            if(status=='complete'){ 
                status='running';   
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url  : APPLICATION_URL+'/getmoreqalist',
                    data:{offset:startLimit,p_title:p_title},
                    success: function(data){
                        status='complete';
                        $(".append_data tr:last").after(data);
                        var totalRecordsLoaded = $('.qa_box').length;
                        var totalRecords = $('#Count_data').val();
                        if(totalRecords == totalRecordsLoaded){
                           $('#loadmore_icon').css("display","none");
                        } 
                        
                    }
                }); 
            }
        }
    }
    // load more product

    // windows scroll 
    /*
        $(window).scroll(function(e){
            if(($(window).scrollTop()+$(window).innerHeight())>=$('footer').offset().top){
                var totalRecordsLoaded = $('.qa_box').length;
                var totalRecords = $('#Count_data').val();
                if(totalRecords > totalRecordsLoaded){
                    loadMoreproduct();
                } 
            }
        });
    */
    //end window scroll




        
</script>

@endsection()
