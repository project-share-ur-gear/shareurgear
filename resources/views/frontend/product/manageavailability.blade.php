
@extends('layouts.app')

@section('content')


<link href="{{FRONT_CSS}}/main.min.css" rel="stylesheet" type="text/css">


<section class="common-section-top login-common-bg">
    <div class="container">
        
    </div>
</section>

<div class="over-all-home">

@include('sidebar')

<section class="profile-page login-page">
    <div class="container">
        <!--  -->
        <h2 class="login-title">Manage Availability</h2>
        <div class="manage-product-table">
            <div class="manage-data-avail">
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <td><div class="p-image"><img src="{{HTTP_UPLOADED_IMAGES_PATH}}/160X160/{{ $product_Data->primary_image }}" alt=""></div></td>
                                <td>{{$product_Data->product_title}}</td>
                                <td> @if(!empty($product_Data->productcategorytitle)) {{$product_Data->productcategorytitle}} @endif </td>
                                <td>${{$product_Data->price_per_day}}</td>
                                <td><span style="color:#8a71b2">Approved</span></td>
 
                                <td>
                                    <div class="A-flex justify-content-end">
                                        <a href="{{ SITE_HTTP_URL.'/edit-product/'.Crypt::encrypt($product_Data->product_id) }}" class="edit-icon"><img src="{{ FRONT_IMG.'/draw1.png'}}" alt=""></a>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="calendar-box">
                    <div id='calendar'></div>
                    <div class="mobile-view">
                        <ul class="">
                                <li> If you want to select one date push the option</li>
                                <li>you can select the date by dragging it</li>
                        </ul>
                    </div> 
                </div>
                
            </div>
         
        </div>
        <!--  -->
     
    </div>

   
</section>


</div>
 

 <div class="modal madel11 fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              
            </div>

            <div class="modal-body">
                <div class="comman-form">
                    <div class="col-12">
                    <div class="form-group">
                        <label class="col-xs-4" for="title">UnAvailability title</label><br>
                        <input type="text" name="title" id="title" class="form-control"/>
                        <input type="hidden" name="s_date" id="s_date"   />
                        <input type="hidden" name="e_date" id="e_date"  />
                    </div>
                    </div>
                </div>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn "  id="save-event" data-inprocess="Saving..." style="background-color:#8a71b2;color:white">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>


<script src="{{ asset('public/js/main.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/plugins/fullcalendar/packages/file/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('public/plugins/fullcalendar/packages/file/moment.min.js') }}"></script> 
 <!-- <script type="text/javascript" src="{{ asset('public/plugins/fullcalendar/packages/file/fullcalendar.min.js') }}"></script>  -->


  

<script>
    
    document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    
    // initialView: 'dayGridMonth',
    // initialDate: '2020-09-07',
    headerToolbar: {
      left: 'prev,next',
      center: 'title',
      right: 'month'
    },
    // dragScroll: true,
    eventLimitText: 'more',
    eventLimitClick: 'popover',
    selectLongPressDelay: false,
    unselectAuto: true,
        @if(!empty($eventdata))
		events: {!!$eventdata!!},
        @endif

        selectable:true,
        selectHelper: true,
        select:function(start, end, allDay)
        {
            var startdate = start.startStr;
            var enddate = start.endStr;
            $('#s_date').val(startdate);
            $('#e_date').val(enddate);
            var tb = new Date();
            var day = tb.getDate();
            var month = tb.getMonth()+1;
            if(day<10){
                day='0'+day;
            } 
            if(month<10){
                month='0'+month;
            } 
            var currentday=tb.getFullYear()+'-'+month+'-'+day;
            if(startdate>=currentday){
                $('.madel11').modal('show');
            }else{
                swal({
                    title: "",
                    text: "Make Unavailability on previous days are not allowed.",
                    imageClass: "contact-form-image",
                    confirmButtonText:"Ok",
                });
            }
        },
        eventClick: function(info) {
            info.jsEvent.preventDefault(); // don't let the browser navigate
            var title_check= info.event._def.title;
            if (info.event.id) {

        swal({   
                title:'',   
                text:'Are you want to sure Delete UnAvailability',   
                type:"warning",   
                showCancelButton:true,   
                confirmButtonText:'Yes',
                cancelButtonText:'Cancle',     
                closeOnConfirm:true,   
                closeOnCancel:true 
                },function(isConfirm){   
                if(isConfirm){     
                    removeAvailability(info.event.id);
                } 
            });

              
            }else{
                  showAppAlert('Sorry','You cannot delete this slots','error');
            }
        }
        // editable:true,
  });

    
  calendar.render();
 
});



$('#save-event').on('click', function() {
    var title  =$("#title").val();
    var s_date  =$("#s_date").val();
    var e_date =$("#e_date").val();
    if(title=='')
    {
        $.notify({
            message: "something is going wrong."
            },{
            type: 'danger',
            timer: 1000
        });
    }
    else
    {

        freezeButton('#save-event',$('#save-event').data('inprocess'),'disabled');
        setTimeout(function(){ addunAvailability(s_date,e_date,title); }, 1000);
         
    }
});







function addunAvailability(startdate, enddata,title){
    var p_id='{{$product_id}}';
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        });
      $.ajax({
      type: 'POST',
      url  :   APPLICATION_URL+'/add-event/{{$product_id}}',
      data: { 
          start: startdate,
          end:  enddata,
          title: title,
          p_id:p_id,
        },
      dataType: 'JSON',
      success: function(response) {
          if(response=='1')
          {
            $(".modal .close").click();
            showpopupmessage('Unavability Added Sucessfully','success');
            // window.location.href = '{{ url("/manage-availability/$product_id")}}';
            setTimeout(function(){
                location.reload();
            },1000)
          }
      }
       
    });
}

function removeAvailability(remove_id){
    $.ajax({
        type: 'get',
        url  :   APPLICATION_URL+'/remove-event/'+remove_id,
        // dataType: 'JSON',
        success: function(response) {
            if(response=='sucess')
            {
                showpopupmessage('Unavability Remove Sucessfully','success');
                // window.location.href = '{{ url("/manage-availability/$product_id")}}';
                setTimeout(function(){
                    location.reload();
                },1000)
            }else{
                showpopupmessage('Invalid Request','error');
                setTimeout(function(){
                    location.reload();
                },1000)


            }
        }
       
    });
}



</script>


@endsection()