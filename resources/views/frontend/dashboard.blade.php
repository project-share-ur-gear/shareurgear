@extends('layouts.app')
@section('content')
@include('frontend.breadcrumb')

<section class="main-dashboard">
    <div class="container">
        <h4 class="text-blue">Dashboard</h4>
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="dashboard-detail-block">
                    <img src="{{ FRONT_IMG }}/dashboard-icon1.png" class="img-fluid float-left mr-2" />
                    <span class="font-size-15 text-left d-block overflow-hidden">$0<br/>Total Earned</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="dashboard-detail-block">
                    <img src="{{ FRONT_IMG }}/dashboard-icon2.png" class="img-fluid float-left mr-2" />
                    <span class="font-size-15 text-left d-block overflow-hidden">0<br/>Total Sessions</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="dashboard-detail-block">
                    <img src="{{ FRONT_IMG }}/dashboard-icon3.png" class="img-fluid float-left mr-2" />
                    <span class="font-size-15 text-left d-block overflow-hidden">0<br/>Upcoming Sessions</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="dashboard-detail-block">
                    <img src="{{ FRONT_IMG }}/dashboard-icon4.png" class="img-fluid float-left mr-2" />
                    <span class="font-size-15 text-left d-block overflow-hidden">0<br/>Completed Sessions</span>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12 col-md-12 col-lg-3">
                @include('frontend.navbar')
            </div>
            <div class="col-12 col-md-12 col-lg-9">
                <div class="tab-content" id="v-pills-tabContent">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                        <h5 class="pb-3 border-bottom font-weight-500">Upcoming Sessions</h5>
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Created Session</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Attend Session</a>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection