<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{SITE_HTTP_URL}}">Home</a></li>
            <li class="breadcrumb-item"><a href="{{SITE_HTTP_URL}}/user-profile">My Profile</a></li>
        </ol>
    </nav>
</div><!--container-->