@php
$fullPath=Route::getCurrentRoute();
@endphp
<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
    <a class="nav-link" href="{{SITE_HTTP_URL.'/user/dashboard' }}" data-title="user/dashboard">Dashboard</a>
    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Completed Session <i class="fas fa-chevron-down float-right mt-1"  data-toggle="collapse" href="#completed-session-collapse" role="button" aria-expanded="false" aria-controls="completed-session-collapse"></i>
        <div class="collapse" id="completed-session-collapse">
            <b class="text-dark-grey d-block">Created Session</b>
            <b class="text-dark-grey d-block">Attended Session</b>

        </div>
    </a>
    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Session Management <i class="fas fa-chevron-down float-right mt-1"  data-toggle="collapse" href="#session-management-collapse" role="button" aria-expanded="false" aria-controls="session-management"></i>
        <div class="collapse" id="session-management-collapse">
            <b class="text-dark-grey d-block">Created Session</b>
            <b class="text-dark-grey d-block">Attended Session</b>

        </div>
    </a>
    <a class="nav-link" href="{{SITE_HTTP_URL.'/courses' }}" data-title="courses">Course Management</a>
    <a class="nav-link" id="v-pills-wallet-tab" data-toggle="pill" href="#v-pills-wallet" role="tab" aria-controls="v-pills-wallet" aria-selected="false">My Wallet</a>
</div>

<script>
var current_route = '{{$fullPath->uri}}';  //+'-'+current_action;
$(document).ready(function(){
  $('[data-title="'+current_route+'"]').addClass('active');
});
</script>
