@php
$locale=App::getLocale();
$homeData = DB::table('homepage')->get();
$home_info=array();
foreach($homeData as $allData){
    $home_info[$allData->home_key]=$allData->home_content;
}

$catData = DB::table('services')->where('parent_id','=',NULL)->where('status','1')->get();
$cat_info=array();
foreach($catData as $allData){
    $name='name_'.$locale;
    $cat_info[$allData->id]=$allData->$name;
}

$servicesData = DB::table('services')->where('parent_id','!=',NULL)->where('status','1')->get();
$service_info=array();
foreach($servicesData as $allData){
    $name='name_'.$locale;
    $service_info[$allData->id]=$allData->$name;
}

$cat_info = array(__('general.category'))+$cat_info;
$service_info=array(__('general.services'))+$service_info;
@endphp
<section class="about-us-page" style="background-image:url({{SITE_HTTP_URL}}{{ Storage::url('app/public/static/'.$home_info['searchbar_image']) }})">
    <div class="container">
        <div class="about-banner">
            <h2 class="title1">{!! $pageHeading !!}</h2>
            <div class="search-panel">
                <h4 class="title">{!! $home_info['searchbar_title_'.$locale] !!}</h4>
                <div class="form-search">
                    <form class="form-inline" method="get" action="{{ url('/search-providers') }}">
                        <div class="form-group">
                            <select name="category" class="form-control selectpicker categoryselector">
                                <?php foreach($cat_info as $key=>$cat_values) {  ?>
                               		 <option value="<?=$key?>"><?=$cat_values?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <select name="service" class="form-control custom-select serviceselector">
                                <option value="">{{ __('general.choose_service') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="location" id="location" type="text" maxlength="75" class="form-control border-0" placeholder="{{ __('general.search_location_placeholder') }}">
                        </div>
                        <button type="submit" class="search-btns">
                            {!! $home_info['searchbar_button_'.$locale] !!}
                        </button>
                    </form>
                </div>
            </div>
            <a href="{{ url('signup') }}">{!! $home_info['searchbar_search_'.$locale] !!}</a>
        </div>
    </div>
</section>
        </div>
    </div>
</section>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqVOKulpEhFwPZd7R5h_zqW70Ch19DVOI&libraries=places&sensor=false&callback=initMap" defer async></script>

<script type="text/javascript">
function initMap(){
    var input = document.getElementById('location');
    var autocomplete = new google.maps.places.Autocomplete(input);
}
</script>