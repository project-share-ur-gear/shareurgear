<?php
$requestPath = explode('/',Request::path());

$providerPaths = '';
if(isset($requestPath[1])){
	if(!empty($requestPath[0]) && $requestPath[1]){
		$providerPaths = $requestPath[0].'/'.$requestPath[1];
	}
}
$requestPath = $requestPath[0];
?>

<script src="{{ asset('public/js/popper.min.js') }}"></script>
<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('public/js/mdb.min.js') }}"></script>

<script src="{{ asset('public/js/bootstrap-select.js') }}"></script>

<script src="{{ asset('public/plugins/customscroolbar/jquery.mCustomScrollbar.min.js') }}"></script>

<script src="{{ asset('public/js/jquery-validation/jquery.validate.min.js?v=').env('APP_VERSION') }}"></script>
<script src="{{ asset('public/js/jquery-validation/additional-methods.min.js?v=').env('APP_VERSION') }}"></script>
<script src="{{ asset('public/plugins/chosen/chosen.jquery.js?v=').env('APP_VERSION') }}"></script>
<script src="{{ asset('public/js/jquery.maskedinput.js?v=').env('APP_VERSION') }}"></script>
<script src="{{ asset('public/js/bootstrap-notify.js') }}"></script>
<script src="{{ asset('public/js/moment.js') }}"></script>

<script src="{{ asset('public/plugins/selectv2/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('public/plugins/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.js') }}"></script>


<script src="{{ asset('public/js/select2.min.js') }}"></script>
<script src="{{ asset('public/js/slick.min.js') }}"></script>

<script src="{{ asset('public/js/aos.js') }}"></script>
<script src="{{ asset('public/plugins/blockui/jquery.blockui.min.js') }}"></script>
<script src="{{ asset('public/js/jquery.blockUI.js') }}"></script>

<script src="{{ asset('public/js/general.js?time='.time()) }}"></script>
<script src="{{ asset('public/js/custom.js') }}"></script>
<script src="{{ asset('public/plugins/bootstrap-sweetalert/sweetalert.js') }}"></script>
