<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    @php
    $requestPath = explode('/',Request::path());
    $fullRequestPath = $requestPath;
    $requestPath = $requestPath[0];
	@endphp
    
    
    @if(!empty($metaTitle))
    <title>{!! $metaTitle !!} | {{ config('app.name', $site_configs['site_name_en']) }}</title>
    <meta name="keywords" content="{!! $metaKeywords !!}">       
    <meta name="description" content="{!! $metaDescription !!}">
    @endif

    @yield('share.meta')
    
    <link rel="canonical" href="{{url()->current()}}" /> 

	<!-- FAV ICON -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ FRONT_IMG }}/favicons/favicon.png?v=1.0">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ FRONT_IMG }}/favicons/favicon.png?v=1.0">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ FRONT_IMG }}/favicons/favicon.png?v=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    {{-- <!-- FAV ICON -->
	<link rel="shortcut icon" href="{{ FRONT_IMG }}/favicons/favicon.png"> --}}
    <!--<link rel="apple-touch-icon" sizes="60x60" href="{{ FRONT_IMG }}/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ FRONT_IMG }}/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ FRONT_IMG }}/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ FRONT_IMG }}/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ FRONT_IMG }}/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ FRONT_IMG }}/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ FRONT_IMG }}/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ FRONT_IMG }}/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ FRONT_IMG }}/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ FRONT_IMG }}/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ FRONT_IMG }}/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ FRONT_IMG }}/favicons/favicon-16x16.png">
    <link rel="manifest" href="{{ FRONT_IMG }}/favicons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ FRONT_IMG }}/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">-->
        
    

    <!-- Font aweeosme -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Font import poppins -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- All CSS files are included in this file -->
    @include('layouts.cssincluder')

    <script src="{{ asset('public/js/jquery.min.js') }}"></script>
	@php
    	$fullPath = Route::getCurrentRoute();
    @endphp
    <!-- Scripts -->
    
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
        
        var APPLICATION_NAME = "{{ env('APP_NAME') }}";
        var APPLICATION_URL = "{{ env('APP_URL') }}";
        var PANEL_PREFIX = '';
		var ROUTE_NAME='{{$fullPath->uri}}';
		var IMAGE_VALID_SIZE=parseInt('<?=IMAGE_VALID_SIZE?>');
		var locale='<?=$locale?>';
        var fieldRequired = '{{ __('general.required_field') }}';
        var validEmailText = '{{ __('general.enter_valid_email') }}';
        var validNumber = '{{ __('general.enter_valid_number') }}';
        var digitsOnly = '{{ __('general.enter_digits_only') }}'

        /* Commonly used text */
        var yesBtnAlert         = '{{ __('general.yes') }}';
        var cancelBtnAlert      = '{{ __('general.cancel') }}';

        var invalidReq = '{{ __('general.Invalid request to access page! Please check your information and try again') }}';
        var noresults = '{{ __('general.No Results found') }}';
		var confirmedText = '{{ __("general.confirmed") }}';
        var invalidImage = '{{ __("general.Uploaded image is not valid") }}';
        var confirmPhoneToProceedText = '{{ __("general.Please confirm your phone number first to proceed") }}';
        var ChooseTxt = '{{ __("general.choose") }}';

        
    </script>
    
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    @php    
    if(checkIos()) { 
    @endphp
    <link href="{{FRONT_CSS}}/ios-design.css?time=<?=time()?>" rel="stylesheet"  type="text/css" >
   	@php }
    @endphp



  </head>
  <body class="body-cssimg">
   
    @include('layouts.elements.header')
    @include('layouts.elements.message')        

    <div class="content-wrapper">
        @yield('content')
    </div>
    
    @include('layouts.elements.footer')
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="upcomingmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header" style="border-bottom: 1px solid #dee2e6;padding: 1rem 1rem;">
                    <h5 class="modal-title" id="exampleModalLabel">Notice</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    Feature to be developed in upcoming milestones
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" style="background-color:#8a71b2 !important;" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
        
        jQuery(document).ready(function($) {
            $('.nextMilestoneButton').click(function(event) {
                /* Act on the event */
                $('#upcomingmodal').modal({show:true,keyboard:false})
            }); 
        });
    </script>
    
    <!-- All JS links are included here -->
    @include('layouts.jsincluder')
    
    </body>
</html>