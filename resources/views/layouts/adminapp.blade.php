<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

	<!-- FAV ICON -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/img/favicons/favicon.png?v=').env('APP_VERSION') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/img/favicons/favicon.png?v=').env('APP_VERSION') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/img/favicons/favicon.png.png?v=').env('APP_VERSION') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }} | {{ $pageTitle}}</title>

    
    <!-- Bootstrap 3.3.7 -->
    <link href="{{ asset('public/admin_ui/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('public/admin_ui/bower_components/font-awesome/css/font-awesome.min.css') }}">

    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('public/admin_ui/bower_components/Ionicons/css/ionicons.min.css') }}">

    <!-- DataTables -->
    {{-- <link rel="stylesheet" href="{{ asset('public/admin_ui/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}"> --}}
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/v/bs/dt-1.10.25/b-1.7.1/r-2.2.9/datatables.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public/admin_ui/dist/css/AdminLTE.min.css') }}">

    <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load.--->
    <link rel="stylesheet" href="{{ asset('public/admin_ui/dist/css/skins/_all-skins.min.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link href="{{ asset('public/admin_ui/bootstrap/css/bootstrap-datetimepicker.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_ui/plugins/datatables/dataTables.bootstrap.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('public/admin_ui/bootstrap/css/source_sans_pro.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    {{-- <link href="{{ asset('public/admin_ui/dist/css/admin_style.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_ui/dist/css/admin_custom.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_ui/dist/css/admin_custom_responsive.css?v=').env('APP_VERSION') }}" rel="stylesheet">    
        
    <link href="{{ asset('public/plugins/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css"> --}}

    <link href="{{ asset('public/plugins/bootstrap-star-rating-master/css/star-rating.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{ asset('public/admin_ui/dist/css/admin_custom.css') }}" rel="stylesheet" type="text/css">

    <!-- Scripts -->
    <script src="{{ asset('public/admin_ui/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>

        var APPLICATION_NAME = "{{ env('APP_NAME') }}";
        var APPLICATION_URL = "{{ env('APP_URL') }}";
		    var ADMIN_APPLICATION_URL = "{{ env('ADMIN_APP_URL') }}";
        var PANEL_PREFIX = 'admin';
    </script>
    <style type="text/css">
        #alert{padding-left: 20px;margin-bottom: 0;border-radius: 0;}
    </style>
</head>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  @include('layouts.elements.admin_header')

  @include('layouts.elements.admin_sidebar')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @include('layouts.elements.admin_message')
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        {{ $pageTitle }}
        <!-- <small>Version 1.0</small> -->
      </h1>
      <ol class="breadcrumb">
        <li>
          <a href="{{ url('/admin/dashboard') }}"><i class="fa fa-dashboard"></i>&nbsp;Home</a></li>
        <li class="active">{{ $pageTitle }}</li>
      </ol>
    </section>
    
    <?php //echo $this->Session->flash(); ?>

    @yield('content')

  </div><!-- /.content-wrapper -->

    @include('layouts.elements.admin_footer')

    <?php //echo $this->element('admin_control_sidebar');?>

    <?php //echo $this->element('sql_dump'); ?>

</div><!-- ./wrapper -->

	<script src="{{ asset('public/admin_ui/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Slimscroll -->
    <script src="{{ asset('public/admin_ui/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    
    <!-- FastClick -->
    <script src="{{ asset('public/admin_ui/bower_components/fastclick/lib/fastclick.js')}}"></script>

    <script src="{{ asset('public/admin_ui/bootstrap/js/bootstrap-datetimepicker.min.js?v=').env('APP_VERSION') }}"></script>

    <!-- AdminLTE App -->
    <script src="{{ asset('public/admin_ui/dist/js/adminlte.min.js')}}"></script>
    
    <script src="{{ asset('public/js/jquery-validation/jquery.validate.min.js?v=').env('APP_VERSION') }}"></script>
	  <script src="{{ asset('public/js/jquery-validation/additional-methods.min.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/js/ckeditor/ckeditor.js?v=').env('APP_VERSION') }}"></script>
    {{-- <script src="{{ asset('public/admin_ui/dist/js/jquery.dataTables.min.js?v=').env('APP_VERSION') }}"></script>
	  <script src="{{ asset('public/admin_ui/dist/js/datatables.bootstrap.js?v=').env('APP_VERSION') }}"></script> --}}
    <script src="https://cdn.datatables.net/v/bs/dt-1.10.25/b-1.7.1/r-2.2.9/datatables.min.js"></script>
    <script src="{{ asset('public/plugins/bootstrap-tagsinput-latest/dist/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-star-rating-master/js/star-rating.min.js') }}"></script>
    <script src="{{ asset('public/admin_ui/dist/js/general.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/admin_ui/dist/js/custom.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/plugins/bootstrap-sweetalert/sweetalert.js') }}"></script>


</body>
</html>
