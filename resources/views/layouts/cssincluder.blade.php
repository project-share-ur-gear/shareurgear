<?php
$requestPath = explode('/',Request::path());
$providerPaths = '';
if(isset($requestPath[1])){
	if(!empty($requestPath[0]) && $requestPath[1]){
		$providerPaths = $requestPath[0].'/'.$requestPath[1];
	}
}
$requestPath = $requestPath[0];
?>

<!-- Font aweeosme -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

<!-- Font import poppins -->
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">

<!-- Bootstrap CSS -->
<link href="{{FRONT_CSS}}/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="{{FRONT_CSS}}/mdb.min.css" rel="stylesheet" type="text/css">
<link href="{{ asset('public/plugins/chosen/chosen.min.css') }}" rel="stylesheet" type="text/css">

<link href="{{FRONT_CSS}}/owl.carousel.min.css" rel="stylesheet" type="text/css">
<link href="{{FRONT_CSS}}/owl.theme.default.min.css" rel="stylesheet" type="text/css">

<link href="{{FRONT_CSS}}/bootstrap-select.css" rel="stylesheet" type="text/css">
<link href="{{ asset('public/plugins/selectv2/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('public/plugins/bootstrap-tagsinput-latest/src/bootstrap-tagsinput.css') }}" rel="stylesheet" type="text/css">


<link href="{{FRONT_CSS}}/select2.min.css" rel="stylesheet" type="text/css">
<link href="{{FRONT_CSS}}/slick.css" rel="stylesheet" type="text/css">
<link href="{{FRONT_CSS}}/slick-theme.css" rel="stylesheet" type="text/css">

<link href="{{FRONT_CSS}}/aos.css" rel="stylesheet" type="text/css">
<link href="{{FRONT_CSS}}/animate.css" rel="stylesheet" type="text/css">

<link href="{{FRONT_CSS}}/custom.css" rel="stylesheet" type="text/css">
<link href="{{FRONT_CSS}}/screen.css" rel="stylesheet" type="text/css">
<link href="{{ asset('public/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">



