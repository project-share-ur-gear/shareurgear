<?php
$requestPath = explode('/',Request::path());
$requestPath = $requestPath[0];
$fullPath=Route::getCurrentRoute();
$isUser = Auth::guard('user')->id();
$user =  auth()->guard('user')->user(); 
?>

@section('content')
    <?php 
        if(empty($browse_controller)){
            $browse_controller='';
        }
        if(empty($category)){
            $category='';
        }
        
    ?>
  <?php 
	            $url=SITE_HTTP_URL_API.'getcat';
				$curl = curl_init();
				curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'get',
                    CURLOPT_HTTPHEADER => array(
                        'Content-Type: application/json',
                        'token: '.CURL_KEY.''
                    ),
				));
                $response = curl_exec($curl);
                if($response=='failed'){
					return  redirect(route('static.errorpage'));
				}else if(empty($response)){
					$category_Data=array();
				}else{
                    $category_Data= json_decode($response);
                }
				curl_close($curl);

    ?>
<?php $local=SITE_HTTP_URL;?>


<header id="header">
    <div class="main-header">
        <div class="container-fluid">
            <div class="header-section">
                <a class="navbar-brand desktop-logo" href="<?php echo SITE_HTTP_URL?>"> 
                	<!-- <img src="{{ FRONT_IMG.'/logo.png' }}" class="searchimg"/> -->
                    <img src="<?php echo SITE_HTTP_URL?>/storage/app/public/logo/{{$site_configs['site_logo']}}" class="searchimg"/>
                </a>
                <!-- search box -->
                <nav class="nav-header navbar-expand-xl p-0">
                    <div class="search-icons d-xl-none d-block"><i class="fas fa-search"></i></div>
                    <div class="search-panels pb-0 common-h-bar">
                        <span class="closebtn">×</span>
                           
                          @if($browse_controller!='browse_controller')
                        <form class="search_rental" method="get" action="{{ url('/browse-rental') }}">
                            @endif
                           
                            <div class="seach-panels">
                                <div class="search-row form-row align-items-center">
                                    <div class="col-lg-5 col-sm-5">
                                        <div class="form-group"><input type="text" class="form-control" placeholder="What are you looking for?" id="keyword" name="keyword" value="@if(!empty($keyword)){{$keyword}}@endif" ></div>
                                    </div>
                                    <div class="col-lg-4 col-sm-4">
                                        <div class="form-group type-select">
                                            <select class="form-control selectpicker" id="category" name="category" >
                                                <option value="0">Category </option>
                                                @if(!empty($category_Data))
                                                    @foreach($category_Data as $key => $value)
                                                        <option  value="{{$value->product_category_id}}" @if($category == $value->product_category_id) selected @endif>{{$value->productcategorytitle}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-sm-3">
                                        <div class="search-btn btn " id="search_button"  >Search</div>
                                    </div>
                                </div>
                            </div>
                      
                        @if($browse_controller!='browse_controller')
                        </form> 
                        @endif
                          
                    </div>
                    
                    <ul class="navbar-nav d-none d-xl-flex">
                        <li class="nav-item    "><a href="{{ url('/browse-rental') }}" class="nav-link {{Request::path() === 'browse-rental' ? 'active':''}}"> <span>Browse</span> </a> </li>

                        @if(Auth::guard('user')->check()==false)
                        <li class="nav-item"><a class="nav-link {{ Request::path() === 'login' ? 'active':'' }}" href="{{ url('login') }}"><span>Login</span></a> </li>
                        <li class="nav-item"><a class="nav-link signup-btn" href="{{ url('signup') }}">Sign Up</a></li>
                        @endif
                    </ul>
                   

                    @if(Auth::guard('user')->check()==true)
                    @php

                        $profileImage=FRONT_IMG.'/nophoto.png';
                        if(Auth::guard('user')->user()->profile_image!='')
                            $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.Auth::guard('user')->user()->profile_image);
                    @endphp
                    <div class="user nav-item d-none d-xl-flex">
                        <a class="dropdown-toggle dropdown nav-link" data-toggle="dropdown">
                            <span> {{ Auth::guard('user')->user()->name }} </span> 
                                <div class="user_image"> 
                                    <img src="{{ $profileImage }}" alt="">
                                </div>
                        </a>
                        <div class="dropdown-menu header_menu" aria-labelledby="Preview">
                            <a class="dropdown-item" href="{{ SITE_HTTP_URL.'/account-setting' }}">
                                {{ __('general.profile') }}
                            </a>
                            <a class="dropdown-item" href="" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('general.header_logout') }}
                            </a>
                        </div>
                    </div>
                    @endif
                </nav>
            	<!-- remove -->
                
                <div class="d-block d-xl-none">
                    <div id="btn">
                        <div id='top'></div>
                        <div id='middle'></div>
                        <div id='bottom'></div>
                    </div>
                
                    <div id="box">
                        <ul id="items">
                            <li class="item "><a href="{{ url('/browse-rental') }}" class="nav-link {{Request::path() === 'browse-rental' ? 'active':''}} "> <span>Browse</span> </a> </li>
                            @if(Auth::guard('user')->check()==false)

                            <li class="item"><a class="nav-link {{ Request::path() === 'login' ? 'active':'' }}" href="{{ url('login') }}"> Login </a></li>
                            <li class="item"> <a class="nav-link {{ Request::path() === 'signup' ? 'active':'' }}" href="{{ url('signup') }}">  Sign Up</a></li>
                            @endif
                            @if(Auth::guard('user')->check()==true)
              
                            <li class="item">
                                <a class="nav-link " href="{{ SITE_HTTP_URL.'/account-setting' }}">
                                    {{ __('general.profile') }}
                                </a>
                            </li>
                        
                            <li class="item "> <a class="nav-link {{ Request::path() === 'payout-setting' ? 'active':'' }}" href="{{ SITE_HTTP_URL.'/payout-setting' }}">Payout Settings</a> </li>
                            <li class="item "> <a class="nav-link {{ Request::path() === 'manage-product' ? 'active':'' }}" href="{{ SITE_HTTP_URL.'/manage-product' }}">Manage Products </a> </li>
                            <li class="item "> <a class="nav-link {{ Request::path() === 'add-product' ? 'active':'' }}" href="{{ SITE_HTTP_URL.'/add-product/add' }}">Add Products </a> </li>
                            <li class="item "> <a class="nav-link {{ Request::path() === 'wish-list' ? 'active':'' }}" href="{{ SITE_HTTP_URL.'/wish-list' }}">Wish List</a> </li>
                            <li class="item  "> <a class="nav-link {{ Request::path() === 'manage-Q&a' ? 'active':'' }}" href="{{ SITE_HTTP_URL.'/manage-Q&a' }}">Manage Q&A</a> </li>
                            <li class="item "> <a class="nav-link {{ Request::path() === 'messaging' ? 'active':'' }}" href="{{ SITE_HTTP_URL.'/messaging' }}">Messaging <span class="msg_count"></span></a> </li>
         
                            <li class="item {{ Request::path() === 'my-bookings' ? 'active':'' }}"> <a class="nav-link" href="{{ route('booking.mybookings') }}">My Bookings</a> </li>
                            <li class="item {{ Request::path() === 'sharer-bookings' ? 'active':'' }}"> <a class="nav-link" href="{{ route('booking.sharerbookings') }}">Sharer Bookings</a> </li>
                      
                            <li class="item">
                                <a href="javascript:void(0)" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    {{ __('general.header_logout') }}
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            <!-- remove -->
            <form id="logout-form" action="{{ url('user/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
            </div>
        </div>
    </div>
</header>


<script>
function toggleNav() { $('body').toggleClass('navOpen');}

var sidebarBox = document.querySelector('#box');
var sidebarBtn = document.querySelector('#btn');
var pageWrapper = document.querySelector('#main-content');


sidebarBtn.addEventListener('click', function(event){
    if (this.classList.contains('active')) {
        this.classList.remove('active');
        sidebarBox.classList.remove('active');
    } 
    else {
        this.classList.add('active');
        sidebarBox.classList.add('active');
    }
    $('body').toggleClass('navOpen');
});

$(document).ready(function(){
    $('.search-icons').on('click', function() {
        $('.common-h-bar').toggle(function() {
            $(this).animate({             
            }, 500);
        });
        $('body').toggleClass('navOpen');
    }); 

    $('.closebtn').on('click', function() {
        $('.common-h-bar').toggle(function() {
            $(this).animate({              
            }, 500);
        });
        $('body').removeClass('navOpen');
    });
});

$('#search_button').on('click', function() {
    $(".search_rental").submit();
}); 

/*
$(window).scroll(function () {
    if (sc > 150) {
        $("#header").addClass("header-fixed")
    } 
    else {
        $("#header").removeClass("header-fixed")
    }
});*/

</script>