
<header class="main-header">

  <!-- Logo -->
  <a href="{{ env('APP_URL').'/admin/dashboard' }}" class="logo">
	<!-- mini logo for sidebar mini 50x50 pixels -->
	<span class="logo-mini"><b>{{ substr(env('APP_NAME'),0,1) }}</b></span>
	<!-- logo for regular state and mobile devices -->
	<span class="logo-lg"><b>{{ env('APP_NAME') }}</b></span>
  </a>

  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</a>
	<!-- Navbar Right Menu -->
	<div class="navbar-custom-menu">
	  <ul class="nav navbar-nav">
		<!-- User Account: style can be found in dropdown.less -->
		<li class="dropdown user user-menu">
		  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
			  @if(Session::get('AdminData')->profile_image!='' && file_exists(('storage/app/public/admin_profile_photo/'.Session::get('AdminData')->profile_image)) )
				
				<img src="{{SITE_HTTP_URL}}{{ Storage::url('admin_profile_photo/thumb_'.Session::get('AdminData')->profile_image) }}" alt="{{ Session::get('AdminData')->name }}" title="{{ Session::get('AdminData')->name }}"  class="user-image">

			  @else

				<img src="{{ asset('storage/image_not_found_mini.jpg') }}" alt="{{ Auth::guard('admin')->user()->name }}" title="{{ Auth::guard('admin')->user()->name }}" class="user-image">

			  @endif 
			  
			
			<span class="hidden-xs">
			  {{-- Auth::guard('admin')->user()->name --}}
			  {{ Session::get('AdminData')->name }}
			</span>
		  </a>
		  <ul class="dropdown-menu">
			<!-- User image -->
			<li class="user-header">
			  @if(Session::get('AdminData')->profile_image!='' && file_exists(('storage/app/public/admin_profile_photo/'.Session::get('AdminData')->profile_image )) )
				
				<img src="{{SITE_HTTP_URL}}{{ Storage::url('admin_profile_photo/thumb_'.Session::get('AdminData')->profile_image) }}" alt="{{ Session::get('AdminData')->name }}" title="{{ Session::get('AdminData')->name }}"  class="img-circle">

			  @else

				<img src="{{ asset('storage/image_not_found_medium.jpg') }}" alt="{{ Session::get('AdminData')->name }}" title="{{ Session::get('AdminData')->name }}" class="img-circle">

			  @endif              
			  <p>
				{{ Session::get('AdminData')->name }}
				<small>{{ Session::get('AdminData')->email }}</small>
				<small>
				  {{ __('admin.labels.lastloggedinon')}}: 
				  {{ \Carbon\Carbon::parse(Session::get('AdminData')->last_logged_in_on)->format( env('PHP_DATETIME_DISPLAYING_FORMAT') )}}
				</small>
			  </p>
			</li>
			<!-- Menu Footer-->
			<li class="user-footer">
			  <div class="pull-left">
				<a href="{{ url('admin/edit') }}" class="btn btn-success btn-flat">Profile</a>
			  </div>
			  <div class="pull-right">
				  <a href="{{ url('admin/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-danger btn-flat">Sign Out</a>

				  <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
					  {{ csrf_field() }}
				  </form>
			  </div>
			</li>
		  </ul>
		</li>

	  </ul>
	</div>

  </nav>
</header>