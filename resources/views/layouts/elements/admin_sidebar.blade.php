@php
    $fullPath=Route::getCurrentRoute();
@endphp

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    	<!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                @if(Session::get('AdminData')->profile_image!='' && file_exists(('storage/app/public/admin_profile_photo/'.Session::get('AdminData')->profile_image )) )
                <img src="{{SITE_HTTP_URL}}{{ Storage::url('admin_profile_photo/thumb_'.Session::get('AdminData')->profile_image) }}" alt="{{ Session::get('AdminData')->name }}" title="{{ Session::get('AdminData')->name }}"  class="img-circle">
                @else
                <img src="{{ asset('storage/admin_profile_photo/image_not_found_medium.jpg') }}" alt="{{ Session::get('AdminData')->name }}" title="{{ Session::get('AdminData')->name }}" class="img-circle">
                @endif
            </div>
            <div class="pull-left info">
            	<p>{{ Session::get('AdminData')->name }}</p>
            	<a href="javascript:void(0)">Administrator</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
        	<li class="header">MAIN NAVIGATION</li>
            <li data-set="admin/dashboard">
                <a href="{{ url('/admin/dashboard') }}">
                    <i class="fa fa-dashboard"></i> 
                    <span>Dashboard</span>
                </a>
            </li>
            <li data-set="admin/config">
            	<a href="{{ url('/admin/config') }}">
                    <i class="fa fa-cogs"></i>
                    <span>Configurations</span>
                </a>
            </li>
            <li class="treeview">
            	<a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Static Content</span>
                    <span class="pull-right-container">
                    	<i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li data-set="admin/pages/index" class="childmenu">
                    	<a href="{{ url('/admin/pages/index') }}"><i class="fa fa-list"></i> <span>Pages</span></a>
                    </li>
                     
                    <li data-set="admin/templates/index" class="childmenu">
                    	<a href="{{ url('/admin/templates/index') }}"><i class="fa fa-envelope"></i> <span>Email Templates</span></a>
                    </li>
                    <li data-set="admin/faq/index" class="childmenu">
                    	<a href="{{ url('/admin/faq/index') }}"><i class="fa fa-question-circle"></i> <span>Faq</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
            	<a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Products</span>
                    <span class="pull-right-container">
                    	<i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                
                    <li data-set="admin/product/product-categories" class="childmenu">
                    	<a href="{{ url('/admin/product/product-categories') }}"><i class="fa fa-list"></i> <span>Product Categories</span></a>
                    </li>
                     
                    <li data-set="admin/product/sub-categories" class="childmenu">
                    	<a href="{{ url('/admin/subproduct/sub-product') }}"><i class="fa fa-list-alt"></i> <span>Sub Categories</span></a>
                    </li>
                    <li data-set="admin/product/product-requests" class="childmenu">
                    	<a href="{{ url('/admin/product/product-request') }}"><i class="fa fa-database"></i> <span>Product Requests</span></a>
                    </li>
                    <li data-set="admin/product/manage-products" class="childmenu {{ Request::path() === 'admin/product/manage-products' ? 'active':'' }}">
                    	<a href="{{ url('/admin/product/manage-request') }}"   ><i class="fa fa-product-hunt"></i> <span>Manage Products</span></a>
                    </li>
                </ul>
            </li>

            <li class="treeview">
            	<a href="#">
                    <i class="fa fa-product-hunt"></i>
                    <span>Manage Bookings</span>
                    <span class="pull-right-container">
                    	<i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                
                    <li data-set="{{  route('admin.booking.bookings')  }}" class="childmenu">
                    	<a href="{{ route('admin.booking.bookings') }}"><i class="fa fa-list"></i> <span>Bookings</span></a>
                        <a href="{{ route('admin.booking.cancelbookings') }}"><i class="fa fa-list"></i> <span>Cancel Bookings</span></a>
                        <a href="{{ route('admin.booking.reviews') }}"><i class="fa fa-list"></i> <span>Bookings Reviews</span></a>
                    </li>
                </ul>
            </li>

            <li data-set="admin/specialties/index" class=" {{ Request::path() === 'admin/users/client' ? 'active':'' }}">
                <a href="{{ url('/admin/users/client') }}">
                    <i class="fa fa-user"></i>
                    <span>Manage User</span>
                </a>
            </li> 

        </ul>
    </section>
</aside>

<script>
var current_route = '{{Request::path()}}';  //+'-'+current_action;
$(document).ready(function(){
	$('[data-set="'+current_route+'"]').addClass('active');
	if($('[data-set="'+current_route+'"]').hasClass("childmenu")){
		$('[data-set="'+current_route+'"]').parents(".treeview").attr("class","treeview active open-menu");
	}
	$('.content-wrapper').css('min-height',$('.main-sidebar').innerHeight()+'px');
});
</script>