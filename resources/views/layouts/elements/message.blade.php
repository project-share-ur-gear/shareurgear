@if(Session::has('success'))
    <div id="alert" class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('success') }}&nbsp;&nbsp;
    </div>
@endif

@if(Session::has('error'))
    <div id="alert" class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('error') }}&nbsp;&nbsp;
    </div>
@endif

@if(Session::has('warning'))
    <div id="alert" class="alert alert-warning">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('warning') }}&nbsp;&nbsp;
    </div>
@endif

@if(Session::has('info'))
    <div id="alert" class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('info') }}&nbsp;&nbsp;
    </div>
@endif


@if(Session::has('status'))
    <div id="alert" class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        {{ Session::get('status') }}&nbsp;&nbsp;
    </div>
@endif


@if(count($errors))
    <div id="alert" class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        @foreach($errors->all() as $error) 
            <p>{{ $error }}</p>
        @endforeach 
    </div>
@endif


