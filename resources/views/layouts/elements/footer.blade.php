<footer>
    <div class="container">
        <div class="footer-paddings ">
            <!-- <a href="#" class="footer-logo"><img src="{{ FRONT_IMG.'/logo.png' }}" alt=""></a> -->
            <div class="row align-items-center">
                <div class="col-lg-6">          
                    <ul class="Footer-nav">
                        <li><a href="{{ route('static.faq') }}" class="footer-navlink {{ Request::path() === 'faq' ? 'active':'' }}">FAQ </a></li>
                         <li><a href="{{ route('contactus.create') }}" class="footer-navlink {{ Request::path() === 'contact-us' ? 'active':'' }}">Contact Us</a></li>
                        <li><a href="{{ route('static.about') }}" class="footer-navlink {{ Request::path() === 'about' ? 'active':'' }}">About Us </a></li>
                        <li><a href="{{ route('static.termsconditions') }}" class="footer-navlink {{ Request::path() === 'terms-and-conditions' ? 'active':'' }}">Terms and Conditions </a></li>
                    </ul>
                </div>  
                <div class="col-lg-6">
                    <ul class="social-icon">
                        @if(!empty($site_configs['site_facebook_link']))
                        <li><a href="{{ $site_configs['site_facebook_link'] }}" target="_blank" class="icon-link"> <img src="{{ FRONT_IMG.'/facebook.png' }}" alt=""> </a></li>
                        @endif
                        @if(!empty($site_configs['site_twitter_link']))
                        <li><a href="{{ $site_configs['site_twitter_link'] }}" target="_blank" class="icon-link"> <img src="{{ FRONT_IMG.'/twitter.png' }}" alt=""> </a></li>
                        @endif
                        @if(!empty($site_configs['site_youtube_link']))
                        <li><a href="{{ $site_configs['site_youtube_link'] }}" target="_blank" class="icon-link"> <img src="{{ FRONT_IMG.'/youtube-logotype.png' }}" alt=""></a></li>
                        @endif
                        @if(!empty($site_configs['site_instagram_link']))
                        <li><a href="{{ $site_configs['site_instagram_link'] }}" target="_blank" class="icon-link"> <img src="{{ FRONT_IMG.'/instagram.png' }}" alt=""></a></li>
                        @endif
                    </ul>
                    <p class="copy-right-text"> {{ str_replace('[year]',date('Y'),$site_configs['site_copyright_text']) }} <!-- {{ str_replace('[year]',date('Y'),$site_configs['site_copyright_text']) }} --> </p>
                </div>
            </div>
        </div>
    </div>
</footer>