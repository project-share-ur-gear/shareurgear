<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('public/img/favicons/apple-touch-icon.png?v=').env('APP_VERSION') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('public/img/favicons/favicon-32x32.png?v=').env('APP_VERSION') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('public/img/favicons/favicon-16x16.png?v=').env('APP_VERSION') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }} | {{ $pageHeading}}</title>

    <!-- Styles -->
    <link href="{{ asset('public/admin_ui/bootstrap/css/bootstrap.min.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_ui/bootstrap/css/font-awesome.min.css?v=').env('APP_VERSION') }}" rel="stylesheet">

    <link href="{{ asset('public/admin_ui/bootstrap/css/source_sans_pro.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_ui/dist/css/admin_style.css?v=').env('APP_VERSION') }}" rel="stylesheet">
    <link href="{{ asset('public/admin_ui/dist/css/admin_custom.css?v=').env('APP_VERSION') }}" rel="stylesheet">

    <link href="{{ asset('public/admin_ui/plugins/iCheck/square/blue.css?v=').env('APP_VERSION') }}" rel="stylesheet">

    <!-- Scripts -->
	<script src="{{ asset('public/admin_ui/plugins/jQuery/jQuery-2.1.4.min.js?v=').env('APP_VERSION') }}"></script>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>

        var APPLICATION_NAME = "{{ env('APP_NAME') }}";
        var APPLICATION_URL = "{{ env('APP_URL') }}";
		var ADMIN_APPLICATION_URL = "{{ env('ADMIN_APP_URL') }}";
        var PANEL_PREFIX = 'admin';
    </script>

    
</head>
<body class="login-page">

    @yield('content')
  
  	<script src="{{ asset('public/admin_ui/plugins/jQuery/jQuery-2.1.4.min.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/admin_ui/bootstrap/js/bootstrap.min.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/admin_ui/plugins/iCheck/icheck.min.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/admin_ui/dist/js/jquery.cookie.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/js/jquery-validation/jquery.validate.min.js?v=').env('APP_VERSION') }}"></script>
	<script src="{{ asset('public/js/jquery-validation/additional-methods.min.js?v=').env('APP_VERSION') }}"></script>
    <script src="{{ asset('public/admin_ui/dist/js/general.js?v=').env('APP_VERSION') }}"></script>
    
</body>

</html>
