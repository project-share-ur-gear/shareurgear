<form class="" id="" method="get" action="{{ url('/browse-rental') }}">
<div class="filter-data common-form">

    <div class="form-row">
        <div class="col-xl-3 col-lg-6">
            <div class="form-group check_cat">
                    <div class="pro-cateOption type-select ">
                        <select class="form-control  custom-select Required" name="sub_cat" id="sub_cat" required aria-required="true">
                            <option value="0" >Sub-Category </option>
                            @if($sub_category_data!='')
                                @foreach($sub_category_data as $key => $value)
                                    <option  value="{{$key}}" @if($sub_category == $key) selected @endif>{{$value}}</option>
                                @endforeach
                            @endif
                            
                        </select>
                    </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="form-group">
                
                <input type="text" class="form-control" placeholder="Location"  id="location" name="location" value="@if($location){{$location}}@endif" >
                <input type="hidden" name="loc_lat" id="loc_lat" value="@if(!empty($loc_lat)) {{$loc_lat}}  @endif">
                <input type="hidden" name="loc_long" id="loc_long" value="@if(!empty($loc_long)) {{$loc_long}}  @endif">
                <input type="hidden" name="center_lat" id="center_lat" value="@if(!empty($center_lat)) {{$center_lat}}  @endif">
                <input type="hidden" name="center_lng" id="center_lng" value="@if(!empty($center_lng)) {{$center_lng}}  @endif">
                <input type="hidden" name="keyword" id="keyword2" value="@if(!empty($keyword)) {{$keyword}}  @endif">
                <input type="hidden" name="zoom" id="zoom_mp" value="{{$zoom}}">
                <input type="hidden" name="category" id="pro_category" value="{{$category}}">
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="form-group">
                <div class="pro-cateOption type-select">
                    <select class="form-control selectpicker"  id="price_details" name="price_details">
                        <option value="0">Price Range  </option>
                        <option value="h_to_l" @if($price=='h_to_l') selected @endif >Hight To low </option>
                        <option value="l_to_h" @if($price=='l_to_h') selected @endif  >Low To High  </option>
                        
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-6">
            <div class="form-group">
                    <div class="pro-cateOption type-select ">
                        <select class="form-control custom-select Required"  id="Rating" required aria-required="true">
                                <option>Rating </option>
                                <option>Rating </option>
                                <option>Rating </option>
                        </select>
                    </div>
            </div>
        </div>
        
    </div>
    <div  class="reset_button_text"> <span class="reset_button"> Reset  </span></div>

</div>

</form>

