@extends('layouts.app')
@section('content')

@php
    $pageContent  = json_decode($pageData->page_content);    
    $url1=SITE_HTTP_URL_API.'getcat';
    $returnData=Curlgetequest($url1,CURL_KEY);
    if($returnData=='failed'){
        return  redirect(route('static.errorpage'));
    }else if(empty($returnData)){
        $category_Data=array();
    }else{
        $category_Data= json_decode($returnData);
    }
@endphp

<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/star-rating.css" rel="stylesheet" type="text/css">
<link href="{{ SITE_HTTP_URL }}/public/plugins/star-rating/css/theme.css" rel="stylesheet" type="text/css">

<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/star-rating.js"></script>
<script src="{{ SITE_HTTP_URL }}/public/plugins/star-rating/js/theme.js"></script>

<style>
    .navbar-nav .nav-link {color:#343342;}
    .user .dropdown-toggle{color:#343342 !important; }
    .main-header .search-panels.common-h-bar, .search-icons{display:none !important;}
    #btn div {background:#000;}
    .home-banner{background: url({{STORAGE_IMG_PATH.'/app/public/pages_images/'.$pageContent->banner_image}})no-repeat; background-position: center; background-size: cover;}
    .rating-container .caption { display:none !important; }
    .rating-container .clear-rating { display:none !important; }
    .rating-container .rating-stars:focus { outline: unset !important; }
    .theme-krajee-svg .empty-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star2.svg" }}') !important; }
    .theme-krajee-svg .filled-stars .krajee-icon-star { background-image: url('{{ FRONT_IMG."/star.svg" }}') !important; }
</style>

<div class="over-all-home">

<!-- top-banner-section -->
<section class="home-banner">
    <div class="container">
        <div class="main-content">
            <h2>{{ $pageContent->title}} </h2>

            <div class="search-panels">
                <form class="" method="get" action="{{ url('/browse-rental') }}">
                    <div class="search-row form-row align-items-center">
                        <div class="col-lg-5 col-sm-5">
                            <div class="form-group"><input type="text" class="form-control" placeholder="What are you looking for?" name="keyword" id="keyword" ></div>
                        </div>
                        <div class="col-lg-4 col-sm-4">
                            <div class="form-group type-select">
                                <select class="form-control selectpicker" id="category" name="category">
                                <option value="0">Category </option>
                                @foreach($category_Data as $key => $value)
                                    <option  value="{{$value->product_category_id}}"> {{$value->productcategorytitle}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 col-sm-3">
                            <div class="search-btn btn " id="search_button" >Search</div>
                        </div>
                    </div>
                </form>
            </div>
            <h6 class="subtitle">{{ $pageContent->title_desc}}</h6>
        </div>
    </div>
</section>

<section class="discover-best">
    <div class="container">
        <div class="discover-best-content">
            <h2 class="title">{{ $pageContent->book_appont_btn_txt}}</h2>

            @php
            $catData = array_chunk($category_Data,2);
            @endphp
            <div class="slider-data slick-slider">
                <!-- slick-slider -->

                @foreach($catData as $category)
                <div class="slider-div slick-slide">
                    @foreach($category as $dualCat)
                    <div class="slider-items">
                        <img src="{{SITE_HTTP_URL}}{{ Storage::url('/app/public/speciaity_images/'.$dualCat->product_cat_image) }}" alt="">
                        <div class="overlay-img">
                            <div class="tag-ds">{{$dualCat->productcategorytitle}}</div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach

                
                  
            </div>
        </div>
    </div>
</section>

<section class="Featured-Gear">
    <div class="text-gear">Gear</div>
    <div class="container">
        <div class="FG-data-featured">
            @if(!empty($product_Data))
                <h2 class="title"> <span>{{ $pageContent->benfits_heading }}</span> {{ $pageContent->benfits_heading_first }}</h2>
                <div class="FG-gear-card">
                    <div class="row justify-content-center">
                        @foreach($product_Data as $key => $value)
                            <div class="col-lg-4 col-sm-6">
                                <div class="fg-card">
                                    <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$value->product_id }}">   
                                        <div class="card">
                                            <img class="card-img-fg" src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $value->primary_image }}" alt="">
                                            <div class="card-overlay">
                                                <h4 class="p-title">{{$value->product_title}}</h4>
                                                <div class="p-subTitle">
                                                    <p>{{$value->productcategorytitle}} </p>
                                                    <ul class="rating-star">
                                                        <input type="number" id="viwrating" name="starRating" class="rating required" style="width: 5px;" min=0 max=5 step=1 data-size="sm" data-ltr="true" value="{{ bcdiv($value->pro_avg_rating,1,2) }}" readonly>
                                                        <li><span href="#" class="star-span"> ({{ $value->pro_total_rating!=''?$product_data->pro_total_rating:'0' }})</span></li>
                                                    </ul>
                                                </div>
                                                <div class="media-data">
                                                    <div class="media">
                                                        @php
                                                            $profileImage=FRONT_IMG.'/nophoto.png';
                                                            if($value->profile_image!='')
                                                            $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$value->profile_image);
                                                        @endphp
                                                        <img class="media-img" src="{{$profileImage}}" alt="">
                                                        <div class="media-body">
                                                            <p class="m-title">{{$value->name}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="price-d">${{$value->price_per_day}}<sub>/Day</sub></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center">
                        <a href="{{ url('/browse-rental') }}" class="btn view-all">View All</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</section>

<section class="gear-section">
    <div class="gear-dots gear-dots-pos1"><img src="{{  FRONT_IMG.'/dots3.png' }}" alt=""></div>
    <div class="gear-dots gear-dots-pos2"><img src="{{  FRONT_IMG.'/dots3.png' }}" alt=""></div>
    <div class="container">
        <div class="gaer-contents">
            <h2 class="G-title">{{ $pageContent->question_title}}</h2>
           {!!  $pageContent->question_desc !!}
        </div>
    </div>
</section>

@if(!empty($getRecentBookings))
    <section class="recent-gears">
        <div class="text-gear">Gear</div>
        <div class="container">
            <div class="FG-data-featured">
                <h2 class="title">  <span>{!! $pageContent->booked_title!!} </span>  {!! $pageContent->booked_title_span2!!}       </h2>
                <div class="FG-gear-card">
                    <div class="form-row justify-content-center fg-scroll-m">
                        @foreach($getRecentBookings as $key => $bookings)
                            <div class="col-xl-3 col-lg-4 col-sm-6 col-6 fg-scroll-md">
                                <a href="{{ SITE_HTTP_URL.'/rental-detail-page/'.$bookings['product_id'] }}">   
                                    <div class="fg-card">
                                        <div class="card">
                                            <div class="booked-img"><img src="{{ FRONT_IMG.'/Booked.png' }}" alt=""></div>
                                            <img class="card-img-fg" src="{{HTTP_UPLOADED_IMAGES_PATH}}/300X300/{{ $bookings['primary_image'] }}" alt="">
                                            <div class="card-overlay">
                                                <h4 class="p-title">{{ $bookings['product_title'] }}</h4>
                                                <div class="p-subTitle">
                                                    <p>{{ $bookings['subcategorytitle'] }}</p>
                                                    <ul class="rating-star">
                                                        <input type="number" id="viwrating" name="starRating" class="rating required" style="width: 5px;" min=0 max=5 step=1 data-size="sm" data-ltr="true" value="{{ bcdiv($bookings['pro_avg_rating'],1,2) }}" readonly>
                                                        <li><span href="#" class="star-span"> ({{ $bookings['pro_total_rating']!=''?$bookings['pro_total_rating']:'0' }})</span></li>
                                                    </ul>
                                                </div>
                                                <div class="media-data">
                                                    <div class="media">
                                                        @php
                                                            $profileImage=FRONT_IMG.'/nophoto.png';
                                                            if($bookings['profile_image']!='')
                                                            $profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$bookings['profile_image']);
                                                        @endphp
                                                        <img class="media-img" src="{{ $profileImage }}" alt="">
                                                        <div class="media-body">
                                                            <p class="m-title">{{  $bookings['name'] }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="price-d">${{ $bookings['price_per_day'] }}<sub>/Day</sub></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach



                    </div>
                </div>
            </div>             
        </div>
    </section>
@endif

<section class="how-it-section">
    <div class="text-gear">Works</div>
    <div class="container">
        <div class="FG-data-featured">
        
       
            <h2 class="title"> <span>{!! $pageContent->how_it_work!!} </span>{!! $pageContent->how_it_work2!!}</h2>
            <div class="how-data-section">
                <div class="hs-content">
                    <div class="hs-icon" style="background: rgb(255, 243, 110);"><img src="{{  FRONT_IMG.'/idea.png' }}" alt=""></div>
                    <div class="contents">
                        <h2 class="hs-idea">01</h2>
                        <h6 class="hs-title" style="color:#fff36e">{{$pageContent->how_it_work_first_title}}</h6>
                              <p>{{$pageContent->how_it_work_first_desc}}</p>
                    </div>
                </div> 
                
                
                <div class="hs-content">
                    <div class="hs-icon" style="background:#8a71b2;"><img src="{{  FRONT_IMG.'/mail.png' }}" alt=""></div>
                    <div class="contents">
                        <h2 class="hs-idea">02</h2>
                        <h6 class="hs-title" style="color:#8a71b2">{{  $pageContent->how_it_work_second_title}}</h6>
                        <p>{{ $pageContent->how_it_work_second_desc}}</p>
                    </div>
                </div>        
                <div class="hs-content">
                    <div class="hs-icon" style="background:#8eb2ff;"><img src="{{  FRONT_IMG.'/profile.png' }}" alt=""></div>
                    <div class="contents">
                        <h2 class="hs-idea">03</h2>
                        <h6 class="hs-title" style="color:#8eb2ff">{{ $pageContent->how_it_work_third_title}} </h6>
                        <p>{{ $pageContent->how_it_work_third_desc}} </p>
                    </div>
                </div> 
                <div class="hs-content">
                    <div class="hs-icon" style="background:#abdbff;"><img src="{{  FRONT_IMG.'/loupe.png' }}" alt=""></div>
                    <div class="contents">
                        <h2 class="hs-idea">04</h2>
                        <h6 class="hs-title" style="color:#abdbff">{{ $pageContent->how_it_work_fourth_title}}</h6>
                        <p>{{ $pageContent->how_it_work_fourth_desc}}</p>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</section>



</div>

<script>

$(document).ready(function(e) {
    if ($(document).width() > 1024) {
        AOS.init({ once: true });
    } else {
        $('[data-aos]').removeAttr('data-aos');
    }
});

$(function(){

$('.slider-data').slick({
  dots: false,
  infinite: false,
  speed: 300,
  slidesToShow: 4,
  slidesToScroll: 2,
  responsive: [
    {
      breakpoint: 1224,
      settings: {
        slidesToShow: 4,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
        infinite: true,
        dots: false
      }
    },
    {
      breakpoint: 700,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    
  ]
});

});

   $(document).on("click","#search_button",function(){
            $("form").submit();
    });


</script>

@endsection