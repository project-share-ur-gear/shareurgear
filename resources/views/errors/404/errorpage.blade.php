@extends('layouts.app')

@section('content')

<style>
    footer, header{display:none;}
</style>



<div class="error_elemenet" >
    <div id="particles-js" class="js-particle"></div>
    <div class="container">
        <div class="error_body">
            <div class="error-sec">
                <h3 class="heading">404</h3>
                <p class="not-found-txt">Page Not Found !!</p>
                <div class="text-center"><a href="<?php echo SITE_HTTP_URL?>" class="homelink join-btn btn">Go to Home Page</a></div>
            </div>
        </div>
    </div>
</div>

<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1561436720/particles.js"></script>
<script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1561436735/app.js"></script>

<script>
$(document).ready(function(){
 console.clear();
var count_particles, stats, update;
stats = new Stats;
stats.setMode(0);
stats.domElement.style.position = 'absolute';
stats.domElement.style.left = '0px';
stats.domElement.style.top = '0px';
document.body.appendChild(stats.domElement);
count_particles = document.querySelector('.js-count-particles');
update = function() {
stats.begin();
stats.end();
if (window.pJSDom[0].pJS.particles && window.pJSDom[0].pJS.particles.array) {
count_particles.innerText = window.pJSDom[0].pJS.particles.array.length;
}
requestAnimationFrame(update);
};
requestAnimationFrame(update);


});
</script>



@endsection()