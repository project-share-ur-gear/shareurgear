@extends('layouts.adminapp')

@section('content')


 
<style type="text/css">
  span.invalid-feedback {color: #F00;}
</style>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">* Required fields</span></div>
          <div class="clear10"></div>
			{!! Form::open(['url' => 'admin/config/update', 'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm', 'enctype' => 'multipart/form-data']) !!}
            @method('PATCH')
            @csrf
          
          <div class="row m-0">
          		<div class="col-md-6">
                	<h4 class="boxheading"><u><strong>General Settings:</strong></u></h4>
                     @foreach($configArr['SITE_CONFIG'] as $status_key => $all_val) 
                      <div class="form-group row">
                              <div class="col-md-6">{!! Form::label('title',$all_val->title) !!}<span class="error-message">*</span></div>
                              <div class="col-md-6">
                              	@if($all_val->config_key=='site_logo')
                                	{!! Form::file($all_val->config_key,array('id'=>$all_val->config_key,'class'=>'imageValid')) !!} 
                                    <br />
                                    <div style="background-color: #e2e2e2;padding:10px;display:inline-block;">
                                    	<img src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/logo/'.$all_val->value) }}" alt="{{ $all_val->title}}" title="{{ $all_val->title }}" class="img-fluid rounded img-responsive" style="max-width:100px;">
                                    </div>
                                @else
                                	{!! Form::text($all_val->config_key, $all_val->value ?? old($all_val->config_key), array('placeholder'=>$all_val->title,'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100', 'id'=>$all_val->config_key )) !!}
                                @endif
                                
                                @if($errors->has($all_val->config_key))
                                  <div class="error-message">{{ $errors->first($all_val->config_key) }}</div>
                                @endif
                              </div>
                          </div>
                     @endforeach	
                </div>
                
                <div class="col-md-6">
                	<h4 class="boxheading"><u><strong>Social Settings:</strong></u></h4>
                     @foreach($configArr['SITE_SOCIAL'] as $status_key => $all_val) 
                      <div class="form-group row">
                              <div class="col-md-6">{!! Form::label('title',$all_val->title) !!} </div>
                              <div class="col-md-6">
                                {!! Form::text($all_val->config_key, $all_val->value ?? old($all_val->config_key), array('placeholder'=>$all_val->title, 'class'=>'form-control', 'maxlength'=>'100', 'id'=>$all_val->config_key)) !!}
                                @if($errors->has($all_val->config_key))
                                  <div class="error-message">{{ $errors->first($all_val->config_key) }}</div>
                                @endif
                              </div>
                          </div>
                     @endforeach	
                </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <h4 class="boxheading"><u><strong>Google Keys:</strong></u></h4>
                 @foreach($configArr['SITE_GOOGLE'] as $status_key => $all_val) 
                  <div class="form-group row">
                          <div class="col-md-6">{!! Form::label('title',$all_val->title) !!}<span class="error-message">*</span></div>
                          <div class="col-md-6">
                            {!! Form::text($all_val->config_key, $all_val->value ?? old($all_val->config_key), array('placeholder'=>$all_val->title,'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100', 'id'=>$all_val->config_key)) !!}
                            @if($errors->has($all_val->config_key))
                              <div class="error-message">{{ $errors->first($all_val->config_key) }}</div>
                            @endif
                          </div>
                      </div>
                 @endforeach  
              </div>

              <div class="col-md-6">
               <h4 class="boxheading"><u><strong>Stripe Payment Setting:</strong></u></h4>
                 @foreach($configArr['SITE_STRIPE'] as $status_key => $all_val) 
                  <div class="form-group row">
                          <div class="col-md-6">{!! Form::label('title',$all_val->title) !!}</div>
                          <div class="col-md-6">
                            {!! Form::text($all_val->config_key, $all_val->value ?? old($all_val->config_key), array('placeholder'=>$all_val->title,'required'=>FALSE, 'class'=>'form-control', 'maxlength'=>'250', 'id'=>$all_val->config_key)) !!}
                            @if($errors->has($all_val->config_key))
                              <div class="error-message">{{ $errors->first($all_val->config_key) }}</div>
                            @endif
                          </div>
                      </div>
                 @endforeach  
              </div>
           
              <div class="clearfix"></div>
              <div class="col-md-6">
               <h4 class="boxheading"><u><strong>Website Commission Setting</strong></u></h4>
                 @foreach($configArr['SITE_MATRIX'] as $status_key => $all_val) 
                  <div class="form-group row">
                          <div class="col-md-6">{!! Form::label('title',$all_val->title) !!}</div>
                          <div class="col-md-6">
                            {!! Form::text($all_val->config_key, $all_val->value ?? old($all_val->config_key), array('placeholder'=>$all_val->title,'required'=>FALSE, 'class'=>'form-control number', 'maxlength'=>'100', 'id'=>$all_val->config_key)) !!}
                            @if($errors->has($all_val->config_key))
                              <div class="error-message">{{ $errors->first($all_val->config_key) }}</div>
                            @endif
                          </div>
                      </div>
                 @endforeach  
              </div>
          </div>
           

          <div class="form-group row">
            <div class="text-center">
              {!! Form::submit("Update", array('class'=>'btn btn-primary' )) !!}
            </div>
          </div>
          {!! Form::close() !!}

        </div>
      </div>
    </div>
  </div>
</section>

@endsection