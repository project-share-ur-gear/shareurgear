@extends('layouts.adminapp')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-header with-border sbh search-block">
                    <button id="deleteTrigger" class="btn btn-danger">{{ __('Delete User') }}</button>
                </div>
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            {{-- <td width="10%">S No.</td> --}}
                            <td width="5%"><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                            <td width="20%">Title</td>
                            <td width="20%">Price per Date</td>
                            <td width="5%">STATUS</td>
                            <td width="15%">Approve/Decline</td>
                            <td width="15%">View</td>
                        </tr>
                    </thead>
                    {{-- <thead>
                        <tr>
                        <td><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                            <!-- <td><input type="text" data-column="1" class="search-input-text form-control input-sm"></td> -->
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead> --}}
                    <tbody></tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>


    <div class="modal common-modals fade add" id="manage1ModalCenter"  data-backdrop="false" role="dialog" aria-labelledby="manage1ModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="manage1ModalLongTitle"> Decline </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                
                        <form class="client_approve" id="client_approve" method="post" action="{{ url('admin/product/decline-prduct') }}" enctype= 'multipart/form-data'>
                        @csrf
                            <input type="hidden" name="product_id" id="product_id" value="">
                            <div class="form-group">
                            <label class="address-label"> Enter reason </label>
                                <textarea name="decline_resaon" id="decline_resaon" class="form-control required" rows="5" cols="100" style="resize: none" aria-required="true" maxlength="1500" required ></textarea>
                            </div>
                            <button type="button" id="dec" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic waves-effect waves-classic" style="width:20%;">Submit</button>
                            <a href="javascript:void(0);" class="btn btn-sm btn-icon btn-dark btn-round waves-effect waves-classic waves-effect waves-classic" style="width:20%;" id="modelclose">Cancel</a>
                        </form>

                </div>                                  
                </div>
            </div>
    </div>














<script>
$(function() {
    var dataTable = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        autoWidth: false,
        aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
        iDisplayLength: 10,
        ajax: '{{ url("admin/product/getproductrequest") }}',
        dataType: JSON,
        columns: [
            {
                data: 'product_id',
                "render": function (data, type, full, meta) {
                    return '<input type="checkbox" id="deleteRow_'+ full.product_id +'" class="deleteRow" value="'+ full.product_id +'" />';
                },
                "orderable": false
            },
            {data: 'product_title', name: 'product_title'},
            // {data: 'productcategorystatus', name: 'productcategorystatus'},
            {data: 'price_per_day', name: 'price_per_day'},
            { 
                data: 'product_request_status',
                "render": function (data, type, full, meta) {
                
                    if((full.product_request_status=='0'))  
                    {
                        return  '<p class="text-success text-center">Pending</p>';
                    }else if((full.product_request_status=='1'))
                    {
                        return  '<p class="text-success text-center">Approve</p>';
                    }else{
                        return '<p class="text-danger text-center">Decline</p>';
                    }
                }

             },
            {
                 data: 'product_request_status',
                "render": function (data, type, full, meta) {
                    var edit_url = '{{ url("admin/product/add-product-categories") }}/'+full.product_id;
                    var status_url = '{{ url("admin/product/status-main-cat") }}/'+full.product_id;
                    var status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-small" title="Status"><i class="fa fa-remove"></i></a>';
                    if(full.productcategorystatus=='1'){
                        status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-small" title="Status"><i class="fa fa-check"></i></a>';
                    }
                    if((full.product_request_status=='0'))  
                    {
                            var Accept = '<a href="javascript:void(0);" class="btn  btn-success" id="accpect_trigger" data-id="'+full.product_id+'" >{{ __('Approve') }}</a>';
                            var decline = ' <a href="javascript:void(0);" class="btn  btn-danger" id="decline_trigger" data-id="'+full.product_id+'">{{ __('Decline') }}</a>';
                            return Accept + decline ;
                    }else if((full.product_request_status=='1'))
                    {
                        return  '<p class="text-success text-center">Approve</p>';
                    }else{
                        return '<p class="text-danger text-center">Decline</p>';
                    }
                }
            },
            { 
                data: 'product_id',
                "render": function (data, type, full, meta) {
                
                    var detail_url = '{{ url("admin/product/show-product") }}/'+full.product_id;
                    return '<a href="' + detail_url + '" class="btn btn-info btn-flat btn-small" title="Detail"><i class="fa fa-file-text"></i></a>';
              
                }

             },
           
          
               
        ],
        columnDefs: [
           { "orderable": false, "targets": [0,2]  }
        ],
        "order": [[ 2, "asc" ]],
        

    });

    // $("#data-table_filter label").css("display","none");  // hiding global search box
    // $('#data-table_filter').append($('.search-block').html());




 $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });
    $(document.body).on('click', '.deleteRow' ,function(){ // single checked
        var is_all_checked = true;
        var status = this.checked;
        $(".deleteRow").each( function() {
            if($(this).prop("checked")!=status){
                is_all_checked = false;
            }
        });
        $("#bulkDelete").prop("checked",is_all_checked);
    });



    $('.search-input-text').on( 'keyup', function () {   // for text boxes
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    });

      $('#deleteTrigger').on("click", function(event){
        if( $('.deleteRow:checked').length > 0 ){
            var check = confirm("{{ __('Are you sure? You want to delete Categories ?') }}");    
            if(check == true){
                var ids = [];
                $('.deleteRow').each(function(){
                    if($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });
                var ids_string = ids.toString();
                $.ajax({
                    type    : "POST",
                    type    : 'DELETE',
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url     : "{{ url('admin/product/product-destroy') }}",
                    data    : {data_ids:ids_string },
                    success : function(result){
                        if(result.success){
                            showpopupmessage('Product Delete Sucessfully','success');
                            setTimeout(function(){
                                location.reload();
                            },2000)
                        }
                        dataTable.draw();
                    },
                    async:false
                });
            }
        }
        else {
            showpopupmessage("{{ __('Please Select Atleast one') }}", "error");
        }
    });

    $(document).on("click","#accpect_trigger",function() {
        var dataId = $(this).attr("data-id");
       
      /* var check = confirm("{{ __('Are you sure? You want to Approve product ?') }}");

        if(check == true){
              window.location.href="{{ url('admin/product/approve-prduct') }}/"+dataId;
        } */

       swal({
            title: "Are you sure?",
            text: " You want to Approve product ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            cancelButtonColor: "#DD6B55",
            closeOnConfirm: true,
            closeOnCancel: true
        },function(isConfirm) {
            if (isConfirm) {
                window.location.href="{{ url('admin/product/approve-prduct') }}/"+dataId;
            }else{
                return false;
            }
        }
    );

    });

      $(document).on("click","#decline_trigger",function(){
        var dataId = $(this).attr("data-id");
        $('#product_id').val(dataId);
         $('#manage1ModalCenter').modal({
            show: true,
            keyboard:true,
        });

        // var check = confirm("{{ __('Are you sure? You want to Decline product ?') }}");
        // if(check == true){
        //       window.location.href="{{ url('admin/product/decline-prduct') }}/"+dataId;
        // }
    });
    $('#dec').click(function(){
      
        $("#client_approve").validate();
        if($("#client_approve").valid()){
            $("#client_approve").submit();
        }else{
            return false;
        }
    });


        $('#modelclose').click(function(){
            $('#manage1ModalCenter').modal('hide');
        });

});




</script>

@endsection
