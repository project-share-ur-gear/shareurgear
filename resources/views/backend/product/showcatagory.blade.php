@extends('layouts.adminapp')

@section('content')
<?php  $last_names = array_column($sub_cat_data, 'subcategorytitle');  $List = implode(', ', $last_names);   ?>
<section class="content">
	<div class="row">

    
	    <div class="col-md-12">
            <div class="form-group row">
              <div class="col-md-6">
                <a href="{{ url('admin/product/product-categories') }}" class="btn btn-primary">{{ __('Back') }}</a>
              </div>
            </div>
            <div class="box">
            
                <div class="box-header with-border" >
                	<div class="clear10"></div>
                	<div class="col-md-12" align="right">
                        <span class="error-message"></span>
                    </div>
                	<div class="clear10"></div>
                	<h4 class="boxheading"><u><strong>Category:</strong></u></h4>
                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Category:</strong></div>
                            <div class="col-md-8">{{$cat_data->productcategorytitle}}</div>
                        </div>

                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Sub Catagory</strong></div>
                            <div class="col-md-8">{{$List}}</div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Catagory Image</strong></div>
                            <div class="col-md-8"><img src="{{SITE_HTTP_URL}}{{ Storage::url('/app/public/speciaity_images/'.$cat_data->product_cat_image) }}"   class="img-fluid rounded img-responsive">
                  </div>
                        </div>
                        
                    </div>

                    
    </div>
</section>
                 
@endsection