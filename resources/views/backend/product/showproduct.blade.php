@extends('layouts.adminapp')

@section('content')

<?php   $product_images=unserialize($product_data->product_images); ?>

<section class="content">
	<div class="row">


    
	    <div class="col-md-12">


            @if($product_data->product_request_status=='2')
                <div class="form-group row">
                    <div class="col-md-6">
                        <a href="{{ url('admin/product/product-request') }}" class="btn btn-primary">{{ __('Back') }}</a>
                    </div>
                </div>
            @endif
            @if($product_data->product_request_status=='0')
                <div class="form-group row">
                        <div class="col-md-6">
                            <a href="{{ url('admin/product/product-request') }}" class="btn btn-primary">{{ __('Back') }}</a>
                        </div>
                </div>
            @endif
            @if($product_data->product_request_status=='1')
            <div class="form-group row">
                        <div class="col-md-6">
                            <a href="{{ url('admin/product/manage-request') }}" class="btn btn-primary">{{ __('Back') }}</a>
                        </div>
                </div>
            
            @endif


            <div class="box">
                <div class="box-header with-border" >
                	<div class="clear10"></div>
                	<div class="col-md-12" align="right">
                        <span class="error-message"></span>
                    </div>
                	<div class="clear10"></div>
                	<h4 class="boxheading"><u><strong>Show Product:</strong></u></h4>
                    <div class="form-group row ">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Title:</strong></div>
                            <div class="col-md-8">{{$product_data->product_title}}</div>
                        </div>

                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Tags</strong></div>
                            <div class="col-md-8">@if(!empty($product_data->tags)) {{$product_data->tags}}   @endif </div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Price Per Day</strong></div>
                            <div class="col-md-8">{{$product_data->price_per_day}}</div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Location</strong></div>
                            <div class="col-md-8">{{$product_data->location}}</div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Product Listing</strong></div>
                            <div class="col-md-8">{{$product_data->product_desp}}</div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Product Added By</strong></div>
                            <div class="col-md-8">{{$product_data->name}}</div>
                        </div>
                        
                    </div>

                      <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Product Category</strong></div>
                            <div class="col-md-8">{{$product_data->productcategorytitle}}</div>
                        </div>
                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Product Sub Category</strong></div>
                            <div class="col-md-8">{{$product_data->subcategorytitle}}</div>
                        </div>
                        
                    </div>



                    <div class="form-group row">
                        <div class="col-sm-12 row">
                            <div class="col-md-3"><strong>Product Images</strong></div>
                                <div class="col-md-8" >
                                    <div class="" style="display:flex;flex-wrap: wrap;">
                                        @foreach($product_images as $key => $value)
                                            <div class="div_content  img-thumbnail">
                                                <img src="{{HTTP_UPLOADED_IMAGES_PATH}}/160X160/{{$key}}" width="100px"  >
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    @if($product_data->product_request_status=='0')
                        <div class="form-group row">
                            <div class="col-sm-12 row">
                                <div class="col-md-3"><strong>Status</strong></div>
                                <div class="col-md-4"><p class="text-warning">Pending</p></div>
                            </div>
                            
                        </div>
                    @endif

                       @if($product_data->product_request_status=='1')
                        <div class="form-group row">
                            <div class="col-sm-12 row">
                                <div class="col-md-3"><strong>Product Listing</strong></div>
                                <div class="col-md-4"><p class="text-success">Accpect</p></div>
                            </div>
                            
                        </div>
                    @endif
                    @if($product_data->product_request_status=='2')
                        <div class="form-group row">
                            <div class="col-sm-12 row">
                                <div class="col-md-3"><strong>Product Listing</strong></div>
                                <div class="col-md-4"><p class="text-danger">Decling</p></div>
                            </div>
                            
                        </div>
                    @endif


                       @if($product_data->product_request_status=='2')
                       
                        <div class="form-group row">
                            <div class="col-sm-12 row">
                                <div class="col-md-3"><strong>Product Cencle Description</strong></div>
                                <div class="col-md-4"><p class="text-danger"><?php if(!empty($product_data->product_cancle_desc)) { echo nl2br($product_data->product_cancle_desc); } ?></p></div>
                            </div>
                            
                        </div>


                    @endif



                  
                  
                  
    </div>
</section>


@endsection
