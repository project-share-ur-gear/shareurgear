@extends('layouts.adminapp')

@section('content')

@php
    $pageUrl='admin/product/create-product-categories';
    if(isset($cat_Data)){
        $pageUrl='admin/product/update-product-categories/'.$cat_Data->product_category_id; 
        
    }
    
@endphp


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form class="contact_form" id="contact_form" action="{{url($pageUrl) }}" method="post" enctype ='multipart/form-data'>
            @csrf
                <div class="box">
                    <div class="box-header with-border " style="margin:10px;">
                    <div class="clear10"></div>
                        <div class="form-group row">
                            <label>Categary Title</label>
                            <input type="text" class="form-control required" name="c_title" id="c_title" value="@if(!empty($cat_Data->productcategorytitle)){{$cat_Data->productcategorytitle}}@endif"  required>
                        </div>
                        <div class="form-group row">
                          {!! Form::label('blog_image', __('Upload Image(1268 × 846px)')) !!}<span class="error-message">*</span>
                           
                                @if(isset($cat_Data->product_cat_image) && $cat_Data->product_cat_image!='' && file_exists(STORAGE_IMG_ROOT.'/app/public/speciaity_images/'.$cat_Data->product_cat_image))
                                    {!! Form::file('blog_image', array()) !!}
                                    @if($errors->has('blog_image'))
                                    <div class="error-message">{{ $errors->first('blog_image') }}</div>
                                    @endif
                                    <br>   
                                    <div style="height: 150px;width: 150px;">
                                        <img src="{{SITE_HTTP_URL}}{{ Storage::url('/app/public/speciaity_images/'.$cat_Data->product_cat_image) }}" alt="{{ $cat_Data->product_cat_image }}" title="{{ $cat_Data->product_cat_image }}" class="img-fluid rounded img-responsive">
                                    </div>
                                @else
                                    {!! Form::file('blog_image', array('required'=>'required')) !!}
                                    @if($errors->has('blog_image'))
                                    <div class="error-message">{{ $errors->first('blog_image') }}</div>
                                    @endif
                                @endif
                       
                        </div>
                        </br>
                        <div class="form-group row" style="text-align:center">
                            <button class="btn  submit-btn my-3 save_data_btn" data-inprocess="Saving..." data-default="Save" type="button">Save</button> 
                        </div>
                    </div>
                </div>
            </form>
        </div>
        
    </div>

</section>   
<script type="text/javascript">
  $(document).ready(function(e) {
    $('.save_data_btn').click(function(event) {
               /* Act on the event */
                if($('#contact_form').valid()){
                    $('#contact_form').submit();
                }else{
                    var ele  = "";
                    $('.help-block').each(function(index, el) {
                        if($(el).is(':visible')  && ele==""){
                            ele = el;
                        }   
                    });
                    $('html, body').animate({
                            scrollTop: $(ele).offset().top-150
                    }, 500);
                }
        });
  
  });      
      
</script>
@endsection

