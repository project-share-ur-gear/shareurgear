@extends('layouts.adminapp')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-body">
                    <div class="list-table-wrapper">
                        <table id="data-table" class="table">
                            <thead>
                                <tr>
                                    {{-- <td width="10%">S No.</td> --}}
                                    <td width="5%"><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                                    <td width="20%">Title</td>
                                    <td width="20%">Price per Date</td>
                                    <td width="5%">STATUS</td>
                                    <td width="15%">Approve/Decline</td>
                                    <td width="15%">Featured Gear</td>
                                    <td width="15%">View</td>
                                </tr>
                            </thead>
                            {{-- <thead>
                                <tr>
                                <td><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                                    <!-- <td><input type="text" data-column="1" class="search-input-text form-control input-sm"></td> -->
                                    <td></td>
                                    <td>&nbsp;</td>
                                </tr>
                            </thead> --}}
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<script>

    $(function() { 
        var dataTable = $('#data-table').DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            autoWidth: false,
            aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
            iDisplayLength: 10,
            ajax: '{{ url("admin/product/get-managerequest") }}',
            dataType: JSON,
            columns: [
                {
                     data: 'product_id', 
                    "render": function (data, type, full, meta) {
                        return '<input type="checkbox" id="deleteRow_'+ full.product_id +'" class="deleteRow" value="'+ full.product_id +'" />';
                    },
                    "orderable": false
                },
                {data: 'product_title', name: 'product_title'},
                // {data: 'productcategorystatus', name: 'productcategorystatus'},
                {data: 'price_per_day', name: 'price_per_day'},
                {
                    data: 'product_admin_status', 
                    "render": function (data, type, full, meta) {
                        var status_url = '{{ url("admin/product/product-status") }}/'+full.product_id+'';
                        var status_link;
                        if(full.product_admin_status=='1'){
                            status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-sm" title="Status"><i class="fa fa-check"></i></a>';    
                            
                        }else{
                        status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-sm" title="Status"><i class="fa fa-remove"></i></a>';
                        }
                        return status_link;
                    },
                },
                {
                    data: 'product_request_status',
                    "render": function (data, type, full, meta) {
                        var edit_url = '{{ url("admin/product/add-product-categories") }}/'+full.product_id;
                        var status_url = '{{ url("admin/product/status-main-cat") }}/'+full.product_id;
                        var status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-small" title="Status"><i class="fa fa-remove"></i></a>';
                        if(full.productcategorystatus=='1'){
                            status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-small" title="Status"><i class="fa fa-check"></i></a>';
                        }
                        if((full.product_request_status=='0'))  
                        {
                                var Accept = '<a href="javascript:void(0);" class="btn  btn-success" id="accpect_trigger" data-id="'+full.product_id+'" >{{ __('Approve') }}</a>';
                                var decline = ' <a href="javascript:void(0);" class="btn  btn-danger" id="decline_trigger" data-id="'+full.product_id+'">{{ __('Decline') }}</a>';
                                return Accept + decline ;
                        }else if((full.product_request_status=='1'))
                        {
                            return  '<p class="text-success text-center">Approve</p>';
                        }else{
                            return '<p class="text-danger text-center">Decline</p>';
                        }
                    }
                },
                { 
                    data: 'product_f_gear', 
                    "render": function (data, type, full, meta) {
                        var status_url = '{{ url("admin/product/fetured-gear") }}/'+full.product_id+'';
                        var status_link;
                        if(full.product_f_gear=='2'){
                            status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-sm" title="Status"><i class="fa fa-check"></i></a>';
                        }else{
                        status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-sm" title="Status"><i class="fa fa-remove"></i></a>';
                        }
                        return status_link;       
                    }

                },
                { 
                    data: 'product_id',
                    "render": function (data, type, full, meta) {
                        var detail_url = '{{ url("admin/product/show-product") }}/'+full.product_id;
                        return '<a href="' + detail_url + '" class="btn btn-info btn-flat btn-small" title="Detail"><i class="fa fa-file-text"></i></a>';
                    }

                },  
            ],
            columnDefs: [
            { "orderable": false, "targets": [0,2]  }
            ],
            "order": [[ 2, "asc" ]],
        });

        // $("#data-table_filter label").css("display","none");  // hiding global search box
        // $('#data-table_filter').append($('.search-block').html());

        $("#bulkDelete").on('click',function() { // bulk checked
            var status = this.checked;
            $(".deleteRow").each( function() {
                $(this).prop("checked",status);
            });
        });

        $(document.body).on('click', '.deleteRow' ,function(){ // single checked
            var is_all_checked = true;
            var status = this.checked;
            $(".deleteRow").each( function() {
                if($(this).prop("checked")!=status){
                    is_all_checked = false;
                }
            });
            $("#bulkDelete").prop("checked",is_all_checked);
        });
        
        $('.search-input-text').on( 'keyup', function () {   // for text boxes
            var i =$(this).attr('data-column');  // getting column index
            var v =$(this).val();  // getting search input value
            dataTable.columns(i).search(v).draw();
        });

        $('#deleteTrigger').on("click", function(event){
            if( $('.deleteRow:checked').length > 0 ){

                    var check = confirm("{{ __('Are you sure? You want to delete Categories ?') }}");    
        
                
                if(check == true){
                    var ids = [];
                    $('.deleteRow').each(function(){
                        if($(this).is(':checked')) {
                            ids.push($(this).val());
                        }
                    });
                    var ids_string = ids.toString();
                    $.ajax({
                        type    : "POST",
                        type    : 'DELETE',
                        headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        url     : "{{ url('admin/product/approve-destroy') }}",
                        data    : {data_ids:ids_string },
                        success : function(result){
                            if(result.success){
                                showpopupmessage('Account(s) deleted successfully','success');
                                setTimeout(function(){
                                    location.reload();
                                },2000)
                            }
                            dataTable.draw();
                        },
                        async:false
                    });
                }
            }
            else {
                showpopupmessage("{{ __('users.messages.select_atleast_one') }}", "error");
            }
        });

        $(document).on("click","#accpect_trigger",function() {
            var dataId = $(this).attr("data-id");
        
            var check = confirm("{{ __('Are you sure? You want to Approve product ?') }}");
            if(check == true){
                window.location.href="{{ url('admin/product/approve-prduct') }}/"+dataId;
            }
        });

        $(document).on("click","#decline_trigger",function(){
            var dataId = $(this).attr("data-id");
        
            var check = confirm("{{ __('Are you sure? You want to Decline product ?') }}");
            if(check == true){
                window.location.href="{{ url('admin/product/decline-prduct') }}/"+dataId;
            }
        });
    });

</script>

@endsection
