@extends('layouts.adminapp')

@section('content')

<section class="content">
	<div class="row">
	    <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                	<div class="clear10"></div>
                	<div class="col-md-12" align="right">
                        <span class="error-message"></span>
                    </div>
                	<div class="clear10"></div>
                	<h4 class="boxheading"><u><strong>Profile:</strong></u></h4>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4">
                    		<strong>
                               
                            </strong>
                            </div>
                    		<div class="col-md-8">
                                <img src="{{ getUserImage($user->profile_image) }}" class="img-responsive" width="100" />
                            </div>
                    	</div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            @php $label = 'Name'; $name = $user->name; @endphp
                            <div class="col-md-4"> <strong>{{ $label }}</strong> </div>
                            <div class="col-md-8"> {{ $name }} </div>
                        </div>
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>{{ __('users.labels.email') }}</strong></div>
                            <div class="col-md-8">{{ ($user->email) }}</div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>User Type</strong></div>
                            <div class="col-md-8">{{ ucfirst($user->type) }}</div>
                        </div>

                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Phone</strong></div>
                            <div class="col-md-8">{{ $user->phone_number }}</div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Address</strong></div>
                            <div class="col-md-8">{{ $user->address }}</div>
                        </div>

                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Decline Bookings</strong></div>
                            <div class="col-md-8">{{ $user->decline_bookings }}</div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>{{ __('users.labels.status') }}</strong></div>
                            <div class="col-md-6">
                                @if($user->status=='1') Active @else Inactive @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</section>
                 
@endsection