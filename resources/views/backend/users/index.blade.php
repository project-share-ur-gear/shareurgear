

@extends('layouts.adminapp')

@section('content')
<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                         
                        <tr>
                            <td ><button id="deleteTrigger" class="btn btn-danger btn-xs">{{ __('Delete User') }}</button></td>
                            <td >{{ __('users.labels.name') }}</td>
                            <td >{{ __('users.labels.email') }}</td>
                            <!-- <td >{{ __('Type') }}</td> -->
                            <td >{{ __('Status') }}</td>
                            <td >{{ __('Updated On') }}</td>
                            <!-- <td>{{ __('Last Login') }}</td> -->
                            <td  class="">{{ __('Actions') }}</td>
                        </tr>
                    </thead>
                     
                    <tbody></tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>


<script>
    var usertype = '{{$type}}';
$(function() {
    var dataTable = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        autoWidth: false,
        aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
        iDisplayLength: 10,
        ajax: {
          url: '{{ url("admin/users/getdata/$type") }}',
          error: function(error){}          
        },
        dataType: JSON,
        columns: [
            {
                data: 'id', 
                "render": function (data, type, full, meta) {
                    return '<input type="checkbox" id="deleteRow_'+ full.id +'" class="deleteRow" value="'+ full.id +'" />';
                },
                "orderable": false
            },
            {
                data: 'name', 
                "render": function (data, type, full, meta) {
                     return full.name+'<br><code>ID: '+full.id+'</code>';
                },
            },
			{
                data: 'email', 
                "render": function (data, type, full, meta) {
					var verified = '<a class="label label-danger" title="Not Verified">Not Verified</a>';
                    if(full.email_verified=='1'){
                        verified = '<a class="label label-success" title="Verified">Verified</a>';
                    }
					var deletedStatus = '';
                    if(full.deleted_status=='1'){
                        deletedStatus = '<a class="label label-warning" title="Verified">Account Deleted On: '+full.deleted_on+' </a>';
                    }
					
                    return full.email+'<br>'+verified+'<br>'+deletedStatus;
                },
            },
            // {
            //     data: 'type', 
            //     "render": function (data, type, full, meta) {
            //         return full.type.toUpperCase();
            //     },
            // },

            {
                data: 'status', 
                "render": function (data, type, full, meta) {
                    var status_url = '{{ url("admin/users/changestatus") }}/'+full.id+'/{{$type}}';
                    var status_link;
                    if(full.status=='1'){
                         status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-sm" title="Status"><i class="fa fa-check"></i></a>';    
                        
                    }else{
                       status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-sm" title="Status"><i class="fa fa-remove"></i></a>';
                    }
                    return status_link;
                },
            },
            
            {
                data: 'id', 
                "render": function (data, type, full, meta) {
                    return full.updated_at; 
                }
            },
            {
                data: 'id', 
                "render": function (data, type, full, meta) {
                    
                    var access_url = '{{ url("admin/users/accessaccount") }}/'+full.id;
   				   var detail_url = '{{ url("admin/users/show") }}/'+full.id;
					    return ' <a href="' + detail_url + '" class="btn btn-primary btn-flat btn-sm" title="Detail"><i class="fa fa-eye"></i></a> <a target="_blank" href="' + access_url + '" class="btn btn-warning btn-flat btn-sm" title="Access Account" target="_new"><i class="fa fa-lock"></i></a>';
                    
                }
            }
        ],
        columnDefs: [
           { "orderable": false, "targets": [3,5]  }
        ],
        "order": [[ 4, "desc" ]],
    });

   /* $("#data-table_filter label").css("display","none");  // hiding global search box
    $('#data-table_filter').append($('.search-block').html());*/


    $('.search-input-text').on( 'keyup', function () {   // for text boxes
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );


    $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });
    $(document.body).on('click', '.deleteRow' ,function(){ // single checked
        var is_all_checked = true;
        var status = this.checked;
        $(".deleteRow").each( function() {
            if($(this).prop("checked")!=status){
                is_all_checked = false;
            }
        });
        $("#bulkDelete").prop("checked",is_all_checked);
    });
    
    $('#deleteTrigger').on("click", function(event){
        if( $('.deleteRow:checked').length > 0 ){
            if(usertype=="customer"){
                var check = confirm("{{ __('users.messages.delete_confirmation') }}");
            }else{
                var check = confirm("{{ __('Are you sure? You want to delete selected user?') }}");    
            }   
            
            if(check == true){
                var ids = [];
                $('.deleteRow').each(function(){
                    if($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });
                var ids_string = ids.toString();
                $.ajax({
                    type    : "POST",
                    type    : 'DELETE',
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url     : "{{ url('admin/users/destroy') }}",
                    data    : {data_ids:ids_string },
                    success : function(result){
                        if(result.success){
                            showpopupmessage('Account(s) deleted successfully','success');
                            setTimeout(function(){
                                location.reload();
                            },2000)
                        }
                        dataTable.draw();
                    },
                    async:false
                });
            }
        }
        else {
            showpopupmessage("{{ __('users.messages.select_atleast_one') }}", "error");
        }
    });
});
</script>
@endsection