@extends('layouts.adminapp')

@section('content')


@php
    $pageUrl='admin/pages/store';
    $buttonText='general.save';
    if(isset($page)){
        $pageUrl='admin/pages/update/'.$page->id; 
        $buttonText='Update';
    } 

    $dnone = "";


@endphp




<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">*Required Fields</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' => $pageUrl, 'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm', 'enctype' => 'multipart/form-data']) !!}
         
            @if(isset($page))
                  @method('PATCH')
                  @csrf
            @endif
            
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('title_en', __('Title')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('title_en', $page->title_en ?? old('title_en'), array('placeholder'=>__('pages.labels.title_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('title_en'))
                  <div class="error-message">{{ $errors->first('title_en') }}</div>
                @endif
              </div>
          </div>
           
          <div class="form-group row {{$dnone}}">
              <div class="col-md-3">{!! Form::label('meta_title_en', __('Meta Title')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('meta_title_en', $page->meta_title_en ?? old('meta_title_en'), array('placeholder'=>__('pages.labels.meta_title_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('meta_title_en'))
                  <div class="error-message">{{ $errors->first('meta_title_en') }}</div>
                @endif
              </div>
          </div>
          
           
          
           <div class="form-group row {{$dnone}}">
              <div class="col-md-3">{!! Form::label('meta_keywords_en', __('Meta Keywords')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('meta_keywords_en', $page->meta_keywords_en ?? old('meta_keywords_en'), array('placeholder'=>__('pages.labels.meta_keywords_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('meta_keywords_en'))
                  <div class="error-message">{{ $errors->first('meta_keywords_en') }}</div>
                @endif
              </div>
          </div>
          
           
          <div class="form-group row {{$dnone}}">
              <div class="col-md-3">{!! Form::label('meta_desc_en', __('Meta Description')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::textarea('meta_desc_en', $page->meta_desc_en ?? old('meta_desc_en'), array('placeholder'=>__('pages.labels.meta_desc_en'),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('meta_desc_en'))
                  <div class="error-message">{{ $errors->first('meta_desc_en') }}</div>
                @endif
              
              </div>
          </div>
          
          <div>
             @foreach($pagecontent as $status_key => $all_val)

            
              <div class="form-group row">
                      <div class="col-md-3">{!! Form::label($all_val->page_title,$all_val->page_title) !!}<span class="error-message">*</span></div>
                      <div class="col-md-9">
                       @if($all_val->page_type=='text')
                         
                        	{!! Form::text($all_val->page_key, $all_val->page_content ?? old($all_val->page_title), array('placeholder'=>$all_val->page_title,'required'=>'required', 'class'=>'form-control', 'maxlength'=>'5000', 'id'=>$all_val->page_key )) !!}
                       @elseif($all_val->page_type=='file')
                        	@php 
                            $filecheck='imageValid';
                            if($all_val->page_video==1)
                                    $filecheck='videoValid';
                            @endphp  
                       	 	{!! Form::file($all_val->page_key,array('id'=>$all_val->page_key,'class'=>$filecheck)) !!} 
                            <br><br>
                            
                             @if($all_val->page_video==0)
                            	<div style="background-color: #e2e2e2;padding:10px;display:inline-block;">
                                	<img src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/static/'.$all_val->page_content) }}" alt="{{ $all_val->page_title }}" title="{{ $all_val->page_title }}" class="img-fluid rounded img-responsive" style="max-width:100px;">
                                </div>
                            @else
                            	<video width="200" height="100" controls>
                                	<source src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/static/'.$all_val->page_content) }}" type="video/mp4">
                                </video>
                            @endif
                            
                       @elseif($all_val->page_type=='textarea')
                       			@php 
                                $editor='';
                                if($all_val->page_editor==1)
                                		$editor='editor';
                                @endphp         
                        	{!! Form::textarea($all_val->page_key, $all_val->page_content ?? old($all_val->page_title), array('placeholder'=>$all_val->page_title,'required'=>'required', 'class'=>'form-control '.$editor, 'id'=>$all_val->page_key )) !!}     
                       @endif
                         
                        @if($errors->has($all_val->page_key))
                          <div class="error-message">{{ $errors->first($all_val->page_key) }}</div>
                        @endif
                      </div>
                  </div>
             @endforeach	
        </div>
          
         
          <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              {!! Form::submit(__($buttonText), array('class'=>'btn btn-primary','name'=>'bttnsubmit','id'=>'bttnsubmit')) !!}
              <a href="{{ url('admin/pages/index') }}" class="btn btn-warning">Cancel</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

@endsection

<script src="https://maps.googleapis.com/maps/api/js?key={{$site_configs['site_google_key']}}&v=3.exp&sensor=false&libraries=places"></script>

<script>
          // $('#cnt_address_lat').parent().find('input').attr('type','hidden');
          // $('#cnt_address_long').parent().find('input').attr('type','hidden');
          // $('label[for="cnt_address_lat"]').hide();
          // $('label[for="cnt_address_long"]').hide();
  



  function init() {
    
    var input = document.getElementById('cnt_address');
    var options = {
      types: ['geocode'],  
      //componentRestrictions: {country: countryCode}
    };
    var autocomplete = new google.maps.places.Autocomplete(input,options);
   
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
      var place = autocomplete.getPlace();
     
    $('.location-help-block').remove();


      document.getElementById('cnt_address_lat').value = place.geometry.location.lat();
    $('span[for="cnt_address_lat"]').remove();
      document.getElementById('cnt_address_long').value = place.geometry.location.lng();
      $('span[for="cnt_address_long"]').remove();


      if(document.getElementById('cnt_address_lat').value == '' && document.getElementById('cnt_address_long').value== ''){
   $("#file_error1").html("Please enter the location from given suggestions");
   $("#file_error1").css("width","100%");
   $("#file_error1").css("margin-top",".286rem");
   $("#file_error1").css("font-size", "80%");
   $("#file_error1").css("color","#f44336");
   isSubmit=false;
   return false;
      }else{
        $("#file_error1").html("");
         isSubmit=true;
      }
      // console.log(document.getElementById('client_address_lat').value);
      // console.log(document.getElementById('client_address_long').value);
      //to remove error if address is filled
      $("input[name='address']").valid();
      
       });
  }


    google.maps.event.addDomListener(window, 'load', init);


        

</script>