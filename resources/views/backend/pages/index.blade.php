@extends('layouts.adminapp')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            {{-- <td width="5%">S No.</td> --}}
                            <td width="20%">Title</td>
                            <td width="20%">Modified on</td>
                            <td width="15%">Actions</td>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <td><input type="text" data-column="2" class="search-input-text form-control input-sm"></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>


<script>
$(function() {
    var dataTable = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        autoWidth: false,
        aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
        iDisplayLength: 10,
        ajax: '{{ url("admin/pages/getdata") }}',
        dataType: JSON,
        columns: [
			{
                data: 'title_en', 
                "render": function (data, type, full, meta) {
					var Title = $('<textarea />').html(removeTags(full.title_en)).text();
                    return removeTags(Title);
                },
            },
            {data: 'modified_on', name: 'modified_on'},
            {
                data: 'id',
                "render": function (data, type, full, meta) {
                  
                    if(full.page_type=="static"){
                        var edit_url = '{{ url("admin/pages/edit") }}/'+full.id;
                    }else{
                        var edit_url = '{{ url("admin/pages/editdesigned") }}/'+full.page_key;
                    }
                    

                    var detail_url = '{{ url("admin/pages/show") }}/'+full.id;
                    return '<a href="' + edit_url + '" class="btn btn-info btn-flat btn-small" title="Update"><i class="fa fa-edit"></i></a> <a href="' + detail_url + '" class="btn btn-info btn-flat btn-small" title="Detail"><i class="fa fa-file-text"></i></a> ';
                }
            }
        ],
        columnDefs: [
           { "orderable": false, "targets": [1,2]  }
        ],
        "order": [[ 0, "asc" ]],
        

    });

    $("#data-table_filter label").css("display","none");  // hiding global search box
    $('#data-table_filter').append($('.search-block').html());


    $('.search-input-text').on( 'keyup', function () {   // for text boxes
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );


});

</script>

@endsection
