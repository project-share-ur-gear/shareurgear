@extends('layouts.adminapp')

@section('content')


@php

$pageContent = json_decode($content->page_content);
 
@endphp

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">*Required Fields</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' =>URL('admin/pages/editphysiopage'),'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm', 'enctype' => 'multipart/form-data']) !!}
         
           @method('POST')
           @csrf
          
          <h3 class="page_tags">Banner Section <hr></h3>    
          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('title',"Title") !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('title', $pageContent->title ?? old('title'), array('placeholder'=>"Title",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('title'))
                  <div class="error-message">{{ $errors->first('title') }}</div>
                @endif
              </div>
          </div>
           
 
          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('banner_image', __('Banner Image')) !!}<span class="error-message">*</span></div>
               <div class="col-md-12">
                  @if(isset($pageContent->banner_image) && $pageContent->banner_image!='' && file_exists(STORAGE_IMG_ROOT.'/app/public/pages_images/'.$pageContent->banner_image))
                    {!! Form::file('banner_image', array('accept'=>"image/*")) !!}
                    @if($errors->has('banner_image'))
                      <div class="error-message">{{ $errors->first('banner_image') }}</div>
                    @endif
                    <br><br>
                    <div style="background-color: #e2e2e2;padding:10px;display:inline-block;">
                      <img src="{{STORAGE_IMG_PATH.'/app/public/pages_images/'.$pageContent->banner_image}}" class="img-fluid rounded img-responsive" width="200">
                    </div>
                  @else
                    {!! Form::file('banner_image', array('required'=>'required','accept'=>"image/*")) !!}
                    @if($errors->has('banner_image'))
                      <div class="error-message">{{ $errors->first('banner_image') }}</div>
                    @endif
                  @endif
                </div>
          </div>


          <h3 class="page_tags">Benefits Offered Section <hr></h3>    
          <div class="form-group">
              <label>{!! Form::label('benfits_heading', __('Top Heading')) !!} *</label>
              {!! Form::text('benfits_heading', $pageContent->benfits_heading ?? old('benfits_heading'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
              @if($errors->has('benfits_heading'))
                  <div class="error-message">{{ $errors->first('benfits_heading') }}</div>
              @endif
               
          </div> 

          <div class="row" id="benefits_row">
             @if(!empty($pageContent->benefits))
               @foreach($pageContent->benefits as $key=> $value)
               <div class="col-md-12 benefits_item">
                 <div class="ed_bock_item">
                    <div class="form-group">
                        <label>Title *</label>
                        <input type="text" name="benefits[{{$key}}][title]" class="form-control required" value="{{$value->title}}">
                    </div> 
                    <div class="form-group">
                        <label>Description *</label>
                        <textarea name="benefits[{{$key}}][desc]" class="form-control editor required">{{$value->desc}}</textarea>
                    </div>  
                    <div class="form-group">
                        <label>Image *</label>
                        <input  type="file" name="benefits_image{{$key}}" class="form-control" accept="image/*">
                        <div class="image_preview" style="margin-top:10px;">
                          <img src="{{STORAGE_IMG_PATH.'/app/public/pages_images/'.$value->image}}" class="img-fluid rounded img-responsive" width="120">
                        </div>
                    </div>   
                    <div class="form-group">
                       <button type="button" class="btn btn-xs btn-danger remove_bene_job">Remove Card</button>
                    </div>
                  </div> 
              </div>
              @endforeach
            @endif

          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-xs btn-success add_benefit_card">+ Add Benefit Card</button>
            </div>
          </div> 


 

          <h3>How it works - Section<hr></h3>  

          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('how_it_work_heading', __('Heading')) !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('how_it_work_heading', $pageContent->how_it_work_heading ?? old('how_it_work_heading'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('how_it_work_heading'))
                  <div class="error-how_it_work_heading">{{ $errors->first('how_it_work_heading') }}</div>
                @endif
              </div>
          </div>

           

          <h4 class="text-danger">Steps Section</h4>
          <div class="row" id="qd_steps_row">
             @if(!empty($pageContent->qd_steps))
               @foreach($pageContent->qd_steps as $key=> $value)
               <div class="col-md-12 qd_item">
                 <div class="ed_bock_item">
                    <div class="form-group">
                        <label>Heading *</label>
                        <input type="text" name="qd_steps[{{$key}}][heading]" class="form-control required" value="{{$value->heading}}">
                    </div> 
                    <div class="form-group">
                        <label>Description *</label>
                        <textarea name="qd_steps[{{$key}}][desc]" class="form-control editor required">{{$value->desc}}</textarea>
                    </div>  
                    <div class="form-group">
                        <label>Normal Image *</label>
                        <input  type="file" name="qd_steps_normal{{$key}}" class="form-control" accept="image/*">
                        <div class="image_preview" style="margin-top:10px;">
                          <img src="{{STORAGE_IMG_PATH.'/app/public/pages_images/'.$value->normal_image}}" class="img-fluid rounded img-responsive" width="120">
                        </div>
                    </div> 
                     <div class="form-group">
                        <label>GIF Image *</label>
                        <input  type="file" name="qd_steps_gif{{$key}}" class="form-control" accept="image/*">
                        <div class="image_preview" style="margin-top:10px;">
                          <img src="{{STORAGE_IMG_PATH.'/app/public/pages_images/'.$value->gif_image}}" class="img-fluid rounded img-responsive" width="120">
                        </div>
                    </div>  
                    <div class="form-group">
                       <button type="button" class="btn btn-xs btn-danger remove_step_btn">Remove Step</button>
                    </div>
                  </div> 
              </div>
              @endforeach
            @endif

          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-xs btn-success add_new_step">+ Add New Step</button>
            </div>
          </div> 


          


          <h3>Testimonials - Section<hr></h3>  

          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('testi_heading', __('Heading')) !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('testi_heading', $pageContent->testi_heading ?? old('testi_heading'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('testi_heading'))
                  <div class="error-testi_heading">{{ $errors->first('testi_heading') }}</div>
                @endif
              </div>
          </div>

          <div class="row" id="tetimonial_row">
             @if(!empty($pageContent->testimonial))
               @foreach($pageContent->testimonial as $key=> $value)
               <div class="col-md-12 testi_item">
                 <div class="ed_bock_item">
                    <div class="form-group">
                        <label>Name *</label>
                        <input type="text" maxlength="20" name="testimonial[{{$key}}][name]" class="form-control required" value="{{$value->name}}">
                    </div> 

                    <div class="form-group">
                        <label>Location *</label>
                        <input type="text" maxlength="20" name="testimonial[{{$key}}][location]" class="form-control required" value="{{$value->location}}">
                    </div>

                    <div class="form-group">
                        <label>User Image *</label>
                        <input  type="file" name="testi_image{{$key}}" class="form-control" accept="image/*">
                        <div class="image_preview" style="margin-top:10px;">
                          <img src="{{STORAGE_IMG_PATH.'/app/public/pages_images/'.$value->image}}" class="img-fluid rounded img-responsive" width="120">
                        </div>
                    </div> 

                    <div class="form-group">
                        <label>Content *</label>
                        <textarea name="testimonial[{{$key}}][content]" class="form-control editor required">{{$value->content}}</textarea>
                    </div>  
                    
                      
                    <div class="form-group">
                       <button type="button" class="btn btn-xs btn-danger remove_testi_btn">Remove Testimonial</button>
                    </div>
                  </div> 
              </div>
              @endforeach
            @endif

          </div>

          <div class="row">
            <div class="col-md-12">
              <button type="button" class="btn btn-xs btn-success add_new_testi">+ Add New Testimonial</button>
            </div>
          </div>


          <h3>Bottom Content - Section<hr></h3>   
          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('btm_content_heading', __('Bottom Section Heading')) !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('btm_content_heading', $pageContent->btm_content_heading ?? old('btm_content_heading'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'50' )) !!}
                @if($errors->has('btm_content_heading'))
                  <div class="error-btm_content_heading">{{ $errors->first('btm_content_heading') }}</div>
                @endif
              </div>
          </div>

          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('btm_content_desc', __('Bottom Section Content')) !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::textarea('btm_content_desc', $pageContent->btm_content_desc ?? old('btm_content_desc'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control editor', 'maxlength'=>'10000' )) !!}
                @if($errors->has('btm_content_desc'))
                  <div class="error-btm_content_desc">{{ $errors->first('btm_content_desc') }}</div>
                @endif
              </div>
          </div>

           <div class="form-group row">
              <div class="col-md-12">{!! Form::label('btm_content_btn_txt', __('Bottom Section Button Text')) !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('btm_content_btn_txt', $pageContent->btm_content_btn_txt ?? old('btm_content_btn_txt'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'30' )) !!}
                @if($errors->has('btm_content_btn_txt'))
                  <div class="error-btm_content_btn_txt">{{ $errors->first('btm_content_btn_txt') }}</div>
                @endif
              </div>
          </div>


          <h3>Meta Block <hr></h3>  
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('meta_title_en', __('Meta Title')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('meta_title_en', $pageContent->meta_title_en ?? old('meta_title_en'), array('placeholder'=>__('pages.labels.meta_title_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('meta_title_en'))
                  <div class="error-message">{{ $errors->first('meta_title_en') }}</div>
                @endif
              </div>
          </div>
                    
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('meta_keywords_en', __('Meta Keywords')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('meta_keywords_en', $pageContent->meta_keywords_en ?? old('meta_keywords_en'), array('placeholder'=>__('pages.labels.meta_keywords_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('meta_keywords_en'))
                  <div class="error-message">{{ $errors->first('meta_keywords_en') }}</div>
                @endif
              </div>
          </div>
          
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('meta_desc_en', __('Meta Description')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::textarea('meta_desc_en', $pageContent->meta_desc_en ?? old('meta_desc_en'), array('placeholder'=>__('pages.labels.meta_desc_en'),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('meta_desc_en'))
                  <div class="error-message">{{ $errors->first('meta_desc_en') }}</div>
                @endif
              
              </div>
          </div>
           
          <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              {!! Form::submit(__("Update"), array('class'=>'btn btn-primary','name'=>'bttnsubmit','id'=>'bttnsubmit')) !!}
              <a href="{{ url('admin/pages/index') }}" class="btn btn-warning">Cancel</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function() {
    $('.add_benefit_card').click(function(event) {
      /* Act on the event */
      var job_box = $('.benefits_item').length;
      job_box = job_box+1;
      var htmlstring = '<div class="col-md-12 benefits_item">'+
                          '<div class="ed_bock_item">'+
                            '<div class="form-group">'+
                                '<label>Title *</label>'+
                                '<input type="text" name="benefits['+job_box+'][title]" class="form-control required" value="">'+
                            '</div>'+ 
                            '<div class="form-group">'+
                                '<label>Description *</label>'+
                                '<textarea name="benefits['+job_box+'][desc]" class="form-control editor required"></textarea>'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>Image *</label>'+
                                '<input  type="file" name="benefits_image'+job_box+'" class="form-control required" accept="image/*">'+
                            '</div>'+   
                            '<div class="form-group">'+
                               '<button type="button" class="btn btn-xs btn-danger remove_bene_job">Remove Card</button>'+
                            '</div>'+
                          '</div>'+
                      '</div>';

      $('#benefits_row').append(htmlstring);      
      CKEDITOR.replace('benefits['+job_box+'][desc]');     
    });

    $(document).on('click','.remove_bene_job',function(e){
      $(this).parents('.benefits_item').remove();
    });

    $(document).on('click','.remove_step_btn',function(e){
      $(this).parents('.qd_item').remove();
    });

    $(document).on('click','.remove_testi_btn',function(e){
      $(this).parents('.testi_item').remove();
    });

    $('.add_new_step').click(function(event) {
      /* Act on the event */
      var item = $('.qd_item').length;
      item = item+1;
      var htmlstring = '<div class="col-md-12 qd_item">'+
                          '<div class="ed_bock_item">'+
                            '<div class="form-group">'+
                                '<label>Heading *</label>'+
                                '<input type="text" name="qd_steps['+item+'][heading]" class="form-control required" value="">'+
                            '</div>'+ 
                            '<div class="form-group">'+
                                '<label>Description *</label>'+
                                '<textarea name="qd_steps['+item+'][desc]" class="form-control editor required"></textarea>'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>Normal Image *</label>'+
                                '<input  type="file" name="qd_steps_normal'+item+'" class="form-control required" accept="image/*">'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>GIF Image *</label>'+
                                '<input  type="file" name="qd_steps_gif'+item+'" class="form-control required" accept="image/*">'+
                            '</div>'+   
                            '<div class="form-group">'+
                               '<button type="button" class="btn btn-xs btn-danger remove_step_btn">Remove Card</button>'+
                            '</div>'+
                          '</div>'+
                      '</div>';

      $('#qd_steps_row').append(htmlstring);      
      CKEDITOR.replace('qd_steps['+item+'][desc]');     
    });

    $('.add_new_testi').click(function(event) {
      /* Act on the event */
      var item = $('.testi_item').length;
      item = item+1;
      var htmlstring = '<div class="col-md-12 testi_item">'+
                          '<div class="ed_bock_item">'+
                            '<div class="form-group">'+
                                '<label>Name *</label>'+
                                '<input maxlength="20" type="text" name="testimonial['+item+'][name]" class="form-control required" value="">'+
                            '</div>'+ 
                            '<div class="form-group">'+ 
                                '<label>Location *</label>'+ 
                                '<input maxlength="20" type="text" name="testimonial['+item+'][location]" class="form-control required">'+ 
                            '</div>'+ 
                            '<div class="form-group">'+
                                '<label>User Image *</label>'+
                                '<input  type="file" name="testi_image'+item+'" class="form-control required" accept="image/*">'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>Content *</label>'+
                                '<textarea name="testimonial['+item+'][content]" class="form-control editor required"></textarea>'+
                            '</div>'+  
                            
                            '<div class="form-group">'+
                               '<button type="button" class="btn btn-xs btn-danger remove_testi_btn">Remove Testimonial</button>'+
                            '</div>'+
                          '</div>'+
                      '</div>';

      $('#tetimonial_row').append(htmlstring);      
      CKEDITOR.replace('testimonial['+item+'][content]');     
    });

    

  }); 
</script>

@endsection



