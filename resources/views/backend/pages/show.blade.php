@extends('layouts.adminapp')

@section('content')

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border pages">

          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message"></span></div>
          <div class="clear10"></div>
          
          <div class="form-group row">
              <div class="col-md-3"><strong>{{ __('Title') }}</strong></div>
              <div class="col-md-6">{!! html_entity_decode($page->title_en) !!}</div>
          </div>
          
          <div class="form-group row">
              <div class="col-md-3"><strong>{{ __('Meta Title') }}</strong></div>
              <div class="col-md-6">{{ $page->meta_title_en }}</div>
          </div>
           
          <div class="form-group row">
              <div class="col-md-3"><strong>{{ __('Meta Keywords') }}</strong></div>
              <div class="col-md-6">{{ $page->meta_keywords_en }}</div>
          </div>
          
          <div class="form-group row">
              <div class="col-md-3"><strong>{{ __('Meta Description') }}</strong></div>
              <div class="col-md-6">{{ $page->meta_desc_en }}</div>
          </div>
           
         
          
          @foreach($pagecontent as $allVal)
            <div class="form-group row">
            		 <div class="col-md-3"><strong>{{ $allVal->page_title }}</strong></div>
                     <div class="col-md-9">
            			@if($allVal->page_type=='file')
                        	 @if($allVal->page_video==0)
                            	<div style="background-color: #e2e2e2;padding:10px;display:inline-block;">
                                	<img src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/static/'.$allVal->page_content) }}" alt="{{ $allVal->page_title }}" title="{{ $allVal->page_title }}" class="img-fluid rounded img-responsive" style="max-width:100px;">
                                </div>
                            @else
                            	<video width="200" height="100" controls>
                                	<source src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/static/'.$allVal->page_content) }}" type="video/mp4">
                                </video>
                            @endif
                        @else
                        	{!! html_entity_decode($allVal->page_content)!!}
                        @endif 
                  	</div>
              </div>
          @endforeach    
         
          <div class="form-group row">
              <div class="col-md-3"><strong>Modified on</strong></div>
              <div class="col-md-6">{{ date('M d, Y h:i A', strtotime($page->updated_at)) }}</div>
          </div>

          <div class="form-group row">
              <div class="col-md-3"></div>
              <div class="col-md-6">
                <a href="{{ url('admin/pages/index') }}" class="btn btn-primary">{{ __('Back') }}</a>
              </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection