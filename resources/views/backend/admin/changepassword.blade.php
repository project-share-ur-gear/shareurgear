@extends('layouts.adminapp')

@section('content')

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">{{ __('*Required Fields') }}</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' => 'admin/updatepassword', 'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm', 'enctype' => 'multipart/form-data']) !!}

            @method('PATCH')
            @csrf
          
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('current_password', __('admin.labels.current_password')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::password('current_password',  array('placeholder'=>__('admin.labels.current_password'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'30' )) !!}
                @if($errors->has('current_password'))
                  <div class="error-message">{{ $errors->first('current_password') }}</div>
                @endif
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('password', __('admin.labels.new_password')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::password('password',  array('placeholder'=>__('admin.labels.new_password'),'required'=>'required', 'class'=>'form-control passcheck', 'maxlength'=>'30','id'=>'password' )) !!}
                @if($errors->has('password'))
                  <div class="error-message">{{ $errors->first('password') }}</div>
                @endif
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('password_confirmation', __('admin.labels.new_confirm_password')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::password('password_confirmation', array('placeholder'=>__('admin.labels.new_confirm_password'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'30' )) !!}
                @if($errors->has('password_confirmation'))
                  <div class="error-message">{{ $errors->first('password_confirmation') }}</div>
                @endif
              </div>
          </div>

          <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
              {!! Form::submit("Update", array('class'=>'btn btn-primary' )) !!}
              <a href="{{ url('admin/edit') }}" class="btn btn-warning">Cancel</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

@endsection