@extends('layouts.adminapp')

@section('content')


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">{{ __('*Required Fields') }}</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' => 'admin/update', 'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm', 'enctype' => 'multipart/form-data','autocomplete'=>"off"]) !!}

            @method('PATCH')
            @csrf
          
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('name', __('admin.labels.name')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('name', old('name', $admin->name), array('placeholder'=>__('admin.labels.name'),'required'=>'required', 'class'=>'form-control required', 'maxlength'=>'100' )) !!}
                @if($errors->has('name'))
                  <div class="error-message">{{ $errors->first('name') }}</div>
                @endif
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('email', __('admin.labels.email')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::email('email', old('email', $admin->email), array('placeholder'=>__('admin.labels.email'),'required'=>'required', 'class'=>'form-control email checkemail', 'maxlength'=>'255' )) !!}
                @if($errors->has('email'))
                  <div class="error-message">{{ $errors->first('email') }}</div>
                @endif
              </div>
          </div>

          <div class="form-group row checkerPwd" style="display:none">
              <div class="col-md-2">{!! Form::label('current_password', __('Current Password')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::password('current_password',  array('placeholder'=>__('admin.labels.current_password'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'30' )) !!}
                @if($errors->has('current_password'))
                  <div class="error-message">{{ $errors->first('current_password') }}</div>
                @endif
              </div>
          </div>

          <div class="form-group row">
            <div class="col-md-2">{!! Form::label('profile_image', __('admin.labels.upload_photo')) !!}</div>
            <div class="col-md-6">
              @if($admin->profile_image!='' && file_exists(public_path('storage/admin_profile_photo/'.$admin->profile_image)) )
                {!! Form::file('profile_image', array()) !!}
                @if($errors->has('profile_image'))
                  <div class="error-message">{{ $errors->first('profile_image') }}</div>
                @endif
                <br><br>
                <img src="{{ Storage::url('admin_profile_photo/thumb_'.$admin->profile_image) }}" alt="{{ $admin->name }}" title="{{ $admin->name }}" class="img-fluid rounded img-responsive">
              @else
                {!! Form::file('profile_image', array()) !!}
                @if($errors->has('profile_image'))
                  <div class="error-message">{{ $errors->first('profile_image') }}</div>
                @endif
              @endif
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
              {!! Form::button(__('Update'), array('class'=>'btn btn-primary updateProfile' )) !!}
              <a href="{{ url('admin/dashboard') }}" class="btn btn-warning">{{ __('Cancel') }}</a>

              <a href="{{ url('admin/changepassword') }}" class="btn btn-info">{{ __('admin.labels.change_password') }}</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  var currentEmail =  '{{$admin->email}}';
  
  jQuery(document).ready(function($) {
    $('.updateProfile').click(function(event) {
      /* Act on the event */
      if($('#pageForm').valid()){
        $('#pageForm').submit();  
      }
    });

    $('#email').keyup(function(event) {
      var value = $(this).val();
      if($.trim(value)!=currentEmail){
        $('#current_password').val('');
        $('.checkerPwd').fadeIn();
      }else{
        $('.checkerPwd').hide();
      }
    })

  });
</script>

@endsection