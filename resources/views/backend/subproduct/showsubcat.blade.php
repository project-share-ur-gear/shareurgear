@extends('layouts.adminapp')

@section('content')

<section class="content">
	<div class="row">

               

	    <div class="col-md-12">

            <div class="form-group row">
                        <div class="col-md-6">
                            <a href="{{ url('admin/subproduct/sub-product') }}" class="btn btn-primary">{{ __('Back') }}</a>
                        </div>
            </div>
            <div class="box">
                <div class="box-header with-border" >
                	<div class="clear10"></div>
                	<div class="col-md-12" align="right">
                        <span class="error-message"></span>
                    </div>
                	<div class="clear10"></div>
                	<h4 class="boxheading"><u><strong>Sub Category:</strong></u></h4>
                   
                  
                  
 
                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Title</strong></div>
                            <div class="col-md-8">{{$sub_cat_data->subcategorytitle}}</div>
                        </div>

                        
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Main Catagory</strong></div>
                            <div class="col-md-8">{{$sub_cat_data->main_catagory_title}}</div>
                        </div>
                        
                    </div>
                  
    </div>
</section>
                 
@endsection