@extends('layouts.adminapp')

@section('content')

@php
    $pageUrl='admin/subproduct/add-sub-product';
    if(isset($sub_cat_data)){
        $pageUrl='admin/subproduct/update-subcat/'.$sub_cat_data->sub_category_id; 
        
    }
    
@endphp
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form class="contact_form" id="contact_form" action="{{url($pageUrl) }}" method="post">
            @csrf
                <div class="box">
                    <div class="box-header with-border " style="margin:10px;">
                    <div class="clear10"></div>
                        <div class="form-group ">
                            <label>Categary Sub Title</label>
                            <input type="text" class="form-control required" name="c_title" id="c_title" value="@if(!empty($sub_cat_data->subcategorytitle)){{$sub_cat_data->subcategorytitle}}@endif"  required>
                        </div>
                        <div class="form-group">
                                <label for="">Product Category</label>
                                <div class="pro-cateOption type-select">
                                    <select class="form-control custom-select Required" name="product_category" id="product_category" required aria-required="true">
                                    <option >Choose Product-Category</option>
                                    @foreach($product_categories as $key => $value)
                                        <option value="{{$value->product_category_id}}" @if(!empty($sub_cat_data))@if($sub_cat_data->product_category_id_for_sub==$value->product_category_id){{'selected'}}@endif @endif  >{{$value->productcategorytitle}}</option>
                                    @endforeach
                                    </select>
                                </div>
                        </div>



                        <div class="form-group row">
                            <button class="btn  submit-btn my-3 save_data_btn" data-inprocess="Saving..." data-default="Save" type="button">Save</button> 
                        </div>
                    </div>



                </div>
            </form>
        </div>
        
    </div>

</section>   
<script type="text/javascript">
  $(document).ready(function(e) {
    $('.save_data_btn').click(function(event) {
               /* Act on the event */
                if($('#contact_form').valid()){
                    $('#contact_form').submit();
                }else{
                    var ele  = "";
                    $('.help-block').each(function(index, el) {
                        if($(el).is(':visible')  && ele==""){
                            ele = el;
                        }   
                    });
                    $('html, body').animate({
                            scrollTop: $(ele).offset().top-150
                    }, 500);
                }
        });
  
  });      
      
</script>
@endsection

