@extends('layouts.adminapp')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
            <div class="box-header with-border sbh search-block">
                <a href="{{ url('admin/subproduct/create-sub-product') }}" class="btn btn-primary">{{ __('Add Categories') }}</a>
                <button id="deleteTrigger" class="btn btn-danger">{{ __('Delete User') }}</button>
            </div>
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            {{-- <td width="5%">S No.</td> --}}
                            <td width="5%"><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                            <td width="20%">Sub Title</td>
                            <td width="20%">Date</td>
                            <td width="15%">Actions</td>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                              <td><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                            <!-- <td><input type="text" data-column="1" class="search-input-text form-control input-sm"></td> -->
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>


<script>
$(function() {
    var dataTable = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        autoWidth: false,
        aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
        iDisplayLength: 10,
        ajax: '{{ url("admin/subproduct/subgetproductcategories") }}',
        dataType: JSON,
        columns: [
            {
                "render": function (data, type, full, meta) {
                    return '<input type="checkbox" id="deleteRow_'+ full.sub_category_id +'" class="deleteRow" value="'+ full.sub_category_id +'" />';
                },
                "orderable": false
            },
            {data: 'subcategorytitle', name: 'subcategorytitle'},
            // {data: 'productcategorystatus', name: 'productcategorystatus'},
            {data: 'sub_category_added_date', name: 'sub_category_added_date'},
            {
                "render": function (data, type, full, meta) {
                    var edit_url = '{{ url("admin/product/add-product-categories") }}/'+full.sub_category_id;
                    var status_url = '{{ url("admin/resource/changestatus") }}/'+full.sub_category_id;
                    var status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-small" title="Status"><i class="fa fa-remove"></i></a>';
                    if(full.subcategorystatus=='1'){
                        status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-small" title="Status"><i class="fa fa-check"></i></a>';
                    }
                    



                    return '<a href="' + edit_url + '" class="btn btn-info btn-flat btn-small" title="Update"><i class="fa fa-edit"></i></a> '+status_link;
                
                
                
                
                }
            }
        ],
        columnDefs: [
           { "orderable": false, "targets": [0,2]  }
        ],
        "order": [[ 2, "asc" ]],
        

    });

    // $("#data-table_filter label").css("display","none");  // hiding global search box
    // $('#data-table_filter').append($('.search-block').html());
    $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });
    $(document.body).on('click', '.deleteRow' ,function(){ // single checked
        var is_all_checked = true;
        var status = this.checked;
        $(".deleteRow").each( function() {
            if($(this).prop("checked")!=status){
                is_all_checked = false;
            }
        });
        $("#bulkDelete").prop("checked",is_all_checked);
    });




    $('.search-input-text').on( 'keyup', function () {   // for text boxes
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    });

      $('#deleteTrigger').on("click", function(event){
        if( $('.deleteRow:checked').length > 0 ){

                var check = confirm("{{ __('Are you sure? You want to delete Categories ?') }}");    
      
            
            if(check == true){
                var ids = [];
                $('.deleteRow').each(function(){
                    if($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });
                var ids_string = ids.toString();
                $.ajax({
                    type    : "POST",
                    type    : 'DELETE',
                    headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url     : "{{ url('admin/product/destroy') }}",
                    data    : {data_ids:ids_string },
                    success : function(result){
                        if(result.success){
                            showpopupmessage('Account(s) deleted successfully','success');
                            setTimeout(function(){
                                location.reload();
                            },2000)
                        }
                        dataTable.draw();
                    },
                    async:false
                });
            }
        }
        else {
            showpopupmessage("{{ __('users.messages.select_atleast_one') }}", "error");
        }
    });



});

</script>

@endsection
