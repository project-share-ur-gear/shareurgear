@extends('layouts.adminapp')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-header with-border sbh search-block">

                </div>
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <td width="5%">S. No</td>
                            <td width="20%">User</td>
                            <td width="20%">Location</td>
                            <td width="10%">Booking Price</td>
                            <td width="20%">Created On</td>
                            <td width="15%">{{ __('Actions') }}</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script>

var slength=[0,5];
$(document).ready(function(){
    $("#data-table").DataTable({
        "bProcessing":false,
        "bServerSide":true,
        "bAutoWidth":true,
        "responsive":true,
        "responsive": {
            "details": {
                renderer: function(api, rowIdx, columns){
                    var $row_details = $.fn.DataTable.Responsive.defaults.details.renderer(api, rowIdx, columns);
                    return $row_details;
                }
            }
        },
        "bInfo":false,
        "pagingType":"full_numbers",
        "order":[[4,"desc"]],
        "aoColumnDefs":[
            {"bSortable":false,"aTargets":slength},
            {"bSearchable":false,"aTargets":slength}
        ],
        "sAjaxSource": "{{ route('admin.booking.getcancelbookings')}}",
        "iDisplayLength":10,
        "aLengthMenu":[[10,50,100],[10,50,100]],
        "fnDrawCallback":function(oSettings){
            if($(".deimg").length>0){
                $('.deimg').initial(); 
            }
        },
    });
});
</script>

@endsection
