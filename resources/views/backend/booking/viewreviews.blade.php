@extends('layouts.adminapp')

@section('content')

<section class="content">
	<div class="row">
        <div class="col-md-12">

            <div class="form-group row">
                <div class="col-md-6">
                    <a href="{{ route('admin.booking.reviews') }}" class="btn btn-primary">{{ __('Back') }}</a>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border" >
                    <div class="clear10"></div>
                    <div class="col-md-12" align="right">
                        <span class="error-message"></span>
                    </div>
                    <div class="clear10"></div>
                    <h4 class="boxheading"><u><strong>View Reviews:</strong></u></h4>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>User</strong></div>
                            <div class="col-md-8">{{  $getReviewData->name }}</div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Product </strong></div>
                            <div class="col-md-8"><a target="_blank" href="{{  url('admin/product/show-product/'.$getReviewData->review_product_id) }}">{{  $getReviewData->product_title }}</a></div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Rating</strong></div>
                            <div class="col-md-8">
                            
                            @for($i = 1; $i <= 5; $i++)
                                <i class="fa fa-star" aria-hidden="true" style="font-size: 22px;color:{{ ($getReviewData->review_rating >= $i)?'#f7a40c':'#dddcd8'}}"></i>
                            @endfor
                        </div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-10 row">
                            <div class="col-md-4"><strong>Messsage</strong></div>
                            <div class="col-md-8">{!!  nl2br($getReviewData->review_messsage) !!}</div>
                        </div>
                    </div>
                    


                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Date</strong></div>
                            <div class="col-md-8">{{  date('d-m-Y',strtotime($getReviewData->review_dated_on)) }}</div>
                        </div>
                    </div>

                </div>
            </div>
                
        </div>
    </div>
</section>
                 
@endsection