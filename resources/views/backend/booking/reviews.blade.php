@extends('layouts.adminapp')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-header with-border sbh search-block">
                    <button id="deleteTrigger" class="btn btn-danger">{{ __('Delete') }}</button>
                </div>
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            <td width="5%"><input type="checkbox" name="bulkDelete" id="bulkDelete" value="all" /></td>
                            <td width="20%">User</td>
                            <td width="20%">Product</td>
                            <td width="10%">Rating</td>
                            <td width="20%">Created On</td>
                            <td width="15%">{{ __('Actions') }}</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>

<script>
/*
$(function() {
    var dataTable = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        autoWidth: false,
        aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
        iDisplayLength: 10,
        ajax: '{{ url("admin/faq/getdata") }}',
        dataType: JSON,
        columns: [
            {
                data: 'id', 
                "render": function (data, type, full, meta) {
                    return '<input type="checkbox" id="deleteRow_'+ full.id +'" class="deleteRow" value="'+ full.id +'" />';
                },
                "orderable": false
            },
            {data: 'question_en', name: 'question_en'},
            {data: 'sort_order', name: 'sort_order'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {
                data: 'id', 
                "render": function (data, type, full, meta) {
                    var edit_url = '{{ url("admin/faq/edit") }}/'+full.id;
                    var detail_url = '{{ url("admin/faq/show") }}/'+full.id;
                    var status_url = '{{ url("admin/faq/changestatus") }}/'+full.id;
                    
                    var status_link = '<a href="' + status_url + '" class="btn btn-danger btn-flat btn-small" title="Status"><i class="fa fa-remove"></i></a>';
                    if(full.status=='1'){
                        status_link = '<a href="' + status_url + '" class="btn btn-success btn-flat btn-small" title="Status"><i class="fa fa-check"></i></a>';
                    }
                    return '<a href="' + edit_url + '" class="btn btn-info btn-flat btn-small" title="Update"><i class="fa fa-edit"></i></a> <a href="' + detail_url + '" class="btn btn-info btn-flat btn-small" title="Detail"><i class="fa fa-file-text"></i></a> '+status_link;
                }
            }
        ],
        columnDefs: [
           { "orderable": false, "targets": [0,1,4]  }
        ],
        "order": [[ 2, "asc" ]],
        

    });


    $('.search-input-text').on( 'keyup', function () {   // for text boxes
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );


    $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
    });

    $(document.body).on('click', '.deleteRow' ,function(){ // single checked
        var is_all_checked = true;
        var status = this.checked;
        $(".deleteRow").each( function() {
            if($(this).prop("checked")!=status){
                is_all_checked = false;
            }
        });
        $("#bulkDelete").prop("checked",is_all_checked);
    });

    $('#deleteTrigger').on("click", function(event){
        if( $('.deleteRow:checked').length > 0 ){
            var check = confirm("{{ __('faq.messages.delete_confirmation') }}");
            if(check == true){
                var ids = [];
                $('.deleteRow').each(function(){
                    if($(this).is(':checked')) {
                        ids.push($(this).val());
                    }
                });
                var ids_string = ids.toString();
                $.ajax({
                    type: "POST",
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    url: "{{ url('admin/faq/destroy') }}",
                    data: {data_ids:ids_string },
                    success: function(result) {
                        if(result.success){
                            showpopupmessage(result.success, "success");
                        }
                        dataTable.draw();
                        
                    },
                    async:false
                });
            }

        } else {
            showpopupmessage("{{ __('faq.messages.select_atleast_one') }}", "error");
        }
    }); 
});*/


var slength=[0,5];
$(document).ready(function(){
    $("#data-table").DataTable({
        "bProcessing":false,
        "bServerSide":true,
        "bAutoWidth":true,
        "responsive":true,
        "responsive": {
            "details": {
                renderer: function(api, rowIdx, columns){
                    var $row_details = $.fn.DataTable.Responsive.defaults.details.renderer(api, rowIdx, columns);
                    return $row_details;
                }
            }
        },
        "bInfo":false,
        "pagingType":"full_numbers",
        "order":[[4,"desc"]],
        "aoColumnDefs":[
            {"bSortable":false,"aTargets":slength},
            {"bSearchable":false,"aTargets":slength}
        ],
        "sAjaxSource": "{{ route('admin.booking.getreviews')}}",
        "iDisplayLength":10,
        "aLengthMenu":[[10,50,100],[10,50,100]],
        "fnDrawCallback":function(oSettings){
            if($(".deimg").length>0){
                $('.deimg').initial(); 
            }
        },
    });
});

$("#bulkDelete").on('click',function() { // bulk checked
    var status = this.checked;
    $(".deleteRow").each( function() {
        $(this).prop("checked",status);
    });
});

$(document.body).on('click', '.deleteRow' ,function(){ // single checked
    var is_all_checked = true;
    var status = this.checked;
    $(".deleteRow").each( function() {
        if($(this).prop("checked")!=status){
            is_all_checked = false;
        }
    });
    $("#bulkDelete").prop("checked",is_all_checked);
});

$('#deleteTrigger').on("click", function(event){
    if( $('.deleteRow:checked').length > 0 ){
        var check = confirm("{{ __('Are you sure you want to delete reviews.') }}");
        if(check == true){
            var ids = [];
            $('.deleteRow').each(function(){
                if($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            var ids_string = ids.toString();
            $.ajax({
                type: "POST",
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                url: "{{ route('admin.booking.removereviews') }}",
                data: {data_ids:ids_string },
                success: function(result) {
                    var obj = JSON.parse(result);
                    console.log(obj);
                    if(obj.status){
                        showpopupmessage(obj.msg, "success");

                        setTimeout(function(){
                            location.reload()
                        },1000);
                    }                    
                },
                async:false
            });
        }

    } else {
        showpopupmessage("Please select at least one review to perform this action.  ", "error");
    }
}); 
</script>

@endsection
