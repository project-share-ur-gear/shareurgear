@extends('layouts.adminapp')

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js" integrity="sha512-eYSzo+20ajZMRsjxB6L7eyqo5kuXuS2+wEbbOkpaur+sA2shQameiJiWEzCIDwJqaB0a4a6tCuEvCOBHUg3Skg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<style>
hr {
    border-top: 1px solid #c3bdbd59;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
<section class="content">
	<div class="row">
        <div class="col-md-12">

            <div class="form-group row">
                <div class="col-md-6">
                    <a href="{{ route('admin.booking.bookings') }}" class="btn btn-primary">{{ __('Back') }}</a>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border" >
                    <div class="clear10"></div>
                    <div class="col-md-12" align="right">
                        <span class="error-message"></span>
                    </div>
                    <div class="clear10"></div>
                    <h4 class="boxheading"><u><strong>View Bookings:</strong></u></h4>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>User</strong></div>
                            <div class="col-md-8">{{  $bookingData->name }}</div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Product </strong></div>
                            <div class="col-md-8"><a target="_blank" href="{{  url('admin/product/show-product/'.$bookingData->booking_product_id) }}">{{  $bookingData->product_title }}</a></div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Start Date</strong></div>
                            <div class="col-md-8">{{  date('d-m-Y',strtotime($bookingData->booking_start_date)) }}</div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>End Date</strong></div>
                            <div class="col-md-8">{{  date('d-m-Y',strtotime($bookingData->booking_end_date)) }}</div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Location</strong></div>
                            <div class="col-md-8">{{ $bookingData->booking_location }}</div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Booking Price</strong></div>
                            <div class="col-md-8">{{ $bookingData->booking_price }}</div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Booking Distance Price</strong></div>
                            <div class="col-md-8">{{ $bookingData->booking_distance_price }}</div>
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Booking Extra Item Price</strong></div>
                            <div class="col-md-8">{{ $bookingData->booking_extra_item_price }}</div>
                        </div>
                    </div>
                    
                    <div class="form-group row ">
                        <div class="col-sm-6 row">
                            <div class="col-md-4"><strong>Booking Total Price</strong></div>
                            <div class="col-md-8">{{ $bookingData->booking_total_price }}</div>
                        </div>
                    </div>

                    @if($bookingData->booking_status == '2')           
                        <div class="form-group row ">
                            <div class="col-sm-6 row">
                                <div class="col-md-4"><strong>Cancel Reason</strong></div>
                                <div class="col-md-8">{{ $bookingData->booking_cancelled_reason }}</div>
                            </div>
                        </div>
                    @endif
                    <hr/>

                    @if(!empty($checkinCheckout))
                        <h4 class="boxheading"><u><strong>Booking Check In - Check Out:</strong></u></h4>
                        
                        @foreach($checkinCheckout as $key => $value)
                            <h4 class="boxheading"><u>{{ ($value['ckio_type']=='checkin')?'Check In':'Check Out'  }}:</u></h4>
                                
                            <div class="form-group row ">
                                <div class="col-sm-6 row">
                                    <div class="col-md-4"><strong>Images</strong></div>
                                    <div class="col-md-8">
                                        <div style="width:200px;height:200px;">
                                            <img src="{{ HTTP_UPLOADED_IMAGES_PATH.'/'.$value['ckio_images'] }}" style="width:100%;height:100%;" />
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6 row">
                                    <div class="col-md-4"><strong>{{ ($value['ckio_type']=='checkin')?'Check In':'Check Out'  }} By </strong></div>
                                    <div class="col-md-8">{{ $value['ckio_user_type'] }}</div>
                                </div>

                                <div class="col-sm-6 row">
                                    <div class="col-md-4"><strong>Date / Time</strong></div>
                                    <div class="col-md-8">{{ date('d M, Y h:i a',strtotime($value['ckio_date'].' '.$value['ckio_time'])) }}</div>
                                </div>
                                
                                <div class="col-sm-12 row">
                                    <div class="col-md-4"><strong>Description</strong></div>
                                    <div class="col-md-8">{!! nl2br($value['ckio_description']) !!}</div>
                                </div>
                            </div>
                        @endforeach
                    <hr/>
                    @endif
                            
                    @if(!empty($getReportBy))
                        <h4 class="boxheading"><u><strong>Booking Reports:</strong></u></h4>
                        
                        @foreach($getReportBy as $key => $value)
                            <div class="form-group row ">            
                                <div class="col-sm-6 row">
                                    <div class="col-md-4"><strong>Report By </strong></div>
                                    <div class="col-md-8">{{ $value['report_by_type'] }}</div>
                                </div>

                                <div class="col-sm-6 row">
                                    <div class="col-md-4"><strong>Date / Time</strong></div>
                                    <div class="col-md-8">{{ date('d M, Y h:i a',strtotime($value['report_on'])) }}</div>
                                </div>
                                
                                <div class="col-sm-12 row">
                                    <div class="col-md-4"><strong>Description</strong></div>
                                    <div class="col-md-8">{!! nl2br($value['report_summary']) !!}</div>
                                </div>
                            </div>
                            <hr/>
                        @endforeach 
                    @endif

                    <div class="form-group row ">
                        <div class="col-sm-6">
                            <form method="post" id="refund_amount_form" class="refund_amount_form">
                                @csrf
                                <h4 class="boxheading" style="text-align: center;"><u><strong>Refund Amount:</strong></u></h4>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control required money_charm" name="refund_amount" id="refund_amount" placeholder="Refund Amount" maxlength="10">
                                    </div>
                                </div>
                                <input type="hidden" name="action" id="action" value="refund"/>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button class="btn btn-primary pntn" type="button" onclick="submitFrom('refund_amount_form');">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-sm-6">
                            <form method="post" id="release_amount_form" class="release_amount_form">
                                @csrf
                                <h4 class="boxheading" style="text-align: center;"><u><strong>Release Amount:</strong></u></h4>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control required money_charm" name="release_amount" id="release_amount" placeholder="Release Amount" maxlength="10">
                                    </div>
                                </div>
                                <input type="hidden" name="action" id="action" value="release"/>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button class="btn btn-primary pntn" type="button" onclick="submitFrom('release_amount_form');">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="col-sm-6">
                            <form method="post" id="charge_amount_form" class="release_amount_form">
                                @csrf
                                <h4 class="boxheading" style="text-align: center;"><u><strong>Extra Charge Amount:</strong></u></h4>
                                <div class="col-sm-8">
                                    <div class="form-group">
                                        <input type="text" class="form-control required money_charm" name="charge_amount" id="charge_amount" placeholder="Charge Amount" maxlength="10">
                                    </div>
                                </div>
                                <input type="hidden" name="action" id="action" value="charge"/>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button class="btn btn-primary pntn" type="button" onclick="submitFrom('charge_amount_form');">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        
                        @if(!empty($getTranscation))
                            <div class="col-sm-12" style="padding: 0px 90px;">
                                <h4 class="boxheading" style="text-align: center;"><u><strong>Transcations:</strong></u></h4>
                                <table>
                                    <tr>
                                        <th>Txn Type</th>
                                        <th>Txn Id</th>
                                        <th>Txn Amount</th>
                                        <th>Txn Date</th>
                                    </tr>
                                    @foreach($getTranscation as $key => $value)
                                        <tr>
                                            <td>{{ ucfirst($value['txn_type']) }}</td>
                                            <td><code>{{ $value['txn_rrc_id'] }}</code></td>
                                            <td>{{ $value['txn_amount'] }}</td>
                                            <td>{{ date('d M, Y h:i a',strtotime($value['txn_on'])) }}</td>
                                        </tr>
                                    @endforeach            
                                </table>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
                
        </div>
    </div>
</section>
<script> 
    $(document).ready(function(){
        $('#refund_amount_form').validate();
        $('#release_amount_form').validate();
    });      

    $(document).on('keyup','.money_charm',function () { 
        this.value = this.value.replace(/[^0-9\.]/g,'');
    });

    $(document).on('keyup','.money_charm',function(e){
        var inputVal = $(this).val();
        var inputVal = $(this).val();
        var inputLen = $(this).val().length;
        var dott = inputVal.toString().indexOf(".");
        var allowDi = parseFloat(inputLen) - parseFloat(dott);
        if(dott != -1){var allowDi = parseFloat(inputLen) - parseFloat(dott);}
        else{var allowDi = '0';}
        if((inputLen > 5 && allowDi == 0) || (inputLen > 8 || allowDi >3)){
            $(this).val(tempInput);
        }else{
            tempInput = inputVal;
        }
    });

    $(document).keypress(
    function(event){
        if (event.which == '13') {
        event.preventDefault();
        }
    });

    function submitFrom(from){
        if ($('#'+from).valid()) {
            blockUI();
            $('.pntn').attr('disabled','disabled');
            $('#'+from).submit();
        }
    }

    function blockUI() {
        $.blockUI({
            css: {
                backgroundColor: 'transparent',
                border: 'none'
            },
            message: '<div class="spinner"><div class="spinner-border m-5" role="status"><span class="sr-only">Loading...</span> Loading....</div></div>',
            baseZ: 1500,
            overlayCSS: {
                backgroundColor: '#FFFFFF',
                opacity: 0.7,
                cursor: 'wait'
            }
        });
    } //end Blockui   
</script>
@endsection