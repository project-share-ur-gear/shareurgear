@extends('layouts.adminapp')

@section('content')

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">

          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message"></span></div>
          <div class="clear10"></div>
          
         
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Question:') }}</strong></div>
              <div class="col-md-6">{{ $faq->question_en }}</div>
          </div>
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Answer:') }}</strong></div>
              <div class="col-md-10">{!!html_entity_decode($faq->answer_en)!!}</div>
          </div>
          
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Status:') }}</strong></div>
              <div class="col-md-6">
                @if($faq->status=='1')
                  Enabled
                @else
                  Disabled
                @endif
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Added On:') }}</strong></div>
              <div class="col-md-6">{{ $faq->created_at }}</div>
          </div>
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Modified On:') }}</strong></div>
              <div class="col-md-6">{{ $faq->updated_at }}</div>
          </div>

          <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-6">
                <a href="{{ url('admin/faq/index') }}" class="btn btn-primary">{{ __('Back') }}</a>
              </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection