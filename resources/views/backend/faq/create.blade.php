@extends('layouts.adminapp')

@section('content')


@php
    $pageUrl='admin/faq/store';
    $buttonText='Save';
    if(isset($faq)){
        $pageUrl='admin/faq/update/'.$faq->id; 
        $buttonText='Update';
    }  
@endphp


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">{{ __('*Required Fields') }}</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' => $pageUrl, 'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm']) !!}
         
            @if(isset($faq))
                  @method('PATCH')
                  @csrf
            @endif
           
            
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('question_en', __('Question')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::textarea('question_en', $faq->question_en ?? old('question_en'), array('placeholder'=>__('faq.labels.question_en'),'required'=>'required', 'class'=>'form-control editor', 'maxlength'=>'100' )) !!}
                @if($errors->has('question_en'))
                  <div class="error-message">{{ $errors->first('question_en') }}</div>
                @endif
              </div>
          </div>
          
           
          
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('answer_en', __('Answer')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::textarea('answer_en', $faq->answer_en ?? old('answer_en'), array('placeholder'=>__('faq.labels.answer_en'),'required'=>'required','class'=>'form-control required editor' )) !!}
                @if($errors->has('answer_en'))
                  <div class="error-message">{{ $errors->first('answer_en') }}</div>
                @endif
              
              </div>
          </div>

          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('sort_order', __('Sort Order')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::number('sort_order', $faq->sort_order ?? old('sort_order'), array('placeholder'=>__('Sort Order'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('sort_order'))
                  <div class="error-message">{{ $errors->first('sort_order') }}</div>
                @endif
              </div>
          </div>   
          
           
          
           <div class="form-group row">
              <div class="col-md-2">{!! Form::label('status', __('faq.labels.status')) !!}</div>
              <div class="col-md-6">
                @foreach($status_list as $status_key => $status)
                   {!! Form::radio('status', $status_key, isset($faq) && $faq->status==$status_key ?? true :: false, array('id'=>'status_'.$status_key, 'required'=>'required' )); !!}
                  {!! Form::label('status_'.$status_key, $status ) !!}
                @endforeach
                @if($errors->has('status'))
                  <div class="error-message">{{ $errors->first('status') }}</div>
                @endif
              </div>
          </div>
         

          <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
              {!! Form::submit(__($buttonText), array('class'=>'btn btn-primary','name'=>'bttnsubmit','id'=>'bttnsubmit')) !!}
              <a href="{{ url('admin/faq/index') }}" class="btn btn-warning">{{ __('Cancel') }}</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

@endsection