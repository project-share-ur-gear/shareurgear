@extends('layouts.adminapp')

@section('content')

<style type="text/css">
.starboxer a{cursor: default;padding: 22.5px 15px !important}
.boxlineker{text-align: center;display: inline-block;width: 100%;padding: 8px;color: #FFF;}
.boxlineker:hover, .boxlineker:focus{opacity: 0.9}
.bg-grey{background: #696969 !important;color: #FFF !important}
.starboxer i{color: gold;margin-right: 5px}
</style>

 
 
<section class="content" style="height: auto !important; min-height: 0px !important;">
    <div class="row">
        
        


        <div class="col-lg-4 col-xs-6">

            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{    $pages}}</h3>
                    <p>Manage Pages</p>
                </div>
                <div class="icon">
                    <i class="fa fa-briefcase"></i>
                </div>
                <a href="{{ route('admin.pages.index') }}" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        <div class="col-lg-4 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{ $emailTempCount }}</h3>
                    <p>Email Templates</p>
                </div>
                <div class="icon">
                    <i class="fa fa-envelope"></i>
                </div>
                <a href="{{ url('admin/templates/index') }}" class="small-box-footer">View All <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>

        
    </div>

     
     

    
</div>
 

@endsection