@extends('layouts.adminapp')

@section('content')

@php

$template->content_en=str_replace(array("{logo_url}","{site_name}"),array(env('APP_URL').'/storage/app/public/logo/'.$site_config['site_logo'],$site_config['site_name_en']),$template->content_en);
$template->content_fr=str_replace(array("{logo_url}","{site_name}"),array(env('APP_URL').'/storage/app/public/logo/'.$site_config['site_logo'],$site_config['site_name_en']),$template->content_fr);
@endphp
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">

          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message"></span></div>
          <div class="clear10"></div>
          
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Title:') }}</strong></div>
              <div class="col-md-6">{{ $template->title_en }}</div>
          </div>
           
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Subject:') }}</strong></div>
              <div class="col-md-10">{!!html_entity_decode($template->subject_en)!!}</div>
          </div>
            
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Content:') }}</strong></div>
              <div class="col-md-10">{!!html_entity_decode($template->content_en)!!}</div>
          </div>
           
          
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Added On') }}</strong></div>
              <div class="col-md-6">{{ $template->created_at }}</div>
          </div>
          <div class="form-group row">
              <div class="col-md-2"><strong>{{ __('Updated On') }}</strong></div>
              <div class="col-md-6">{{ $template->updated_at }}</div>
          </div>

          <div class="form-group row">
              <div class="col-md-2"></div>
              <div class="col-md-6">
                <a href="{{ url('admin/templates/index') }}" class="btn btn-primary">{{ __('Back') }}</a>
              </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</section>

@endsection