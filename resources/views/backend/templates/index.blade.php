@extends('layouts.adminapp')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12"> 
            <div class="box">
                <div class="box-body">
                <div class="list-table-wrapper">
                <table id="data-table" class="table">
                    <thead>
                        <tr>
                            {{-- <td width="5%">S No.</td> --}}
                            <td width="20%">{{ __('Title') }}</td>
                            <td width="20%">{{ __('Subject') }}</td>
                            <td width="20%">{{ __('Added On') }}</td>
                            <td width="20%">{{ __('Updated On') }}</td>
                            <td width="15%">{{ __('Actions') }}</td>
                        </tr>
                    </thead>
                    <thead>
                        <tr>
                            <td><input type="text" data-column="1" class="search-input-text form-control input-sm"></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                </div>

                </div>
            </div>
        </div>
    </div>
</section>


<script>
$(function() {
    var dataTable = $('#data-table').DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        autoWidth: false,
        aLengthMenu: [[5, 10, 25, 50, 100, 500, -1], [5, 10, 25, 50, 100, 500, "All"]],
        iDisplayLength: 10,
        ajax: '{{ url("admin/templates/getdata") }}',
        dataType: JSON,
        columns: [
            {data: 'title_en', name: 'title_en'},
			{data: 'subject_en', name: 'subject_en'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {
                data: 'id',
                "render": function (data, type, full, meta) {
                    var edit_url = '{{ url("admin/templates/edit") }}/'+full.id;
					var detail_url = '{{ url("admin/templates/show") }}/'+full.id;
                     return '<a href="' + edit_url + '" class="btn btn-info btn-flat btn-small" title="Update"><i class="fa fa-edit"></i></a> <a href="' + detail_url + '" class="btn btn-info btn-flat btn-small" title="Detail"><i class="fa fa-file-text"></i></a> ';
                }
            }
        ],
        columnDefs: [
           { "orderable": false, "targets": [1,4] }
        ],
        "order": [[ 4, "desc" ]],
        

    });

    $("#data-table_filter label").css("display","none");  // hiding global search box
    $('#data-table_filter').append($('.search-block').html());


    $('.search-input-text').on( 'keyup', function () {   // for text boxes
        var i =$(this).attr('data-column');  // getting column index
        var v =$(this).val();  // getting search input value
        dataTable.columns(i).search(v).draw();
    } );



});

</script>

@endsection
