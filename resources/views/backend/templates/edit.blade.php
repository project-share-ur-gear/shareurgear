@extends('layouts.adminapp')

@section('content')


@php
    $pageUrl='admin/templates/update/'.$template->id; 
    $buttonText='general.labels.update';
@endphp


<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">{{ __('*Required Fields') }}</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' => $pageUrl, 'method' => 'post', 'class' => 'profile_form', 'name' => 'pageForm', 'id' => 'pageForm']) !!}
         
           
          @method('PATCH')
          @csrf
           
            
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('title_en', __('Title')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('title_en', $template->title_en ?? old('title_en'), array('placeholder'=>__('templates.labels.title_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('title_en'))
                  <div class="error-message">{{ $errors->first('title_en') }}</div>
                @endif
              </div>
          </div>
          
           
           <div class="form-group row">
              <div class="col-md-2">{!! Form::label('subject_en', __('Subject')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('subject_en', $template->subject_en ?? old('subject_en'), array('placeholder'=>__('templates.labels.subject_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('subject_en'))
                  <div class="error-message">{{ $errors->first('subject_en') }}</div>
                @endif
              </div>
          </div>
          
            
          <div class="form-group row">
              <div class="col-md-2">{!! Form::label('content_en', __('Body')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::textarea('content_en', $template->content_en ?? old('content_en'), array('placeholder'=>__('templates.labels.content_en'),'required'=>'required','class'=>'form-control required editor' )) !!}
                @if($errors->has('content_en'))
                  <div class="error-message">{{ $errors->first('content_en') }}</div>
                @endif
              
              </div>
          </div>
          
           
          

          <div class="form-group row">
            <div class="col-md-2"></div>
            <div class="col-md-6">
              {!! Form::submit(__('Update'), array('class'=>'btn btn-primary','name'=>'bttnsubmit','id'=>'bttnsubmit')) !!}
              <a href="{{ url('admin/templates/index') }}" class="btn btn-warning">{{ __('Cancel') }}</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

@endsection