@extends('layouts.adminapp')

@section('content')


@php

$pageContent = json_decode($content->page_content);

@endphp

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <div class="clear10"></div>
            <div class="col-md-12" align="right"><span class="error-message">*Required Fields</span></div>
          <div class="clear10"></div>
          {!! Form::open(['url' =>URL('admin/pages/edithomepage'),'method' => 'post', 'class' => 'page_form', 'name' => 'pageForm', 'id' => 'pageForm', 'enctype' => 'multipart/form-data']) !!}
         
           @method('POST')
           @csrf
          
          <h3 class="page_tags">Home Banner Section <hr></h3>    
          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('title',"Title") !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('title', $pageContent->title ?? old('title'), array('placeholder'=>"Title",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('title'))
                  <div class="error-message">{{ $errors->first('title') }}</div>
                @endif
              </div>
          </div>
          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('title_desc',"Title Two") !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('title_desc', $pageContent->title_desc ?? old('title_desc'), array('placeholder'=>"Description",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('title_desc'))
                  <div class="error-message">{{ $errors->first('title_desc') }}</div>
                @endif
              </div>
          </div>

<div class="form-group row">
              <div class="col-md-12">{!! Form::label('banner_image', __('Banner Image')) !!}<span class="error-message">*</span></div>
               <div class="col-md-12">
                  @if(isset($pageContent->banner_image) && $pageContent->banner_image!='' && file_exists(STORAGE_IMG_ROOT.'/app/public/pages_images/'.$pageContent->banner_image))
                    {!! Form::file('banner_image', array('accept'=>"image/*")) !!}
                    @if($errors->has('banner_image'))
                      <div class="error-message">{{ $errors->first('banner_image') }}</div>
                    @endif
                    <br><br>
                    <div style="background-color: #e2e2e2;padding:10px;display:inline-block;">
                      <img src="{{STORAGE_IMG_PATH.'/app/public/pages_images/'.$pageContent->banner_image}}" class="img-fluid rounded img-responsive" width="200">
                    </div>
                  @else
                    {!! Form::file('banner_image', array('required'=>'required','accept'=>"image/*")) !!}
                    @if($errors->has('banner_image'))
                      <div class="error-message">{{ $errors->first('banner_image') }}</div>
                    @endif
                  @endif
                </div>
          </div>
    
          

          <div class="form-group row">
              <div class="col-md-12">{!! Form::label('book_appont_btn_txt', __('Third Title')) !!}<span class="error-message">*</span></div>
              <div class="col-md-12">
                {!! Form::text('book_appont_btn_txt', $pageContent->book_appont_btn_txt ?? old('book_appont_btn_txt'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('book_appont_btn_txt'))
                  <div class="error-message">{{ $errors->first('book_appont_btn_txt') }}</div>
                @endif
              </div>
          </div>

          <div class="form-group">
              
              <label>Feature Title *</label>
                    <textarea name="benfits_heading" class="form-control editor required">{{  $pageContent->benfits_heading }}</textarea>
          </div> 

          </div> 

        <div class="form-group">
              <label>{!! Form::label('question_title', __('Question Title')) !!} *</label>
              {!! Form::text('question_title', $pageContent->question_title ?? old('question_title'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
              @if($errors->has('question_title'))
                  <div class="error-message">{{ $errors->first('question_title') }}</div>
              @endif
               
            
              <div class="form-group">
                    <label>Description *</label>
                    <textarea name="question_desc" class="form-control editor required">{{  $pageContent->question_desc }}</textarea>
              </div>  
         


            <div class="form-group">
            
              <label>Booked Title *</label>
                    <textarea name="booked_title" class="form-control editor required">{{  $pageContent->booked_title }}</textarea>
          </div>
          <div class="form-group">
                <label>How it Work Title *</label>
                    <textarea name="how_it_work" class="form-control editor required">{{  $pageContent->how_it_work }}</textarea>
               
          </div> 

          <div class="form-group">
              <label>{!! Form::label('how_it_work_first_title', __('How it Work First Title')) !!} *</label>
              {!! Form::text('how_it_work_first_title', $pageContent->how_it_work_first_title ?? old('how_it_work_first_title'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
              @if($errors->has('how_it_work_first_title'))
                  <div class="error-message">{{ $errors->first('how_it_work_first_title') }}</div>
              @endif
               
          </div> 
     
          <div class="form-group ">
             {!! Form::label('how_it_work_first_desc', __('How It Work First Description')) !!}<span class="error-message">*</span>
          
                {!! Form::textarea('how_it_work_first_desc', $pageContent->how_it_work_first_desc ?? old('how_it_work_first_desc'), array('placeholder'=>__('pages.labels.how_it_work_first_desc'),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('how_it_work_first_desc'))
                  <div class="error-message">{{ $errors->first('how_it_work_first_desc') }}</div>
                @endif
              
              </div>
      


          <div class="form-group">
              <label>{!! Form::label('how_it_work_second_title', __('How it Work Second Title')) !!} *</label>
              {!! Form::text('how_it_work_second_title', $pageContent->how_it_work_second_title ?? old('how_it_work_second_title'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
              @if($errors->has('how_it_work_second_title'))
                  <div class="error-message">{{ $errors->first('how_it_work_second_title') }}</div>
              @endif
               
          </div> 
     
          <div class="form-group ">
             {!! Form::label('how_it_work_second_desc', __('How It Work Second Description')) !!}<span class="error-message">*</span>
          
                {!! Form::textarea('how_it_work_second_desc', $pageContent->how_it_work_second_desc ?? old('how_it_work_second_desc'), array('placeholder'=>__(''),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('how_it_work_second_desc'))
                  <div class="error-message">{{ $errors->first('how_it_work_second_desc') }}</div>
                @endif
              
              </div>
  


          <div class="form-group">
              <label>{!! Form::label('how_it_work_third_title', __('How it Work Third Title')) !!} *</label>
              {!! Form::text('how_it_work_third_title', $pageContent->how_it_work_third_title ?? old('how_it_work_third_title'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
              @if($errors->has('how_it_work_third_title'))
                  <div class="error-message">{{ $errors->first('how_it_work_third_title') }}</div>
              @endif
               
          </div> 
     
          <div class="form-group ">
             {!! Form::label('how_it_work_third_desc', __('How It Work Third Description')) !!}<span class="error-message">*</span>
          
                {!! Form::textarea('how_it_work_third_desc', $pageContent->how_it_work_third_desc ?? old('how_it_work_third_desc'), array('placeholder'=>__(''),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('how_it_work_third_desc'))
                  <div class="error-message">{{ $errors->first('how_it_work_third_desc') }}</div>
                @endif
              
              </div>


          <div class="form-group">
              <label>{!! Form::label('how_it_work_fourth_title', __('How it Work Fourth Title')) !!} *</label>
              {!! Form::text('how_it_work_fourth_title', $pageContent->how_it_work_fourth_title ?? old('how_it_work_fourth_title'), array('placeholder'=>"Text",'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
              @if($errors->has('how_it_work_fourth_title'))
                  <div class="error-message">{{ $errors->first('how_it_work_fourth_title') }}</div>
              @endif
               
          </div> 
     
          <div class="form-group ">
             {!! Form::label('how_it_work_fourth_desc', __('How It Work Fourth Description')) !!}<span class="error-message">*</span>
          
                {!! Form::textarea('how_it_work_fourth_desc', $pageContent->how_it_work_fourth_desc ?? old('how_it_work_fourth_desc'), array('placeholder'=>__(''),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('how_it_work_fourth_desc'))
                  <div class="error-message">{{ $errors->first('how_it_work_fourth_desc') }}</div>
                @endif
              
              </div>









          </div>  




  






          </div>
    

          <h3>Meta Block <hr></h3>  
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('meta_title_en', __('Meta Title')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('meta_title_en', $pageContent->meta_title_en ?? old('meta_title_en'), array('placeholder'=>__('pages.labels.meta_title_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('meta_title_en'))
                  <div class="error-message">{{ $errors->first('meta_title_en') }}</div>
                @endif
              </div>
          </div>
                    
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('meta_keywords_en', __('Meta Keywords')) !!}<span class="error-message">*</span></div>
              <div class="col-md-6">
                {!! Form::text('meta_keywords_en', $pageContent->meta_keywords_en ?? old('meta_keywords_en'), array('placeholder'=>__('pages.labels.meta_keywords_en'),'required'=>'required', 'class'=>'form-control', 'maxlength'=>'100' )) !!}
                @if($errors->has('meta_keywords_en'))
                  <div class="error-message">{{ $errors->first('meta_keywords_en') }}</div>
                @endif
              </div>
          </div>
          
          <div class="form-group row">
              <div class="col-md-3">{!! Form::label('meta_desc_en', __('Meta Description')) !!}<span class="error-message">*</span></div>
              <div class="col-md-8">
                {!! Form::textarea('meta_desc_en', $pageContent->meta_desc_en ?? old('meta_desc_en'), array('placeholder'=>__('pages.labels.meta_desc_en'),'required'=>'required','class'=>'form-control required' )) !!}
                @if($errors->has('meta_desc_en'))
                  <div class="error-message">{{ $errors->first('meta_desc_en') }}</div>
                @endif
              
              </div>
          </div>








           
          <div class="form-group row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              {!! Form::submit(__("Update"), array('class'=>'btn btn-primary','name'=>'bttnsubmit','id'=>'bttnsubmit')) !!}
              <a href="{{ url('admin/pages/index') }}" class="btn btn-warning">Cancel</a>
            </div>
          </div>
          {!! Form::close() !!}


        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function() {

    $('.page_form').validate({
      
    });

    $('.add_benefit_card').click(function(event) {
      /* Act on the event */
      var job_box = $('.benefits_item').length;
      job_box = job_box+1;
      var htmlstring = '<div class="col-md-12 benefits_item">'+
                          '<div class="ed_bock_item">'+
                            '<div class="form-group">'+
                                '<label>Title *</label>'+
                                '<input type="text" name="benefits['+job_box+'][title]" class="form-control required" value="">'+
                            '</div>'+ 
                            '<div class="form-group">'+
                                '<label>Description *</label>'+
                                '<textarea name="benefits['+job_box+'][desc]" class="form-control editor required"></textarea>'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>Image *</label>'+
                                '<input  type="file" name="benefits_image'+job_box+'" class="form-control required" accept="image/*">'+
                            '</div>'+   
                            '<div class="form-group">'+
                               '<button type="button" class="btn btn-xs btn-danger remove_bene_job">Remove Card</button>'+
                            '</div>'+
                          '</div>'+
                      '</div>';

      $('#benefits_row').append(htmlstring);      
      CKEDITOR.replace('benefits['+job_box+'][desc]');     
    });

    $(document).on('click','.remove_bene_job',function(e){
      $(this).parents('.benefits_item').remove();
    });

    $(document).on('click','.remove_step_btn',function(e){
      $(this).parents('.qd_item').remove();
    });

    $(document).on('click','.remove_testi_btn',function(e){
      $(this).parents('.testi_item').remove();
    });

    $('.add_new_step').click(function(event) {
      /* Act on the event */
      var item = $('.qd_item').length;
      item = item+1;
      var htmlstring = '<div class="col-md-12 qd_item">'+
                          '<div class="ed_bock_item">'+
                            '<div class="form-group">'+
                                '<label>Heading *</label>'+
                                '<input type="text" name="qd_steps['+item+'][heading]" class="form-control required" value="">'+
                            '</div>'+ 
                            '<div class="form-group">'+
                                '<label>Description *</label>'+
                                '<textarea name="qd_steps['+item+'][desc]" class="form-control editor required"></textarea>'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>Normal Image *</label>'+
                                '<input  type="file" name="qd_steps_normal'+item+'" class="form-control required" accept="image/*">'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>GIF Image *</label>'+
                                '<input  type="file" name="qd_steps_gif'+item+'" class="form-control required" accept="image/*">'+
                            '</div>'+   
                            '<div class="form-group">'+
                               '<button type="button" class="btn btn-xs btn-danger remove_step_btn">Remove Card</button>'+
                            '</div>'+
                          '</div>'+
                      '</div>';

      $('#qd_steps_row').append(htmlstring);      
      CKEDITOR.replace('qd_steps['+item+'][desc]');     
    });

    $('.add_new_testi').click(function(event) {
      /* Act on the event */
      var item = $('.testi_item').length;
      item = item+1;
      var htmlstring = '<div class="col-md-12 testi_item">'+
                          '<div class="ed_bock_item">'+
                            '<div class="form-group">'+
                                '<label>Name *</label>'+
                                '<input maxlength="20" type="text" name="testimonial['+item+'][name]" class="form-control required" value="">'+
                            '</div>'+ 
                            '<div class="form-group">'+ 
                                '<label>Location *</label>'+ 
                                '<input maxlength="20" type="text" name="testimonial['+item+'][location]" class="form-control required">'+ 
                            '</div>'+ 
                            '<div class="form-group">'+
                                '<label>User Image *</label>'+
                                '<input  type="file" name="testi_image'+item+'" class="form-control required" accept="image/*">'+
                            '</div>'+  
                            '<div class="form-group">'+
                                '<label>Content *</label>'+
                                '<textarea name="testimonial['+item+'][content]" class="form-control editor required"></textarea>'+
                            '</div>'+  
                            
                            '<div class="form-group">'+
                               '<button type="button" class="btn btn-xs btn-danger remove_testi_btn">Remove Testimonial</button>'+
                            '</div>'+
                          '</div>'+
                      '</div>';

      $('#tetimonial_row').append(htmlstring);      
      CKEDITOR.replace('testimonial['+item+'][content]');     
    });

    

  }); 
</script>

@endsection



