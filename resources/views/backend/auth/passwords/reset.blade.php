<?php prd("Dsafsa"); ?>
@extends('layouts.adminlogin')

@section('content')

<div class="login-box resetbox">
  <!-- /.login-logo -->
  <div class="login-box-body">
   <div class="login-logo">
        <a href="{{SITE_HTTP_URL}}/admin"><img src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/logo/'.$site_configs['site_logo']) }}" alt="Site Logo" title="Site Logo"  class="user-image"><br /><div class="logoheading"><b>Reset Password</b></div></a>
    </div>
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            @if (count($errors)) 

               <div class="alert alert-success">
                    @foreach($errors->all() as $error) 
                        <p>{{ $error }}</p>
                    @endforeach 
                </div>
            @endif                
        </div>
    </div>

    <form class="form-horizontal profile_form" role="form" method="POST" action="{{ url('/admin/password/resetpassword/'.$token) }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <?php /*?><div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<?php */?>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label style="color: #000;" for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label style="color: #000;" for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-2">
                                 <button type="submit" class="btn btn-primary btn-block btn-flat fr">Reset Password</button>
                            </div>
                        </div>
                    </form>
    <br>
    
  </div><!-- /.login-box-body -->
</div>
@endsection
