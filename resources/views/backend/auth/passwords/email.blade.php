
@extends('layouts.adminlogin')

<!-- Main Content -->
@section('content')

<div class="login-box">
   <!-- /.login-logo -->
  <div class="login-box-body">
    <div class="login-logo">
        <a href="{{SITE_HTTP_URL}}/admin"><img src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/logo/'.$site_configs['site_logo']) }}" alt="Site Logo" title="Site Logo"  class="user-image"><br /><div class="logoheading"><b>Forgotten Password?</b></div></a>
      </div>
    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            @if (count($errors)) 

               <div class="alert alert-success">
                    @foreach($errors->all() as $error) 
                        <p>{{ $error }}</p>
                    @endforeach 
                </div>
            @endif                
        </div>
    </div>

    <form role="form" method="POST" action="{{ url('/admin/password/email') }}" class="profile_form">
      {{ csrf_field() }}

      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control " name="email" value="{{ old('email') }}" placeholder="Email" maxlength="255" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <div class="error-message">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
      </div>

      <div class="row">
        <div class="col-xs-8">
          <p></p>	
          <a href="{{ url('admin/login') }}"> << Back to Sign In</a>
        </div><!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat fr">Send</button>
        </div><!-- /.col -->
      </div>
    </form>

  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

@endsection