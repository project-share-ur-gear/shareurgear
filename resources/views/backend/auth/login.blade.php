@extends('layouts.adminlogin')

@section('content')

<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">	
  	<div class="login-logo">
        <a href="{{SITE_HTTP_URL}}/admin"><img src="{{SITE_HTTP_URL}}{{ Storage::url('app/public/logo/'.$site_configs['site_logo']) }}" alt="Site Logo" title="Site Logo"  class="user-image"><br /><div class="logoheading"><b>Get Logged In</b></div></a>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            @if (count($errors)) 

               <div class="alert alert-success">
                    @foreach($errors->all() as $error) 
                        <p>{{ $error }}</p>
                    @endforeach 
                </div>
            @endif                
        </div>
    </div>

    <form role="form" method="POST" action="{{ url('admin/authenticate') }}" class="profile_form">
      {{ csrf_field() }}

      <div class="form-group has-feedback">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" maxlength="255" required autofocus>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <div class="error-message">
                <strong>{{ $errors->first('email') }}</strong>
            </div>
        @endif
      </div>

      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control" name="password" placeholder="Password" maxlength="32" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="error-message">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>

      <div class="row">
        <div class="col-xs-8">    
          <?php /*?><div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember_me" id="remember_me"> Remember Me
            </label>
          </div>      <?php */?> 
          <p></p>
          <a href="{{ url('admin/password/reset') }}">Forgot Your Password?</a>                 
        </div><!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat fr">Sign In</button>
        </div><!-- /.col -->
      </div>
    </form>

    
    <br>
    
  </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<script>
  $('document').ready(function(){
	 $('#password').rules('remove','minlength maxlength');
    $('#UserRememberMe').on('ifChecked', function(event){
        $.cookie('UserEmail', $('#UserEmail').val(), { expires: 7, path: APPLICATION_URL });
        $.cookie('UserPassword', $('#UserPassword').val(), { expires: 7, path: APPLICATION_URL });
    });

    $('#UserRememberMe').on('ifUnchecked', function(event){
        $.removeCookie('UserEmail');
        $.removeCookie('UserPassword');
    });

    if(typeof $.cookie('UserEmail') === "undefined"){
      //nothing to do
    } else {
      //set cookies value to textbox
      $('#UserEmail').val($.cookie('UserEmail'));
      $('#UserPassword').val($.cookie('UserPassword'));
      $('#UserRememberMe').iCheck('check');
    } 
  });
</script>










<div style="display: none;">

    @if ( !Auth::guard('admin')->check() ) 
        <li><a href="{{ url('admin/login') }}">Admin Login</a></li>
        <li><a href="{{ url('admin/register') }}">Admin Register</a></li>
    @else
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
            </a>

            <ul class="dropdown-menu" role="menu">
                <li>
                    <a href="{{ url('admin/logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ url('admin/logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </li>
    @endif

</div>

<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>





@endsection
