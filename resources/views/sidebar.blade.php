<?php $user =  auth()->guard('user')->user(); ?>
<style>
.dropdown-item.active, .dropdown-item:active { background-color: #8a71b2; }
</style>
<div class="menu-navbar d-xl-block d-none">
    <div class="container">
        <div class="nav-menu">
            <ul class="nav-data">
                <li class="nav-active {{ Request::path() === 'account-setting' ? 'active':'' }}"><a href="{{ SITE_HTTP_URL.'/account-setting' }}" class="nav-links">Account Settings </a></li>
                <li class="nav-active {{ Request::path() === 'payout-setting' ? 'active':'' }}"><a href="{{ SITE_HTTP_URL.'/payout-setting' }}" class="nav-links">Payout Settings </a></li>
                <li class="nav-active {{ Request::path() === 'manage-product' ? 'active':'' }}"><a href="{{ SITE_HTTP_URL.'/manage-product' }}" class="nav-links">Manage Products   </a></li>
                <li class="nav-active {{ Request::path() === 'wish-list' ? 'active':'' }}"><a href="{{ SITE_HTTP_URL.'/wish-list' }}" class="nav-links">Wish List  </a></li>
                <li class="nav-active {{ Request::path() === 'manage-Q&a' ? 'active':'' }}"><a href="{{ SITE_HTTP_URL.'/manage-Q&a' }}" class="nav-links">Manage Q&A </a></li>
                <li class="nav-active {{ Request::path() === 'messaging' ? 'active':'' }}"><a href="{{ SITE_HTTP_URL.'/messaging' }}" class="nav-links">Messaging  <span class="msg_count"> </span></a></li>
                <li class="nav-active {{ (Request::path() == 'my-bookings' || Request::path() == 'sharer-bookings') ? 'active':'' }}  dropdown">
                    <a href="Javascript:void(0);" class="nav-links dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown">Bookings</a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a  class="dropdown-item {{ Request::path() === 'my-bookings' ? 'active':'' }}" href="{{ route('booking.mybookings') }}">My Bookings</a>
                        <a class="dropdown-item {{ Request::path() === 'sharer-bookings' ? 'active':'' }}" href="{{ route('booking.sharerbookings') }}">Sharer Bookings</a>
                    </div>
                </li>
            </ul>
            <a href="{{ SITE_HTTP_URL.'/add-product/add' }}" class="add-product btn ">Add Product</a>
        </div>
    </div>
</div>

<script>

$(document).ready(function(){
    $.fn.getmessageCounts();
});

$.fn.getmessageCounts = function(){  
    $.ajax({
            url:APPLICATION_URL+'/getmessagetotalcount',
            method: 'get',
            async: false,
            success:function(response) {
                response=JSON.parse(response);
                if(response.success)
                {
                    if(response.count!='0'){
                        if($(".msg_count" ).find('span').hasClass( "counts" )){
                            $('.counts').text(response.count);
                        }
                        else{
                            $(".msg_count").html('<span class="counts" style="padding: 6px;">'+response.count+'</span>');
                        }
                    }
                    else{
                        $(".msg_count").html('');
                    }
                }else{
                    $(".msg_count").html(' ');
                }
                
            },
        
    }); 
}

var userId ='{{$user->id}}';
if(userId!=''){
    setInterval(function(){
        $.fn.getmessageCounts();
    }, 10000);
}
</script>