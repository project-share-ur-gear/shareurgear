<?php

$lang_module_name = 'Withdrawal request';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title'  => $lang_module_name.'s',
        'view_page_title'       => 'View '.$lang_module_name,
    ],
    'labels' => [
        'datetime'      => 'Date',
        'client_name'   => 'User name',
        'amount'        => 'Amount',
        'status'        => 'Status',
    ],
    'messages' => [
        
    ]
];
