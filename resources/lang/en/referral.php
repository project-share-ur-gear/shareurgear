<?php

$lang_module_name = 'Referrals';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title'      => $lang_module_name,
        'view_page_title'           => 'View '.$lang_module_name,
        'invite_friends'            => 'Invite your friends',
        'share_referral_link_title' => 'Share the referral link'
    ],
    'labels' => [
        'pagetitle'     => 'Referral and Earnings',
        'howitworks'    => 'How it works',
        'invite_btn'    => 'Send Invitation',
        'email_placeholder' => 'Email Addresses...',
        'copy_link_btn' => 'Copy link',
        'name'      => 'Name',
        'email'     => 'Email',
        'date_registered' => 'Date Registered',
        'earning' => 'Earning'
    ],
    'messages' => [
        'howitworks_point1' => 'Invite your friends who do not yet know ProxiVie. Invite people who do not yet have a ProxiVie account to register using your link.',
        'howitworks_point2' => 'They get a discount. Your friends who register on ProxiVie from your link receive a coupon of $10 reduction on their first service.',
        'howitworks_point3' => 'Tell your friend not to forget to book. After registering, they can use the coupon to reserve a service.',
        'howitworks_point4' => 'When the coupon is used by your friend, you receive $10 in your wallet.',
        'referral_link_txt' => 'Send your referral link to friends and tell them how cool ProxiVie is!',
        'referral_discount_text' => 'You and your friends get 10$ off on the first service he takes.',
        'invite_friends_text' => 'Insert your friends’ addresses and send them invitations to join ProxiVie!',
        'share_referral_link_txt' => 'you can also share your referral link by copying and sending it or sharing it on your social media.',
        'reference_joined_link' => 'Users has joined from your reference',
        'reference_earning_txt' => 'Earning from your reference',
        'num_click_txt' => 'Number of clicks',
        'num_conversions' => 'Number of conversions',
    ]
];
