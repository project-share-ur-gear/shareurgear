<?php

$lang_module_name = 'Page';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title' => $lang_module_name.'s',
        'add_new_page_title' => 'Add New '.$lang_module_name,
        'update_page_title' => 'Update '.$lang_module_name,
    ],
    'labels' => [
        'title_en' => 'Title(English)',
		'title_fr' => 'Title(French)',
        'meta_title_en' => 'Meta Title(English)',
		'meta_title_fr' => 'Meta Title(French)',
		'meta_keywords_en' => 'Meta Keywords(English)',
		'meta_keywords_fr' => 'Meta Keywords(French)',
        'meta_desc_en' => 'Meta Description(English)',
		'meta_desc_fr' => 'Meta Description(French)',
    ],
    'messages' => [
        'added_success' => $lang_module_name.' added successfully.',
        'updated_success' => $lang_module_name.' updated successfully.',
        'deleted_success' => $lang_module_name.'(s) deleted successfully.',
        'delete_confirmation' => 'Are you sure? You want to delete selected '.$lang_module_name.'(s).',
        'select_atleast_one' => 'Please select at least one '.$lang_module_name.' to perform this action.',
		'file_not_available' => 'File not available.',
    ]

];
