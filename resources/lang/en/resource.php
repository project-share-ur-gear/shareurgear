<?php

$lang_module_name = 'Resource';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title' => $lang_module_name.'s',
        'add_new_page_title' => 'Add New '.$lang_module_name,
        'add_new_page_title_subjects' => 'Add New Subject',
		'add_new_page_title_city' => 'Add New City',
        'update_page_title' => 'Update '.$lang_module_name,
		'update_new_page_title_city' => 'Update New City',
    ],
    'labels' => [
		'name_en' => 'Title(English)',
		'name_fr' => 'Title(French)',
		'title_en' => 'Title(English)',
		'title_fr' => 'Title(French)',
		'desc_en' => 'Content(English)',
		'desc_fr' => 'Content(French)',
		'status' => 'Status',
		'link' => 'Link',
		'image' => 'Upload Image',
    ],
    'messages' => [
        'added_success' => $lang_module_name.' added successfully.',
		'added_success_city' => 'Provider city added successfully.',
        'updated_success' => $lang_module_name.' updated successfully.',
		'updated_success_city' => 'Provider city updated successfully.',
		'updated_success_keyword' => 'Keyword updated successfully.',
        'deleted_success' => $lang_module_name.'(s) deleted successfully.',
		'deleted_success_city' => 'Provider city(s) deleted successfully.',
        'delete_confirmation' => 'Are you sure? You want to delete selected '.$lang_module_name.'(s).',
		'delete_confirmation_city' => 'Are you sure? You want to delete selected city(s).',
        'select_atleast_one' => 'Please select at least one '.$lang_module_name.' to perform this action.',
		'select_atleast_one_city' => 'Please select at least one city to perform this action.',
    ]

];
