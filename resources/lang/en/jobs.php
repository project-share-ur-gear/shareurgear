<?php

$lang_module_name = 'Jobs';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title'  => $lang_module_name.'s',
        'view_page_title'       => 'View '.$lang_module_name,
    ],
    'labels' => [
        'title_en'          => 'Job title',
        'client_title'      => 'Client name',
        'provider_title'    => 'Provider name',
        'job_amount'        => 'Amount',
        'booking_status'    => 'Status',
        'job_datetime'      => 'Booking Date & Time'
    ],
    'messages' => [

    ]
];
