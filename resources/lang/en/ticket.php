<?php

$lang_module_name = 'Ticket';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title'  => $lang_module_name.'s',
        'view_page_title'       => 'View '.$lang_module_name,
    ],
    'labels' => [
        'title_en'          => 'Ticket title',
        'ticket_no'         => 'Ticket number',
        'ticket_datetime'   => 'Date',
        'client_name'       => 'Name',
        'provider_title'    => 'Provider name',
        'job_amount'        => 'Amount',
        'ticket_status'     => 'Status',
        'job_datetime'      => 'Date'
    ],
    'messages' => [

    ]
];
