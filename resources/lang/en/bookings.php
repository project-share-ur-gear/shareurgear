<?php

$lang_module_name = 'Bookings';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title' => $lang_module_name.'s',
        'view_page_title' => 'View '.$lang_module_name,
    ],
    'labels' => [
        'title_en'          => 'Booking title',
        'client_title'      => 'Client name',
        'provider_title'    => 'Provider name',
        'booking_amount'    => 'Booking Amount',
        'booking_datetime'  => 'Booking Date time',
        'booking_status'    => 'Booking status',
    ],
    'messages' => [

    ]
];
