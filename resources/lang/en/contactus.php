<?php

$lang_module_name = 'Contact Us';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'page_title' => 'Contact Us'
    ],
    'labels' => [
        'name' => 'Name',
        'email' => 'Email',
        'type' => 'Enquiry Type',
        'message' => 'Message'
    ],
    'messages' => [
        'posted_success' => 'Your enquiry has been sent successfully. We will contact you soon. Thank you.',
    ]

];
