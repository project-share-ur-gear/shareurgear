<?php

$lang_module_name = 'Service Sub Category';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title' => $lang_module_name.'s',
        'add_new_page_title' => 'Add New '.$lang_module_name,
        'update_page_title' => 'Update '.$lang_module_name,
    ],
    'labels' => [
        'category' => 'Category',
		'name_en' => 'Name(English)',
		'name_fr' => 'Name(French)',
		'description_en' => 'Content(English)',
		'description_fr' => 'Content(French)',
		'content_en' => 'About Service(English)',
		'content_fr' => 'About Service(French)',
		'image' => 'Upload Image (1260px x 950px)',
		'status' => 'Status',
    ],
    'messages' => [
        'added_success' => $lang_module_name.' added successfully.',
        'updated_success' => $lang_module_name.' updated successfully.',
        'deleted_success' => $lang_module_name.'(s) deleted successfully.',
        'delete_confirmation' => 'Are you sure? You want to delete selected '.$lang_module_name.'(s).',
        'select_atleast_one' => 'Please select at least one '.$lang_module_name.' to perform this action.',
    ]

];
