<?php

$lang_module_name = 'Admin';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'update_page_title' => 'Update Profile',
        'changepassword_page_title' => 'Change Password',
    ],
    'labels' => [
        'name' => 'Name',
        'email' => 'Email',
        'upload_photo' => 'Upload Photo',
        'photo' => 'Photo',

        'lastloggedinon' => 'Last Logged In On',
        'change_password' => 'Change Password',
        'current_password' => 'Current Password',
        'new_password' => 'New Password',
        'new_confirm_password' => 'New Confirm Password',
    ],
    'messages' => [
        'updated_success' => 'Your profile information has been updated successfully.',
        'password_changed_success' => 'Your account passwod has been changed successfully.',
        'current_password_not_match_error' => 'Current password does not match, please enter correct password.',
		'password_not_match_error' => 'Password does not match, please enter same password.',
		'invalid_email' => 'Email address is not valid.',
		
    ]

];
