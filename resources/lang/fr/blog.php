<?php
$lang_module_name = 'Blog';

return [

    /*
    |--------------------------------------------------------------------------
    | Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the particular module 
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'titles' => [
        'list_data_page_title' => $lang_module_name.'s',
        'add_new_page_title' => 'Ajouter un nouveau '.$lang_module_name,
		'add_new_page_title_category' => 'Ajouter un nouveau '.$lang_module_name.' Category',
        'update_page_title' => 'Mettre à jour le '.$lang_module_name,
    ],
    'labels' => [
		'category' => 'Catégorie',
		'name_en' => 'Titre (anglais)',
		'name_fr' => 'Titre (français)',
        'title_en' => 'Title(English)',
		'title_fr' => 'Title(French)',
		'image' => 'Image(825px x 525px)',
		'description_en' => 'Description(English)',
		'description_fr' => 'Description(French)',
		'tags_en' => 'Tags(English)',
		'tags_fr' => 'Tags((French)',
		'status' => 'Status',
    ],
    'messages' => [
        'added_success' => $lang_module_name.' added successfully.',
		'added_success_category' => $lang_module_name.' category added successfully.',
        'updated_success' => $lang_module_name.' updated successfully.',
		'updated_success_category' => $lang_module_name.' category updated successfully.',
        'deleted_success' => $lang_module_name.'(s) deleted successfully.',
	 	'deleted_success_category' => $lang_module_name.'(s) category deleted successfully.',
        'delete_confirmation' => 'Are you sure? You want to delete selected '.$lang_module_name.'(s).',
		'delete_confirmation_category' => 'Are you sure? You want to delete selected '.$lang_module_name.'(s). categories',
        'select_atleast_one' => 'Please select at least one '.$lang_module_name.' to perform this action.',
		'select_atleast_one_category' => 'Please select at least one '.$lang_module_name.' category to perform this action.',
    ]

];
