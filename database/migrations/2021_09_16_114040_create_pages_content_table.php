<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesContentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages_content', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('page_id')->index('page_id');
			$table->string('page_key', 250);
			$table->string('page_type', 250);
			$table->string('page_title', 250);
			$table->text('page_content');
			$table->enum('page_editor', array('0','1'))->nullable()->default('0')->comment('0=No,1=Yes');
			$table->enum('page_video', array('0','1'))->default('0')->comment('0=No,1=Yes');
			$table->timestamps(10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages_content');
	}

}
