<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable();
			$table->string('first_name', 100);
			$table->integer('last_name');
			$table->string('email');
			$table->string('skype', 50)->nullable();
			$table->string('contact_no', 50)->nullable();
			$table->string('full_address')->nullable();
			$table->string('profile_image', 100)->nullable();
			$table->timestamps(10);
			$table->smallInteger('status')->default(1)->comment('1 for Enabled, 2 for Disabled');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
