<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table)
		{
			$table->integer('product_id', true);
			$table->string('product_title');
			$table->integer('product_category');
			$table->integer('sub_category');
			$table->text('tags');
			$table->float('price_per_day', 10, 0);
			$table->string('location', 400);
			$table->float('p_latitude', 10, 0);
			$table->float('p_longitude', 10, 0);
			$table->string('city')->nullable();
			$table->string('state')->nullable();
			$table->string('postal')->nullable();
			$table->string('country')->nullable();
			$table->text('product_images')->nullable();
			$table->string('primary_image')->nullable();
			$table->text('product_desp');
			$table->integer('product_added_by');
			$table->enum('product_request_status', array('0','1','2'))->default('0')->comment('0=>pending,1=>admin accpet, 2=>admin decline');
			$table->timestamp('product_added_on')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->enum('product_admin_status', array('0','1'))->default('1');
			$table->enum('product_delete_status', array('0','1'))->default('1');
			$table->enum('product_f_gear', array('1','2'))->default('1');
			$table->text('product_cancle_desc');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
