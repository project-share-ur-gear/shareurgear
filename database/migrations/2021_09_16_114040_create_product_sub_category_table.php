<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSubCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_sub_category', function(Blueprint $table)
		{
			$table->integer('sub_category_id', true);
			$table->string('subcategorytitle');
			$table->integer('product_category_id_for_sub');
			$table->enum('subcategorystatus', array('0','1'))->default('1');
			$table->timestamp('sub_category_added_date')->default(DB::raw('CURRENT_TIMESTAMP'));
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('product_sub_category');
	}

}
