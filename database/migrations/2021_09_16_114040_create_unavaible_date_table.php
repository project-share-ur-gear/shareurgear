<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnavaibleDateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('unavaible_date', function(Blueprint $table)
		{
			$table->integer('u_id', true);
			$table->string('u_title');
			$table->date('u_sdate');
			$table->date('u_edate');
			$table->integer('u_pid');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('unavaible_date');
	}

}
