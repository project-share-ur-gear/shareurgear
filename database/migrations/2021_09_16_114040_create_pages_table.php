<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('page_key', 100)->unique('page_key');
			$table->string('title_en', 100)->unique('pages_title_unique');
			$table->string('meta_title_en', 250);
			$table->text('meta_keywords_en');
			$table->text('meta_desc_en');
			$table->timestamps(10);
			$table->enum('status', array('1','2'))->default('1')->comment('1 for Enabled, 2 for Disabled');
			$table->enum('page_type', array('static','designed','dynamic'))->default('static');
			$table->text('page_content')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pages');
	}

}
