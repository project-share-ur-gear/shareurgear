<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 191);
			$table->string('email', 191)->unique();
			$table->string('password', 191);
			$table->char('usertype', 1)->default('W');
			$table->string('profile_image', 100)->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->string('reset_key', 250)->nullable();
			$table->enum('email_verified', array('0','1'))->default('0')->comment('0=No,1=Yes');
			$table->dateTime('last_logged_in_on')->nullable();
			$table->timestamps(10);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('admins');
	}

}
