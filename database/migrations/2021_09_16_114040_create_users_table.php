<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->enum('type', array('client'))->nullable()->comment('\'client\',\'provider\'');
			$table->string('name')->nullable();
			$table->string('email', 191);
			$table->string('email_updated');
			$table->string('password')->nullable();
			$table->string('profile_image', 100)->nullable();
			$table->string('address', 250)->nullable();
			$table->string('latitude', 100)->nullable();
			$table->string('longitude', 100)->nullable();
			$table->string('phone_number', 250)->nullable();
			$table->string('reset_key', 250)->nullable();
			$table->enum('status', array('1','2'))->default('1')->comment('1 for Enabled, 2 for Disabled');
			$table->boolean('verified')->default(0);
			$table->enum('email_verified', array('0','1'))->default('0')->comment('0=Pending,1=Verified');
			$table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->enum('deleted_status', array('0','1'))->default('0')->comment('0=No,1=Yes');
			$table->dateTime('deleted_on')->nullable();
			$table->dateTime('last_logged_in_on')->default('0000-00-00 00:00:00');
			$table->text('client_account_id');
			$table->enum('stripe_account', array('0','1'))->default('0');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
