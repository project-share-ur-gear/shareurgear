<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

use DB;
use App;

class MailSenders extends Mailable
{
    use Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data = array())
    {
        // MSR
        $this->data = (array) $data;

        $siteConfigData = DB::table('config')->get();
        foreach($siteConfigData as $key=>$configs){
            $config_data[$configs->config_key] = $configs->value;
            /*$config_groups[$configs->config_group][$configs->config_key] = $configs->value;*/
        }

        $config_data['site_copyright_text']= str_replace('[year]',date('Y'),$config_data['site_copyright_text']);
       
        if(isset($data['user_id'])){
            $getUserSettings = getUserSettingsViaEmail($data['user_id']);
        }

        $dontSendMail = $xSendMsg = 0;

		$locale=App::getLocale();

        $locale = 'en';
        $mailType = $data['mail_template'];
		$contentBox='content_'.$locale;
		$subject='subject_'.$locale;
        $recipient_info = [
            "sender"    => [
                "name"      =>  $config_data['site_title_'.$locale],
                "email"     =>  $config_data['site_email']
            ],
            "receiver"  => [
                "name"      =>  "",
                "email"     =>  ""
            ],
            "subject"   => "",
            "msr"   => [
                "{site_name}"   => $config_data['site_name_'.$locale],
                "{logo_url}"    => STORAGE_IMG_PATH.'/app/public/logo/'.$config_data['site_logo'],
                "{site_logo}"   => '<img alt="'.$config_data['site_title_'.$locale].'" src="'.STORAGE_IMG_PATH.'/app/public/logo/'.$config_data['site_logo'].'" style="border:none;max-width:150px;" />',
                "{site_link}"   => SITE_HTTP_URL,
                "{site_title}"  => $config_data['site_title_'.$locale],
                "{date}"        => date("l d F"),
                "{year}"        => date("Y"),
                "{link_about}"  => SITE_HTTP_URL."/about",
                "{link_contact}"=> SITE_HTTP_URL."/contact",
                "{link_login}"  => SITE_HTTP_URL."/login",
                "{copyright}"  => $config_data['site_copyright_text'],
                "{contact_mail_address}"  => $config_data['site_contact_email']
            ],
            "message" => ""
        ];
        switch ($mailType) {

           
            case 'contact_us_admin':
      
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();
                $recipient_info["receiver"]['name']         = $config_data['site_title_'.$locale];
                $recipient_info["receiver"]['email']        = trim($config_data['site_email']);
                
                $recipient_info["msr"]["{guest_name}"]      = htmlentities($data['guest_name'], ENT_COMPAT, 'UTF-8');
                $recipient_info["msr"]["{guest_email}"]     = $data['guest_email'];
				$recipient_info["msr"]["{guest_message}"]   = $data['guest_message'];
                $recipient_info["msr"]["{phone_number}"]   = $data['phone_number'];
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);

                $recipient_info['sender']['email'] = $data['site_email'];
                break;

            case 'registration_email':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();
                
                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                
                $recipient_info["msr"]["{user_name}"]       = htmlentities($data['user_name'], ENT_COMPAT, 'UTF-8');
                $recipient_info["msr"]["{reset_link}"]      = $data['link'];

                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);
                break;

            case 'admin_registration_email':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();

                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                
                $recipient_info["msr"]["{user_name}"]      = $data['user_name'];
                $recipient_info["msr"]["{user_email}"]     = $data['user_email'];
				 $recipient_info["msr"]["{user_password}"]     = $data['user_password'];
                $recipient_info["msr"]["{reset_link}"]     = $data['link'];
                
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);

                break;

            case 'reset_password':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();

                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                $recipient_info["msr"]["{user_name}"]       = $data['user_name'];
                $recipient_info["msr"]["{reset_link}"]      = $data['link'];
                $finf_geo_details = getBrowser();
                $os=getOS();
                $browser = $finf_geo_details['name'] . ' (Version: ' . $finf_geo_details['version'] . ')';
                $recipient_info["msr"]["{user_ip}"]     = $_SERVER['REMOTE_ADDR'];
                $recipient_info["msr"]["{browser}"]     =  $browser;
                $recipient_info["msr"]["{user_os}"]     = $os;
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);
                break;

            case 'change_password_email':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();

                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                
                $recipient_info["msr"]["{user_name}"]       = $data['user_name'];
                $recipient_info["msr"]["{user_email}"]      = $data['user_email'];
                $recipient_info["msr"]["{reset_link}"]      = $data['link'];

                $finf_geo_details = getBrowser();
                $os=getOS();
                $browser = $finf_geo_details['name'] . '(Version: ' . $finf_geo_details['version'] . ')';
                
                $recipient_info["msr"]["{user_ip}"]     = $_SERVER['REMOTE_ADDR'];
                $recipient_info["msr"]["{user_device}"] = $finf_geo_details['device'];
                $recipient_info["msr"]["{browser}"]     =  $browser;
                $recipient_info["msr"]["{user_os}"]     = $os;
                
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);
                break;
				
                case 'old_email_changed':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();

                $finf_geo_details = getBrowser();
                $os=getOS();
                $browser = $finf_geo_details['name'] . ' (Version: ' . $finf_geo_details['version'] . ')';
                $ip=$data['ip'];
             
                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                $recipient_info["msr"]["{user_name}"]       = $data['user_name'];
                $recipient_info["msr"]["{os}"]              =  $os;  
                $recipient_info["msr"]["{ip}"]              = $ip;  
                $recipient_info["msr"]["{user_device}"] = $finf_geo_details['device'];
                $recipient_info["msr"]["{browser}"]         =  $browser;
                $recipient_info["msr"]["{changed_email}"]   = $data['changed_email'];  
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);
            
            break;
            case 'new_email_change_mail':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();

                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                $recipient_info["msr"]["{user_name}"]       = $data['user_name'];
                $recipient_info["msr"]["{verify_link}"]     = $data['verify_link']; 
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);
            
            break;
            case 'common_notification':
                $getTemplate = DB::table('templates')->where('temp_key',$data['mail_template'])->get()->first();
                $recipient_info["receiver"]['name']         = $data['user_name'];
                $recipient_info["receiver"]['email']        = trim($data['user_email']);
                $recipient_info["msr"]["{user_name}"]       = htmlentities($data['user_name'], ENT_COMPAT, 'UTF-8');
                $recipient_info["msr"]["{message}"]         =$data['message'];
               
                if(isset($data['subject'])){
                    $recipient_info["subject"] = $data['subject'];
                }
                $recipient_info["message"]  = str_ireplace(array_keys($recipient_info["msr"]),array_values($recipient_info["msr"]),$getTemplate->$contentBox);
            break;


        }
		

        if(empty($dontSendMail)){
           
    		if(isset($data['subject'])){
                return $this->to($recipient_info["receiver"]['email'])
                ->from(env('MAIL_USERNAME'),$recipient_info['sender']['name'])
                ->html($recipient_info['message'])
                ->subject($recipient_info["subject"])
                ->with($this->data);
            }
            else{
                return $this->to($recipient_info["receiver"]['email'])
                ->from(env('MAIL_USERNAME'),$recipient_info['sender']['name'])
                ->html($recipient_info['message'])
                ->subject($getTemplate->$subject)
                ->with($this->data);
            }
        }
        else{
            return $this->to('techdemo422@gmail.com')
                ->from(env('MAIL_USERNAME'),$recipient_info['sender']['name'])
                ->html($recipient_info['message'])
                ->subject($getTemplate->$subject)
                ->with($this->data);
        }
    }

    public function build()
    {
    }

    private function sendMessage($message, $recipient){
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);
        try{
            $client->messages->create($recipient, 
                ['from' => $twilio_number, 'body' => $message] );
        }
        catch(Exception $e){
            //prd($e->getMessage());
        }
        
        return true;
    }

}