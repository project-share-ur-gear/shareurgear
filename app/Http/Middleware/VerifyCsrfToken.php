<?php



namespace App\Http\Middleware;



use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;



class VerifyCsrfToken extends Middleware

{

    /**

     * Indicates whether the XSRF-TOKEN cookie should be set on the response.

     *

     * @var bool

     */

    protected $addHttpCookie = true;



    /**

     * The URIs that should be excluded from CSRF verification.

     *

     * @var array

     */

    protected $except = [

        'contact-us/store',
		'/post-contact',
		'/service-payment/1/success',
        '/service-payment/2/notify',
        '/service-payment/3/cancel',
        
        '/accept-job-paypalpayment/1/success',
        '/accept-job-paypalpayment/2/notify',
        '/accept-job-paypalpayment/3/cancel',

        '/book-accept-paypalpayment/1/success',
        '/book-accept-paypalpayment/2/notify',
        '/book-accept-paypalpayment/3/cancel',
        
        '/sendtip-payment-paypal/1/success',
        '/sendtip-payment-paypal/2/notify',
        '/sendtip-payment-paypal/3/cancel',

        '/paypal-withdrawal/1/success',
        '/paypal-withdrawal/2/notify',
        '/paypal-withdrawal/3/cancel',
        
        '/sendeventnotification'
    ];
}

