<?php



namespace App\Http\Middleware;



use Closure;



use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;



class CheckUser

{

    public function handle($request, Closure $next)

    {
            /* 
                    if(!empty(auth()->guard('user')->id()))

                    {

                        $data = DB::table('users')

                                ->select('users.id')

                                ->where(['users.id',auth()->guard('user')->id()],['status',"1"])

                                ->get();

                        

                        if (!$data[0]->id  )

                        {

                            return redirect()->intended('login/')->with('status', 'You do not have access to user side');

                        }

                        return $next($request);

                    }

                    else 

                    {

                        return redirect()->intended('login/')->with('status', 'Please Login to access user area');

                    }
            */
            if(!empty(auth()->guard('user')->id()))
            {   
                $data = DB::table('users')

                        ->select('users.id')

                        ->where([['users.id',auth()->guard('user')->id()],['status',"1"]])

                        ->get()->first();

                
                if (empty($data->id))

                {
                    auth()->guard('user')->logout();
                    return redirect()->intended('login/')->with('status', 'You do not have access to user side.');

                }

                return $next($request);

            }

            else 

            {
                return redirect()->intended('login/')->with('status', 'Please Login to access user area');
            }

    }

}

