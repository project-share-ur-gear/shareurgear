<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\DB;

class CommonFunctionality
{

    // public $attributes;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // echo 'i am calling from middleware';
        $data = DB::table('pages')
                    ->select('pages.id','pages.title_en')
                    ->where('pages.status', '1')
                    ->get();

        \Illuminate\Support\Facades\View::share('cms_pages', $data);

        // define('CMS_PAGES', $data);

        // $request->attributes->add(['cmspages' => $data]);

        return $next($request);
    }
}
