<?php

namespace Illuminate\Foundation\Auth;
namespace App\Http\Controllers\backend\Auth;

use App\Model\backend\Admin;
use Illuminate\Http\Request;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
Use DB;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
    * Display the form to request a password reset link.
    *
    * @return \Illuminate\Http\Response
    */
    public function showLinkRequestForm()
    {
        /* logged in user can not see this page */
        $login_user_id = auth()->guard('admin')->id();
        if(!empty($login_user_id)) {
            return redirect()->intended('admin/dashboard');
        }
        return view('backend/auth/passwords/email',array('pageHeading'=>'Forgot Password'));
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
    
        $this->validateEmail($request);
		
        $email=$request->only('email');

        $url=SITE_HTTP_URL_API.'sendResetLinkEmail';

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PATCH',
          CURLOPT_POSTFIELDS =>json_encode($email['email']),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: '.CURL_KEY.''
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $userData=json_decode($response);
        if(empty($userData)){
            return  redirect(route('static.errorpage'));
        }
        if(count($userData)==0){
            return redirect('/admin')->with('status', 'Email does not exist');
		}
		$reset_key=md5(generateRandomString(10));
		$link=SITE_HTTP_URL.'/admin/password/reset/'.$reset_key;
		$update = DB::table('admins')->where('email','=',$email['email'])->update(array('reset_key'=>$reset_key));
		$mailData = [
			'user_email'    => $email['email'],
			'user_name'    => $userData[0]->name,
			'link' => $link,
			'mail_template' => 'reset_password'
		];
		$sendMail = Mail::send(new MailSenders($mailData));
	 	 return redirect()->intended('admin/login')->with('status','Mail has been sent to your account to restore your password.');
		

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
    }

    /**
     * Get the response for a successful password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkResponse(Request $request, $response)
    {
        return redirect('admin/login')->with('status', trans($response));
    }

    /**
     * Get the response for a failed password reset link.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => trans($response)]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }
}
