<?php

namespace Illuminate\Foundation\Auth;
namespace App\Http\Controllers\backend\Auth;

use App\Model\backend\Admin;
use Illuminate\Http\Request;

use Validator;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\ResetsPasswords;

use DB;
use Hash;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

   // use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
	
	public function showResetForm(Request $request, $token = null)
    {
		$userData = DB::table('admins')->select('*')->where('reset_key','=',$token)->get();
		if(count($userData)==0){
			 return redirect()->intended('admin/login')->with('status','Invalid request');
		}
		
        if (is_null($token)) {
            return $this->getEmail();
        }

        $email = $request->input('email');

		$pageHeading='Reset Password';
        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email','pageHeading'));
        }

        if (view()->exists('backend.auth.passwords.reset')) {
            return view('backend.auth.passwords.reset')->with(compact('token', 'email','pageHeading'));
        }
		
        return view('backend.auth.reset')->with(compact('token', 'email','pageHeading'));
    }
	
	protected function resetpassword(Request $request,$token){
		$userData = DB::table('admins')->select('*')->where('reset_key','=',$token)->get();
		if(count($userData)==0){
			 return redirect()->intended('admin/login')->with('status','Invalid request');
		}
		
		$messages = array(
            'password.required' => 'Please enter password',
			'password_confirmation.required' => 'Please enter password',
        );
        $rules = array(
            'password' => 'required|min:8|confirmed',
        );
               
        $input = $request->all();
		$password=Hash::make($input['password']);
		$update = DB::table('admins')->where('reset_key','=',$token)->update(array('password'=>$password,'reset_key'=>NULL));
		return redirect()->intended('admin/login')->with('status','You have successfully changed your password.');
        
    }
}
