<?php

namespace App\Http\Controllers\backend\Auth;
use App\Model\backend\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Session;

use DB;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function getLoginForm()
    {
		
		/* Set admin session in "AdminData" variable after login */
					 
        /* logged in user can not see this page */
        $login_user_id = auth()->guard('admin')->id();
        if(!empty($login_user_id)) {
            return redirect()->intended('admin/dashboard');
        }
        return view('backend/auth/login',array('pageHeading'=>'Login'));
    }	
    
    public function authenticate(Request $request)
    {
        
        $email = $request->input('email');
        $password = $request->input('password');
         
        if (auth()->guard('admin')->attempt(['email' => $email, 'password' => $password ])) 
        {
            /* Set admin session in "AdminData" variable after login */
			$mm=DB::table('admins')->where('id', '=', auth()->guard('admin')->id())->update(array('last_logged_in_on'=>date('Y-m-d H:i:s')));
					 
            $users = DB::table('admins')
                     ->select('name','email','profile_image','last_logged_in_on')
                     ->where('id', '=', auth()->guard('admin')->id())
                     ->first();
			
						 
            Session::put('AdminData', $users);
            
            return redirect()->intended('admin/dashboard');
        }
        else
        {
            return redirect()->intended('admin/login')->with('status','Invalid Login Credentials !');
        }
    }
    
    
    public function getLogout() 
    {
        auth()->guard('admin')->logout();
        return redirect()->intended('admin/login');
    }
    
}
