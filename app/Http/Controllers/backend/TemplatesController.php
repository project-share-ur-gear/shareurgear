<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Template;
use App\Model\backend\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use Yajra\DataTables\Facades\DataTables;

use DB;


class TemplatesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $template;

    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Templates';
        return view('backend.templates.index', compact('pageTitle'));
    }

    /**
     * Display a listing of the resource by ajax using datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getdata(Request $request)
    {


        $url=SITE_HTTP_URL_API.'template-getdata/template';
        $curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY
          ),
		));
		$getPageresponse = curl_exec($curl);
		curl_close($curl);


        if($getPageresponse=='failed'){
            $getPageresponse = [];
            $query='';
            return 0;
        }
        else{
            $query=json_decode($getPageresponse);

            return Datatables::of($query)->make();
        }

        if(!empty($query))
        {
            return datatables()->collection($query)->toJson();    
        } else {
            return redirect('/admin/templates/index')->with('error', __('general.messages.unauthorized_access') );
        }
    }
    
   


    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Template $template, $id)
    {
        
        $pageTitle = 'Email Templates: View Template Information';

        if (!$template = $this->template->find($id))
            return redirect()->back();

            $url=SITE_HTTP_URL_API.'show-template/'.$id;
         
            $curlKey=CURL_KEY;

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'token: '.$curlKey,
                    
                ),
            ));



            
            $response = curl_exec($curl);
         
            $err = curl_error($curl);
            
            curl_close($curl);
            $response=json_decode($response);
            if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            $site_config = (array) $response;

         
            return view('backend.templates.show', compact('pageTitle','template','site_config'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Template $template, $id)
    {

		
        if (!$template = $this->template->find($id))
            return redirect()->back();
            $url=SITE_HTTP_URL_API.'edittemplate/'.$id;
  
            $curlKey = CURL_KEY;
	
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL =>  $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'token: '.$curlKey,
               
              ),
            ));
            
            $response = curl_exec($curl);
            
            $err = curl_error($curl);
            
            curl_close($curl);
    
           
            $template=json_decode($response);
           
            if(empty($template)){
                return  redirect(route('static.errorpage'));
            }
            $pageTitle = 'Templates: Update Template: '.$template->title_en;
          	

            
            return view('backend.templates.edit', ['pageTitle'=> $pageTitle, 'template'=> $template]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Template $template, $id)
    {

        //
        if (!$page_obj = $this->template->find($id))
            return redirect()->back();

        $validator = Validator::make($request->all(), [
            'title_en'=>'required|unique:templates,title_en,'.$id.'|max:255',
			'subject_en'=>'required|unique:templates,subject_en,'.$id.'|max:255',
			'content_en'=>'required',
		]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        } else {

			$configdata = DB::table('config')
				 ->select('*')->get();
			$site_config=array();
			foreach($configdata as $allVal){
				$site_config[$allVal->config_key]=$allVal->value;
			}
		
			$content_en=str_replace(array(env('APP_URL').'/storage/app/public/logo/'.$site_config['site_logo'],$site_config['site_name_en']),array("{logo_url}","{site_name}"),$request->get('content_en'));
		    $new_data_array = [
                'title_en' => $request->get('title_en'),
				'subject_en'=> $request->get('subject_en'),
				'content_en'=> $content_en,
				'updated_at'=>date('Y-m-d H:i:s')
            ];
            
            $url=SITE_HTTP_URL_API.'update-template/'.$id;
            
            $curl = curl_init();
            
            $curlKey = CURL_KEY;

            curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PATCH',
            CURLOPT_POSTFIELDS => json_encode($new_data_array),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json",
                'token: '.$curlKey,
            ),
            ));

            $response = curl_exec($curl);
            
            if($response=="failed"){
                return  redirect(route('static.errorpage'));
            }
            $err = curl_error($curl);
            curl_close($curl);
          if($response=='1')
          {
            return redirect('/admin/templates/index')->with('success', __('templates.messages.updated_success') );
          }else{
            return redirect('/admin/templates/index')->with('error', __('somthing is wrong') );
          }
           
            
            
      
            return redirect('/admin/templates/index')->with('success', __('templates.messages.updated_success') );
        }
      
    }    
}
