<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Admin;
use App\Model\backend\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use DB;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;

class BookingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $admin;

    public function __construct(Admin $admin){
        $this->admin = $admin;
    }

	/** Bookings  */
	public function bookings(Request $request){
		$pageTitle = ' Bookings';
		return view('backend.booking.bookings',compact('pageTitle'));
	}

	public function cancelbookings(Request $request){
		$pageTitle = 'Cancel Bookings';
		return view('backend.booking.cancelbookings',compact('pageTitle'));
	}
	
	public function getbookings(Request $request){
		
		if($request->ajax()){
			
			$getUrl = $request->fullUrl();

			$aColumns = [
				'booking_id',
				'name',
				'booking_location',
				'booking_price',
				'booking_approve_status',
				'booking_time',
				'booking_user_id',
			];
	
			$sTable = 'bookings';
			$sIndexColumn = 'booking_id';

			$getUrl = str_replace(SITE_HTTP_URL.'/',SITE_HTTP_URL_API,$getUrl);
			$getData = $this->getCurl($getUrl);
			
			$iTotal = $getData['iTotal'];
			$iFilteredTotal = $getData['iFilteredTotal'];
			$qry = $getData['qry'];

			$output = array(
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
			
			$j=1;

			foreach($qry as $row1){

				$row=array();
				
				$row[] = $j;

				$row[] = $row1['name'].'</br><code>Booking Id: #'.$row1['booking_id'].'</code>';	
				
				$row[] = $row1['booking_location'];	
				
				$row[] = $row1['booking_price'];
				
				switch ($row1['booking_approve_status']) {
					case '0': $Apstatus = 'Pending'; break;
					case '1': $Apstatus = 'Approved'; break;
					case '2': $Apstatus = 'Decline'; break;
				}
				$row[] = $Apstatus;
				
				$row[] = date('Y-m-d H:i:s',strtotime($row1['booking_time']));
							
				$uViewUrl = route('admin.booking.viewbookings',['bkid'=>encrypt($row1['booking_id'])]);
				$row[] = '<a href="'.$uViewUrl.'" title="View Booking"><button type="button" class="btn btn-soft-primary waves-effect waves-light"><i class="fa fa-eye"></i></button></a> ';
				
				$output['aaData'][] = $row;
				$j++;
			}	
			
			echo json_encode( $output );
			exit();
		}else{
			return redirect('/')->with('error','Unauthorized access.');
		}
	}

	public function getcancelbookings(Request $request){
		
		if($request->ajax())
		{
			$getUrl = $request->fullUrl();
			
			$aColumns = [
				'booking_id',
				'name',
				'booking_location',
				'booking_price',
				'booking_time',
				'booking_user_id',
			];

			$sTable = 'bookings';
			$sIndexColumn = 'booking_id';

			$getUrl = str_replace(SITE_HTTP_URL.'/',SITE_HTTP_URL_API,$getUrl);
			$getData = $this->getCurl($getUrl);
			
			$iTotal = $getData['iTotal'];
			$iFilteredTotal = $getData['iFilteredTotal'];
			$qry = $getData['qry'];
			
			$output = array(
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
			
			$j=1;

			foreach($qry as $row1)
			{
				$row=array();
				
				$row[] = $j;

				$row[] = $row1['name'];	
				
				$row[] = $row1['booking_location'];	
				
				$row[] = $row1['booking_price'];
								
				$row[] = date('Y-m-d H:i:s',strtotime($row1['booking_time']));
							
				$uViewUrl = route('admin.booking.viewbookings',['bkid'=>encrypt($row1['booking_id'])]);
				$row[] = '<a href="'.$uViewUrl.'" title="View Booking"><button type="button" class="btn btn-soft-primary waves-effect waves-light"><i class="fa fa-eye"></i></button></a> ';
				
				$output['aaData'][] = $row;
				$j++;
			}	
			
			echo json_encode( $output );
			exit();
		}else{
			return redirect('/')->with('error','Unauthorized access.');
		}
	}
	
	public function viewbookings(Request $request, $bkid){

		if(empty($bkid)){
			return redirect(route('admin.booking.bookings'))->with('error','Invalid Request!!');
		}
			
		$bkid = decrypt($bkid);

		$getUrl = SITE_HTTP_URL_API.'view-bookings/'.$bkid;
		$getData = $this->getCurl($getUrl);
		$bookingData = (object)$getData['bookingData'];
		$getExtraItems = (object)$getData['getExtraItems'];
		$getReportBy = $getData['getReportBy'];
		$checkinCheckout = $getData['checkinCheckout'];

		$getSharer = $this->getCurl(SITE_HTTP_URL_API.'get-user/'.$bookingData->product_added_by); 
		$getRenter = $this->getCurl(SITE_HTTP_URL_API.'get-user/'.$bookingData->booking_user_id);

		$getTranscation = $this->getCurl(SITE_HTTP_URL_API.'get-transcation/'.$bookingData->booking_id);

		if(empty($bookingData)){
			return redirect(route('admin.booking.bookings'))->with('error','Invalid Booking Request!!');
		}

		if($request->post()){
			$getConfig = $this->getConfigs();
			$uViewUrl = route('admin.booking.viewbookings',['bkid'=>encrypt($bkid)]);
			require_once(ROOT_PATH.'/vendor/stripe/vendor/autoload.php');
			$stripe = new \Stripe\StripeClient($getConfig['stripe_secret_key']);
			$data = $request->all();
			
			$chargeId  = $bookingData->booking_charge_id;
			$transferId  = $getSharer['client_account_id'];
			$CustId  = $getRenter['stripe_cust_account_id'];
			
			$txnData['txn_booking_id'] = $bkid;
			$balance = $stripe->balance->retrieve();
			$available_amount = $balance['available']['0']['amount'];

			if($data['action']=='refund'){
				$txnData['txn_type'] = 'refund';
				$txnData['txn_amount'] = $data['refund_amount'];

				if($available_amount < $txnData['txn_amount']){
					$message = 'Your can not make a '.$data['action'].', becouse your available balance is '.$available_amount;
					return redirect($uViewUrl)->with('error',$message);
				}

				try {
					$transcation = $stripe->refunds->create([
						'charge' => $chargeId,
						'amount' => ($txnData['txn_amount']*100)
					]);
				} catch (\Exception $e) {
					$errorMessage = $e->getMessage();
					return redirect($uViewUrl)->with('error',$errorMessage);
				}

				$msg = 'Fund refund successfully.';
				
			}else if($data['action']=='release'){
				
				$txnData['txn_type'] = 'release';
				$txnData['txn_amount'] = $data['release_amount'];

				if($available_amount < $txnData['txn_amount']){
					$message = 'Your can not make a '.$data['action'].', because your available balance is '.$available_amount;
					return redirect($uViewUrl)->with('error',$message);
				}

				try {
					$transcation = $stripe->transfers->create([
						'amount' => ($txnData['txn_amount']*100),
						'currency' => 'usd',
						'destination' => $transferId,
						'description' => 'Transfers for Booking Id: '.$bkid,
					]);
				} catch (\Exception $e) {
					$errorMessage = $e->getMessage();
					return redirect($uViewUrl)->with('error',$errorMessage);
				}

				$msg = 'Fund release successfully.';

			}else if($data['action']=='charge'){

				$txnData['txn_type'] = 'charge';
				$txnData['txn_amount'] = $data['charge_amount'];
				
				try{
					$payLoad = [
						'amount' => ($txnData['txn_amount']*100),
						'currency' => 'usd',
						'customer' => $CustId,
						'description' => 'Booking Extra Charge for Booking Id: '.$bkid,
					];

					$transcation = $stripe->charges->create($payLoad);

				}catch(\Exception $e){
					$errorMsg = $e->getMessage();
					return redirect($uViewUrl)->with('error',$errorMsg);
				}
				
				$msg = 'Fund charge successfully.';
				
			}else{
				return redirect($uViewUrl)->with('error','Invalid Request!!');
			}

			if(!empty($transcation->id)){
				$txnData['txn_rrc_id'] = $transcation->id;
				
				$url = SITE_HTTP_URL_API.'add-transcation';
				$addTxn = json_decode($this->postCurl($url,$txnData));
	
				if($addTxn->status){
					return redirect($uViewUrl)->with('success',$msg);
				}else{
					return redirect($uViewUrl)->with('success',$addTxn['msg']);
				}
			}
		}

		$pageTitle = 'View Bookings';
		return view('backend.booking.viewbooking',compact('pageTitle','bookingData','getExtraItems','getReportBy','checkinCheckout','getTranscation'));
	}
	/** End  */
	
	public function reviews(Request $request){
		$pageTitle = 'Reviews';
		return view('backend.booking.reviews',compact('pageTitle'));
	}

	public function getreviews(Request $request){
		
		if($request->ajax())
		{
			$getUrl = $request->fullUrl();
			
			$aColumns = [
				'review_id',
				'name',
				'product_title',
				'review_rating',
				'review_dated_on',
				'review_user_id',
				'review_product_id'
			];

			$sTable = 'booking_reviews';
			$sIndexColumn = 'review_id';

			$getUrl = str_replace(SITE_HTTP_URL.'/',SITE_HTTP_URL_API,$getUrl);
			$getData = $this->getCurl($getUrl);

			$iTotal = $getData['iTotal'];
			$iFilteredTotal = $getData['iFilteredTotal'];
			$qry = $getData['qry'];
			
			$output = array(
				"iTotalRecords" => $iTotal,
				"iTotalDisplayRecords" => $iFilteredTotal,
				"aaData" => array()
			);
			
			$j=1;

			foreach($qry as $row1)
			{
				$row=array();
				
				$row[] = '<input type="checkbox" id="deleteRow_'.$row1['review_id'].'" class="deleteRow" value="'.$row1['review_id'].'" />';

				$row[] = $row1['name'];	
				
				$row[] = $row1['product_title'];	
				$star = '';
				for($i = 1; $i <= 5; $i++){
					$color = ($row1['review_rating'] >= $i)?'#f7a40c':'#dddcd8';
					$star.='<i class="fa fa-star" aria-hidden="true" style="font-size: 18px;color:'.$color.'"></i>';
				}
				$row[] = $star;
								
				$row[] = date('M d, Y h:i A',strtotime($row1['review_dated_on']));
							
				$uViewUrl = route('admin.booking.viewreviews',['rvid'=>encrypt($row1['review_id'])]);
				$row[] = '<a href="'.$uViewUrl.'" title="View Booking"><button type="button" class="btn btn-soft-primary waves-effect waves-light"><i class="fa fa-eye"></i></button></a> ';
				
				$output['aaData'][] = $row;
				$j++;
			}	
			
			echo json_encode( $output );
			exit();
		}else{
			return redirect('/')->with('error','Unauthorized access.');
		}
	}

	public function viewreviews(Request $request,$rvid){
		if(empty($rvid)){
			return redirect(route('admin.booking.reviews'))->with('error','Invalid Request!!');
		}

		$rvid = decrypt($rvid);

		$getUrl = SITE_HTTP_URL_API.'admin-view-review/'.$rvid;
		$getReviewData = (object)$this->getCurl($getUrl);
		
		if(empty($getReviewData)){
			return redirect(route('admin.booking.reviews'))->with('error','Invalid Request!!');
		}

		$pageTitle = 'View Reviews';
		return view('backend.booking.viewreviews',compact('pageTitle','getReviewData'));
	}

	public function removereviews(Request $request){
		if($request->ajax()){
            $ids = $request->data_ids;
			
			$data['ids'] = $ids;

			$getUrl = SITE_HTTP_URL_API.'remove-reviews';
			$getRespose = $this->postCurl($getUrl,$data);
			echo $getRespose;exit;
		}
		exit;
	}
	
	/** Curl */
	public function getCurl($url){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);
		
		curl_close($curl);
		return json_decode($response,true);
    }

	public function postCurl($url,$postData){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>$postData,
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		return $response;
    }

	public function getConfigs(){
		$getUrl = SITE_HTTP_URL_API.'get-configs';
		$getData = $this->getCurl($getUrl);
		$getData = array_column($getData,'value','config_key');
		return $getData;
	}
}
