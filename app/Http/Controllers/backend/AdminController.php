<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Admin;
use App\Model\backend\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use DB;
use Hash;
use Intervention\Image\ImageManagerStatic as Image;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $admin;

    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }

    
    public function index()
    {
        return view('home');
    }
    
    public function dashboard()
    {	

		
		$url=SITE_HTTP_URL_API.'dashboard';
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => 	$url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
          ),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$data=json_decode($response);

		if(empty($data)){
			return  redirect(route('static.errorpage'));
		}

		$pageTitle = $data->pageTitle;
		$emailTempCount = $data->emailTempCount;
		$pages = $data->pages;
		return view('backend.dashboard', compact('pageTitle','emailTempCount','pages'));
	  
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        

        $pageTitle = __('admin.titles.update_page_title');
         
        $logged_in_user_id = auth()->guard('admin')->id();
        
        if (!$admin = $this->admin->find($logged_in_user_id))
            return redirect()->back();

        return view('backend.admin.edit', compact('pageTitle','admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {	
		$logged_in_user_id = auth()->guard('admin')->id();
    	if (!$admin_obj = $this->admin->find($logged_in_user_id))
    		return redirect()->back();

    	$validator = Validator::make($request->all(), [
            'name'=>'required|max:100',
            'email'=>'required|email|unique:admins,email,'.$logged_in_user_id.'|max:255',
            'profile_image'=>'mimes:jpeg,jpg,png,gif|max:5000',
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        // ->with('errors', ['Falha no Upload'])
                        ->withErrors($validator)
                        ->withInput();
        } else {

			$user_info = DB::table('users')->select('*')->where('email','=',$request->get('email'))->get();
			if(count($user_info)>0){
				return redirect()
                        ->back()->with('error', __('admin.messages.invalid_email') );
			}
			else{
				$new_data_array = [
					'name' => $request->get('name'),
					'email'=> $request->get('email'),
					'profile_image'=> $request->get('profile_image'),
				];
				
	
				if ($request->hasFile('profile_image') && $request->file('profile_image')->isValid()) {
	
					// Remove image if exists
					if ($new_data_array['profile_image']){
						if (Storage::exists("public/admin_profile_photo/{$admin_obj->profile_image}")){
							Storage::delete("public/admin_profile_photo/{$admin_obj->profile_image}");
							Storage::delete("public/admin_profile_photo/thumb_{$admin_obj->profile_image}");
						}
					}
	
					// $name = kebab_case($request->name);

					$extension = $request->profile_image->extension();
					$nameImage = $request->name.'-'.time().'.'.$extension;
					
					$new_data_array['profile_image'] = $nameImage;
	
					//file store location: storage\app\products
					$upload = $request->profile_image->storeAs('public/admin_profile_photo', $nameImage);
					$phpArr=array('php','php3','php4','php5','phtml');
					if(in_array($extension,$phpArr)){
						return redirect()
								->back()
								->with('errors', __('general.messages.photo_not_uploaded'))
								->withInput();
					}
					if(strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<?php') !== false){
						return redirect()
								->back()
								->with('errors', __('general.messages.photo_not_uploaded'))
								->withInput();
					}
					if (strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<?= ') !== false) {
						return redirect()
								->back()
								->with('errors', __('general.messages.photo_not_uploaded'))
								->withInput();
					}
					if(strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<? ') !== false) {
						return redirect()
								->back()
								->with('errors', __('general.messages.photo_not_uploaded'))
								->withInput();
					}
	
					if (!$upload){
						return redirect()
								->back()
								->with('errors', __('general.messages.photo_not_uploaded'))
								->withInput();
					} else {
	
						if($request->hasFile('profile_image')) {
							 // thumb 150 x 150 
							$image       = $request->file('profile_image');
							$filename    = 'thumb_'.$nameImage;
							// $filename    = 'thumb_'.$image->getClientOriginalName();
	
							$canvas = Image::canvas(150, 150);
							$image  = Image::make($image->getRealPath())->resize(150, 150, function($constraint)
							{
								$constraint->aspectRatio();
							});
							$canvas->insert($image, 'center');
							$canvas->save('storage/admin_profile_photo/'.$filename);
	
						}
	
					}
				} else {
					$new_data_array['profile_image'] = $admin_obj->profile_image;
				}
			
				$url=SITE_HTTP_URL_API.'adminupdateprofile/'.$logged_in_user_id;
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => $url,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => '',
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => 'PATCH',
				  CURLOPT_POSTFIELDS =>json_encode($new_data_array),
				  CURLOPT_HTTPHEADER => array(
					'Content-Type: text/plain',
					'token: '.CURL_KEY.''
				  ),
				));
				
				$response = curl_exec($curl);
				
				curl_close($curl);
				$data=json_decode($response);
				if(empty($data)){
					return  redirect(route('static.errorpage'));
				}
				Session::put('AdminData', $data);
				return redirect('/admin/edit')->with('success', __('admin.messages.updated_success') );

			}
 
        }
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function changepassword(Admin $admin)
    {
        //       
        $pageTitle = __('admin.titles.changepassword_page_title');

        $logged_in_user_id = auth()->guard('admin')->id();
        
        if (!$admin = $this->admin->find($logged_in_user_id))
            return redirect()->back();

        return view('backend.admin.changepassword', compact('pageTitle','admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function updatepassword(Request $request, Admin $admin)
    {
		
	
		//
        $logged_in_user_id = auth()->guard('admin')->id();
		
        if (!$admin_obj = $this->admin->find($logged_in_user_id))
            return redirect()->back();

        $request_data = $request->All();

        $messages = [
            'current_password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
        ];

        $validator = Validator::make($request_data, [
            'current_password' => 'required',
            'password' => 'required|same:password',
            'password_confirmation' => 'required|same:password',     
        ], $messages);



        
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        // ->with('errors', ['Falha no Upload'])
                        ->withErrors($validator)
                        ->withInput();
        } else {

            $current_password = $admin_obj->password;
			if($request_data['password']!=$request_data['password_confirmation']){
				return redirect('/admin/changepassword')->with('error', __('admin.messages.password_not_match_error') )->withInput();
			}
			else{
				if(Hash::check($request_data['current_password'], $current_password))
				{           
					// $user_id = Auth::User()->id;     
					// $obj_user = User::find($user_id);
					$admin_obj->password = Hash::make($request_data['password']);

					$user['email']=$admin_obj->email;
					$user['password']=$admin_obj->password;
					$url=SITE_HTTP_URL_API.'updatepassword';
					// $admin_obj->save(); 
					$curl = curl_init();
					curl_setopt_array($curl, array(
					  CURLOPT_URL => $url,
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => '',
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => 'PATCH',
					  CURLOPT_POSTFIELDS =>json_encode($user),
					  CURLOPT_HTTPHEADER => array(
						'Content-Type: application/json',
						'token: '.CURL_KEY.''
					  ),
					));
					
					$response = curl_exec($curl);
					
					curl_close($curl);
					if(empty($response)){
						return  redirect(route('static.errorpage'));
					}
				if($response=='1')
				{
					$finf_geo_details=getBrowser();
					$deviceInfo=$finf_geo_details['device'].', '.$finf_geo_details['name'];		
					$mailData = [
						'user_os'=>getOS(),
						'user_ip'=>$_SERVER['REMOTE_ADDR'],
						'user_device'=>$deviceInfo,
						'user_email'    =>Session::get('AdminData')->email,
						'user_name'    => Session::get('AdminData')->name,
						'link'    => env('ADMIN_APP_URL').'/changepassword',
						'mail_template' => 'change_password_email'
					];
					$sendMail = Mail::send(new MailSenders($mailData));
		
					return redirect('/admin/changepassword')->with('success', __('admin.messages.password_changed_success') );
				}else{
					return redirect('/admin/changepassword')->with('error', __('admin.Somthing is wrong please check') )->withInput();
				}
	
				} else {
					return redirect('/admin/changepassword')->with('error', __('admin.messages.current_password_not_match_error') )->withInput();
				}
			}

        }
      
    }
	
	public function checkemail(Request $request,Admin $admin)
    {
	
		if($request->ajax()){
            $email_address = strtolower($request->get('email'));
			$user_info = DB::table('users')
				->select('*')->where('email','=',$email_address)->get();
			if(count($user_info)>0)
				echo json_encode($email_address." is not valid.");
			else
				echo json_encode("true");
			exit();

        } else {
            return redirect('/admin/dashboard')->with('error', __('general.messages.unauthorized_access') );
        }
		
    }
	
	public function checkemailexist(Request $request,Admin $admin)
    {
		if($request->ajax()){
            $email_address = strtolower($request->get('email'));
			$user_info = DB::table('admins')
				->select('*')->where('email','=',$email_address)->get();
			if(count($user_info)==0)
				echo json_encode($email_address." does not exist.");
			else
				echo json_encode("true");
			exit();

        } else {
            return redirect('/admin/dashboard')->with('error', __('general.messages.unauthorized_access') );
        }
		
    }

	public function checkoldpassword(Request $request,Admin $admin)
    {
			if($request->ajax()){
			$logged_in_user_id = auth()->guard('admin')->id();
        	$admin_obj = $this->admin->find($logged_in_user_id);
			$current_password=$admin_obj->password;
			if(!Hash::check($request->get('current_password'), $current_password)){           
                echo json_encode("Current password is not correct.");
            } else {
               echo json_encode("true");
            }

        } else {
            return redirect('/admin/dashboard')->with('error', __('general.messages.unauthorized_access') );
        }
		
    }

    public function mediaupload(Request $request, Admin $admin){

    	 
    	$validator = Validator::make($request->all(), []);
        $funcNum = $request->CKEditorFuncNum;
        $message = 'Please upload a valid image';
        $url='';
        if ($validator->fails()) {
        	 
			echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
			exit();
            
        } else {
        	if ($request->hasFile('upload') && $request->file('upload')->isValid()) {
	
				$name = kebab_case($request->name);
				$extension = $request->upload->extension();
				$nameImage = $name.'-'.time().'.'.$extension;
				 

				//file store location: storage\app\products
				$upload = $request->upload->storeAs('public/media_images', $nameImage);
				 
				if (!$upload){
					echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
					exit();
				} else {

					if($request->hasFile('upload')) {
						$message="";
						$url = HTTP_MEDIA_IMAGES_PATH."/".$nameImage;
						echo "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>"; 
						exit;

					}

				}
			}

        }	
		
 	}
}
