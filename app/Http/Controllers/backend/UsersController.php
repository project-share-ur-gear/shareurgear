<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\User;
use App\Model\backend\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use DB;
use Hash;


class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $pageTitle = ucfirst($type).' Users';
        return view('backend.users.index', compact('pageTitle','type'));
    }

    /**
     * Display a listing of the resource by ajax using datatable.
     *
     * @return \Illuminate\Http\Response
     */

    public function getdata(Request $request,$type){
        //
        $url=SITE_HTTP_URL_API.'getuserdata';
        if($request->ajax()){
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'token: '.CURL_KEY
                ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);

            if($response=='failed'){
                $response = [];
                $query='';
                return 0;
            }
            else{
                $users=json_decode($response);
                return Datatables::of($users)->make();
            }
        
        } else {
            return redirect('/admin/users/index')->with('error', __('general.messages.unauthorized_access') );
        }
    }
    
   
	 /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, $id){

        $pageTitle = 'User: View Information';
        $curl = curl_init();

        $url=SITE_HTTP_URL_API.'showuserdata/'.$id;

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY
          ),
        ));
        
        $response = curl_exec($curl);

        curl_close($curl);

        $user = json_decode($response);
        
        if (!$user){
            return redirect()->back();
        }
            
	    return view('backend.users.show', compact('pageTitle','user'));
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->ajax()){
            $ids = $request->data_ids;
            $url=SITE_HTTP_URL_API.'userdestroy';
            $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($ids),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.CURL_KEY.''
                ),

            ));

            $response = curl_exec($curl);
        if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='1')
            {
                return response()->json(['success'=>'Account(s) successfully deleted']);
            }else{
                return redirect('/admin/users/client')->with('error', __('Somthing is Wrong') );
            }

         

			// $new_data_array = [
            //     'deleted_status' => '1',
			// 	'deleted_on' => date('Y-m-d H:i:s')
            // ];
            // $output = DB::table("users")->whereIn('id',explode(",",$ids))->update($new_data_array);
            
            return response()->json(['success'=>'Account(s) successfully deleted']);
        } else {
            return redirect('/admin/users/index')->with('error', __('general.messages.unauthorized_access') );
        }
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function changestatus(Request $request, User $user, $id,$type)
    {
        if (!$page_obj = $this->user->find($id)){
            return redirect()->back();
        }

            $url=SITE_HTTP_URL_API.'userchangestatus/'.$id;
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'token: '.CURL_KEY.''
                    ),
            ));
            
            $response = curl_exec($curl);
            if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='0'){
                return redirect('/admin/users/'.$type)->with('error', __('Somthing is Wrong') );

            }else{
                return redirect('/admin/users/'.$type)->with('success', ucfirst($type).__('users.messages.updated_success') );
            }

		    
    }

	 /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function accessaccount(User $user, $id)
    {
        //
        if (!$user = $this->user->find($id))
            return redirect()->back();

        foreach((array)$user as $k=>$value){
            $UserData[] = $value;
        }

        Session::put('UserData',$UserData[11]);
		auth()->guard('user')->loginUsingId($id);
		return redirect()->intended('/account-setting');
    }

}
