<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Faq;
use App\Model\backend\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Client\Pool;
use Yajra\DataTables\Facades\DataTables;

use DB;
use App;


class FaqController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $faq;
	private $status_list;

    public function __construct(Faq $faq)
    {
        $this->faq = $faq;
		$this->status_list = array('1'=>'Enable', '2'=>'Disable');
		
		 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Faq';
        return view('backend.faq.index', compact('pageTitle'));
    }

    /**
     * Display a listing of the resource by ajax using datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getdata(Request $request){
        $url=SITE_HTTP_URL_API.'faq-getdata/2';
        
        if($request->ajax()){

            $curl = curl_init();
            $curlKey=CURL_KEY;

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'token: '.$curlKey
                ),
            ));
        
            $response = curl_exec($curl);
            curl_close($curl);
        

            if($response=='failed'){
                $response = [];
                $query='';
                return 0;
            }
            else{
                $query=json_decode($response);
                return Datatables::of($query)->make();
            }
    
        } else {
            return redirect('/admin/faq/index')->with('error', __('general.messages.unauthorized_access') );
        }    
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){

        $url=SITE_HTTP_URL_API.'faq-create';
     
        $curl = curl_init();
        $path = SITE_HTTP_URL_API.'faq-create';
        curl_setopt_array($curl, array(
        CURLOPT_URL => $path,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-type: application/json'
        ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $get_response=json_decode($response);
        return view('backend.faq.create', ['pageTitle'=>$get_response->pageTitle, 'status_list'=>$get_response->status_list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	  
        $validator = Validator::make($request->all(), [
			//'category'=>'required',
            'question_en'=>'required|max:250',
			'answer_en'=>'required|max:5000',
			'status'=>'required',
            'sort_order'=>'required|numeric|min:1'
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        // ->with('errors', ['Falha no Upload'])
                        ->withErrors($validator)
                        ->withInput();
        } else {
            $faq = new Faq([
                'question_en' => $request->get('question_en'),
                'answer_en'=> $request->get('answer_en'),
                'status'=> $request->get('status'),
                'sort_order'=> $request->get('sort_order'),
            ]);
            $curlKey=CURL_KEY;
            $faq_data=json_encode($faq);
            $url=SITE_HTTP_URL_API.'add-faq';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL =>  $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>$faq_data,
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.$curlKey,
                ),
            ));

            $response = curl_exec($curl);
            if($response=='failed'){
                return  redirect(route('static.errorpage'));
            }

            curl_close($curl);
            
            if($response=='3')
            {
                return redirect('/admin/faq/index')->with('error', __('Order number already exist') );  
            }else if($response=='0'){
                return redirect('/admin/faq/index')->with('success', __('faq.messages.added_success') );
            }else{
                return redirect('/admin/faq/index')->with('error', __('something is wrong') );
            }
            
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq, $id){


        $url=SITE_HTTP_URL_API.'show-faq';
        
        $curlKey=CURL_KEY;
        $curl = curl_init();
          curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'token: '.$curlKey,
        ),
        
        ));
        $response = curl_exec($curl);

        if($response=="failed"){
            return  redirect(route('static.errorpage'));
        }

        $response=json_decode($response);
        
        curl_close($curl);
        if (!$faq = $this->faq->find($id))
            return redirect()->back();
		return view('backend.faq.show', ['pageTitle'=> $response->pageTitle, 'faq'=>$faq]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq, $id)
    {
        $url=SITE_HTTP_URL_API.'edit-faq/'.$id;
        $curlKey=CURL_KEY;
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
        
            'token: '.$curlKey,
        ),
        ));
      
    
        $response = curl_exec($curl);
        
        curl_close($curl);
         $data=json_decode($response);
         if (!$faq = $this->faq->find($id))
            return redirect()->back();
        return view('backend.faq.create', ['pageTitle'=> $data->pageTitle, 'faq'=> $faq, 'status_list'=>$this->status_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq, $id)
    {
       
        if (!$page_obj = $this->faq->find($id))
            return redirect()->back();

        $validator = Validator::make($request->all(), [
            //'category'=>'required',
			'question_en'=>'required|max:250',
			'answer_en'=>'required|max:5000',
			'status'=>'required',
            'sort_order'=>'required|numeric|min:1'
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        // ->with('errors', ['Falha no Upload'])
                        ->withErrors($validator)
                        ->withInput();
        } else {

           
            $new_data_array = [
                'question_en' => $request->get('question_en'),
                'answer_en'=> $request->get('answer_en'),
                'status'=> $request->get('status'),
                'sort_order'=> $request->get('sort_order'),
                'updated_at'=>date('Y-m-d H:i:s')
            ];
            $url=SITE_HTTP_URL_API.'faq-update/'.$id;
        
            $curlKey=CURL_KEY;
            $curl = curl_init();
            curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'PATCH',
              CURLOPT_POSTFIELDS =>json_encode($new_data_array),
              CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: '.$curlKey,
              ),
            ));
            
            $response = curl_exec($curl);
            if($response=='failed'){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
                if($response=='1'){
                    return redirect('/admin/faq/index')->with('success', __('faq.messages.updated_success') );
                }else{
                    return redirect('/admin/faq/index')->with('error', __('something is wrong') );
                }

        }
      
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if($request->ajax()){
            $ids = $request->data_ids;
    

           $url=SITE_HTTP_URL_API.'faq-destroy';
           $curl = curl_init();
           $curlKey=CURL_KEY;
                curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($ids),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: text/plain',
                    'token: '.$curlKey,
                ),
                ));
                $response = curl_exec($curl);
                curl_close($curl);

                if($response=='failed'){
                    return  redirect(route('static.errorpage'));
                }

                if($response=='1')
                {
                    return response()->json(['success'=>__('faq.messages.deleted_success')]);
                }else{
                    return response()->json(['success'=>__('faq.messages.deleted_success')]);
              
                }

        } else {
            return redirect('/admin/faq/index')->with('error', __('general.messages.unauthorized_access') );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function changestatus(Request $request, Faq $faq, $id)
    {
        //
        if (!$page_obj = $this->faq->find($id))
            return redirect()->back();

            $status = 1;
            if($page_obj->status=='1'){
                $status = 2;
            }

            $new_data_array = [
                'status' => $status
            ];
        $url=SITE_HTTP_URL_API.'faq-status/'.$id;
        $curlKey=CURL_KEY;
          $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL =>  $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($new_data_array),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.$curlKey,
                ),
            ));

            $response = curl_exec($curl);
            if($response=='failed'){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='1')
            {
             return redirect('/admin/faq/index')->with('success', __('faq.messages.updated_success') );
            }else{
                return redirect('/admin/faq/index')->with('error', __('something is wrong') ); 
            }
    }
    
}
