<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Config;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

use Yajra\DataTables\Facades\DataTables;
use Intervention\Image\ImageManagerStatic as Image;

use DB;

class ConfigController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $config;

    public function __construct(Config $config){
        $this->config = $config;
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
	*/

    public function index(Config $config){	
		$url=SITE_HTTP_URL_API.'config';
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		$response=json_decode($response);
		$configArr= (array) $response;

		if(empty($configArr)){
			return  redirect(route('static.errorpage'));
		}

    	
		$pageTitle = 'Configurations';	
        return view('backend.config.index', ['pageTitle'=> $pageTitle,'configArr'=>$configArr]);
    }

    /**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \App\Page  $page
	 * @return \Illuminate\Http\Response
	*/
    
	public function update(Request $request, Config $config){

		$url=SITE_HTTP_URL_API.'getconfig';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'token: '.CURL_KEY.''
		),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		
		$configdata=(array) json_decode($response);
		if(empty($configdata)){
			return  redirect(route('static.errorpage'));
		}
		$site_config = array();
		foreach($configdata as $allVal){
			if($allVal->config_key=='site_logo')
				$site_config[$allVal->config_key]='mimes:jpeg,jpg,png,gif,svg|max:10000';
			else
				if($allVal->config_group!="SITE_SOCIAL" and $allVal->config_group!="SITE_STRIPE" and $allVal->config_group!="SITE_MATRIX"){ 
					$site_config[$allVal->config_key] = 'required|max:255';	
				}
		}		 
        $validator = Validator::make($request->all(), $site_config);
		
		if ($validator->fails()) {
			return redirect()
						->back()
						->with('errors', ['Falha no Upload'])
						->withErrors($validator)
						->withInput();
		}else{
			$url=SITE_HTTP_URL_API.'update-config';
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'PATCH',
			CURLOPT_POSTFIELDS =>json_encode($request->all()),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json',
				'token: '.CURL_KEY.''
			),

			));

			$response = curl_exec($curl);

			curl_close($curl);
			if(empty($response)){
				return  redirect(route('static.errorpage'));
			}
			$width=137;
			$height=103;
			$keyName='site_logo';
			if ($request->hasFile($keyName) && $request->file($keyName)->isValid()) {
			
				
				// $name = kebab_case($request->get($keyName));
				$extension = $request->$keyName->extension();
				$nameImage = rand().time().'.'.$extension;
				$updated_value=$nameImage;
				//file store location: storage\app\products
				$upload = $request->$keyName->storeAs('public/logo', $nameImage);
				list($width1, $height1) = getimagesize(STORAGE_IMG_ROOT.'/app/public/logo/'.$nameImage);
				if(($width1<$width || $height1<$height) && $extension!='svg'){
					return redirect()->back()->with('error',"Image dimension is not correct");
				}
				
				$phpArr=array('php','php3','php4','php5','phtml');
				if(in_array($extension,$phpArr)){
					return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
				}
				if(strpos(file_get_contents($_FILES[$keyName]['tmp_name']), '<?php') !== false){
					return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
				}
				if (strpos(file_get_contents($_FILES[$keyName]['tmp_name']), '<?= ') !== false) {
					return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
				}
				if(strpos(file_get_contents($_FILES[$keyName]['tmp_name']), '<? ') !== false) {
					return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
				}
				
				// Remove image if exists
				if (Storage::exists("public/logo/{$site_config['site_logo']}")){
					Storage::delete("public/logo/{$site_config['site_logo']}");
					Storage::delete("public/logo/thumb_{$site_config['site_logo']}");
				}
				
				
	
				if (!$upload){
					return redirect()
							->back()
							->with('errors', __('pages.messages.file_not_available'))
							->withInput();
				} 
				else {
	
					if($extension!='svg'){
						if($request->hasFile($keyName)) {
							// thumb 150 x 150 
							$image       = $request->file($keyName);
							$filename    = 'thumb_'.$nameImage;
							// $filename    = 'thumb_'.$image->getClientOriginalName();
		
							$ratio = $width / $height;
							$otherheight =floor ($width / $ratio);
							$canvas = Image::canvas($width, $otherheight);
							$image  = Image::make($image->getRealPath())->resize($width, $otherheight, function($constraint)
							{
								$constraint->aspectRatio();
							});
							$canvas->insert($image, 'center');
							$canvas->save('storage/app/public/logo/'.$filename);
						}
					}
					else{
						$filename=$updated_value;
					}
	
					$url=SITE_HTTP_URL_API.'update-config-logo';
					$curl = curl_init();

					curl_setopt_array($curl, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'PATCH',
					CURLOPT_POSTFIELDS =>json_encode($filename),
					CURLOPT_HTTPHEADER => array(
						'Content-Type: application/json',
						'token: '.CURL_KEY.''
					),

					));
					
					$response = curl_exec($curl);
					
					curl_close($curl);
					if(empty($response)){
						return  redirect(route('static.errorpage'));
					}
					if($response=='1')
					{

					}else{
						return redirect()->back()->with('error', __('Invalid Request') )->withInput();
					}
				}
			} 
			
			

			return redirect('/admin/config')->with('success', __('config.messages.updated_success') );
		}
      
	}

}
