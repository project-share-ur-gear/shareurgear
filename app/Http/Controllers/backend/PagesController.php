<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Page;
use App\Model\backend\Country;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;

use DB;


class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Show the application dashboard.
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Pages';
        return view('backend.pages.index', compact('pageTitle'));
    }

    /**
     * Display a listing of the resource by ajax using datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getdata(Request $request){
	
        $url=SITE_HTTP_URL_API.'getdata';
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY
			),
		));

		$response = curl_exec($curl);
		curl_close($curl);

		if($response=='failed'){
			$response = [];
			$query='';
			return 0;
		}
		else{
			$query=json_decode($response);
			return datatables()->collection($query)->toJson();
		}
    }
    
    


    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
    */

    public function show(Page $page, $id){
		if (!$page = $this->page->find($id))
		return redirect()->back();
		$url=SITE_HTTP_URL_API.'page-show/'.$id;

		$curlKey = CURL_KEY;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token:'.$curlKey,
			),
		));
		

		$response = curl_exec($curl);
		
		curl_close($curl);
		if($response=="failed"){
			return  redirect(route('static.errorpage'));
		}
		$pagecontent=json_decode($response);
	
		$pageTitle = 'Pages: View Page Information';
		return view('backend.pages.show', ['pageTitle'=> $pageTitle, 'page'=> $page, 'pagecontent'=>$pagecontent]);
    }

    public function edit(Page $page, $id){

		if (!$page = $this->page->find($id))
		return redirect()->back();

		$url=SITE_HTTP_URL_API.'edit/'.$id;
	
		$curlKey = CURL_KEY;
	
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token:'.$curlKey,
			),
		));
		$getPageresponse = curl_exec($curl);
		
		curl_close($curl);
		$pagecontent=json_decode($getPageresponse);
		if(empty($pagecontent)){
			return  redirect(route('static.errorpage'));
		}

		$pageTitle = 'Pages: Update Page: '.strip_tags($page->title_en);
	    return view('backend.pages.create', ['pageTitle'=> $pageTitle, 'page'=> $page,'pagecontent'=>$pagecontent]);
    }

    public function update(Request $request, Page $page, $id){
		
		
		if (!$page_obj = $this->page->find($id))
		return redirect()->back();

		$inputarr=[
			'title_en'=>'required|unique:pages,title_en,'.$id.'|max:255',
			'meta_title_en'=>'required|max:255',
			'meta_keywords_en'=>'required',
			'meta_desc_en'=>'required',
		];
				
		$pagecontent = DB::table('pages_content')->select('*')->where('page_id','=',$id)->get();
		
		foreach($pagecontent as $allVal){
			if($allVal->page_type=='text'){
				$inputarr[$allVal->page_key]='required|max:5000';
			}
			else if($allVal->page_type=='textarea'){
				if($id==14 || $id==15){}
				else
					$inputarr[$allVal->page_key]='required';	
			}
			else if($allVal->page_type=='file'){
				if($allVal->page_video==1)
					$inputarr[$allVal->page_key]='mimes:mp4|max:20000';
				else
					$inputarr[$allVal->page_key]='mimes:jpeg,jpg,png,gif,svg|max:10000';	
			}
		}
						
		$validator = Validator::make($request->all(), $inputarr);

		if ($validator->fails()) {
			return redirect()
						->back()
						// ->with('errors', ['Falha no Upload'])
						->withErrors($validator)
						->withInput();
		} else {

			$new_data_array = [
				'title_en' => strip_tags($request->get('title_en')),
				'meta_title_en'=> strip_tags($request->get('meta_title_en')),
				'meta_keywords_en'=> strip_tags($request->get('meta_keywords_en')),
				'meta_desc_en' => strip_tags($request->get('meta_desc_en')),
				'updated_at'=>date('Y-m-d H:i:s')
			];

			$output = DB::table("pages")->where('id',$id)->update($new_data_array);

			// INSERT OTHER CONTENT OF THIS PAGE
			$cropArr=array(
				11=>array(
					103=>array(47,72),
					106=>array(80,80),
					109=>array(80,80),
				),
				13=>array(
					594=>array(605,715),
				),
				18=>array(
					500=>array(536,415),
					501=>array(536,389),
					502=>array(127,196),
				),
				19=>array(
					503=>array(536,415),
					504=>array(536,389),
					505=>array(127,196),
				),
				20=>array(
					506=>array(536,415),
					507=>array(536,389),
					508=>array(127,196),
				),
				21=>array(
					509=>array(536,415),
					510=>array(536,389),
					511=>array(127,196),
					516=>array(536,415),
					521=>array(536,415),
				),
				/*1=>array(
					39=>array(65,65),
					44=>array(65,65),
					49=>array(65,65),
					54=>array(65,65),
					57=>array(570,440),
					58=>array(370,205),
					59=>array(370,205),
					80=>array(1900,545),
					70=>array(50,50),
					73=>array(50,50),
					76=>array(50,50),
					79=>array(50,50),
					11690=>array(50,50),
					11696=>array(65,65),
				),*/

			);
			

			foreach($pagecontent as $allVal){
				
				$keyName=$allVal->page_key;
			

				if($allVal->page_type=='file'){
					
					if(!empty($cropArr[$id][$allVal->id][0])){
						$width= $cropArr[$id][$allVal->id][0];
						$height=$cropArr[$id][$allVal->id][1];
					}else{
						if($request->hasFile($keyName)){
							list($width, $height, $type, $attr) = getimagesize($request->file($keyName)->getPathName()); 		
						}
					}

					

					if ($request->hasFile($keyName) && $request->file($keyName)->isValid()) 
					{
				
					// $name =  kebab_case($request->get($keyName));
					$extension = $request->$keyName->extension();
					$nameImage = rand().time().'.'.$extension;
					$updated_value[$allVal->id]=$nameImage;
					//file store location: storage\app\products
					$upload = $request->$keyName->storeAs('public/static', $nameImage);
					list($width1, $height1) = getimagesize(STORAGE_IMG_ROOT.'/app/public/static/'.$nameImage);
					if(($width1<$width || $height1<$height) && $extension!='svg'){
						return redirect()->back()->with('error',"Image dimension is not correct");
					}
					
					$phpArr=array('php','php3','php4','php5','phtml');
					if(in_array($extension,$phpArr)){
						return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
					}
					if(strpos(file_get_contents($_FILES[$keyName]['tmp_name']), '<?php') !== false){
						return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
					}

					if (strpos(file_get_contents($_FILES[$keyName]['tmp_name']), '<?=') !== false) {
						return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
					}
					
					/*if(strpos(file_get_contents($_FILES[$keyName]['tmp_name']), '<?') !== false) {
						return redirect()->back()->with('error', __('general.messages.photo_not_valid') )->withInput();
					}*/
					
					// Remove image if exists
					if (Storage::exists("public/static/{$allVal->page_content}")){
						Storage::delete("public/static/{$allVal->page_content}");
						Storage::delete("public/static/thumb_{$allVal->page_content}");
					}
					
					if (!$upload){
						return redirect()
								->back()
								->with('errors', __('pages.messages.file_not_available'))
								->withInput();
					} else {

						if($request->hasFile($keyName) && $allVal->page_video==0) {
							if($extension!='svg'){
								// thumb 150 x 150 
								$image       = $request->file($keyName);
								$filename    = 'thumb_'.$nameImage;
								// $filename    = 'thumb_'.$image->getClientOriginalName();
		
								$ratio = $width / $height;
								$otherheight =floor ($width / $ratio);
								$canvas = Image::canvas($width, $otherheight);
								$image  = Image::make($image->getRealPath())->resize($width, $otherheight, function($constraint)
								{
									$constraint->aspectRatio();
								});
								$canvas->insert($image, 'center');
								$canvas->save('storage/app/public/static/'.$filename);
							}
						}
						else{
							/* VIDEO THUMBNAIL START */
							$ffmpeg='ffmpeg';
							$b=explode('.',$nameImage);
							$c=$b[0];  
							$vthumbImagename=$c.".jpg";
							$strNewFile=STORAGE_IMG_ROOT.'/app/public/static/'.$nameImage;
							$outputFile=STORAGE_IMG_ROOT.'/app/public/static/'.$vthumbImagename;
							$cmdnew=$ffmpeg." -i ".$strNewFile." -ss 00:00:05.000 -t 00:00:50.000 -pix_fmt rgb24 -r 1  -vframes 5 -s 570x320 ".$outputFile." -y 2>&1";
							$outnew=exec($cmdnew,$outputFile,$retnew);
						}

					}
				} 
					else {
						$updated_value[$allVal->id] = $allVal->page_content;
					}
				}
				else{
						$updated_value[$allVal->id]=$request->get($allVal->page_key);
				}
				
			
				// // UPDATE CONTENT
				// $updated_value = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $updated_value);
				// $output = DB::table("pages_content")->where('id',$allVal->id)->update(array('page_content'=>$updated_value));
			}
			

			$curlUrl = SITE_HTTP_URL_API.'update/'.$id;
		
			$postFields = json_encode($updated_value);
			$curlKey=CURL_KEY;
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $curlUrl,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'PATCH',
				CURLOPT_POSTFIELDS => $postFields,
				CURLOPT_HTTPHEADER => array(
					'Content-type: application/json',
					'token: '.$curlKey
				),

			));

			$response = curl_exec($curl);
			
			if(empty($response) && $response!="0" && $response!="1"  ){
				return  redirect(route('static.errorpage'));
			}
			if($response=='0')
			{
				return redirect('/admin/pages/index')->with('success', __('pages.messages.updated_success') );

			}else{
				return redirect('/admin/pages/index')->with('error',"Somthing Went wrong Please check");
			}
		
	
	
		}
	}


	/* Manage Designed Pages Here */
	public function editdesigned(Page $page, $key){
		$url = SITE_HTTP_URL_API.'editdesigned/'.$key;

		$curlKey = CURL_KEY;
		
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.$curlKey
			),
		));
		
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
	
		$response=json_decode($response);
		if(empty($response)){
			return  redirect(route('static.errorpage'));
		}

		if ($err) {
			return redirect('/admin/pages/index')->with('error',"Somthing Went wrong Please check");
		} else {
		  return view('backend.pages.edithomepage', ['content'=>$response->content,'pageTitle'=>$response->pageTitle]);
		}
		exit;			
	}

	public function edithomepage(Request $request){
		
		if($request->all())	{

			$getPageContent = DB::table('pages')->where('page_key','home_page')->first();

			 
			$pageContent  = "";
			if(!empty($getPageContent->page_content)){
				$pageContent = json_decode($getPageContent->page_content);
			}
			 

			$validator = Validator::make($request->all(), [
			]);

			if ($validator->fails()) {
            	return redirect()
					->back()
					->withErrors($validator)
					->withInput();
        	}else{

        		$exceptArr = [
 	 				'_method',
 	 				'_token',
 	 			];

        		$postData = $request->except($exceptArr); 	 
        		
				$pageArr = array(
        			'meta_title_en'=>$postData['meta_title_en'],
        			'meta_keywords_en'=>$postData['meta_keywords_en'],
					'meta_desc_en'=>$postData['meta_desc_en'],
					'updated_at'=>date('Y-m-d H:i:s')
				);
			
				 
				$output = DB::table("pages")->where('page_key','home_page')->update($pageArr);

        		if ($request->hasFile('banner_image') && $request->file('banner_image')->isValid()) {
        			 
					$name = $request->file('banner_image')->getClientOriginalName();
					$name = pathinfo($name, PATHINFO_FILENAME);
					$extension = $request->banner_image->extension();
					$nameImage = $name.'-'.time().'.'.$extension;

	                //store file now
       				$upload = $request->banner_image->storeAs('public/pages_images', $nameImage);
       				if (!$upload){
		                    return redirect()
		                            ->back()
		                            ->with('errors', __('Image not uploaded!'))
		                            ->withInput();
		            } else {
		          		$postData['banner_image'] = $nameImage;	  	

		          		if(!empty($pageContent->banner_image)) {
			          		if(File::exists(STORAGE_IMG_ROOT.'/app/public/pages_images/'.$pageContent->banner_image)) {
							    File::delete(STORAGE_IMG_ROOT.'/app/public/pages_images/'.$pageContent->banner_image);
							}
						}
		            }

				}else{
					$postData['banner_image'] = !empty($pageContent->banner_image)?$pageContent->banner_image:"";	
				}
        		
				$pageArr['page_content'] = json_encode($postData);

				$url = SITE_HTTP_URL_API.'edithomepage/home_page';
				

				$curlKey = CURL_KEY;
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'PATCH',
					CURLOPT_POSTFIELDS => json_encode($pageArr),
					CURLOPT_HTTPHEADER => array(
						"content-type: application/json",
						'token: '.$curlKey
					),
				));


				$response = curl_exec($curl);

				if(empty($response) &&  $response!='0' && $response!='1' ){
					return  redirect(route('static.errorpage'));
				}
				if($response=='0'){
					return redirect('/admin/pages/index')->with('success','Content Updated Successfully.');
				}else{
					return redirect('/admin/pages/index')->with('error',"Somthing Went wrong Please check");
				}
				        		 
        	}

		}else{
			return redirect('/admin/pages/index')->with('success','Please check information again.');	  
		} 
 
	}
}
