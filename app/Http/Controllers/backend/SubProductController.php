<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Page;
use App\Model\backend\Country;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;
// use Illuminate\Http\Client\Request;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Http\Request;
// use GuzzleHttp\Client;

use DB;


class SubProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Show the application dashboard.
     *
     * @return Illuminate\Http\Response
     */
    public function subproduct()
    {
	
        $pageTitle = 'Sub Product Categories';
        return view('backend.subproduct.subproduct', compact('pageTitle'));
    }

    /**
     * Display a listing of the resource by ajax using datatable.
     *
     * @return \Illuminate\Http\Response
     */
  

	public function subgetproductcategories()
    {
			
		$url= SITE_HTTP_URL_API.'subgetproductcategories';

		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'token: '.$curlKey
		),
		));
		$response = curl_exec($curl);
		curl_close($curl);


		if($response=='failed'){
			$response = [];
			$query='';
			return 0;
		}
		else{
			$query=json_decode($response);
			return datatables()->collection($query)->toJson();
		}

	}

	//end sub catgaroies

	public function createsubproduct($id)
    {

		$pageTitle='Add Sub Categories';
		$url=SITE_HTTP_URL_API.'product/getproductcategories';
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL =>   $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_HTTPHEADER => array(
					'token: '.CURL_KEY
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$product_categories=json_decode($response);
			if($product_categories == "failed"){
				return  redirect(route('static.errorpage'));
			}
			$product_categories=(array) $product_categories;
			
			
			if($id=='subcat')
			{
				return view('backend.subproduct.createsubproduct',['pageTitle'=> $pageTitle,'product_categories'=> $product_categories,]);
			}else{
				$url=SITE_HTTP_URL_API.'edit-cat-data/'.$id;
				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL =>   $url,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => '',
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => 'GET',
					CURLOPT_HTTPHEADER => array(
						'token: '.CURL_KEY
					),
				));
				$response = curl_exec($curl);
				curl_close($curl);
				$Data=json_decode($response);

				if($Data=='0')
				{
					return redirect('/admin/subproduct/sub-product')->with('error', __('Invalid Request') ); 
				}else{
					return view('backend.subproduct.createsubproduct',['pageTitle'=> $pageTitle,'product_categories'=> $product_categories,'sub_cat_data'=>$Data]);
				}
				
			}
			
	}

	public function addsubproduct(request $request)
    {
		$url=SITE_HTTP_URL_API.'addsubcat';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'PATCH',
		CURLOPT_POSTFIELDS =>json_encode($request->all()),
		CURLOPT_HTTPHEADER => array(
			'token: '.CURL_KEY
		),
		));

		$response = curl_exec($curl);
		curl_close($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		
		if($response=='1'){
			return redirect('/admin/subproduct/sub-product')->with('success', __("Sub Categories Added Sucessfully") );
		}else if($response=='2')
		{
			return redirect('/admin/subproduct/sub-product')->with('error', __('Sub Cat Allready Added') ); 
			
		}else{
			return redirect('/admin/subproduct/sub-product')->with('error', __('Somthing is wrong please try again') ); 

		}

	
	}


	public function updatesubcat(request $request,$id)
    {
		$url=SITE_HTTP_URL_API.'update-cat-data/'.$id;
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'PATCH',
		  CURLOPT_POSTFIELDS =>json_encode($request->all()),
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'token: '.CURL_KEY

		  ),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		$response_Data=json_decode($response);
	
		if($response_Data->suc_error=='error'){
			return redirect('/admin/subproduct/sub-product')->with('error', __($response_Data->msg) ); 
		}else{
			return redirect('/admin/subproduct/sub-product')->with('success', __($response_Data->msg) );
		}
		
	}
	
	public function destroy(request $request)
    {
	

		if($request->ajax()){
            $ids = $request->data_ids;
			$url=SITE_HTTP_URL_API.'subcatagorydestroy';
			$curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($ids),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.CURL_KEY.''
                ),

            ));
		$response = curl_exec($curl);
		if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='1')
            {
                return response()->json(['success'=>'Sub Categories successfully deleted']);
            }else{
			    return redirect('/admin/subproduct/sub-product')->with('error', __('Somthing is Wrong') );
            }
		}	
	}
	
	public function changestatus($id)
    {
		
		$url=SITE_HTTP_URL_API.'change-subcat-status/'.$id;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			  ),
		));
		
		$response = curl_exec($curl);
		if(empty($response) && ($response!='0')){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);
		if($response=='0'){
			return redirect('/admin/subproduct/sub-product')->with('error', __('Somthing is Wrong') );

		}else{
			return redirect('/admin/subproduct/sub-product')->with('success',__('Status Update Sucessfully') );
		}
	}

	public function showsubcat($id)
    {
		$pageTitle='Show';
		$url=SITE_HTTP_URL_API.'showsubcat/'.$id;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			  ),
		));
		$response = curl_exec($curl);
	
		curl_close($curl);
		$data=json_decode($response);
		if($data->error=='error'){

			return redirect('/admin/subproduct/sub-product')->with('error', __('Invalid Request') );

		}else{

			return view('backend.subproduct.showsubcat',['pageTitle'=> $pageTitle,'sub_cat_data'=>$data->Data]);
		}

		
	
			
	}
}
