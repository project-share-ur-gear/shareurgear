<?php

namespace App\Http\Controllers\backend;

use App\Model\backend\Page;
use App\Model\backend\Country;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Client\Pool;
// use Illuminate\Http\Client\Request;
use Intervention\Image\ImageManagerStatic as Image;

use Illuminate\Http\Request;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
// use GuzzleHttp\Client;

use DB;


class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $page;

    public function __construct(Page $page)
    {
        $this->page = $page;
    }

    /**
     * Show the application dashboard.
     *
     * @return Illuminate\Http\Response
     */
    public function productcategories()
    {
        $pageTitle = 'Product Categories';
        return view('backend.product.productcategories', compact('pageTitle'));
    }

    /**
     * Display a listing of the resource by ajax using datatable.
     *
     * @return \Illuminate\Http\Response
     */
    public function getproductcategories(Request $request)
    {
		
	
		$url= SITE_HTTP_URL_API.'product/getproductcategories';
		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.$curlKey
			),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		if($response=='failed'){
			$response = [];
			$query='';
			return 0;
		}
		else{
			$query=json_decode($response);
			return datatables()->collection($query)->toJson();
		}

    }
    
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */

	public function addproductcategories($id)
    {
	
		if($id=='addcat')
		{
		$pageTitle='Add Categories';
	 	return view('backend.product.addproductcategories',['pageTitle'=> $pageTitle]);
		}else{

			$pageTitle='Update Categories';
			$url= SITE_HTTP_URL_API.'geteditcategries/'.$id;
			$curlKey=CURL_KEY;
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token:'.$curlKey
			),
			));
			$response = curl_exec($curl);
			if($response=='failed'){
				return  redirect(route('static.errorpage'));
			}
			curl_close($curl);
			$data=json_decode($response);
		
			if($data->error_sucs=='error')
			{
				return redirect('/admin/product/product-categories')->with('error', __($data->msg) );  
			}else
			{

				return view('backend.product.addproductcategories',['pageTitle'=> $pageTitle,'cat_Data'=>$data->data,'id'=>$id]);
			}
			
		}
	}
	
	public function createproductcategories(Request $request)
    {
		if ($request->hasFile('blog_image') && $request->file('blog_image')->isValid()) {
			$name = $request->c_title;
			$extension = $request->blog_image->extension();
			$nameImage = $name.'-'.time().'.'.$extension;
		   
			$upload = $request->blog_image->storeAs('public/speciaity_images', $nameImage);
			
			list($width, $height) = getimagesize(STORAGE_IMG_ROOT.'/app/public/speciaity_images/'.$nameImage);
			
			$phpArr=array('php','php3','php4','php5','phtml');
			if(in_array($extension,$phpArr)){
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}
			if(strpos(file_get_contents($_FILES['blog_image']['tmp_name']), '<?php') !== false){
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}
			if (strpos(file_get_contents($_FILES['blog_image']['tmp_name']), '<?= ') !== false) {
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}
			if(strpos(file_get_contents($_FILES['blog_image']['tmp_name']), '<? ') !== false) {
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}

			if (!$upload){
				return redirect()
						->back()
						->with('errors', __('general.messages.photo_not_uploaded'))
						->withInput();
			} else {

				if($request->hasFile('blog_image')) {
					$image       = $request->file('blog_image');
					$filename    = $nameImage;
				}

			}
		} 

		if(empty($filename)){ }else{
				
			$data['File_name']=$filename;
		}
		
		$data['c_title']=$request->get('c_title');




		$curlKey=CURL_KEY;
		$url= SITE_HTTP_URL_API.'create-product-categories';
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>json_encode($data),
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'token: '.$curlKey
		  ),
		
		));
		
		$response = curl_exec($curl);
		curl_close($curl);
	
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		$reponse_Data=json_decode($response);
		$reponse_Data=(array) $reponse_Data;

		if($reponse_Data['suc_error']=='error')
		{
			return redirect('/admin/product/product-categories')->with('error', __($reponse_Data['msg']) );  
		}else{
			return redirect('/admin/product/product-categories')->with('success', __($reponse_Data['msg']) );
		}
	 	
	}

	public function updateproductcategories(Request $request,$id)
	{


		if ($request->hasFile('blog_image') && $request->file('blog_image')->isValid()) {
			$name = $request->c_title;
			$extension = $request->blog_image->extension();
			$nameImage = $name.'-'.time().'.'.$extension;
		  	//file store location: storage\app\products
			$upload = $request->blog_image->storeAs('public/speciaity_images', $nameImage);
			list($width, $height) = getimagesize(STORAGE_IMG_ROOT.'/app/public/speciaity_images/'.$nameImage);
			$phpArr=array('php','php3','php4','php5','phtml');
			if(in_array($extension,$phpArr)){
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}
			if(strpos(file_get_contents($_FILES['blog_image']['tmp_name']), '<?php') !== false){
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}
			if (strpos(file_get_contents($_FILES['blog_image']['tmp_name']), '<?= ') !== false) {
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}
			if(strpos(file_get_contents($_FILES['blog_image']['tmp_name']), '<? ') !== false) {
				return redirect()->back()->with('error', __('Photo Not Valid!') )->withInput();
			}

			if (!$upload){
				return redirect()
						->back()
						->with('errors', __('general.messages.photo_not_uploaded'))
						->withInput();
			} else {

				if($request->hasFile('blog_image')) {
					$image       = $request->file('blog_image');
					$filename    = $nameImage;
				}

			}
		} 

		if(empty($filename)){ }else{
			$data['File_name']=$filename;
		}

		$data['c_title']=$request->get('c_title');
		$curlKey=CURL_KEY;
		$url= SITE_HTTP_URL_API.'updatecategries/'.$id;
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'POST',
		  CURLOPT_POSTFIELDS =>json_encode($data),
		  CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'token: '.$curlKey
		  ),
		
		));
		
		$response = curl_exec($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);


		$response_Data=json_decode($response);

		if($response_Data->suc_error=='error')
		{
			return redirect('/admin/product/product-categories')->with('error', __($response_Data->msg) ); 
		}else
		{
			return redirect('/admin/product/product-categories')->with('success', __($response_Data->msg) );
		}
		
	
	}

	public function destroy(Request $request)
	{
	
		
		if($request->ajax()){
            $ids = $request->data_ids;
			$url=SITE_HTTP_URL_API.'catagorydestroy';
			$curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($ids),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.CURL_KEY.''
                ),

            ));

			$response = curl_exec($curl);
		if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='1')
            {
                return response()->json(['success'=>'Categories successfully deleted']);
            }else{
			    return redirect('/admin/product/product-categories')->with('error', __('Somthing is Wrong') );
            }
		}	
	}

	public function statusmaincat($id)
	{
			$url=SITE_HTTP_URL_API.'changemaincat/'.$id;
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'GET',
				CURLOPT_HTTPHEADER => array(
					'token: '.CURL_KEY.''
				),
			));
			
			$response = curl_exec($curl);
			if(empty($response) && ($response!='0')){
				return  redirect(route('static.errorpage'));
			}
			curl_close($curl);
			if($response=='0'){
				return redirect('/admin/product/product-categories')->with('error', __('Somthing is Wrong') );

			}else{
				return redirect('/admin/product/product-categories')->with('success',__('Status Update Sucessfully') );
			}
	}

	public function showcatagory($id)
	{
		$pageTitle='Category Show';
		$url=SITE_HTTP_URL_API.'showcat/'.$id;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			  ),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		$data=json_decode($response);
		if($data->error=='error'){
			return redirect('/admin/subproduct/sub-product')->with('error', __('Invalid Request') );
		}else{

			return view('backend.product.showcatagory',['pageTitle'=> $pageTitle,'sub_cat_data'=>$data->Data->sub_category_Data,'cat_data'=>$data->Data->category_Data]);
		}

	}

	public function productrequest()
	{
		$pageTitle = 'Product Request';
        return view('backend.product.productrequest', compact('pageTitle'));

	}

	public function getproductrequest()
	{
	
		$url= SITE_HTTP_URL_API.'product/getproductrequest';
		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'token: '.$curlKey
		  ),
		));
		$response = curl_exec($curl);

		curl_close($curl);
		if($response=='failed'){
			$response = [];
			$query='';
			return 0;
		}
		else{
			$query=json_decode($response);
			return datatables()->collection($query)->toJson();
		}
	}

	public function approveprduct($id){
		$url= SITE_HTTP_URL_API.'approve-product/'.$id;
		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'token: '.$curlKey
		  ),
		));
		$response = curl_exec($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);
		if($response=='0'){
			return redirect('/admin/product/product-request')->with('error', __('Invalid Request') );
		}
	
		if(!empty($response)){
			$userdata=(json_decode($response));
			$mailData = [
				'user_name'		=> $userdata->name,
				'user_email'	=> $userdata->email,
				'subject'		=> 'Approved Product',
				'mail_template'	=> 'common_notification',
				'message'       => "Your request for the Product $userdata->product_name approved by administrator"
			];
			$sendMail = Mail::send(new MailSenders($mailData));
			return redirect('/admin/product/manage-request')->with('success', __('Product approve Sucessfully') );
		}else{
				return redirect('/admin/product/product-request')->with('error', __('Invalid Request') );
		}
	}	

	public function declineprduct(request $request){
		
		$url= SITE_HTTP_URL_API.'decline-prduct';
		$response=Curlpostequest($url,json_encode($request->all()));
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		if($response=='0'){
			return redirect('/admin/product/product-request')->with('error', __('Invalid Request') );
		}
		if(!empty($response)){
			$userdata=(json_decode($response));
			$reason=nl2br($request['decline_resaon']);
			$mailData = [
				'user_name'		=> $userdata->name,
				'user_email'	=> $userdata->email,
				'subject'		=> 'Decline Product',
				'mail_template'	=> 'common_notification',
				'message'       => "Your request for the Product $userdata->product_name Declined by administrator. Reason is as follows :<br>"."$reason",

			];
			$sendMail = Mail::send(new MailSenders($mailData));
			return redirect('/admin/product/product-request')->with('success', __('Product Decline Sucessfully') );
		}else{
				return redirect('/admin/product/product-request')->with('error', __('Invalid Request') );
		}
	}

	public function showproduct($id){
		$pageTitle='Show Product';
		$url= SITE_HTTP_URL_API.'show-product/'.$id;
		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'token: '.$curlKey
		  ),
		));
		$response = curl_exec($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);
		$data=json_decode($response);
		if($data->error_msg=='success'){
			return view('backend.product.showproduct',['pageTitle'=> $pageTitle,'product_data'=>$data->Data]);
		}else{
				return redirect('/admin/product/product-request')->with('error', __('Invalid Request') );
		}
	}

	public function getmanagerequest(){
		$url= SITE_HTTP_URL_API.'product/getmanagerequest';
		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => 'GET',
		  CURLOPT_HTTPHEADER => array(
			'token: '.$curlKey
		  ),
		));
		$response = curl_exec($curl);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);
		if($response=='failed'){
			$response = [];
			$query='';
			return 0;
		}
		else{
			$query=json_decode($response);
			return datatables()->collection($query)->toJson();
		}
	}

	public function managerequest(){

		$pageTitle = 'Manage Product Request';
        return view('backend.product.managerequest', compact('pageTitle'));

	}
	
	public function productstatus($id)
	{
		$url= SITE_HTTP_URL_API.'productstatus/'.$id;
		$curlKey=CURL_KEY;
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'GET',
		CURLOPT_HTTPHEADER => array(
			'token: '.$curlKey
		),
		));
		$response = curl_exec($curl);
	
		if($response=='failed' && $response!='0'  ){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);
		$data=json_decode($response);
		if($data=='1'){
			return redirect('/admin/product/manage-request')->with('success', __('Status Updated Sucessfully') );
		}else{
				return redirect('/admin/product/manage-request')->with('error', __('Invalid Request') );
		}
	}
		
	public function productdestroy(Request $request)
	{
	
		if($request->ajax()){
            $ids = $request->data_ids;
			$url=SITE_HTTP_URL_API.'destroy-product';
			$curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($ids),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.CURL_KEY.''
                ),

            ));
			$response = curl_exec($curl);
			if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='1')
            {
                return response()->json(['success'=>'Product Request successfully deleted']);
            }else{
			    return redirect('/admin/product/product-request')->with('error', __('Somthing is Wrong') );
            }
		}	
	}

	public function approvedestroy(Request $request)
	{
		
		if($request->ajax()){
            $ids = $request->data_ids;
			$url=SITE_HTTP_URL_API.'approve-destroy';
			$curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($ids),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'token: '.CURL_KEY.''
                ),

            ));
			$response = curl_exec($curl);
			if(empty($response)){
                return  redirect(route('static.errorpage'));
            }
            curl_close($curl);
            if($response=='1')
            {
                return response()->json(['success'=>'Categories successfully deleted']);
            }else{
			    return redirect('/admin/product/product-request')->with('error', __('Somthing is Wrong') );
            }
		}	
	}

	public function feturedgear($id)
	{
		$url=SITE_HTTP_URL_API.'feturedgear/'.$id;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));
		
		$response = curl_exec($curl);
		if(empty($response) && ($response!='0')){
			return  redirect(route('static.errorpage'));
		}
		curl_close($curl);
		if($response=='0'){
			return redirect('/admin/product/manage-request')->with('error', __('Somthing is Wrong') );

		}else  if($response=='1') {
			return redirect('/admin/product/manage-request')->with('success',__('Fetured Gear  Update Sucessfully') );
		}else if($response=='3') {
			return redirect('/admin/product/manage-request')->with('error',__('Please admin  Status  enable') );
		}
	}
	
}
