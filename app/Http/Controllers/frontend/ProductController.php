<?php

namespace App\Http\Controllers\frontend;

use App\Model\frontend\User;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Str;
use DB;
use Hash;
use App;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
/* use Intervention\Image\ImageManagerStatic as Image; */
use Uploader;
class ProductController extends Controller
{
	public function _construct(){
		$this->locale=App::getLocale();
	}

	public function manageproduct(request $request){


		$user =  auth()->guard('user')->user();
		if($user->stripe_account=="0")
		{
			return redirect('/payout-setting')->with('success', __('Please Add  Your Stripe Account') );
		}
		$pageHeading="Manage Product";
		$userId = auth()->guard('user')->id();
		/** product categories */
		$url = SITE_HTTP_URL_API.'manage-product/'.$userId;
		$response=Curlgetequest($url,CURL_KEY);
		if($response == "failed"){
			return  redirect(route('static.errorpage'));
		}
		if(!empty($response))
		{
				$products = json_decode($response);
				$count=$products->count_manage_Data;
				$products= (array) $products->product_Data;
		}else
		{
			$count='';
			$products='';
		}
		return view('frontend.product.manageproduct',['countdata'=>$count,'pageHeading'=>$pageHeading,'manageproduct'=>$products,'metaTitle'=>"Manage Product",'metaKeywords'=>"Manage Product", 'metaDescription'=>"Manage Product"]);
	}
	
	public function addproduct($id){

		$user =  auth()->guard('user')->user();
		if($user->stripe_account=="0")
		{
			return redirect('/payout-setting')->with('success', __('Please Add  Your Stripe Account') );
		}
		$delevery_location=array();
		$delevery_to_location=array();
		$userId = auth()->guard('user')->id();


		$url = SITE_HTTP_URL_API.'get-page-data/14';
		$getPageData = json_decode(Curlgetequest($url,CURL_KEY),true);

		if($id!="add"){
			$pageHeading="Edit Product";
			$url=SITE_HTTP_URL_API.'edit-product/'.$id.'/'.$userId;
			$formurl = "/save-product/".$id;

			$url1=SITE_HTTP_URL_API.'deliverylocation/'.$id;
			$response=Curlgetequest($url1,CURL_KEY);
			if(!empty($response))
			{
				$response=json_decode($response);
				$delevery_location=$response->delivery_infomation;
				$delevery_to_location=$response->delivery_to_infomation;
				
			}
		}else{
			$pageHeading="Add Product";
			$url=SITE_HTTP_URL_API.'add-product/add/'.$userId;
			$formurl = "/save-product/add_pro";
		}
		
	


		$products =[];
		/** product categories */
		$response=Curlgetequest($url,CURL_KEY);

		if($response == "invalid_request"){
			return redirect('/manage-product')->with('error',"Invalid Request."); 
		}
		
		$products_data=json_decode($response);
		if($products_data == "failed"){
			return  redirect(route('static.errorpage'));
		}

		$products_data=(array) $products_data;
		$extraItems = array();
		if($products_data['products']!="nodata" && $products_data['products'] != "add_case" && !empty($products_data['products'])  ){
			$products=(array) $products_data['products'];
			$extraItems = (array)$products_data['extraItems'];
		}


		if($products_data['products'] == "nodata"){
			return redirect('/manage-product')->with('error', __('Invalid Request') );
		}
		/** end product categories */
		return view('frontend.product.addproduct',['getPageData'=>$getPageData,'extraItems'=>$extraItems,'delevery_to_location'=>$delevery_to_location,'delevery_location'=>$delevery_location,'formurl'=>$formurl,'product_categories'=>$products_data['product_categories'],'products'=>$products,'pageHeading'=>$pageHeading,'pageData'=>"addproduct",'contactData'=>"addproduct",'metaTitle'=>$getPageData['meta_title_en'],'metaKeywords'=>$getPageData['meta_keywords_en'], 'metaDescription'=>$getPageData['meta_desc_en']]);
	}

	public function saveproduct(Request $request,$id){
		$userId = auth()->guard('user')->id();
		$data = $request->all();

		$extraItemImages = array();
		
		if(!empty($request->file())){
			$filesKeys = array_keys($request->file());
			$isUpload = Uploader::universalUpload(
				array(
					'directory'=>storage_path('uploaded_images/'),
					'files'=>$filesKeys,
					'multiple'=>false,
					'thumb'=>array(array('w'=>300,'h'=>300),array('w'=>160,'h'=>160)),
					'allowExtension' => array("png","PNG","jpg","JPG","jpeg","JPEG"),
				),
			$request);
			
			if($isUpload['success']=='1'){
				$media_path = $isUpload['media_path'];
				foreach($media_path as $x=>$img){
					if(!empty($img['mediaPath'])){
						$extraItemImages[$x] = $img['mediaPath'];
						unset($data[$x]);
					}
				}
			}	
		}	

		$data['extra_product_image'] = $extraItemImages;
		$curl = curl_init();
		$url=SITE_HTTP_URL_API.'save-product/'.$id.'/'.$userId;
		curl_setopt_array($curl, array(
			CURLOPT_URL =>  $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'PATCH',
			CURLOPT_POSTFIELDS =>json_encode($data),
			CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json',
			'token: '.CURL_KEY
			),
		));
		
		$response = curl_exec($curl);

		curl_close($curl);
		if($response == "please_check_information_again"){
			return redirect('/manage-product')->with('error',"Please select category."); 
		}
		if($response == "invalid_request"){
			return redirect('/manage-product')->with('error',"Invalid Request."); 
		}
		if($response == "Make it primary key field must required."){
			return redirect()->back()->with('error',$response)->withInput();
		}
		if($response == "something_is_going_wrong"){
			//return redirect('/add-product')->with('error',"Something is going wrong.");
			return redirect()->back()->with('error',"Something is going wrong.")->withInput();
		}else if($response == "please_check_information_again"){
			return redirect()->back()->with('error',"Something is going wrong.")->withInput();
		}else{
			$data=json_decode($response);

			if(empty($data)){
				return  redirect(route('static.errorpage'));
			} 
			if($data->success=='1' || $data->success=='0')
			{
				if($id=='add')
				{
					return redirect('/manage-product')->with('success',"Product added successfully."); 
				}else{
					return redirect('/manage-product')->with('success',"Product update successfully.");  
				}
				
			}else{
				return redirect()->back()->with('error',"Something is going wrong.")->withInput();
			}
		}
	}

	/** files added from dropzone */
	public function prodocuments(Request $request){

		$files=$_FILES;
		$mimeArr = array(
			"file" => array("image/","admin/")
		);
		$extArr = array(
			"file" => array("jpg","jpeg","png"),
		);
		$checkMimeImage = checkMimeType($files,$mimeArr,$extArr);
		
		if($checkMimeImage["error"]){
			echo json_encode(false);
			exit();	
		}

		if($files['file']['name']!=""){
			$target_file_ck = basename($files['file']['name']);
			$imageFileType = strtolower(pathinfo($target_file_ck,PATHINFO_EXTENSION));
			if(!in_array($imageFileType,array("png","PNG","jpg","JPG","jpeg","JPEG"))){
				return json_encode(false);
				exit();
			}	
			if(strpos(file_get_contents($files['file']['tmp_name']), '<?php') !== false){
				return json_encode(false);
					exit();
			}
		}
		$path = SITE_UPLOADED_IMAGES_PATH;

		if (!file_exists($path)) {
			mkdir($path, 0777, true);
		}
		$file = $request->file('file');
		$isUpload = Uploader::universalUpload(
			array(
				'directory'=>storage_path('uploaded_images/'),
				'files'=>array('file'),
				'multiple'=>false,
				'thumb'=>array(array('w'=>300,'h'=>300),array('w'=>160,'h'=>160)),
				'allowExtension' => array("png","PNG","jpg","JPG","jpeg","JPEG"),
			),
		$request);
				
		return response()->json([
			'name' => $isUpload['media_path']['file']['mediaPath'],
			'orgname' => $isUpload['media_path']['file']['orgName'],
		]);
	}
	   
	/** remove file from dropzone */
	public function removefiles(Request $request){
		$userId = auth()->guard('user')->id();
		if(!empty($request->all()['file'])){
			$imageName = $request->all()['file'];
			$isDelete = Uploader::universalUnlink($imageName ,storage_path('uploaded_images/'));
		}
		else if(isset($request->all()['id'])){
			
			try {
				$prodID  = decrypt($request->all()['proid']);
				
			} catch (DecryptException $e) {
				echo json_encode(false);
				exit();	
			}
			
			$product = (array) DB::table('products')->where(['product_added_by'=>$userId,'product_id'=>$prodID])->first();
		   
				if(empty($product)){
	
					//$this->frontSession['errorMsg']='Invalid request';
					echo json_encode(false);
					exit();	
				}else{	
					$image= (array) unserialize($product['product_images']);
					if(!empty($image)){
						$isDelete = Uploader::universalUnlink($request->all()['id'] ,storage_path('uploaded_images/'));
						$isDelete = Uploader::universalUnlink($request->all()['id'] ,storage_path('uploaded_images/160X160'));
						$isDelete = Uploader::universalUnlink($request->all()['id'] ,storage_path('uploaded_images/300X300'));
						$isDelete = Uploader::universalUnlink($request->all()['id'] ,storage_path('uploaded_images/60X60'));
						unset($image[$request->all()['id']]);
						if(!empty($image)){
							$newimage=serialize($image);
						}else{
							$newimage='';
						}
						$newdata['product_images']=$newimage;
						$update=DB::table('products')->where('product_id', $prodID)->update(array('product_images' => $newimage));  				
						echo json_encode(true);
						exit();	
					}
					else{
						echo json_encode(false);
						exit();		
					}
					
				}
	
		}

		echo json_encode(true);
		exit();
	}

	public function subcategory(Request $request){

		if($request->pro_cat!=''){
			$url=SITE_HTTP_URL_API.'sub-category/'.$request->pro_cat;
			$response=Curlgetequest($url,CURL_KEY);
		
			if($response == "failed" || $response == "product_cat_not_found"){
			
				return response()->json([
					'sub_product_categories' => $response
				]);
			}else{
				$response    = json_decode($response);
				return response()->json([
					'sub_product_categories' => $response
				]);
			}
		}
	}

	public function manageavailability($id){

		$userId = auth()->guard('user')->id();
		$pageHeading="manageavailability";
		$url=SITE_HTTP_URL_API.'check-product/'.$id;
		$response=Curlgetequest($url,CURL_KEY);

		if($response == "failed" && $response != "0"){
			return  redirect(route('static.errorpage'));
		}else if($response=='0'){
			return redirect('/manage-product')->with('error', __('Invalid Request') );
		}else{
			$product_Data=json_decode($response);
		}
		if(!empty($product_Data)){
			if($userId!=$product_Data->product_added_by){
				return redirect('/manage-product')->with('error', __('Invalid Request') );
			}
		}


		$url1=SITE_HTTP_URL_API.'unavailable/'.$id;
		$event_Data=Curlgetequest($url1,CURL_KEY);
	   if($event_Data == "failed" && $event_Data != "0"){
			return  redirect(route('static.errorpage'));
		}
	   	return view('frontend.product.manageavailability',['product_Data'=>$product_Data,'eventdata'=>$event_Data,'product_id'=>$id,'pageHeading'=>$pageHeading,'pageData'=>"manageavailability",'contactData'=>"manage-availability",'metaTitle'=>"manage-availability",'metaKeywords'=>"manage-availability", 'metaDescription'=>"manage-availability"]);
		
	}

	public function addevent($id,request $request){
		$url=SITE_HTTP_URL_API.'check-product/'.$id;
		$response=Curlgetequest($url,CURL_KEY);
		$postData = $request->all();
		$postData['end'] = date('Y-m-d',strtotime($postData['end'].'-1 day'));

		if($response == "failed" || $response == "0"){
			return  'Invalid_Request';
		}else{
			
			$url=SITE_HTTP_URL_API.'add-event/'.$id;
			$response=Curlgetequest($url,CURL_KEY);
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL =>  $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'PATCH',
				CURLOPT_POSTFIELDS =>json_encode($postData),
				CURLOPT_HTTPHEADER => array(
				  'Content-Type: application/json',
				  'token: '.CURL_KEY
				),
			));
		   $response = curl_exec($curl);
		   curl_close($curl);
		   if($response == "failed" && $response != "0"){
				return  redirect(route('static.errorpage'));
			}	
		 	if($response=='1')
			{
				return $response;
			}else{
				return $response;
			}

		}
	}

	public function removeevent($id){
		$url=SITE_HTTP_URL_API.'remove-event/'.$id;
		$response=Curlgetequest($url,CURL_KEY);
		if($response == "failed" && $response != "0"){
			return  redirect(route('static.errorpage'));
		}
		if($response=='sucess'){
			return 'sucess';
		}
		if($response=='invalid_request'){
			return 'invalid_request';
		}
	}

	public function getmoremanageproduct(request $request){
		$userId = auth()->guard('user')->id();
		$explode_data=explode('?',$_SERVER['REQUEST_URI']);
		if(!empty($explode_data[1]))
		{
			$get_Data=$explode_data[1];
		}else{
			$get_Data='';
		}

		$url = SITE_HTTP_URL_API.'getmoremanageproduct/'.$userId.'?'.$get_Data;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'post',
			CURLOPT_POSTFIELDS =>json_encode($request->all()),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json',
				'token: '.CURL_KEY.''
			),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$products = json_decode($response);

		$varriables = array('manageproduct'=>$products);
		return view("frontend.product.getmoremanageproduct")->with($varriables)->render(); 
	}

	public function deleteproduct($id){

		$url=SITE_HTTP_URL_API.'deleteproduct/'.$id;
		$response=Curlgetequest($url,CURL_KEY);
		if($response == "failed" && $response != "0"){
		return  redirect(route('static.errorpage'));
		}
		if($response=='sucess'){
		return redirect('/manage-product')->with('success',"Product Remove successfully."); 
		}
		if($response=='inavlid_request'){
		return redirect('/manage-product')->with('error', __('Invalid Request') );
		}
	}
	
	public function managesearching(request $request){

		$userId = auth()->guard('user')->id();
		$url = SITE_HTTP_URL_API.'searchingproduct/'.$userId;
		$response=Curlpostequest($url,json_encode($request->all()));
		if($response == "failed"){
			return  redirect(route('static.errorpage'));
		}
		if(!empty($response))
		{
				$products = json_decode($response);
				$count=$products->count_manage_Data;
				$products= (array) $products->product_Data;
		}else
		{
			$count='';
			$products='';
		}
		
		$varriables = array('manageproduct'=>$products,'countdata'=>$count);
		return view("frontend.product.managesearching")->with($varriables)->render(); 


	}

	public function getreason($id){
		$url = SITE_HTTP_URL_API.'show-product/'.$id;
		$response=Curlgetequest($url,CURL_KEY);
		if($response == "failed"){
			return  redirect(route('static.errorpage'));
		}
		if(!empty($response)){
			$pro = json_decode($response);
			
			$product_desc=nl2br($pro->Data->product_cancle_desc);
			return($product_desc);
		}else
		{
			$count='';
			$products='';
			return($products);
		}
	}
	
	public function wishlist(){

		$userId = auth()->guard('user')->id();
		$url = SITE_HTTP_URL_API.'wish-list/'.$userId;
		$response=Curlgetequest($url,CURL_KEY);
	
		if($response=='failed'){
			return  redirect(route('static.errorpage'));	
		}
		$data=json_decode($response);
		
		if(!empty($data))
		{
		$wishlist_data=$data->Data;
		$count_data=$data->count_data;

		}else{
			$wishlist_data='';
			$count_data='0';
		}
		$pageHeading="wishlist";
		return view('frontend.product.wishlist',['count_Data'=>$count_data,'wishlist_data'=>$wishlist_data,'pageHeading'=>$pageHeading,'pageData'=>"wish-list",'contactData'=>"wishlist",'metaTitle'=>"wish-list",'metaKeywords'=>"wish-list", 'metaDescription'=>"wish-list"]);
	}

	public function removewishlist($id){
		$userId = auth()->guard('user')->id();
		$url = SITE_HTTP_URL_API.'removewishlist/'.$userId.'/'.$id;
		$response=Curlgetequest($url,CURL_KEY);
		if($response=='invalid_request')
		{
			return 'invalid_request';
		}else{
			return 'remove_sucessfully';	
		}
	}

	public function getmorewishlist(request $request){
	
		$userId = auth()->guard('user')->id();

		$request['user_id']=$userId;
	
		$url = SITE_HTTP_URL_API.'getmorewishlist';
		$response=Curlpostequest($url,json_encode($request->all()));
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}else if(empty($response)){
			$getmoredata='';	
		}else{
			$getmoredata=json_decode($response);
		}
		
		$varriables = array('wishlist_data'=>$getmoredata);
		return view("frontend.product.getmorewishlist")->with($varriables)->render(); 	
	}

	public function manageQa(request $request){

		if(!empty($request['product_id']))
		{
			$product_id=$request['product_id'];
		}else{
			$product_id='';
		}
		$userId = auth()->guard('user')->id();

		$pageHeading="manageQa";
			//check question Data
				$url=SITE_HTTP_URL_API.'manageqa/'.$userId;
				$Data1=Curlgetequest($url,CURL_KEY);
				if($Data1=='failed'){
					return  redirect(route('static.errorpage'));
				}else  if($Data1=='invalid_request'){
					return redirect('/account-setting')->with('error',"Invalid Request");
				}else if($Data1=='empty'){
					$question_Data='';
				}else{
					$Data1=json_decode($Data1);
					
					$countdata=$Data1->question_count;
					$question_Data=$Data1->question_data;
				}
			//end question
			//  get title list
			$url = SITE_HTTP_URL_API.'gettitle/'.$userId;
			$response=Curlgetequest($url,CURL_KEY);
			if($response == "failed"){
				return  redirect(route('static.errorpage'));
			}
			if(!empty($response))
			{
					$products = json_decode($response);
					$product_Data= (array) $products;
			}else
			{
				$product_Data='';
			}

		// end   get title list	
		return view('frontend.product.manageQa',['product_id'=>$product_id,'count_Data'=>$countdata,'product_Data'=>$product_Data,'question_Data'=>$question_Data,'pageHeading'=>$pageHeading,'pageData'=>"manageQa",'contactData'=>"manageQa",'metaTitle'=>"manage Q&A",'metaKeywords'=>"manageQa", 'metaDescription'=>"manageQa"]);
	}

	public function getquestion($id){
		if(empty($id)){ return redirect('/account-setting')->with('error',"Invalid Request"); }
		$url=SITE_HTTP_URL_API.'getquestion/'.$id;
		$Data=Curlgetequest($url,CURL_KEY);
		if($Data=='failed'){
			return  redirect(route('static.errorpage'));
		}else  if($Data=='invalid_request'){
			return redirect('/account-setting')->with('error',"Invalid Request");
		}else{
			$question_Data=json_decode($Data);
			$data['question']=$question_Data->question_answer;
			$data['answer']=nl2br($question_Data->answered_by_owner);
			return $data;
		}
	}

	public function addanswer(request $request){
		$url = SITE_HTTP_URL_API.'addanswer';
		$response=Curlpostequest($url,json_encode($request->all()));

		if($response=='failed')
		{
			return  redirect(route('static.errorpage'));

		}else if($response=='invalid_request'){
			
			return redirect('/account-setting')->with('error',"Invalid Request");

		}else if($response=='1'){

			return '1';

		}
	}

	public function sortingqa(request $request){

		$userId = auth()->guard('user')->id();

		$request['user_id']=$userId;
		$url = SITE_HTTP_URL_API.'sortingqa';
		$response=Curlpostequest($url,json_encode($request->all()));
	
		if($response=='failed')
		{

		}else if(!empty($response)){

			$response=json_decode($response);

			$countdata=$response->question_count;
			$question_Data=$response->question_data;
		
		}else{
			$question_Data='';
			$countdata='0';
		}
		$varriables = array('question_Data'=>$question_Data,'countdata'=>$countdata);
		return view("frontend.product.sortingqa")->with($varriables)->render(); 
	}

	public function getmoreqalist(request $request){
		$userId = auth()->guard('user')->id();

		$request['user_id']=$userId;
		
		$url = SITE_HTTP_URL_API.'getmoreqalist';
		$response=Curlpostequest($url,json_encode($request->all()));
	
		if($response=='failed')
		{
			return  redirect(route('static.errorpage'));
		}else if($response=='invalid_request'){
			return redirect('/account-setting')->with('error',"Invalid Request");
		}else if(!empty($response)){
			$question_Data=json_decode($response);
			$varriables = array('question_Data'=>$question_Data);
			return view("frontend.product.getmoreqalist")->with($varriables)->render(); 
		}
	}
}