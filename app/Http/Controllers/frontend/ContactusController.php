<?php
namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Model\frontend\Contactus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
use DB;
use View;
use App;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\ServiceProvider;


class ContactusController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $contactus;

    public function __construct(Contactus $contactus){
        $this->contactus = $contactus;
    }
    
    /**
     * Contact Us Form
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){   
        $validator = Validator::make($request->all(), [
            'name'      =>  'required|max:50',
			'email'     =>  'required|email|max:100',
            'message'   =>  'required|max:2000'
            
        ]);
        $url=SITE_HTTP_URL_API.'getcontactdata';
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL =>   $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
           ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
    
        $config_data=json_decode($response);
        if(empty($config_data)){
            return  redirect(route('static.errorpage'));
        } 

        $config_data=(array) $config_data;
        $checker = captchaChecker($request->get('g-recaptcha-response'), $config_data['recaptcha_secretkey']);
        
        if(!$checker['success']){
            return redirect('/contact-us')->with('error',  __('general.Something went wrong while validating captcha, please refresh and try again'));
        }
     
        
        if ($validator->fails()) {
            return redirect()
            ->back()
            ->withErrors($validator)
            ->withInput();
        }
        else{
          
            $curl = curl_init();
            $url=SITE_HTTP_URL_API.'getcontentcontact';
            curl_setopt_array($curl, array(
              CURLOPT_URL =>  $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => array(
                'token: '.CURL_KEY.''
               ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            $getContactPage=json_decode($response);

            if(empty($getContactPage)){
                return  redirect(route('static.errorpage'));
            } 

            /* start sending email code */
            $mailData = [
                'guest_name'    => $request->get('name'),
                'guest_email'   => $request->get('email'),
                'guest_message' => nl2br(strip_tags($request->get('message'),'<script>')),
                'phone_number'   => $request->get('phone_number'),
                'mail_template' => 'contact_us_admin',
                'site_email'=>!empty($getContactPage->cms->cnt_email)?$getContactPage->cms->cnt_email:$config_data['site_email']
            ];
            
            $sendMail = Mail::send(new MailSenders($mailData));
                
            return redirect('/contact-us')->with('success', __('We received your message and will get back to you shortly.') );
        }
    }

    public function contactus(){  
        $url=SITE_HTTP_URL_API.'contact';
        
		$curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'token: '.CURL_KEY.''
            ),
		));
        $response = curl_exec($curl);
    
    	curl_close($curl);
        $getContactPage=json_decode($response);
        if(empty($getContactPage)){
			return  redirect(route('static.errorpage'));
		} 
		$pageHeading = $getContactPage->meta_title_en;
        return view('frontend.contactus.create',['pageHeading'=>$pageHeading,'pageData'=>$getContactPage,'contactData'=>$getContactPage,'metaTitle'=>$getContactPage->meta_title_en,'metaKeywords'=>$getContactPage->meta_keywords_en, 'metaDescription'=>$getContactPage->meta_desc_en]);
    }
     
}
