<?php

namespace App\Http\Controllers\frontend\Auth;

use App\Model\frontend\User;
use Illuminate\Http\Request;

use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

use Hash;
use DB;
use App;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
	
	private $gender_info; 
    public function __construct()
    {
        $this->middleware('guest');
		$this->gender_info = array('male'=>__('general.male'), 'female'=>__('general.female'),'other'=>__('general.other'));
    }

    public function signup()
    {    
    
        $curl_url=SITE_HTTP_URL_API.'signup';
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $getPage=json_decode($response);

        if(empty($getPage)){
			return  redirect(route('static.errorpage'));
		} 
        $pageHeading = "Create Your Account - Customer";
        return view('frontend/auth/signup',['pageHeading'=>$pageHeading,'pageData'=>$getPage,'metaTitle'=>$getPage->meta_title_en,'metaKeywords'=>$getPage->meta_keywords_en, 'metaDescription'=>$getPage->meta_desc_en]);
    }
	
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  request  $request
     * @return User
     */
    protected function saveRegisterForm(Request $request){
        
        $curl_url=SITE_HTTP_URL_API.'getconfigdata';
       
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL =>  $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'token: '.CURL_KEY.''
              ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $config_data=(array) json_decode($response);
            
        if(empty($config_data)){
            return  redirect(route('static.errorpage'));
        } 
        $checker = captchaChecker($request->get('g-recaptcha-response'), $config_data['recaptcha_secretkey']);

        $messages = array(
            'name.required'   => 'Please enter name',
			'email.required'        => 'Please enter email',
            'email.unique'          => 'This email is already taken. Please input another email',
            'password.required'     => 'Please enter password',
        );
        
        $rules = array(
            'name'    => 'required|max:30',
		    'email'         => 'required|email|max:100|unique:users',
            'password'      => 'required|min:6|max:30',
        );
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()) {
            return redirect($_SERVER['HTTP_REFERER'])
                            ->withErrors($validator)
                            ->withInput();
        }
        
        $input = $request->all();
       
        $input['reset_key']=md5(generateRandomString(10));
       
        // UPLOAD PROFILE IMAGE
        if ($request->hasFile('profile_image') && $request->file('profile_image')->isValid()) {
            $name = kebab_case($request->name);
            $extension = $request->profile_image->extension();
            $nameImage = $name.'-'.time().'.'.$extension;
            $input['profile_image'] = $nameImage;

            $upload = $request->profile_image->storeAs('public/user_profile_photo', $nameImage);
            
            $phpArr=array('php','php3','php4','php5','phtml');
            if(in_array($extension,$phpArr)){
                return redirect()
                        ->back()
                        ->withErrors([__('general.Please upload valid image')])
                        ->withInput();
            }
            if(strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<?php') !== false){
                return redirect()
                        ->back()
                        ->withErrors([__('general.Please upload valid image')])
                        ->withInput();
            }
            if (strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<?= ') !== false) {
                return redirect()
                        ->back()
                        ->withErrors([__('general.Please upload valid image')])
                        ->withInput();
            }
            
            if (!$upload){
                return redirect()
                        ->back()
                        ->withErrors([__('general.Something went wrong while uploading image, please try again')])
                        ->withInput();
            } 
            else{
                if($request->hasFile('profile_image')) {
                    // thumb 150 x 150 
                    $image       = $request->file('profile_image');
                    $filename    = 'thumb_'.$nameImage;
                    // $filename    = 'thumb_'.$image->getClientOriginalName();

                    list($width, $height) = getimagesize(STORAGE_IMG_ROOT.'/app/public/user_profile_photo/'.$nameImage);
                    $ratio = $width / $height;
                    $otherheight =floor (350 / $ratio);
                    $canvas = Image::canvas(350, $otherheight);
                    
                    $image = Image::make($image->getRealPath())->resize(350, $otherheight, function($constraint){
                        // $constraint->aspectRatio();
                    });
                    
                    $canvas->insert($image, 'center');
                    $canvas->save('storage/app/public/user_profile_photo/'.$filename);
                }
            }
        }
        $input['type']='client';
        $input['created_at']=date('Y-m-d H:i:s');



            $curl_url=SITE_HTTP_URL_API.'saveregister';
      
			$curl = curl_init();
			curl_setopt_array($curl, array(
                CURLOPT_URL => $curl_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'PATCH',
                CURLOPT_POSTFIELDS =>json_encode($input),
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/json',
                    'token: '.CURL_KEY.''
                ),

			));
            $response = curl_exec($curl);
            curl_close($curl);
            $Data =json_decode($response);

            if(empty($Data)){
                return  redirect(route('static.errorpage'));
            } 

        if($Data->msg_error=='0')
        {
            $link=SITE_HTTP_URL.'/user/verify/'.$input['reset_key'];
            $userName =$input['name'];
            $mailData = [
                'user_email'     => $input['email'],
                'user_name'      => $userName,
                'link'           => $link,
                'mail_template'  => 'registration_email'
            ];
        

		    $sendMail = Mail::send(new MailSenders($mailData));
            return redirect('login')->with('success', __($Data->msg));
        }
        
        if($Data->msg_error=='1')
        {
            return redirect($_SERVER['HTTP_REFERER'])->with('error', __($Data->msg)); 
        }
      
    }
	
	public function verifyUser($token){

        $curl_url=SITE_HTTP_URL_API.'verify/'.$token;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
		if($response==0){
			 return redirect('/login')->with('error', __('general.Sorry this request is not valid.'));
		}
		return redirect('/login')->with('success', __('general.Your account is activated successfully'));		 
    }

}