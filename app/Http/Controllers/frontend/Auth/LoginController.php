<?php

namespace App\Http\Controllers\frontend\Auth;
use App\Model\frontend\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use DB;
use App;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		//$this->keyword_info=;
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function getLoginForm(){   
        
        if(!empty($request->url)){
            $request->session()->put('url', $request->url);
        }else{

        }
        $pageHeading= "Login";
		$login_user_id = auth()->guard('user')->id();
        if(!empty($login_user_id)) {
            return redirect()->intended('account-setting');
        }
    
        $curl_url=SITE_HTTP_URL_API.'getlogindata';
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
           ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $getPage=json_decode($response);
      
        if(empty($getPage)){
            return  redirect(route('static.errorpage'));
        } 
        return view('frontend/auth/login',compact('pageHeading'),['pageHeading'=>$pageHeading,'pageData'=>$getPage,'metaTitle'=>$getPage->meta_title_en,'metaKeywords'=>$getPage->meta_keywords_en, 'metaDescription'=>$getPage->meta_desc_en]);

    }
    
    public function authenticate(Request $request){    
        $url = '/account-setting';
        if(!empty($_REQUEST['url'])){
            $url = $_REQUEST['url'];
        }

            $curl_url=SITE_HTTP_URL_API.'getconfigdata';
            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL =>  $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'token: '.CURL_KEY.''
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $config_data=(array) json_decode($response);
        if(empty($config_data)){
            return  redirect(route('static.errorpage'));
        } 
        $checker = captchaChecker($request->get('g-recaptcha-response'), $config_data['recaptcha_secretkey']);
       
        if(!$checker['success']){
            return redirect('/login')->with('error',  __('general.Something went wrong while validating captcha, please refresh and try again'));
        }

        $email = $request->input('email');
        $password = $request->input('password');
        // Load user from database
        $curl_url1=SITE_HTTP_URL_API.'loginauthenication';
        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url1,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PATCH',
            CURLOPT_POSTFIELDS => json_encode($email),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
                'token: '.CURL_KEY.''
            ),

        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $userData=json_decode($response);
        if(($userData=='failed')){
            return  redirect(route('static.errorpage'));
        } 
		if(count($userData)==0){
			return redirect()->intended('login')->with('status',  __('general.Invalid login details!'));
		}
        
		else if ($userData && \Hash::check($password, $userData[0]->password) && $userData[0]->email_verified=='0') {
			return redirect()->intended('login')->with('status', __('general.not_verified'));
        }
		else if ($userData && \Hash::check($password, $userData[0]->password) && $userData[0]->deleted_status=='1') {
			return redirect()->intended('login')->with('status', __('general.not_active_deleted'));
        }
		else if ($userData && \Hash::check($password, $userData[0]->password) && $userData[0]->status=='2') {
			return redirect()->intended('login')->with('status', __('Your Account is blocked by administrator'));
        }
		
        $remember_me = $request->has('remember') ? true : false;
        if (auth()->guard('user')->attempt(['email' => $email, 'password' => $password,'deleted_status'=>'0','email_verified'=>'1','status'=>'1'],$remember_me)) {
            Session::put('UserData',$userData[0]);
            return redirect()->intended($url);   
        }
        else
        {
            return redirect()->intended('login')->with('status', __('general.Invalid login details'));
        }
    }
    
    public function getLogout() {
        auth()->guard('user')->logout();
        return redirect()->intended('login');
    }
    
}
