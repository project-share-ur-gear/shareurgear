<?php

namespace Illuminate\Foundation\Auth;
namespace App\Http\Controllers\frontend\Auth;

use App\Model\frontend\User;
use Illuminate\Http\Request;

use Validator;
use App\Http\Controllers\Controller;
// use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

Use Hash;
Use DB;
Use App;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    // use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
    * Display the form to request a password reset link.
    *
    * @return \Illuminate\Http\Response
    */
    public function showLinkRequestForm()
    {
        // 'http://192.168.0.98/shareurgearApi/api/getforgotdata'
        $curl_url=SITE_HTTP_URL_API.'getforgotdata';
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL =>  $curl_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
     
        $response_Data =  (array) json_decode($response);
        if(empty($response_Data)){
            return  redirect(route('static.errorpage'));
        } 

        $getPage=$response_Data['get_page'];
        $page_info=$response_Data['page_infopage'];
        $pageHeading=$getPage->title_en;
       
        $login_user_id = auth()->guard('user')->id();
        if(!empty($login_user_id)) {
            return redirect()->intended('profile');
        }
        return view('frontend/auth/passwords/email',['pageHeading'=>$pageHeading,'pageData'=>$getPage, 'page_info'=>$page_info,'metaTitle'=>$getPage->meta_title_en,'metaKeywords'=>$getPage->meta_keywords_en, 'metaDescription'=>$getPage->meta_desc_en]);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request){
       

        $curl_url=SITE_HTTP_URL_API.'getconfigdata';

        $this->validateEmail($request);

        $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $curl_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'token: '.CURL_KEY.''
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $config_data=(array) json_decode($response);
        if(empty($config_data)){
            return  redirect(route('static.errorpage'));
        } 

        $checker = captchaChecker($request->get('g-recaptcha-response'), $config_data['recaptcha_secretkey']);
        if(!$checker['success']){
            return redirect('/forgot-password')->with('error',  __('general.Something went wrong while validating captcha, please refresh and try again'));
        }
        
        $email=$request->only('email');
        $emaill =json_encode($email['email']);
    
        

        $curl_url1=SITE_HTTP_URL_API.'getforgotpassword';
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $curl_url1,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'PATCH',
        CURLOPT_POSTFIELDS =>$emaill,
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: '.CURL_KEY.''
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $userData= json_decode($response);
        if(empty($userData)){
            return  redirect(route('static.errorpage'));
        } 

        if(count($userData)==0){
             return redirect('/forgot-password')->with('error', __('general.Email does not exist'));
        }
        $reset_key=md5(generateRandomString(10));
        $link=SITE_HTTP_URL.'/user/password/reset/'.$reset_key;
        $update = DB::table('users')->where('email','=',$email['email'])->update(array('reset_key'=>$reset_key));
        $mailData = [
            'user_email'    => $email['email'],
            'user_name'    => $userData[0]->name,
            'link' => $link,
            'mail_template' => 'reset_password'
        ];
        $sendMail = Mail::send(new MailSenders($mailData));
        return redirect('login')->with('success', __('general.Mail has been sent to your account to restore your password.'));
        
    }

    /**
     * Validate the email for the given request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateEmail(Request $request){
        $request->validate(['email' => 'required|email']);
    }
}
