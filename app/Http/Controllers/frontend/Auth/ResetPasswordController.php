<?php

namespace App\Http\Controllers\frontend\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Hash;
use DB;
use App;
use Validator;
use Illuminate\Support\Facades\Input;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
	
	public function showResetForm(Request $request, $token = null){
		$locale=App::getLocale();
        $title='title_'.$locale;
        
        $url=SITE_HTTP_URL_API.'get-reset-password';
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
        ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $Data=json_decode($response);
        $page_info=$Data->pageInfoData;
        $page_info= (array) $page_info;
        $pageData=$Data->pageData;
        if (is_null($token)) {
            return $this->getEmail();
        }
        $email = $request->input('email');

        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email'));
        }
        $metaTitle=$pageData->meta_title_en;
        $metaKeywords=$pageData->meta_keywords_en;
        $metaDescription=$pageData->meta_desc_en;
      
        if (view()->exists('frontend.auth.passwords.reset')) {
            return view('frontend.auth.passwords.reset')->with(compact('token', 'email','pageHeading','pageData','page_info','metaTitle','metaKeywords','metaDescription'));
        }

        return view('frontend.auth.reset')->with(compact('token', 'email','pageHeading','pageData','page_info'));
    }
	
	protected function resetpassword(Request $request,$token){

        $url=SITE_HTTP_URL_API.'checkresetpassword/'.$token;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
        ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $userData=json_decode($response);
      
      	// if(count($userData)==0){
		// 	 return redirect('/login')->with('error', __('general.Invalid request'));
		// }
		
		$messages = array(
            'password.required' => 'Please enter password',
			'password_confirmation.required' => 'Please enter password',
        );
        $rules = array(
            'password' => 'required|min:8|confirmed',
        );
        
        // $validator = Validator::make(Input::all(), $rules, $messages);
        
        // if ($validator->fails()) {
        //     return redirect('forgot-password')
        //                     ->withErrors($validator)
        //                     ->withInput();
        // }
       
        $input = $request->all();
        $password=Hash::make($input['password']);
        $url=SITE_HTTP_URL_API.'resetpassword/'.$token;        
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'PATCH',
          CURLOPT_POSTFIELDS =>json_encode($password),
          CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: '.CURL_KEY.''
          ),

        ));
        $response = curl_exec($curl);
        curl_close($curl);
        if($response=='0'){
            return redirect('/login')->with('error',__('general.Invalid request'));
        }else{
            return redirect('login')->with('success', __('general.You have successfully changed your password.'));
        }
    }
	
}
