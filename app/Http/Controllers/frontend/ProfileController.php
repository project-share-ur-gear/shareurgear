<?php

namespace App\Http\Controllers\frontend;

use App\Model\frontend\User;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Crypt;
use DB;
use Hash;
use App;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

use Intervention\Image\ImageManagerStatic as Image;

class ProfileController extends Controller
{
    
	public function _construct(){
		$this->locale=App::getLocale();
	}
	
	public function accountsetting(){
	
		$logged_in_user_id = auth()->guard('user')->id();	
        $pageHeading = "Profile";
        $curl = curl_init();
        $url=SITE_HTTP_URL_API.'acccount-setting/'.$logged_in_user_id;
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
          CURLOPT_HTTPHEADER => array(
            'token: '.CURL_KEY.''
        ),
        ));
        
        $response = curl_exec($curl);

        curl_close($curl);
        $userInfo =json_decode($response);

        if(empty($userInfo))
        {
            return redirect('/login')->with('error', __('invalid Request') );  
        }else{
            return view('frontend.profile.accountsetting', compact('pageHeading','userInfo'),['pageHeading'=>$pageHeading,'metaTitle'=>'profile','metaKeywords'=>'profile', 'metaDescription'=>'profile'] );
        }
    
      
	}

	public function updateprofile(Request $request){
		$userId = auth()->guard('user')->id();
		$user =  auth()->guard('user')->user();

        if(empty($request->cpss)){
            $request->cpss='0';   
        }

        $curl = curl_init();
        $url=SITE_HTTP_URL_API.'update-profile/'.$userId;

        curl_setopt_array($curl, array(
            CURLOPT_URL =>  $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode($request->all()),
            CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json',
            'token: '.CURL_KEY.''
            ),
        ));
			
		$response = curl_exec($curl);
		
        curl_close($curl);

        $data=json_decode($response);
        if(empty($data)){
            return  redirect(route('static.errorpage'));
        } 

        if($data->error=='1'){
            return redirect('/account-setting')->with('success',"Profile setting updated successfully."); 
        }
        else if($data->error=='2')
        {
            $verifyLink = SITE_HTTP_URL."/verify-email/". $data->updated_data->reset_key;
            $newMailArr = array(
                "user_name" => ucwords($data->user_data->name),
                "user_email" => $request->get('email'),
                "verify_link" => $verifyLink,
                'mail_template'	=> 'new_email_change_mail'
            );
    
            $sendEmail = Mail::send(new MailSenders($newMailArr));

            /* send email to old email holder to notify that email has been changed */
            $user_os        =   getOS();
            $user_browser   =   getBrowser();
            $user_browser_name = '';
            if(isset($user_browser['name']) and !empty($user_browser['name'])){
                $user_browser_name = $user_browser['name'].' (Version: '.$user_browser['version'].')';
            }	
            $user_os = getOS($user_browser['userAgent']);

            $ip = $_SERVER['REMOTE_ADDR'];

            $oldMailArr = array(
                "user_name" => ucwords($data->user_data->name),
                "user_email" => $data->user_data->email,
                "browser" =>  $user_browser_name,
                "os"=>$user_os,
                "ip"=>$ip,
                "changed_email"=>$request->get('email'),
                'mail_template'	=> 'old_email_changed'
            );	
        
            $sendMail = Mail::send(new MailSenders($oldMailArr));




            return redirect('/account-setting')->with('success', __('Verification Email Send to new email address please verify to change the email'));
        }else if($data->error=='0'){
            return redirect('/account-setting')->with('error', __($data->msg) );
        }else if($data->error=='3'){
            $mailData = [
                'user_name'		=> auth()->guard('user')->user()->first_name,
                'user_email'	=> auth()->guard('user')->user()->email,
                'link'			=> SITE_HTTP_URL.'/forgot-password',
                'mail_template'	=> 'change_password_email'
            ];
            $sendMail = Mail::send(new MailSenders($mailData));
            return redirect('/account-setting')->with('success', __('Your password changed successfully'));

        }

    }
    
    public function payoutsetting(request $request){
       
        $url = SITE_HTTP_URL_API.'getconfigdata';
        $response=Curlgetequest($url,CURL_KEY);
        $config_data=json_decode($response);

        $user =  auth()->guard('user')->user();

        $payLoad=array();
        $payLoad['client_id']=$config_data->plaid_client_id;
        $payLoad['secret']=$config_data->plaid_secret_id;
        $payLoad['client_name']=$user->name;
        $payLoad['user']=array('client_user_id'=>$user->name);
        $payLoad['products'] = ['auth'];
        $payLoad['country_codes'] = ['US'];
        $payLoad['language'] = 'en';
        $payLoad['webhook'] = route('static.webhookresponse');
        $payLoad=json_encode($payLoad);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://sandbox.plaid.com/link/token/create');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payLoad);
        $headers = array();
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        
        $result = curl_exec($ch);
        
        if (curl_errno($ch)) {
            return redirect('/payout-setting')->with('error','Error:' . curl_error($ch));
        }
        curl_close($ch);
        $result = json_decode($result,true);
        $link_token = $result['link_token'];
        $user =  auth()->guard('user')->user();
        $userId = auth()->guard('user')->id();
       
       
        $pageHeading="payoutsetting";
		return view(
            'frontend.profile.payoutsetting',
            [
                'client_account_id'=>$user->client_account_id,
                'stripe_account'=>$user->stripe_account,
                'pageHeading'=>$pageHeading,
                'pageData'=>"payoutsetting",
                'contactData'=>"payoutsetting",
                'metaTitle'=>"payout setting",
                'metaKeywords'=>"payout-setting", 
                'metaDescription'=>"payout-setting",
                'link_token'=>$link_token
            ]);

    }   

    private function callAPI($method, $url, $data){
		$curl = curl_init();
		switch ($method){
			case "POST":
			curl_setopt($curl, CURLOPT_POST, 1);
			if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			break;
			case "PUT":
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
			if ($data)
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);                         
			break;
			
			
			case "DELETE":
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
			break;
			
			default:
			if ($data)
			$url = sprintf("%s?%s", $url, http_build_query($data));
		}
		
		// OPTIONS:
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		// EXECUTE:
		$result = curl_exec($curl);
		if(!$result){die("Connection Failure");}
		curl_close($curl);
		return $result;
    }

    public function stripeconnect(request $request){ 
        
        $userdata = auth()->guard('user')->user();
       
        $url = SITE_HTTP_URL_API.'getconfigdata';
        $response=Curlgetequest($url,CURL_KEY);
        $config_data=json_decode($response);
        $data = $request->all();
        $respArr = array();

        if(!empty($data['plaidPubToken']) && !empty($data['plaidAccToken'])){


            if(empty($userdata->client_account_id)){
                if(empty($data['dob'])){
                    $respArr['error'] = true;
                    $respArr['msg'] = 'Please enter your date of birth, is required.';
                    echo json_encode($respArr);
                    exit;
                }
                if(empty($data['ssnumber'])){
                    $respArr['error'] = true;
                    $respArr['msg'] = 'Please enter your date of birth, is required.';
                    echo json_encode($respArr);
                    exit;
                }
                if(!is_numeric($data['ssnumber'])){
                    $respArr['error'] = true;
                    $respArr['msg'] = 'Inavlid SSN Number.';
                    echo json_encode($respArr);
                    exit;
                }

                $dob = explode('/',$data['dob']);
                $dd = $dob['0'];
                $mm = $dob['1'];
                $yy = $dob['2'];
                $ssnumber  = $data['ssnumber'];
            }


            $headers[] = 'Content-Type: application/json';
            $params = [
                'client_id' => $config_data->plaid_client_id,
                'secret' => $config_data->plaid_secret_id,
                'public_token' => $data['plaidPubToken'],
            ];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://sandbox.plaid.com/item/public_token/exchange");
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            if(!$result = curl_exec($ch)) {
                $respArr['error'] = true;
                $respArr['msg'] = curl_error($ch);
            }
            curl_close($ch);

            $jsonParsed = json_decode($result, true);
            
            if(!empty($jsonParsed['access_token'])){
                $btok_params = [
                    'client_id' => $config_data->plaid_client_id,
                    'secret' => $config_data->plaid_secret_id,
                    'access_token' => $jsonParsed['access_token'],
                    'account_id' => $data['plaidAccToken']
                ];
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://sandbox.plaid.com/processor/stripe/bank_account_token/create");
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($btok_params));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
                curl_setopt($ch, CURLOPT_TIMEOUT, 80);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                if(!$result = curl_exec($ch)) {
                    $respArr['error'] = true;
                    $respArr['msg'] = curl_error($ch);
                }
                curl_close($ch);

                $btok_parsed = json_decode($result, true);
             
            }
          
            if(!empty($btok_parsed['stripe_bank_account_token'])){
                
                $stripe = new \Stripe\StripeClient(
                    $config_data->stripe_secret_key
                );

                try 
                {
                    if(empty($userdata->client_account_id)){
                          
                        $account  = $stripe->accounts->create([
                            'type' => 'custom',
                            'country' => 'US',
                            'business_type'=>"individual",
                            'email' => $userdata->email,
                            'individual'=> [
                                'address'=>[
                                    'city'=>"Hanford",
                                    'country'=>"US",
                                    'line1'=>"225 N Irwin St",
                                    'line2'=>"Hanford, CA",
                                    'postal_code'=>"93230",
                                    'state'=>"CA",
                                ],
                                'dob'=> [
                                    'day'=>$dd,
                                    'month'=>$mm,
                                    'year'=>$yy
                                ],
                                'email'=>$userdata->email,
                                'first_name'    => $userdata->name,
                                'last_name'    => 'shareurgear',
                                'phone'         =>  '4654698521',
                                'ssn_last_4'  => $ssnumber
                               
                            ],
                            'capabilities' => [
                                'card_payments' => ['requested' => true],
                                'transfers' => ['requested' => true],
                            ],
                            'tos_acceptance' => [
                                'date' => time(),
                                'ip' => $_SERVER['REMOTE_ADDR'], // Assumes you're not using a proxy
                            ],
                            "business_profile" => array(
                                'mcc'           => '7394',    // Professional Services
                                'name'          => $userdata->name,
                                'support_url'   =>'https://www.webdemoserver.live/shareurgear/', //SITE_HTTP_URL,
                                'url'           => 'https://www.webdemoserver.live/shareurgear/', //SITE_HTTP_URL,
                            ),
                        ]);
                      
                        if(!empty($account->id)) {
                            $response = $stripe->accounts->createExternalAccount(
                                $account->id,
                                [
                                    'external_account' => $btok_parsed['stripe_bank_account_token']
                                ]
                            );
                        }    
                    }else{
                        $account  =  $stripe->accounts->update(
                            $userdata->client_account_id,[
                                'external_account'=>$btok_parsed['stripe_bank_account_token']
                            ]
                        );

                        if(!empty($account->id)){
                            $respArr['success'] = true;
                            $respArr['msg'] = 'Your payout account updated successfully';
                        }else{
                            $respArr['error'] = true;
                            $respArr['msg'] = 'Somthing went worng please try again letar.';
                        }
                        echo json_encode($respArr);
                        exit;
                    }               
                } catch (\Exception $e) {
                  
                        $respArr['error'] = true;
                        $respArr['msg'] = $e->getMessage();
                }

                if(!empty($account->id)){
                    
                    $stripe_data['client_account_id']=$account->id;
                    $stripe_data['stripe_account']='1';

                    $url = SITE_HTTP_URL_API.'stripeconnect/'.$userdata->id;

                    $response = Curlpostequest($url,json_encode($stripe_data));
                    $response = json_decode($response,true);

                    if(!empty($response)){
                        if($response['error']=='0')
                        {
                            $respArr['success'] = true;
                            $respArr['msg'] = 'Your payout account connect successfully.';
                        }else{
                            $respArr['error'] = true;
                            $respArr['msg'] = 'Somthing went worng please try again letar.';
                        }
                    }
                }
            }
            else{
                $respArr['error'] = true;
                $respArr['msg'] = 'Stripe bank account token not get.';
            }

        }else{
            $respArr['error'] = true;
            $respArr['msg'] = 'Invalid token request.';
        }
        echo json_encode($respArr);
        exit;
    }

}