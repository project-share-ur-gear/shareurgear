<?php
namespace App\Http\Controllers\frontend;

use App\Model\frontend\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use DB;
use Hash;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user;
    private $gender_info; 
    public function __construct(User $user)
    {   
        
        $this->user = $user;
        for($i=date('Y');$i>=2000;$i--){ $yearList[$i]=$i; }
        $this->year_info = $yearList;
        $this->gender_info = array('male'=> __('general.male'), 'female'=> __('general.female'),'other'=> __('general.other'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){   
        return view('home');
    }

	public function saveprofileimage(Request $request, User $user){
        //
        $logged_in_user_id = auth()->guard('user')->id();

        if (!$user_obj = $this->user->find($logged_in_user_id))
            return redirect()->back();

        $validator = Validator::make($request->all(), [
			'profile_image'=>'mimes:jpeg,jpg,png,gif',
        ]);
        if ($validator->fails()) {
            return redirect()
                        ->back()
                        // ->with('errors', ['Falha no Upload'])
                        ->withErrors($validator)
                        ->withInput();
        } else {
			
			$new_data_array=array();
			if ($request->hasFile('profile_image') && $request->file('profile_image')->isValid()) {

                // Remove image if exists
				if (Storage::exists("public/user_profile_photo/{$user_obj->profile_image}")){
					Storage::delete("public/user_profile_photo/{$user_obj->profile_image}");
					Storage::delete("public/user_profile_photo/thumb_{$user_obj->profile_image}");
				}

                // $name = kebab_case($request->name);
                $extension = $request->profile_image->extension();
                $nameImage = $request->name.'-'.time().'.'.$extension;
                $new_data_array['profile_image'] = $nameImage;

                //file store location: storage\app\products
                $upload = $request->profile_image->storeAs('public/user_profile_photo', $nameImage);
				
				$phpArr=array('php','php3','php4','php5','phtml');

				if(in_array($extension,$phpArr)){
                    return redirect()->back()->withErrors('Photo cannot be uploaded')->withInput();
				}
				if(strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<?php') !== false){
					return redirect()->back()->withErrors('Photo cannot be uploaded')->withInput();
				}
				if (strpos(file_get_contents($_FILES['profile_image']['tmp_name']), '<?= ') !== false) {
                    return redirect()->back()->withErrors('Photo cannot be uploaded')->withInput();
				}

                if (!$upload){
                    return redirect()
                            ->back()
                            ->with('errors', __('general.photo_not_uploaded'))
                            ->withInput();
                }
                else{
                    if($request->hasFile('profile_image')) {
                        // thumb 150 x 150 
                        $image       = $request->file('profile_image');
                        $filename    = 'thumb_'.$nameImage;
                        // $filename    = 'thumb_'.$image->getClientOriginalName();

						list($width, $height) = getimagesize(STORAGE_IMG_ROOT.'/app/public/user_profile_photo/'.$nameImage);
						$ratio = $width / $height;
						$otherheight =floor (350 / $ratio);
                        $canvas = Image::canvas(350, $otherheight);
                        $image  = Image::make($image->getRealPath())->resize(350, $otherheight, function($constraint)
                        {
                            //$constraint->aspectRatio();
                        });
                        $canvas->insert($image, 'center');
                        $canvas->save('storage/app/public/user_profile_photo/'.$filename);

                    }

                }
            } else {
                $new_data_array['profile_image'] = $user_obj->profile_image;
            }

           
            $url=SITE_HTTP_URL_API.'update-image/'.$logged_in_user_id;

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'PATCH',
            CURLOPT_POSTFIELDS =>json_encode($new_data_array),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: text/plain'
            ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $data=json_decode($response);
          if($data->error='0')
          {
            return redirect()->back()->with('error', __('somthing is wrong'));
          }else{

            Session::put('UserData', $data->user_data);
            return redirect()->back()->with('success', __('general.Image saved successfully'));
          }
        }
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */

    public function changepassword(User $user){
        //       
        $pageTitle = __('user.titles.changepassword_page_title');

        $logged_in_user_id = auth()->guard('user')->id();
        
        if (!$user = $this->user->find($logged_in_user_id))
            return redirect()->back();

        return view('frontend.user.changepassword', compact('pageTitle','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatepassword(Request $request, User $user){
        $logged_in_user_id = auth()->guard('user')->id();
        if (!$user_obj = $this->user->find($logged_in_user_id))
            return redirect()->back();

        $request_data = $request->All();

        $messages = [
            'current_password.required' 	=> 'Please enter current password',
            'password.required' 			=> 'Please enter password',
            'password_confirmation.same'	=> 'New and confirm password must match'
        ];

        $validator = Validator::make($request_data, [
            'current_password' 		=> 'required',
            'password' 				=> 'required|same:password',
            'password_confirmation' => 'required|same:password',     
        ], $messages);

        
        //-----------

        if ($validator->fails()) {
        	//dd($validator);

            return redirect()
                        ->back()
                        // ->with('errors', ['Falha no Upload'])
                        ->withErrors($validator)
                        ->withInput();
        } else {

            $current_password = $user_obj->password;

            if(Hash::check($request_data['current_password'], $current_password)){           
                $user_obj->password = Hash::make($request_data['password']);;
                $user_obj->save(); 

                /* 
                start sending email code 
				Also use below lines to top
				
				use App\Mail\MailSenders;
				use Illuminate\Support\Facades\Mail;
                */
		        $mailData = [
		            'user_name'		=> auth()->guard('user')->user()->first_name,
		            'user_email'	=> auth()->guard('user')->user()->email,
		            'link'			=> SITE_HTTP_URL.'/forgot-password',
		            'mail_template'	=> 'change_password_email'
		        ];
		        
		        $sendMail = Mail::send(new MailSenders($mailData));
                
                return redirect('/user-profile?type=password')->with('success', __('general.password_changed_success') );
            } 
            else {
                return redirect('/user/changepassword')->withErrors( __('general.New and confirm password must match'))->withInput();
            }

        }
      
    }
	
	public function checkoldpassword(Request $request,User $user)
    {
		if($request->ajax()){
			$logged_in_user_id = auth()->guard('user')->id();
        	$user_obj = $this->user->find($logged_in_user_id);
			$current_password=$user_obj->password;
			if(!Hash::check($request->get('current_password'), $current_password)){           
                echo json_encode( __('general.Current password is not correct.'));
            }
            else{
                echo json_encode("true");
            }

        } else {
            return redirect('/user/dashboard')->with('error', __('general.unauthorized_access') );
        }
		
    }
	
	public function checkemail(Request $request,User $user){
		if($request->ajax()){
            $user_id = false;
            $email_address = strtolower($request->get('email'));

            if(Auth::guard('user')->id()){
                $user_id = Auth::guard('user')->id();
                $user_info = DB::table('users')->where([['email',$email_address],['id','!=',$user_id]])->get()->first();
            }
            
            if(empty($user_info))
            	echo json_encode(true);	
            else
            	echo json_encode("`".$email_address."` already exists.");	
            exit();
        }
        else{
            return redirect('/user/dashboard')->with('error', __('general.unauthorized_access') );
        }
		
    }
	
	public function checkemailexist(Request $request,User $user){
		if($request->ajax()){
            $email_address = strtolower($request->get('email'));
			$user_info = DB::table('users')
				->select('*')->where('email','=',$email_address)->get();	
			if(count($user_info)>0)
				echo json_encode(true);	
			else
				echo json_encode("`".$email_address."` is not valid.");	
			exit();

        } else {
            return redirect('/user/dashboard')->with('error', __('general.unauthorized_access') );
        }
		
    }

    public function experience(Request $request){
        //dd($request->all());

        $certificateUpdate = $experienceUpdate = 0;
        /* Insert Experience values */
        $experienceDesc = $request->get('ue_desc');

        if(!is_null($experienceDesc[0])){
            $removePrev = DB::table('user_experiences')->where('ue_user_id',Auth::guard('user')->user()->id)->delete();
            foreach ($experienceDesc as $key => $desc) {
                $new_data_array= [];
                $new_data_array['ue_desc'] = strip_tags($desc);

                if($_FILES['ue_diploma']['tmp_name'][$key]==""){
                    $new_data_array['ue_diploma']           = $request->get('ue_diploma_o')[$key];
                    $new_data_array['ue_diploma_original']  = $request->get('ue_diploma_ori')[$key];
                }
                else{
                    $name = kebab_case(rand(100000,999999999));
                    $extension = $request->ue_diploma[$key]->extension();
                    $nameImage = $name.'-'.time().'.'.$extension;
                    
                    $phpArr=array('php','php3','php4','php5','phtml');
                    $upload = $request->ue_diploma[$key]->storeAs('public/experience', $nameImage);
                    if(in_array($extension,$phpArr)){
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if(strpos(file_get_contents($_FILES['ue_diploma']['tmp_name'][$key]), '<?php') !== false){
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if (strpos(file_get_contents($_FILES['ue_diploma']['tmp_name'][$key]), '<?= ') !== false) {
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if(strpos(file_get_contents($_FILES['ue_diploma']['tmp_name'][$key]), '<? ') !== false) {
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if (!$upload){
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    } 
                    else {
                        $originalName=$_FILES['ue_diploma']['name'][$key];
                        
                        $new_data_array['ue_diploma'] = $nameImage;
                        $new_data_array['ue_diploma_original'] = $originalName;
                    }
                }

                $new_data_array['ue_user_id'] = Auth::guard('user')->user()->id;
                
                /* insert code goes here */
                $insertExperience = DB::table('user_experiences')->insert($new_data_array);
            }
            $experienceUpdate = 1;
        }
        /* Insert Certificate values */
        $certificateDesc = $request->get('uc_certificate_title');
        

        if(!is_null($certificateDesc[0])){
            $removePrev = DB::table('user_certification')->where('uc_user_id',Auth::guard('user')->user()->id)->delete();
            foreach ($certificateDesc as $key => $title) {
                $new_data_array= [];
                $new_data_array['uc_certificate_title'] = strip_tags($title);
                
                if($_FILES['uc_certificate_photo']['tmp_name'][$key]==""){
                    $new_data_array['uc_certificate_photo'] = $request->get('uc_certificate_photo_o')[$key];
                    $new_data_array['uc_certificate_photo_original'] = $request->get('uc_certificate_photo_ori')[$key];
                }
                else{
                    $name = kebab_case(rand(100000,999999999));
                    $extension = $request->uc_certificate_photo[$key]->extension();
                    $nameImage = $name.'-'.time().'.'.$extension;
                    
                    $phpArr=array('php','php3','php4','php5','phtml');
                    $upload = $request->uc_certificate_photo[$key]->storeAs('public/certificates', $nameImage);
                    if(in_array($extension,$phpArr)){
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if(strpos(file_get_contents($_FILES['uc_certificate_photo']['tmp_name'][$key]), '<?php') !== false){
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if (strpos(file_get_contents($_FILES['uc_certificate_photo']['tmp_name'][$key]), '<?= ') !== false) {
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if(strpos(file_get_contents($_FILES['uc_certificate_photo']['tmp_name'][$key]), '<? ') !== false) {
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    }
                    if (!$upload){
                        return redirect()->back()->withErrors(__('general.Picture cannot be uploaded'))->withInput();
                    } 
                    else {
                        $originalName=$_FILES['uc_certificate_photo']['name'][$key];
                        
                        $new_data_array['uc_certificate_photo'] = $nameImage;
                        $new_data_array['uc_certificate_photo_original'] = $originalName;
                    }
                }

                $new_data_array['uc_user_id'] = Auth::guard('user')->user()->id;

                /* insert code goes here */
                $insertExperience = DB::table('user_certification')->insert($new_data_array);
            }

            $certificateUpdate = 1;
        }

        return redirect('/user-profile?type=experience')->with('success',  __('general.Profile updated successfully'));
    }

    /**---------- */
    public function verifiedemail(Request $request, $key){ 
        if($key==''){
            return redirect('/');
        }
        $logged_in_user_id = auth()->guard('user')->id();
        $user_obj = $this->user->find($logged_in_user_id);


       
        $url=SITE_HTTP_URL_API.'verifiedemail/'.$key;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        $user_info=json_decode($response);
        
        if(empty($user_info)){
            return redirect('/')->with('error', __('general.unauthorized_access') );                
        }else{

        $data_to_update = array();
        $data_to_update['email'] = $user_info->email_updated;
        $data_to_update['reset_key'] = NULL;
        $data_to_update['email_updated'] = NULL;
        $user_update =   DB::table('users')->where('id', $user_info->id)->update($data_to_update);
        auth()->guard('user')->logout();
        return redirect('login')->with('success', __('Email address is verified') );

        }
    }
}
