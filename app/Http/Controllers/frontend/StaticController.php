<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use DB;
use Hash;
use App;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class StaticController extends Controller
{
	public function _construct(){
		$this->locale=App::getLocale();
	}

	public function index(){

		$url=SITE_HTTP_URL_API.'get-pages/home_page';
		$curl = curl_init(); 
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLINFO_HEADER_OUT => true,
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));
		$getPageresponse = curl_exec($curl);
		$headerSent = curl_getinfo($curl, CURLINFO_HEADER_OUT);
		curl_close($curl);

		$getPage=json_decode($getPageresponse);
		
		if(empty($getPage)){ return  redirect(route('static.errorpage')); } 
		
		$getPage=$getPage[0];
		
		$url1 = SITE_HTTP_URL_API.'get-letest-product';
		$product_Data = Curlgetequest($url1,CURL_KEY);
		if($product_Data=='failed'){ return  redirect(route('static.errorpage')); } 
		if(!empty($product_Data)){ $product_Data=json_decode($product_Data); }else{ $product_Data=''; }
		$pageHeading = $getPage->meta_title_en;

		$getUrl = SITE_HTTP_URL_API.'get-recents-bookings';
		$getRecentBookings = json_decode($this->getCurl($getUrl),true);

		return view('welcome',[
			'pageData'=>$getPage,
			'pageHeading'=>$pageHeading,
			'metaTitle'=>$getPage->meta_title_en,
			'metaKeywords'=>$getPage->meta_keywords_en,
			'metaDescription'=>$getPage->meta_desc_en,
			'product_Data'=>$product_Data,
			'getRecentBookings'=>$getRecentBookings
		]);
	}
	
	public function about(){
		$url=SITE_HTTP_URL_API.'about';
		$getPage=Curlgetequest($url,CURL_KEY);
		$getPage=json_decode($getPage);
		if(empty($getPage)){
			return  redirect(route('static.errorpage'));
		}
    	$pageHeading = $getPage->meta_title_en;
		return view('frontend.static.about',['pageHeading'=>$pageHeading,'pageData'=>$getPage,'contactData'=>$getPage,'metaTitle'=>$getPage->meta_title_en,'metaKeywords'=>$getPage->meta_keywords_en, 'metaDescription'=>$getPage->meta_desc_en]);
	}

	public function faq(){
		$url=SITE_HTTP_URL_API.'getfaq';
		$response=Curlgetequest($url,CURL_KEY);
		$response=json_decode($response);

		if(empty($response)){
			return  redirect(route('static.errorpage'));
		}
      	$Data= (array) $response;
		return view('frontend.static.faq',['allFaqs'=>$Data['Faq_Data'],'pageData'=>$Data['faq_content'],'pageHeading'=>$Data['faq_content']->meta_title_en,'metaTitle'=>$Data['faq_content']->meta_title_en,'metaKeywords'=>$Data['faq_content']->meta_keywords_en, 'metaDescription'=>$Data['faq_content']->meta_desc_en]);
	}
	
	public function termsconditions(){
		$url=SITE_HTTP_URL_API.'termsconditions';
		$response=Curlgetequest($url,CURL_KEY);
		$getPage=json_decode($response);
		if(empty($getPage)){
			return  redirect(route('static.errorpage'));
		}
		$pageHeading = $getPage->meta_title_en;
		return view('frontend.static.termsconditions',['getPage'=>$getPage,'pageHeading'=>$pageHeading,'metaTitle'=>$getPage->meta_title_en,'metaKeywords'=>$getPage->meta_keywords_en, 'metaDescription'=>$getPage->meta_desc_en]);
	}
	
	public function contact(){
		
		$url=SITE_HTTP_URL_API.'contact';
		$response=Curlgetequest($url,CURL_KEY);
		$getContactPage=json_decode($response);
		if(empty($getContactPage)){
			return  redirect(route('static.errorpage'));
		}
		$pageHeading = $getContactPage->meta_title_en;
		return view('frontend.static.contact',['pageHeading'=>$pageHeading,'pageData'=>$getContactPage,'contactData'=>$getContactPage,'metaTitle'=>$getContactPage->meta_title_en,'metaKeywords'=>$getContactPage->meta_keywords_en, 'metaDescription'=>$getContactPage->meta_desc_en]);
	
	}

	public function browserental(request $request){
	
		$center_lat = !empty($request->center_lat)?$request->center_lat:"";
		$center_lng = !empty($request->center_lng)?$request->center_lng:"";
		$zoom = !empty($request->zoom)?$request->zoom:"";

		if(!empty($request->loc_lat) && !empty($request->loc_long)){
			$loc_lat  = $request->loc_lat;
			$loc_long  = $request->loc_long;
		}else{
			$loc_lat  = '';
			$loc_long  = '';
		}


		// end all location
		$sub_category=$request->get('sub_cat');
		$price=$request->get('price_details');
		$location = trim(strip_tags($request->get('location')));
		$keyword = trim(strip_tags($request->get('keyword')));
		$category = trim(strtolower(strip_tags($request->get('category'))));
		$explode_data = explode('?',$_SERVER['REQUEST_URI']);	
		if(!empty($explode_data[1])){ $get_Data=$explode_data[1]; } else { $get_Data=''; }

		$sub_category_data='';
		if(!empty($category)){
			$url=SITE_HTTP_URL_API.'sub-category/'.$category;
			$response=Curlgetequest($url,CURL_KEY);
			
			if(!empty($response)){
				$sub_category_data   = json_decode($response);
			}
			else{
				$sub_category_data='';
			}
		}	

		// searching ......
		$url = SITE_HTTP_URL_API.'browse-rental?'.$get_Data;
		$response=Curlgetequest($url,CURL_KEY);
		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}else if($response=='empty'){
			$cat_Data='';
		}else if(empty($response)){
			$product_Data='';
		}else{
			$product_Data=json_decode($response);
		}
		//get lat long 

		$propertylatlong = array();
		$latlngdata = array();
		if(!empty($product_Data)){
			foreach ($product_Data as $PKey => $Pval) {
				$propertylatlong[$PKey] = array();
				$propertylatlong[$PKey]['lat'] = $Pval->p_latitude;
				$propertylatlong[$PKey]['lng'] = $Pval->p_longitude;
				$propertylatlong[$PKey]['product_id'] = $Pval->product_id;
				$latlngdata[] = implode(",",$propertylatlong[$PKey]);
			}
		}
		//end lat long

		// end searching

		// get sub catagory
			$url=SITE_HTTP_URL_API.'get-sub-cat';
			$response=Curlgetequest($url,CURL_KEY);
			if(empty($response)){
				return  redirect(route('static.errorpage'));
			}else if($response=='empty'){
				$cat_Data='';
			}else{
				$cat_Data=json_decode($response);
			}
		// end sub catagory
		
		$pageHeading="browserental";
		return view(
			'frontend.static.browserental',
			[
				'sub_category_data'=>$sub_category_data,
				'sub_category'=>$sub_category,
				'keyword'=>$keyword,
				'center_lat'=>$center_lat,
				'center_lng'=>$center_lng,
				'zoom'=>$zoom,
				'loc_lat'=>$loc_lat,
				'loc_long'=>$loc_long,
				'location'=>$location,
				'price'=>$price,
				'sub_category'=>$sub_category,
				'latlngdata'=>$latlngdata,
				'product_Data'=>$product_Data,
				'pageHeading'=>$pageHeading,
				'pageData'=>"browserental",
				'contactData'=>"browserental",
				'metaTitle'=>"Browse Rental",
				'metaKeywords'=>"browse-rental", 
				'metaDescription'=>"browse-rental",
				'sub_cat_Data'=>$cat_Data,
				'browse_controller'=>'browse_controller',
				'category'=>$category]);
	}


	// 3rd Milestone
	public function userprofile($id){
		$user = auth()->guard('user')->user();
		if(!empty($user->id)){ $logged_user_id = $user->id; } else { $logged_user_id='0'; }

		$userID  = decrypt($id);
		$url = SITE_HTTP_URL_API.'userprofiledata/'.$userID;
		$response = Curlgetequest($url,CURL_KEY);

		if($response=='failed'){
			return  redirect(route('static.errorpage'));
		}else if($response=='invalid_request'){
			return redirect('/')->with('error',"Invalid Request."); 
		}else{

			$data=json_decode($response);
			$Count_data=$data->count_manage_Data;
			$product_data=$data->product_Data;
			$user_data=$data->user_Data;
		}

        $limit = 0;
		$offset = 0;
		$bookingsData = $this->getBookings($userID,$offset,$limit,'sharer');
		$totalOrders = $bookingsData['totalOrders'];

		$getConfigs = array_column(json_decode($this->getCurl(SITE_HTTP_URL_API.'get-configs')),'value','config_key');

		$pageHeading="userprofile";
		return view('frontend.static.userprofile',['getConfigs'=>$getConfigs,'totalOrders'=>$totalOrders,'logged_user_id'=>$logged_user_id,'user_id'=>$userID,'Count_data'=>$Count_data,'product_data'=>$product_data,'user_data'=>$user_data,'pageHeading'=>$pageHeading,'pageData'=>"userprofile",'contactData'=>"user-profile",'metaTitle'=>"user-profile",'metaKeywords'=>"user-profile", 'metaDescription'=>"user-profile"]);
	}
		
    public function getBookings($id,$offSet,$limit,$type,$date=false,$status=false){
		$url = SITE_HTTP_URL_API.'get-my-booking/'.$id.'?type='.$type.'&limit='.$limit.'&offset='.$offSet.'&date='.$date.'&status='.$status;
		$getBookings = $this->getCurl($url);
		return json_decode($getBookings,true);
	}

	public function rentaldetailpage($id){

		$wishlist_data='0';
		$user =  auth()->guard('user')->user();

		// for get product data	
			$url=SITE_HTTP_URL_API.'rentaldetails/'.$id;
			$response=Curlgetequest($url,CURL_KEY);
			if($response=='failed'){
				return  redirect(route('static.errorpage'));	
			}else if($response=='invalid_request'){
				return redirect('/browse-rental')->with('error',"Invalid Request");
			}
			$product_data=json_decode($response);
		// product data end 

		// check wishlist data
			if(!empty($user->id)){

				foreach((array)$user as $k=>$value){
					if(isset($value['id']) && !empty($value['id'])){
						$UserData = $value;
					}
				}

				session()->put('UserData',$UserData);

				$url=SITE_HTTP_URL_API.'checkaddwishlist/'.$id.'/'.$user->id;
				$Data=Curlgetequest($url,CURL_KEY);
				if(empty($Data)){
					return  redirect(route('static.errorpage'));
				}else  if($Data=='invalid_request'){
					return redirect('/browse-rental')->with('error',"Invalid Request");
				}else if($Data=='empty'){
					$wishlist_data='0';
				}else{
					$wishlist_data='1';
				}
			}
		// end wishlist data

	
		//check question Data
			$url=SITE_HTTP_URL_API.'checkquestion/'.$id;
			$Data1=Curlgetequest($url,CURL_KEY);
			if(empty($Data1)){
				return  redirect(route('static.errorpage'));
			}else  if($Data1=='invalid_request'){
				return redirect('/browse-rental')->with('error',"Invalid Request");
			}else if($Data1=='empty'){
				$question_Data='0';
			}else{
				$Data1=json_decode($Data1);
				$question_Data=$Data1->data;
				$question_Data_count=$Data1->count;
			}


		//end question	
		// getunavailable dates
			$url=SITE_HTTP_URL_API.'getunavailable/'.$id;
			$response=Curlgetequest($url,CURL_KEY);
		
			if($response=='failed'){
				return  redirect(route('static.errorpage'));
			}else  if($response=='invalid_request'){
				return redirect('/browse-rental')->with('error',"Invalid Request");
			}else if($response=='empty'){
				$alldisavbledates='0';
			}else{
				$alldisavbledates=json_decode($response);
			}
		// end getunavailable dates

				


		$pageHeading="rentaldetailpage";
		return view('frontend.static.rentaldetailpage',[
			'question_Data_count'=>$question_Data_count,
			'alldisavbledates'=>$alldisavbledates,
			'question_Data'=>$question_Data,
			'wishlist_data'=>$wishlist_data,
			'product_data'=>$product_data,
			'pageHeading'=>$pageHeading,
			'pageData'=>"rentaldetailpage",
			'contactData'=>"rentaldetailpage",
			'metaTitle'=>"Rental Detail Page",
			'metaKeywords'=>"rentaldetailpage",
			'metaDescription'=>"rentaldetailpage"
		 
		]);
	}

	public function errorpage(){
		$pageHeading="errorpage";
		return view('frontend.static.errorpage',['pageHeading'=>$pageHeading,'pageData'=>"errorpage",'contactData'=>"errorpage",'metaTitle'=>"404 page",'metaKeywords'=>"errorpage", 'metaDescription'=>"errorpage"]);
	}

	public function getbounddata(request $request){
		if($request->ajax()){
				$url=SITE_HTTP_URL_API.'getbounddata';
				$curl = curl_init();
				curl_setopt_array($curl, array(
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'post',
				CURLOPT_POSTFIELDS =>json_encode($request->all()),
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/json',
					'token: '.CURL_KEY.''
				),
				));
				$response = curl_exec($curl);
				curl_close($curl);
				if($response=='failed'){
					return  redirect(route('static.errorpage'));
				}else if(empty($response)){
					$Data_show='';
					$Count_data='';
				}else if(!empty($response))
				{

					$product_Data=json_decode($response);
					$Data_show=$product_Data->show_Data;
					
					$Count_data=$product_Data->Count_data;
				}
		
				$varriables = array('product_Data'=>$Data_show,'Count_data'=>$Count_data);
				return view("frontend.static.getbounddata")->with($varriables)->render(); 
		}
	
	}

	public function getmoreproduct(request $request){
	
		if($request->ajax()){
			$url=SITE_HTTP_URL_API.'getmoreproduct';
			$curl = curl_init();
			curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'post',
			CURLOPT_POSTFIELDS =>json_encode($request->all()),
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/json',
				'token: '.CURL_KEY.''
			),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			if($response=='failed'){
				return  redirect(route('static.errorpage'));
			}else if(empty($response)){
				$product_Data='';
			}else if(!empty($response))
			{

				$product_Data=json_decode($response);
				
			}
			$varriables = array('product_Data'=>$product_Data);
			return view("frontend.static.getmoreproduct")->with($varriables)->render(); 
		}
	}

	public function addremovewishlist($produt_id){
		$user =  auth()->guard('user')->user();
		$url=SITE_HTTP_URL_API.'addremovewishlist/'.$produt_id.'/'.$user->id;
		$Data=Curlgetequest($url,CURL_KEY);
		if(empty($Data)){
			return  redirect(route('static.errorpage'));	
		}else if($Data=='invalid_request'){
			return 'invalid_request';

		}else if($Data=='added_sucessfully'){
			return 'added_sucessfully';
		}else{
			return 'remove_sucessfully';
		}
	}
	
	public function askquestion(request $request){
		$user =  auth()->guard('user')->user();
		if(empty($user->id)){
			return 'login_first';
		}else{
			$data = $request->all();
			$data['user_id'] = $user->id;
			$url=SITE_HTTP_URL_API.'addedaskquestion';
			$Data=Curlpostequest($url,json_encode($data));
			return $Data; 
		}
	}

	public function getmoreuserprofilelist(request $request){

		$url=SITE_HTTP_URL_API.'getmoreuserprofilelist';
		$Data=Curlpostequest($url,json_encode($request->all()));
		
		$varriables = array('product_data'=>json_decode($Data));
		return view("frontend.static.getmoreuserprofilelist")->with($varriables)->render(); 
	}
	
	public function getmorequestion(request $request){
		$url=SITE_HTTP_URL_API.'getmorequestion';
		$Data=Curlpostequest($url,json_encode($request->all()));
		$question_Data=json_decode($Data);
		$varriables = array('question_Data'=>$question_Data);
		return view("frontend.static.getmorequestion")->with($varriables)->render(); 

	}

	public function getmessagetotalcount(){	
		$res=array();
		$user =  auth()->guard('user')->user();
		if(empty($user->id)){
			$res['error']=true;
			$res['msg']='user not found';
		}
		else{
			$url = SITE_HTTP_URL_API.'getmessagetotalcount/'.$user->id;
			$response=Curlgetequest($url,CURL_KEY);
	
			if(!empty($response)){
				if($response=='invalid_request'){
					$res['error']=true;
					$res['msg']='user not found';
				}else{
					$res['success']=true;
					$res['msg']='Count Notification';
					$res['count']=$response;
				}
			}else{
				$res['success']=true;
				$res['msg']='Count Notification';
				$res['count']="0";
			}
		}
		echo json_encode($res);
		exit();
	}

	/** Curls */
	public function getCurl($url){

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return $response;
	}

	public function postCurl($url,$postData){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>$postData,
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);
	
		curl_close($curl);
		return $response;
	}
}