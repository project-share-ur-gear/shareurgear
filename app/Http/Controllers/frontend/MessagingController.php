<?php

namespace App\Http\Controllers\frontend;

use App\Model\frontend\User;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File; 
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Str;
use DB;
use Hash;
use App;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

use Uploader;
class MessagingController extends Controller
{
	public function _construct(){
		$this->locale = App::getLocale();
	}

	public function messaging($id=false){

	
		$user =  auth()->guard('user')->user();
		if(empty($user))
		{
			return redirect('/')->with('success', __('Invalid Request') );
		}
		$loggedInUserId = 0;
		$loggedInUserData = '';
		if(isset($user->id) and !empty($user->id)){
			$loggedInUserId =$user->id;
			$loggedInUserData = $user;
		}
		if(!empty($id)){
			$user_id  = decrypt($id);
			if(isUserExists($user_id)=='0'){
				return redirect('/account-setting')->with('error', __('This Account is blocked by administrator') );
			}



		}else{
			$user_id  ='';
		}
		if($user_id==$loggedInUserId){
			return redirect('/account-setting')->with('error', __('Unauthorized access!!') );
		}else if(!empty($user_id)){
			// check requested user is available or not
			if(isUserExists($user_id)=='0'){
				return redirect('/account-setting')->with('error', __('Unauthorized access!!') );
			}
		}
		$pageHeading="messaging";
		return view('frontend.messaging.messaging',['get_userid'=>$user_id,'loggedInUserData'=>$loggedInUserData,'pageHeading'=>$pageHeading,'pageData'=>"messaging",'contactData'=>"messaging",'metaTitle'=>"messaging",'metaKeywords'=>"messaging", 'metaDescription'=>"messaging"]);
	}

	public function messagecontactslist(request $request,$id=false){

		$user =  auth()->guard('user')->user();
		if(empty($user->id)){
			return redirect('/')->with('error', __('invalid Access') );
		}

		$url = SITE_HTTP_URL_API.'messagecontactslist/'.$id;
		$response = Curlpostequest($url,json_encode($request->all()));
		$getContactlist = json_decode($response);

		if(isset($getContactlist->status)){
			if($getContactlist->status==200){
				if(!empty($getContactlist->data)){	
					foreach($getContactlist->data as $key => $row){
						$profileImage = FRONT_IMG.'/nophoto.png';
						if($row->profile_image!='')
						$profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$row->profile_image);
						$row->profile_image = $profileImage;
					}
				}
			}
		}
		
		echo json_encode($getContactlist);
		exit;
	}


	public function getusermesssage(request $request,$id=false){

		$user =  auth()->guard('user')->user();

		if(empty($user->id)){
			return redirect('/')->with('error', __('invalid Access') );
		}

		$url = SITE_HTTP_URL_API.'getusermesssage/'.$id;
		$response=Curlpostequest($url,json_encode($request->all()));

		$Getmessages=json_decode($response);

		if(isset($Getmessages->status))
		{
			if(!empty($Getmessages->data) and count($Getmessages->data) > 0){
				foreach($Getmessages->data as $msg_key => $msg_data){
					// prepare receiver data
					$profileImage=FRONT_IMG.'/nophoto.png';
					if($msg_data->profile_image!='')
					$profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$msg_data->profile_image);
					$msg_data->profile_image=$profileImage;
					$msg_data->message_time = date('c',strtotime($msg_data->message_time));
					$msg_data->message=makeClickableLink(nl2br($msg_data->message));
				}
			}
			echo json_encode($Getmessages);
		}
		exit();

	}

	public function postnewmsg(request $request,$id=false){	
		$user =  auth()->guard('user')->user();
		if(empty($user->id)){
			return redirect('/')->with('error', __('invalid Access') );
		}

		if(isUserExists($request['activeClientId'])=='0'){
			$redirect_url = SITE_HTTP_URL.'/messaging';
			$msg = "Your message cant be Posted";
			echo json_encode(array("status" => 441,"message" => '', "data" => array('redirect_url'=>$redirect_url) ));
			exit;
		}
		
		$url = SITE_HTTP_URL_API.'postnewmsg/'.$id;
		$response=Curlpostequest($url,json_encode($request->all()));
		$Getmessages=json_decode($response);
		$newMessage=array();
			if(!empty($Getmessages->data) and count($Getmessages->data) > 0){
			foreach($Getmessages->data as $msg_key => $msg_data){
				// prepare receiver data
				$profileImage=FRONT_IMG.'/nophoto.png';
				if($msg_data->profile_image!='')
				$profileImage=SITE_HTTP_URL.Storage::url('app/public/user_profile_photo/thumb_'.$msg_data->profile_image);
				$newMessage['profile_image']=$msg_data->profile_image=$profileImage;
				$newMessage['message_time']=$msg_data->message_time = date('c',strtotime($msg_data->message_time));
				$newMessage['message']=$msg_data->message=makeClickableLink(nl2br($msg_data->message));
				$newMessage['message_id']= $msg_data->message_id;
				$newMessage['to_whom'] = $msg_data->to_whom;
				$newMessage['who_message'] = $msg_data->who_message;
				$newMessage['user_id'] = $msg_data->user_id;
					
			}
			echo json_encode(array("status" => 200,"message" => "ok", "data" => $newMessage));
			exit;
		}
		return json_encode($Getmessages);
	}
}