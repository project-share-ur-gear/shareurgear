<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use DB;
use Hash;
use App;
use Uploader;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;


require_once ROOT_PATH."/vendor/twilio-php-master/Twilio/autoload.php";

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;


class BookingController extends Controller{

	public function _construct(){
		$this->locale=App::getLocale();
	}

	public function getbookingform(Request $request){

		$logged_in_user_id = auth()->guard('user')->id();
		$loggedUser = session()->get('UserData');

		if($request->ajax()){
			
			$data = $request->all();

			try {
				$proId = decrypt($data['proid']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid product Id.';
				echo json_encode($resp);exit;
			}
			
			$getProduct = $this->getProduct($proId);

			if($getProduct['status']){
				
				if($getProduct['productData']['product_added_by']==$logged_in_user_id){
					$resp['status'] = false;
					$resp['msg'] = 'Please go to another product, you can not booked own product.';
					echo json_encode($resp);exit;
				}

				$getConfig  = array_column($getProduct['getConfigData'],'value','config_key');

				return view('frontend.booking.getform',compact('getProduct','getConfig','loggedUser'));

			}else{
				echo json_encode($getProduct);exit;
			}
		}else{
			return redirect('/');
		}
		exit;
	}

	public function getbookingdata(Request $request){
		if($request->ajax()){
			$data = $request->all();

			try {
				$proId = decrypt($data['product']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid product.';
				echo json_encode($resp);exit;
			}

			$getProduct = $this->getProduct($proId);
			if($getProduct['status']){
				
				$configData  = array_column($getProduct['getConfigData'],'value','config_key');

				$getEstimate = $this->getBookingEstimate($data,$getProduct);
				$getEstimate['stripe_public_key'] = $configData['stripe_public_key'];

				echo json_encode($getEstimate);exit;
			}else{
				echo json_encode($getProduct);exit;
			}
		}
		exit;
	}

	public function addbooking(Request $request){
		if($request->ajax()){
			$logged_in_user_id = auth()->guard('user')->id();
			$loggedUser = session()->get('UserData');

			//Payment Process and add Booking
			require_once(ROOT_PATH.'/vendor/stripe/vendor/autoload.php');
			$data = $request->all();

			try {
				$proId = decrypt($data['product']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid product.';
				echo json_encode($resp);exit;
			}

			$getProduct = $this->getProduct($proId);
			if($getProduct['status']){
				$rangeOfDates = $getProduct['rangeOfdates'];
				$configData  = array_column($getProduct['getConfigData'],'value','config_key');
				$getEstimate = $this->getBookingEstimate($data,$getProduct);
				$stripe = new \Stripe\StripeClient($configData['stripe_secret_key']);
				$getProductData = $getProduct['productData'];


				$bookingData['booking_user_id'] = $logged_in_user_id;
				$bookingData['booking_product_id'] = $proId;
				$bookingData['booking_start_date'] = date('Y-m-d',strtotime($getEstimate['startDate']));
				$bookingData['booking_end_date'] = date('Y-m-d',strtotime($getEstimate['endDate']));
				$bookingData['booking_location_id'] = (!empty($data['pickup_location']) && isset($data['pickup_location']))?$data['pickup_location']:'custom';
				$bookingData['booking_location'] = $getEstimate['location'];
				$bookingData['booking_location_lat'] = $getEstimate['locationLat'];
				$bookingData['booking_location_long'] = $getEstimate['locationLong'];
				$bookingData['booking_extra_items'] = (!empty($data['extra_item']))?implode(',',$data['extra_item']):NULL;
				$bookingData['booking_price'] = $getEstimate['bookingPrice'];
				$bookingData['booking_distance_price'] = $getEstimate['distancePrice'];
				$bookingData['booking_extra_item_price'] = $getEstimate['extraItem'];
				$bookingData['booking_site_price'] = $getEstimate['siteFee'];
				$bookingData['booking_total_price'] = $getEstimate['totalPrice'];
				$bookingData['booking_created'] = date('Y-m-d');

				if(empty($bookingData['booking_start_date']) ||empty($bookingData['booking_end_date'])){
					$resp['status'] = false;
					$resp['msg'] = 'Invalid request, your request is not valid.';
					echo json_encode($resp);exit;
				}

				if(!empty($rangeOfDates)){
					foreach($rangeOfDates as $k=>$value){
						if($bookingData['booking_start_date'] <= $value && $bookingData['booking_end_date'] >= $value){
							$resp['status'] = false;
							$resp['msg'] = 'You can not booked on selected days is not available.';
							echo json_encode($resp);exit;
						}
					}
				}

				$addBooking = $this->addBookingData($bookingData);
				
				if($addBooking['status']){
					$approveMsg = '';
					$bookingId = $addBooking['bookingId'];
					$stripe_cust_account = $loggedUser['stripe_cust_account_id'];
					
					if(isset($data['stripeToken']) && !empty($data['stripeToken'])){
						
						$stripeToken = $data['stripeToken'];
						
						if(!empty($stripe_cust_account) && !empty($stripeToken)){
							try{
								
								$customers = $stripe->customers->update(
									$stripe_cust_account,
									['source' => $stripeToken]
							  	);
								$chargeData['stripe_cust_account'] = $customers->id;
								$chargeData['booking_user_id'] = $logged_in_user_id;

							}catch(\Exception $e){
								
								$url = SITE_HTTP_URL_API.'delete-booking/'.$bookingId;
								$deleteBooking = $this->getCurl($url);

								$errorMsg = $e->getMessage();
								$resp['status'] = false;
								$resp['msg'] = $errorMsg;
								echo json_encode($resp);exit;
							}
						}else{
							try{

								$customers = $stripe->customers->create([
									'name' => $loggedUser['name'],
									'email' => $loggedUser['email'],
									'source' => $stripeToken,	
									'description' => 'Create customers',
								]);

								$stripe_cust_account = $customers->id;
								
								$chargeData['stripe_cust_account'] = $stripe_cust_account;
								$chargeData['booking_user_id'] = $logged_in_user_id;
							}catch(\Exception $e){

								$url = SITE_HTTP_URL_API.'delete-booking/'.$bookingId;
								$deleteBooking = $this->getCurl($url);

								$errorMsg = $e->getMessage();
								$resp['status'] = false;
								$resp['msg'] = $errorMsg;
								echo json_encode($resp);exit;
							}
							
						}
					}

					try{
						$payLoad = [
							'amount' => ($getEstimate['totalPrice']*100),
							'currency' => 'usd',
							'customer' => $stripe_cust_account,
							'description' => 'Product Rent Charge for Booking Id: '.$bookingId,
						];

						if($getProductData['booking_autoapproval_status']=='0'){
							$payLoad['capture'] = false;

							$chargeData['booking_approve_status'] = 0;
							$approveMsg = ' please wait for booking approve response.';
						}
						
						$charge = $stripe->charges->create($payLoad);

					}catch(\Exception $e){
						$errorMsg = $e->getMessage();
						
						$url = SITE_HTTP_URL_API.'delete-booking/'.$bookingId;
						$deleteBooking = $this->getCurl($url);

						$resp['status'] = false;
						$resp['msg'] = $errorMsg;
						echo json_encode($resp);exit;
					}

					if(!empty($charge->id)){
						$chargeData['booking_charge_id'] = $charge->id;
						$chargeData['booking_status'] = 1;

						$updateBooking = $this->addBookingData($chargeData,$bookingId);

						if($updateBooking['status']){

							$txnData['txn_booking_id'] = $bookingId;
							$txnData['txn_type'] = 'charge';
							$txnData['txn_amount'] = $getEstimate['totalPrice'];
							$txnData['txn_rrc_id'] = $charge->id;

							$url = SITE_HTTP_URL_API.'add-transcation';
							$addTxn = $this->postCurl($url,$txnData);


							$getProductOwner = json_decode($this->getCurl(SITE_HTTP_URL_API.'get-user/'.$getProductData['product_added_by']));
							$getAdmin = json_decode($this->getCurl(SITE_HTTP_URL_API.'get-user/admin'));
							
							$userMailData['name'] = $loggedUser['name'];
							$userMailData['email'] = $loggedUser['email'];
							$userMailData['message'] = 'Your Booking successfully booked on '.$bookingData['booking_start_date'].' to '.$bookingData['booking_end_date'].' for product '.$getProductData['product_title'];
							$userMailData['subject'] = 'New Booking Notification ';
							$userMailData['phoneNumber'] = $loggedUser['phone_number'];
							$this->sendNotification($userMailData);

							$adminMailData['name'] = $getAdmin->name;
							$adminMailData['email'] = $getAdmin->email;
							$adminMailData['message'] = 'New Booking recived on '.$bookingData['booking_start_date'].' to '.$bookingData['booking_end_date'].' by user '.$loggedUser['name'];
							$adminMailData['subject'] = 'New Booking Notification ';
							$this->sendNotification($adminMailData);

							$ownerMailData['name'] = $getProductOwner->name;
							$ownerMailData['email'] = $getProductOwner->email;
							$ownerMailData['message'] = 'New Booking recived on '.$bookingData['booking_start_date'].' to '.$bookingData['booking_end_date'].' for product '.$getProductData['product_title'].' by user '.$loggedUser['name'];
							$ownerMailData['subject'] = 'New Booking Notification ';
							$ownerMailData['phoneNumber'] = $getProductOwner->phone_number;
							$this->sendNotification($ownerMailData);
							
							$resp['status'] = true;
							$resp['msg'] = 'Your Booking Created successfully. '.$approveMsg;
							
							echo json_encode($resp);exit;

						}else{
							echo json_encode($updateBooking);exit;
						}
					}else{
						$url = SITE_HTTP_URL_API.'delete-booking/'.$bookingId;
						$deleteBooking = $this->getCurl($url);

						$resp['status'] = false;
						$resp['msg'] = 'Something went wrong please try again.';
						echo json_encode($resp);exit;
					}
				}else{
					echo json_encode($addBooking);exit;
				}

			}else{
				echo json_encode($getProduct);exit;
			}
		}
		exit;
	}

	public function getBookingEstimate($data,$getProduct){

		$productData = $getProduct['productData'];
		$deliveryLocation = $getProduct['deliveryLocation'];
		$deliveryToLocation = $getProduct['deliveryToLocation'];
		$getExtraItem = $getProduct['getExtraItem'];
		if(!empty($getExtraItem)){
			$getExtraItem = array_column($getExtraItem,'item_price','item_id');
		}

		$homeLocation = $productData['home_location'];
		$homeLocationLat = $productData['home_lat'];
		$homeLocationLong = $productData['home_long'];
		$perDayPrice = $productData['price_per_day'];
		$xtraItemPrice = '0';
		$distancePrice = '0';

		$stDate = date('Y-m-d',strtotime(str_replace('-','/',$data['start_date'])));
		$etDate = date('Y-m-d',strtotime(str_replace('-','/',$data['end_date'])));

		$getBookingDays = $this->getDifference($stDate,$etDate)+1; //In days
		$bookingDaysPrice = bcdiv(($perDayPrice*$getBookingDays),1,2);

		if(!empty($data['delivered_to_you'])){
			$location = $data['delivered_to_you'];
			$locationLat = $data['delivered_address_lat'];
			$locationLong = $data['delivered_address_long'];

			$getDistance = $this->calculateDistanceBetweenTwoPoints($homeLocationLat,$homeLocationLong,$locationLat,$locationLong,'ML');

			foreach($deliveryToLocation as $key=>$values){
				if($getDistance <= $values['d_to_miles'] && $getDistance > 0){
					$distancePrice = $values['d_to_price'];
				}
			}

		}else{
			if($data['pickup_location']=='home'){
				$location = $homeLocation;
				$distancePrice = '0';
			}else{
				foreach($deliveryLocation as $k=>$value){
					if($data['pickup_location'] = $value['d_id']){
						$location = $value['d_location'];
						$distancePrice = (!empty($value['d_price']) && strtolower($value['d_price'])!='free')?$value['d_price']:'0';
						break;
					}
				}
			}
			
			$locationLat = $data['pickup_lat'];
			$locationLong = $data['pickup_lang'];
		}

		if(!empty($data['extra_item'])){
			foreach($data['extra_item'] as $k=>$value){
				$xtraItemPrice+= $getExtraItem[$value];
			}
		}
		
		//booking_site_price
		$url = SITE_HTTP_URL_API.'get-configs';
		$getConfigs = json_decode($this->getCurl($url),true);
		$getConfigs = array_column($getConfigs,'value','config_key');
		$site_matrix_price1 = $getConfigs['site_matrix_price1'];
		
		$subtotal = bcdiv(($bookingDaysPrice+$distancePrice+$xtraItemPrice),1,2);
		$siteFee = (($site_matrix_price1*$subtotal)/100);
		$totalPrice = bcdiv(($bookingDaysPrice+$distancePrice+$xtraItemPrice+$siteFee),1,2);
		
		$resp['status'] = true;
		$resp['perDayPrice'] = $perDayPrice;
		$resp['startDate'] = date('d-m-Y',strtotime($stDate));
		$resp['endDate'] = date('d-m-Y',strtotime($etDate));
		$resp['bookingPrice'] = $bookingDaysPrice;
		$resp['location'] = $location;
		$resp['locationLat'] = $locationLat;
		$resp['locationLong'] = $locationLong;
		$resp['distancePrice'] = bcdiv($distancePrice,1,2);
		$resp['extraItem'] = $xtraItemPrice;
		$resp['siteFee'] = $siteFee;
		$resp['totalPrice'] = $totalPrice;

		return $resp;
	}

	/** My bookings */
	public function mybookings(Request $request){
		$pageHeading="bookingpage";
		$loggedUserId = auth()->guard('user')->id();
		$limit = 10;
		$offset = 0;

		$bookingsData = $this->getBookings($loggedUserId,$offset,$limit,'renter');

		$getBookings = $bookingsData['getBookings'];
		$totalOrders = $bookingsData['totalOrders'];
		$getExtraItems = $bookingsData['getExtraItems'];

		$getPages =array();
		if($totalOrders > $limit){
			$getPages = $this->makePages($totalOrders,$limit);
		}

		return view('frontend.booking.bookingpage',[
			'getPages'=>$getPages,
			'pageHeading'=>$pageHeading,
			'getBookings'=>$getBookings,
			'getExtraItems'=>$getExtraItems,
			'metaTitle'=>"My Booking",
			'metaKeywords'=>"bookingpage",
			'metaDescription'=>"bookingpage"
		]);
	}

	public function getmorebookings(Request $request){
		if($request->ajax()){
			$data = $request->all();
			$activePage = $data['active'];
			$pageHeading="Booking Page";
			$loggedUserId = auth()->guard('user')->id();
			$limit = 10;
			$offset = $data['onpage'];
			
			$bookingsData = $this->getBookings($loggedUserId,$offset,$limit,'renter');
			$getBookings = $bookingsData['getBookings'];
			$totalOrders = $bookingsData['totalOrders'];
			$getExtraItems = $bookingsData['getExtraItems'];

			$getPages =array();
			if($totalOrders > $limit){
				$getPages = $this->makePages($totalOrders,$limit);
			}
	
			return view('frontend.booking.getbookingdata',[
				'pageHeading'=>$pageHeading,
				'getBookings'=>$getBookings,
				'getExtraItems'=>$getExtraItems,
				'getPages'=>$getPages,
				'activePage'=>$activePage
			]);
		}
		exit;
	}

	public function approvestatus(Request $request){
		$loggedUserId = auth()->guard('user')->id();
		$loggedUser = session()->get('UserData');
		
		if($request->ajax()){
			$data = $request->all();

			try {
				$bookingId = decrypt($data['bid']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid Booking Id.';
				echo json_encode($resp);exit;
			}

			$type  = $data['type'];
			
			$getBooking = $this->getBooking($bookingId,$loggedUserId,$type);

			if(isset($getBooking['status']) && empty($getBooking['status'])){
				echo json_encode($getBooking);exit;
			}else{
				if($getBooking['booking_approve_status']=='0'){
					$getProduct = $this->getProduct($getBooking['booking_product_id']);
					if($getProduct['status']){
						$configData  = array_column($getProduct['getConfigData'],'value','config_key');
						$stripe = new \Stripe\StripeClient($configData['stripe_secret_key']);

						if($data['action']=='approve'){

							try{
								$charge = $stripe->charges->capture($getBooking['booking_charge_id'], []);
							}catch(\Exception $e){
								$errorMsg = $e->getMessage();
	
								// $url = SITE_HTTP_URL_API.'delete-booking/'.$bookingId;
								// $deleteBooking = $this->getCurl($url);

								$resp['status'] = false;
								$resp['msg'] = $errorMsg;
								echo json_encode($resp);exit;
							}

							if($charge->status =='succeeded'){
								
								$updateData['booking_approve_status'] = '1';
								$resMesg = 'Booking approved successfully.';
								
								$mailMsg = 'Your Booking booked on '.$getBooking['booking_start_date'].' to '.$getBooking['booking_end_date'].' is approved.';
								$subject = 'Booking Approved Notification';
							}

						}else{

							$updateData['booking_approve_status'] = '2';
							$resMesg = 'Booking decline successfully.';

							$mailMsg = 'Your Booking booked on '.$getBooking['booking_start_date'].' to '.$getBooking['booking_end_date'].' is decline.';
							$subject = 'Booking Decline Notification';
						}

						$updateBooking = $this->addBookingData($updateData,$bookingId);

						if($updateBooking['status']){
							
							$getUser = json_decode($this->getCurl(SITE_HTTP_URL_API.'get-user/'.$getBooking['booking_user_id']));

							$userMailData['name'] = $getUser->name;
							$userMailData['email'] = $getUser->email;
							$userMailData['message'] = $mailMsg;
							$userMailData['subject'] = $subject;
							$userMailData['phoneNumber'] = $getUser->phone_number;
							$this->sendNotification($userMailData);

							$resp['status'] = true;
							$resp['msg'] = $resMesg;
							echo json_encode($resp);exit;
						}else{
							echo json_encode($updateBooking);exit;
						}

					}else{
						$resp['status'] = false;
						$resp['msg'] = 'Invalid request.';
						echo json_encode($resp);exit;
					}
				}else{
					$resp['status'] = false;
					$resp['msg'] = 'Invalid request.';
					echo json_encode($resp);exit;
				}
			}
		}
		exit;
	}
	
	public function givereview(Request $request){

		$loggedUserId = auth()->guard('user')->id();
		if($request->ajax()){
			$data = $request->all();
			try {
				$bookingId = decrypt($data['bkid']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid booking request.';
				echo json_encode($resp);exit;
			}

			$getBooking = $this->getBooking($bookingId,$loggedUserId,'renter');

			if(isset($getBooking['status']) && empty($getBooking['status'])){
				echo json_encode($getBooking);exit;
			}else{
				if($getBooking['booking_status'] =='1' && $getBooking['booking_end_date'] < date('Y-m-d')){
					
					$rating = $data['starRating'];
					$reviews = strip_tags(trim($data['review']));
					if($rating > 5 && $rating < 0){
						$resp['status'] = false;
						$resp['msg'] = 'Invalid star rating value.';
						echo json_encode($resp);exit;
					}

					$insertData['review_booking_id'] = $bookingId;
					$insertData['review_user_id']    = $loggedUserId;
					$insertData['review_product_id'] = $getBooking['booking_product_id'];
					$insertData['review_rating'] = $rating;
					$insertData['review_messsage'] = $reviews;
					$insertData['review_dated_on'] = date('Y-m-d');
				
					$addReview = $this->addReview($insertData);
					
					echo json_encode($addReview);exit;
				}else{
					
					$resp['status'] = false;
					$resp['msg'] = 'Invalid booking request.';
					echo json_encode($resp);exit;
				}
			}
		}
		exit;
	}

	public function getreviews(Request $request){
		if($request->ajax()){
			$data = $request->all();
			$limit = 5;
			$offSet = $data['offset'];

			try {
				$proId = decrypt($data['proId']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid booking request.';
				echo json_encode($resp);exit;
			}

			$getReviws = $this->getreviewsdata($proId,$offSet,$limit);

			if(!empty($getReviws['getReviews'])){
				$reviews = $getReviws['getReviews'];
				$totalreviews = $getReviws['getTotalReviews'];

				return view('frontend.booking.getreviews',[
					'reviews'=>$reviews,
					'totalreviews'=>$totalreviews,
				]);
				exit;
			}
		}
		exit;
	}

	/** Give Review */
	public function addReview($data){
		$url = SITE_HTTP_URL_API.'add-review';
		$getRespose = $this->postCurl($url,$data);
		return json_decode($getRespose,true);
	}

	public function getreviewsdata($id,$offSet,$limit){
		$url = SITE_HTTP_URL_API.'get-reviews/'.$id.'?limit='.$limit.'&offset='.$offSet;
		$getReviews = $this->getCurl($url);
		return json_decode($getReviews,true);
	}

	public function addBookingData($data,$id=''){
		$url = SITE_HTTP_URL_API.'add-booking';
		if(!empty($id)){
			$url = SITE_HTTP_URL_API.'add-booking/'.$id;
		}
		$getRespose = $this->postCurl($url,$data);
		return json_decode($getRespose,true);
	}

	public function makePages($totalOrders,$limit){
		$pageCount = Ceil(($totalOrders/$limit));
		
		for ($i=0; $i < $pageCount; $i++) {
			$page = ($i+1);
			$pageArr[$page] = ($i*$limit);
		}

		return $pageArr;
	}

	public function getBookings($id,$offSet,$limit,$type,$date=false,$status=false){
		$url = SITE_HTTP_URL_API.'get-my-booking/'.$id.'?type='.$type.'&limit='.$limit.'&offset='.$offSet.'&date='.$date.'&status='.$status;
		$getBookings = $this->getCurl($url);
		return json_decode($getBookings,true);
	}

	public function getBooking($bkId,$uid,$type){
		$url = SITE_HTTP_URL_API.'get-booking/'.$bkId.'/'.$uid.'?type='.$type;
		$getBookings = $this->getCurl($url);
		return json_decode($getBookings,true);
	}

	public function getProduct($pid){
		$url = SITE_HTTP_URL_API.'get-product/'.$pid;
		$getRespose = $this->getCurl($url);
		return json_decode($getRespose,true);
	}
	
	/** Share Bookings */
	public function sharerbookings(){
		$pageHeading="bookingpage";
		$loggedUserId = auth()->guard('user')->id();
		$limit = 10;
		$offset = 0;
		$bookingsData = $this->getBookings($loggedUserId,$offset,$limit,'sharer');

		$getBookings = $bookingsData['getBookings'];
		$totalOrders = $bookingsData['totalOrders'];
		$getExtraItems = $bookingsData['getExtraItems'];
		$getTotalSales = $bookingsData['getTotalSales'];
		$getLastWeekSales = $bookingsData['getLastWeekSales'];
		$getLastMonthSales = $bookingsData['getLastMonthSales'];
		$lastWeekTotalSales = $bookingsData['lastWeekTotalSales'];
		$lastMonthTotalSales = $bookingsData['lastMonthTotalSales'];

		$getPages =array();
		if($totalOrders > $limit){
			$getPages = $this->makePages($totalOrders,$limit);
		}

		return view('frontend.booking.sharerbookings',[
			'getPages'=>$getPages,
			'pageHeading'=>$pageHeading,
			'getBookings'=>$getBookings,
			'totalOrders'=>$totalOrders,
			'getExtraItems'=>$getExtraItems,
			'getTotalSales'=>$getTotalSales,
			'getLastWeekSales'=>$getLastWeekSales,
			'getLastMonthSales'=>$getLastMonthSales,
			'lastWeekTotalSales'=>$lastWeekTotalSales,
			'lastMonthTotalSales'=>$lastMonthTotalSales,
			'metaTitle'=>"Sharer Booking",
			'metaKeywords'=>"sharerbookingpage",
			'metaDescription'=>"sharerbookingpage"
		]);
	}

	public function getmoresharerbookings(Request $request){
		if($request->ajax()){
			$data = $request->all();
			$activePage = $data['active'];
			$pageHeading="Booking Page";
			$loggedUserId = auth()->guard('user')->id();
			$limit = 10;
			$offset = $data['onpage'];

			$date='';
			if(isset($data['date']) && !empty($data['date'])){
				$date = date('Y-m-d',strtotime($data['date']));
			}
			$status='';
			if(isset($data['status']) && !empty($data['status'])){
				$status = $data['status'];
			}

			$bookingsData = $this->getBookings($loggedUserId,$offset,$limit,'sharer',$date,$status);
			$getBookings = $bookingsData['getBookings'];
			$totalOrders = $bookingsData['totalOrders'];
			$getExtraItems = $bookingsData['getExtraItems'];

			$getPages =array();
			if($totalOrders > $limit){
				$getPages = $this->makePages($totalOrders,$limit);
			}
	
			return view('frontend.booking.getsharerbookingdata',[
				'pageHeading'=>$pageHeading,
				'getBookings'=>$getBookings,
				'getExtraItems'=>$getExtraItems,
				'getPages'=>$getPages,
				'activePage'=>$activePage
			]);
		}
		exit;
	}
	
	/** Cancel Bookings */
	public function cancelbooking(Request $request){
		$logged_in_user_id = auth()->guard('user')->id();
		$loggedUser = session()->get('UserData');

		if($request->ajax()){
			
			$data = $request->all();
			try {
				$bkId = decrypt($data['id']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid booking Id.';
				echo json_encode($resp);exit;
			}

			try {
				$reason = base64_decode($data['cuiseanna']);
				$type = base64_decode($data['tip']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid request.';
				echo json_encode($resp);exit;
			}

			$getBooking = $this->getBooking($bkId,$logged_in_user_id,$type);
			
			if(!empty($getBooking)){
				if($getBooking['booking_status']=='1' && $getBooking['booking_approve_status']=='1' && $getBooking['booking_refund_status']=='0'){
					
					require_once(ROOT_PATH.'/vendor/stripe/vendor/autoload.php');

					$getProduct = $this->getProduct($getBooking['booking_product_id']);
					$configData  = array_column($getProduct['getConfigData'],'value','config_key');
					$getProductData = $getProduct['productData'];

					$stripe = new \Stripe\StripeClient($configData['stripe_secret_key']);

					$totalPrice = $getBooking['booking_total_price'];
					$chargeId = $getBooking['booking_charge_id'];

					try {

						if($type =='renter'){
							$cancellationCharge = bcdiv((($totalPrice/100)*$configData['site_matrix_price']),1,2);
							$refundPrice =  bcdiv(($totalPrice-$cancellationCharge),1,2);
							if($cancellationCharge > $configData['site_max_fee_price']){
								$refundPrice = bcdiv(($totalPrice-$configData['site_max_fee_price']),1,2);
							}
							
							$refund = $stripe->refunds->create([
								'charge' => $chargeId,
								'amount' => ($refundPrice*100)
							]);

						}else{
							$refundPrice = $totalPrice;

							$refund = $stripe->refunds->create([
								'charge' => $chargeId
							]);
						}
					} catch (\Exception $e) {
						$errorMessage = $e->getMessage();

						$resp['status'] = false;
						$resp['msg'] = $errorMessage;
						echo json_encode($resp);exit;
					}
					
							
					$updateData['booking_status'] = '2';
					$updateData['booking_refund_status'] = '1';
					$updateData['booking_cancelled_by'] = $logged_in_user_id;
					$updateData['booking_cancelled_reason'] = strip_tags($reason);
					$updateData['booking_refund_amt'] = $refundPrice;

					if(!empty($refund->id)){
						$updateData['booking_refund_id'] = $refund->id;
						$isUpdateBooking = $this->addBookingData($updateData,$bkId);


						$txnData['txn_booking_id'] = $bkId;
						$txnData['txn_type'] = 'refund';
						$txnData['txn_amount'] = $refundPrice;
						$txnData['txn_rrc_id'] = $refund->id;

						$url = SITE_HTTP_URL_API.'add-transcation';
						$addTxn = $this->postCurl($url,$txnData);

						$getUser = json_decode($this->getCurl(SITE_HTTP_URL_API.'get-user/'.$getBooking['booking_user_id']));
						$getProductOwner = json_decode($this->getCurl(SITE_HTTP_URL_API.'get-user/'.$getProductData['product_added_by']));
						$getAdmin = json_decode($this->getCurl(SITE_HTTP_URL_API.'get-user/admin'));
						
						$userMailData['name'] = $getUser->name;
						$userMailData['email'] = $getUser->email;
						$userMailData['message'] = 'Your Booking cancelled on '.$getBooking['booking_start_date'].' to '.$getBooking['booking_end_date'].' for product '.$getProductData['product_title'];
						$userMailData['subject'] = 'Booking Cancel Notification ';
						$userMailData['phoneNumber'] = $getUser->phone_number;
						$this->sendNotification($userMailData);

						$adminMailData['name'] = $getAdmin->name;
						$adminMailData['email'] = $getAdmin->email;
						$adminMailData['message'] = 'New Booking cancelled on '.$getBooking['booking_start_date'].' to '.$getBooking['booking_end_date'].' Booking Id: '.$getBooking['booking_id'];
						$adminMailData['subject'] = 'Booking Cancel Notification ';
						$this->sendNotification($adminMailData);

						$ownerMailData['name'] = $getProductOwner->name;
						$ownerMailData['email'] = $getProductOwner->email;
						$ownerMailData['message'] = 'Your Booking cancelled on '.$getBooking['booking_start_date'].' to '.$getBooking['booking_end_date'].' for product '.$getProductData['product_title'];
						$ownerMailData['subject'] = 'Booking Cancel Notification ';
						$ownerMailData['phoneNumber'] = $getProductOwner->phone_number;
						$this->sendNotification($ownerMailData);

						echo  json_encode($isUpdateBooking);exit;
					}

				}else{	
					$resp['status'] = false;
					$resp['msg'] = 'Invalid request.';
					echo json_encode($resp);exit;
				}
			}else{
				$resp['status'] = false;
				$resp['msg'] = 'Invalid booking.';
				echo json_encode($resp);exit;
			}
		}
		exit;
	}

	/** Report issue */
	public function reportissue(Request $request){
		$logged_in_user_id = auth()->guard('user')->id();
		$loggedUser = session()->get('UserData');

		if($request->ajax()){
			$data = $request->all();

			try {
				$bkId = decrypt($data['id']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid booking Id.';
				echo json_encode($resp);exit;
			}

			if(isset($data['action']) && $data['action']=='checked'){
				$url = SITE_HTTP_URL_API.'get-report-issue/'.$bkId.'/'.$logged_in_user_id;
				$getReport  = json_decode($this->getCurl($url));
				if(!empty($getReport)){
					$resp['status'] = true;
					$resp['summary'] = $getReport->report_summary;
				}else{
					$resp['status'] = false;
				}
				echo json_encode($resp);exit;
			}else{

				try {
					$reason = base64_decode($data['aithisg']);
					$type = base64_decode($data['tip']);
				} catch (\Exception $e) {
					$errorMessage = $e->getMessage();
					$resp['status'] = false;
					$resp['msg'] = 'Invalid request.';
					echo json_encode($resp);exit;
				}
				
				$addReport['report_by_type'] = $type;
				$addReport['report_by_id'] = $logged_in_user_id;
				$addReport['report_for_booking'] = $bkId;
				$addReport['report_summary'] = strip_tags($reason);

				$url = SITE_HTTP_URL_API.'add-report-issue';
				$addReport = json_decode($this->postCurl($url,$addReport));
				
				echo json_encode($addReport);exit;
			}
		}
		exit;
	}

	/** Check In Out */
	public function checkinout(Request $request){
		$logged_in_user_id = auth()->guard('user')->id();
		$loggedUser = session()->get('UserData');

		if($request->ajax()){
			$data = $request->all();
	
			try {
				$bkId = decrypt($data['id']);
			} catch (\Exception $e) {
				$errorMessage = $e->getMessage();
				$resp['status'] = false;
				$resp['msg'] = 'Invalid booking Id.';
				echo json_encode($resp);exit;
			}
			$type = $data['type'];
			$userType = base64_decode($data['tip']);
			$action = $data['action'];
			
			$url = SITE_HTTP_URL_API.'get-checkin-checkout/'.$bkId.'/'.$logged_in_user_id.'/'.$type;
			$getRecord  = json_decode($this->getCurl($url));
			if($action=='checked'){

				if(!empty($getRecord)){
					$resp['status'] = true;
					if(!empty($getRecord->ckio_images)){
						$resp['images'] = HTTP_UPLOADED_IMAGES_PATH.'/'.$getRecord->ckio_images;
					}
					$resp['summary'] = $getRecord->ckio_description;
					$resp['datetime'] = date('d M,Y h:i a',strtotime($getRecord->ckio_date.' '.$getRecord->ckio_time));
				}else{
					$resp['status'] = false;
				}
				echo json_encode($resp);exit;
			}else{
				
				if(!empty($getRecord)){
					$resp['status'] = false;
					$resp['msg'] = 'Your request already submitted.';
					echo json_encode($addCheckinOut);exit;
				}

				$insertData['ckio_user_id'] = $logged_in_user_id;
				$insertData['ckio_user_type'] = $userType;
				$insertData['ckio_booking_id'] = $bkId;
				$insertData['ckio_type'] = $type;
				$insertData['ckio_description'] = strip_tags($data['check_in_out_description']);
				$insertData['ckio_date'] = date('Y-m-d',strtotime($data['check_in_out_date']));
				$insertData['ckio_time'] = date('H:i:s');

				if(!empty($request->file())){
					$filesKeys = array_keys($request->file());
					$isUpload = Uploader::universalUpload(
						array(
							'directory'=>storage_path('uploaded_images/'),
							'files'=>$filesKeys,
							'multiple'=>false,
							'thumb'=>array(),//array('w'=>300,'h'=>300),array('w'=>160,'h'=>160)
							'allowExtension' => array("png","PNG","jpg","JPG","jpeg","JPEG"),
						),
					$request);
					
					if($isUpload['success']=='1'){
						$media_path = $isUpload['media_path'];
						foreach($media_path as $x=>$img){
							if(!empty($img['mediaPath'])){
								$insertData['ckio_images'] = $img['mediaPath'];
							}
						}
					}
				}

				$url = SITE_HTTP_URL_API.'add-checkin-checkout';
				$addCheckinOut = json_decode($this->postCurl($url,$insertData));

				echo json_encode($addCheckinOut);exit;
			}
		}
		exit;
	}
	
	/** get Distance beetween */
	public function calculateDistanceBetweenTwoPoints($latitudeOne='', $longitudeOne='', $latitudeTwo='', $longitudeTwo='',$distanceUnit ='',$round=false,$decimalPoints=''){
		if (empty($decimalPoints)) 
		{
			$decimalPoints = '2';
		}
		if (empty($distanceUnit)) {
			$distanceUnit = 'KM';
		}
		$distanceUnit = strtolower($distanceUnit);
		$pointDifference = $longitudeOne - $longitudeTwo;
		$toSin = (sin(deg2rad($latitudeOne)) * sin(deg2rad($latitudeTwo))) + (cos(deg2rad($latitudeOne)) * cos(deg2rad($latitudeTwo)) * cos(deg2rad($pointDifference)));
		$toAcos = acos($toSin);
		$toRad2Deg = rad2deg($toAcos);

		$toMiles  =  $toRad2Deg * 60 * 1.1515;
		$toKilometers = $toMiles * 1.609344; //1.609344
		$toNauticalMiles = $toMiles * 0.8684;
		$toMeters = $toKilometers * 1000;
		$toFeets = $toMiles * 5280;
		$toYards = $toFeets / 3;

		switch (strtoupper($distanceUnit)) 
		{
			case 'ML'://miles
					$toMiles  = ($round == true ? round($toMiles) : round($toMiles, $decimalPoints));
					return $toMiles;
				break;
			case 'KM'://Kilometers
				$toKilometers  = ($round == true ? round($toKilometers) : round($toKilometers, $decimalPoints));
				return $toKilometers;
				break;
			case 'MT'://Meters
				$toMeters  = ($round == true ? round($toMeters) : round($toMeters, $decimalPoints));
				return $toMeters;
				break;
			case 'FT'://feets
				$toFeets  = ($round == true ? round($toFeets) : round($toFeets, $decimalPoints));
				return $toFeets;
				break;
			case 'YD'://yards
				$toYards  = ($round == true ? round($toYards) : round($toYards, $decimalPoints));
				return $toYards;
				break;
			case 'NM'://Nautical miles
				$toNauticalMiles  = ($round == true ? round($toNauticalMiles) : round($toNauticalMiles, $decimalPoints));
				return $toNauticalMiles;
				break;
		}
	}

	/* Return Difference Between Two Dates */
	public function getDifference($date1 ,$date2 , $in ='d'){
		// Calculating the difference in timestamps
		$diff = strtotime($date2) - strtotime($date1);
	
		// 1 day = 24 hours
		// 24 * 60 * 60 = 86400 seconds
		return abs(round($diff / 86400));
	}

	/** Get Date Rage Between two Range */
	public function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
		$array = array(); 
		$interval = new \DateInterval('P1D'); 

		$realEnd = new \DateTime($end); 
		$realEnd->add($interval); 

		$period = new \DatePeriod(new \DateTime($start), $interval, $realEnd); 
		foreach($period as $date) {                  
			$array[] = $date->format($format);  
		} 
		return $array; 
	} 

	/** Curls */
	public function getCurl($url){

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);
	
		curl_close($curl);
		return $response;
    }

	public function postCurl($url,$postData){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>$postData,
			CURLOPT_HTTPHEADER => array(
				'token: '.CURL_KEY.''
			),
		));

		$response = curl_exec($curl);
	
		curl_close($curl);
		return $response;
    }

	public function sendNotification($data){

		$mailData = [
			'user_name'		=> $data['name'],
			'user_email'	=> $data['email'],
			'message'	    => $data['message'],
			'subject'	    => $data['subject'],
			'mail_template'	=> 'common_notification'
		];
		
		$sendMail = Mail::send(new MailSenders($mailData));

		if(isset($data['phoneNumber']) && !empty($data['phoneNumber'])){
			$this->sendinfoSms($data['phoneNumber'],$data['message']);
		}
		return true;
	}

	public function getConfigs(){
		$url = SITE_HTTP_URL_API.'get-configs';
		$getConfigs = json_decode($this->getCurl($url),true);
		$getData = array_column($getConfigs,'value','config_key');
		return $getData;
	}


	public function sendinfoSms($phnnum,$message=""){

		$mobileVerifyerrorMsg='';
		$someerrortxt = "Some error occurred";
		$mobileVerifyerrorMsg='';

		$phoneNumber= str_replace('+','',str_replace('-','',trim($phnnum)));
		
		$getConfig = $this->getConfigs();

		if($phoneNumber!=''){
			$AccountSid = $AuthToken=$AuthFromNumber=$postednumber=$numbercode='';
			if(true){

				$AccountSid = $getConfig['twilio_account_key'];
				$AuthToken =  $getConfig['twilio_auth_key'];
				$AuthFromNumber = $getConfig['twilio_number'];
				$postednumber = $phoneNumber;
				//$numbercode = $code;
			}
			$sendSmsto = '+'.$postednumber;
		
			if(true){

				try{ 

					$client = new Client($AccountSid,$AuthToken);

					if(empty($message)){
						$MESSAGE = "Your order has been confirmed and successfully.";
					}else{
						$MESSAGE = $message;
					}

					$sms = $client->account->messages->create(
						$sendSmsto,
						array(
							'from' => $AuthFromNumber, 
							'body' =>$MESSAGE
						)
					);	
					
					//prd($sms);
				}
				catch (\TwilioException $e){
					//prd($e->getMessage());
				}
				catch(\Exception $e){
					if($e->getCode()=='21211'){
						$mobileVerifyerrorMsg=str_replace("[HTTP 400]","",$e->getMessage());	
						$mobileVerifyerrorMsg="Unable to create record the user number is not valid";
					}else{
						$mobileVerifyerrorMsg=$e->getMessage();
					}

					//prd($mobileVerifyerrorMsg);
				}
			}
		}
	}

}