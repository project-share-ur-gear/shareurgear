<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use DB;
use Hash;
use App;

use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class CronController extends Controller{
	
	public function _construct(){
		$this->locale=App::getLocale();
	}

	public  function checkunapprovedbookings(Request $request){
		$url = env('SITE_HTTP_URL_API').'get-unapporoved-bookings';
		$getUnapprovedBookings = $this->getCurl($url);
	
		if(!empty($getUnapprovedBookings)){
			foreach ($getUnapprovedBookings as $key => $value) {

				$getUser = (object) $this->getCurl(env('SITE_HTTP_URL_API').'get-user/'.$value['booking_user_id']);
				$getProductOwner =  (object) $this->getCurl(env('SITE_HTTP_URL_API').'get-user/'.$value['product_added_by']);
				$getAdmin =  (object) $this->getCurl(env('SITE_HTTP_URL_API').'get-user/admin');
				
				$updateBooking['booking_status'] = '2';
				$this->addBookingData($updateBooking,$value['booking_id']);

				$userMailData['name'] = $getUser->name;
				$userMailData['email'] = $getUser->email;
				$userMailData['message'] = 'Your Booking cancelled on '.$value['booking_start_date'].' to '.$value['booking_end_date'].' because is not approved by product owner.';
				$userMailData['subject'] = 'Unapproved Booking Cancel Notification';
				$userMailData['phoneNumber'] = $getUser->phone_number;
				$this->sendNotification($userMailData);

				$adminMailData['name'] = $getAdmin->name;
				$adminMailData['email'] = $getAdmin->email;
				$adminMailData['message'] = 'New Booking cancelled on '.$value['booking_start_date'].' to '.$value['booking_end_date'].' because is not approved by product owner. For Booking Id: '.$value['booking_id'];
				$adminMailData['subject'] = 'Unapproved Booking Cancel Notification';
				$this->sendNotification($adminMailData);

				$ownerMailData['name'] = $getProductOwner->name;
				$ownerMailData['email'] = $getProductOwner->email;
				$ownerMailData['message'] = 'Your Booking cancelled on '.$value['booking_start_date'].' to '.$value['booking_end_date'].' due to approved by you for product '.$value['product_title'];
				$ownerMailData['subject'] = 'Unapproved Booking Cancel Notification ';
				$ownerMailData['phoneNumber'] = $getProductOwner->phone_number;
				$this->sendNotification($ownerMailData);
			}
		}
		echo 'success';
		exit;
	}

	public function addBookingData($data,$id){
		if(empty($id)){
			return false;
		}
		$url = env('SITE_HTTP_URL_API').'add-booking/'.$id;
		$getRespose = $this->postCurl($url,$data);
		return $getRespose;
	}

	public function checkcheckinout(Request $request){
		
		$url = env('SITE_HTTP_URL_API').'get-checkinout-bookings';
		$getUnapprovedBookings = (object)$this->getCurl($url);

		$getCheckInRecords = (isset($getUnapprovedBookings->getCheckInRecords) && !empty($getUnapprovedBookings->getCheckInRecords))?$getUnapprovedBookings->getCheckInRecords:'';
		$getCheckOutRecords = (isset($getUnapprovedBookings->getCheckOutRecords) && !empty($getUnapprovedBookings->getCheckOutRecords))?$getUnapprovedBookings->getCheckOutRecords:'';

		$url = env('SITE_HTTP_URL_API').'get-user/admin';
		$getAdmin = (object)$this->getCurl($url);

		if(!empty($getCheckInRecords)){
			$notificationData = array();
			foreach($getCheckInRecords as $k=>$value){
			
				$notificationData['status_for_booking'] = $value['booking_id'];
				$notificationData['status_type']  = 'checkin';
				
				$url = env('SITE_HTTP_URL_API').'add-notification';
				$this->postCurl($url,$notificationData);

				$adminMailData['name'] = $getAdmin->name;
				$adminMailData['email'] = $getAdmin->email;
				$adminMailData['message'] = ' User never check In on booking date '.$value['booking_start_date'].' for Booking Id: '.$value['booking_id'];
				$adminMailData['subject'] = 'User Not Check In On Booking Date';
				$this->sendNotification($adminMailData);

			}
		}

		if(!empty($getCheckOutRecords)){
			$notificationData = array();
			foreach($getCheckOutRecords as $k=>$value){

				$notificationData['status_for_booking'] = $value['booking_id'];
				$notificationData['status_type']  = 'checkout';
				
				$url = env('SITE_HTTP_URL_API').'add-notification';
				$this->postCurl($url,$notificationData);

				$adminMailData['name'] = $getAdmin->name;
				$adminMailData['email'] = $getAdmin->email;
				$adminMailData['message'] = ' User never check Out on booking end date '.$value['booking_end_date'].' for Booking Id: '.$value['booking_id'];
				$adminMailData['subject'] = 'User Not Check Out On Booking End Date';

				$this->sendNotification($adminMailData);
			}
		}

		echo 'success';
		exit;
	}

	public function releasefunds(Request $request){
		
		$getConfig = $this->getConfigs();

		require_once(base_path().'/vendor/stripe/vendor/autoload.php');
		$stripe = new \Stripe\StripeClient($getConfig['stripe_secret_key']);

		$url = env('SITE_HTTP_URL_API').'fund-relese-bookings';
		$fundReleaseBookings = $this->getCurl($url);

		$url = env('SITE_HTTP_URL_API').'get-user/admin';
		$getAdmin = (object)$this->getCurl($url);

		if(!empty($fundReleaseBookings)){
			foreach($fundReleaseBookings as $k=>$value){

				$balance = $stripe->balance->retrieve();
				$available_amount = $balance['available']['0']['amount'];

				$getSharer = $this->getCurl(env('SITE_HTTP_URL_API').'get-user/'.$value['product_added_by']);
				$transferId  = $getSharer['client_account_id'];

				$fundTrasnferAmt = ($value['booking_total_price']-$value['booking_site_price']);
				
				if($available_amount < $fundTrasnferAmt ){

					$adminMailData['name'] = $getAdmin->name;
					$adminMailData['email'] = $getAdmin->email;
					$adminMailData['message'] = ' Funds not release for Booking Id: '.$value['booking_id'].' due to low balance.';
					$adminMailData['subject'] = ' Fund Release Error Notification';

					$this->sendNotification($adminMailData);

				}else{
					
					$txnData['fund_booking_id'] = $value['booking_id'];
					$txnData['fund_transfer_amount'] = $fundTrasnferAmt;
					$txnData['fund_transfer_date'] = date('Y-m-d H:i:s');

					$fundTxnData['txn_booking_id'] = $value['booking_id'];
					$fundTxnData['txn_type'] = 'release';
					$fundTxnData['txn_amount'] = $fundTrasnferAmt;
					$fundTxnData['txn_on'] =  date('Y-m-d H:i:s');
					
					try {
						$transcation = $stripe->transfers->create([
							'amount' => ($txnData['fund_transfer_amount']*100),
							'currency' => 'usd',
							'destination' => $transferId,
							'description' => 'Transfers for Booking Id: '.$value['booking_id'],
						]);
					} catch (\Exception $e) {
						$errorMessage = $e->getMessage();
											
						$adminMailData['name'] = $getAdmin->name;
						$adminMailData['email'] = $getAdmin->email;
						$adminMailData['message'] = 'Funds not release for Booking Id: '.$value['booking_id'].' due to '.$errorMessage;
						$adminMailData['subject'] = 'Fund Release Error Notification';
						
						$this->sendNotification($adminMailData);
					}

					if(!empty($transcation->id)){
						$txnData['fund_transfer_id'] = $transcation->id;
						$url = env('SITE_HTTP_URL_API').'add-fund-relese';
						$getRespose = $this->postCurl($url,$txnData);

						$fundTxnData['txn_rrc_id'] =  $transcation->id;

						$url = env('SITE_HTTP_URL_API').'add-transcation';
						$addTxn = $this->postCurl($url,$fundTxnData);
					}
				}
			}
		}

		echo 'success';
		exit;
	}
	
	public function getConfigs(){
		$getUrl = env('SITE_HTTP_URL_API').'get-configs';
		$getData = $this->getCurl($getUrl);
		$getData = array_column($getData,'value','config_key');
		return $getData;
	}


	/** Curls */
	public function getCurl($url){

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'token: GUISGIABHI878645sS56sad20'
			),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		return json_decode($response,true);
	}

	public function postCurl($url,$postData){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS =>$postData,
			CURLOPT_HTTPHEADER => array(
				'token: GUISGIABHI878645sS56sad20'
			),
		));

		$response = curl_exec($curl);
		
		curl_close($curl);
		return json_decode($response,true);
	}

	/*** Send Mail */
	public function sendNotification($data){

		$mailData = [
			'user_name'		=> $data['name'],
			'user_email'	=> $data['email'],
			'message'	    => $data['message'],
			'subject'	    => $data['subject'],
			'mail_template'	=> 'common_notification'
		];
		
		$sendMail = Mail::send(new MailSenders($mailData));

		if(isset($data['phoneNumber']) && !empty($data['phoneNumber'])){
			$this->sendinfoSms($data['phoneNumber'],$data['message']);
		}

		return true;
	}

	public function sendinfoSms($phnnum,$message=""){

		require_once base_path()."/vendor/twilio-php-master/Twilio/autoload.php";
		
		$getConfig = $this->getConfigs();
		
		$mobileVerifyerrorMsg='';
		$someerrortxt = "Some error occurred";
		$mobileVerifyerrorMsg='';

		$phoneNumber= str_replace('+','',str_replace('-','',trim($phnnum)));

		if($phoneNumber!=''){
			$AccountSid = $AuthToken=$AuthFromNumber=$postednumber=$numbercode='';
			if(true){

				$AccountSid = $getConfig['twilio_account_key'];
				$AuthToken =  $getConfig['twilio_auth_key'];
				$AuthFromNumber = $getConfig['twilio_number'];
				$postednumber = $phoneNumber;
			}
			$sendSmsto = '+'.$postednumber;
		
			if(true){

				try{ 

					$client = new Client($AccountSid,$AuthToken);

					if(empty($message)){
						$MESSAGE = "Your order has been confirmed and successfully.";
					}else{
						$MESSAGE = $message;
					}

					$sms = $client->account->messages->create(
						$sendSmsto,
						array(
							'from' => $AuthFromNumber, 
							'body' =>$MESSAGE
						)
					);				
				}
				catch (\TwilioException $e){}
				catch(\Exception $e){
					if($e->getCode()=='21211'){
						$mobileVerifyerrorMsg=str_replace("[HTTP 400]","",$e->getMessage());	
						$mobileVerifyerrorMsg="Unable to create record the user number is not valid";
					}else{
						$mobileVerifyerrorMsg=$e->getMessage();
					}
				}
			}
		}
	}
	
}