<?php
 
namespace App\Library;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Image;
Use DB;

 

class Uploader  
{
    public static function universalUpload($options = array(),$request){
   
        $multiple = false; 

        if(empty($options['directory'])){
            return array('success'=>false,'error'=>true,'message'=>"No Defined Path",'files_upload'=>0,'media_path'=>array());
        }
 
        if(empty($options['files'])){
            return array('success'=>false,'error'=>true,'message'=>"No Files Found.",'files_upload'=>0,'media_path'=>array());
        }

        if(isset($options['multiple']) and $options['multiple'])
		{
			$multiple = true; 
 		}
        $path = $options['directory'];
        $files = $options['files'];
        $thumb = $options['thumb'];
        $allowExtension = !empty($options['allowExtension'])?$options['allowExtension']:array('jpg','png','gif','bmp','webp','svg','JPG','PNG','GIF','BMP','WebP','SVG');
        $filesArray =array();
        foreach ($files as $k => $fileKey) 
		{   
            if($request->hasFile($fileKey))
			{   
                if($multiple){
                    foreach($request->file($fileKey) as $FKey=>$file)
                    {
                        $getFile = Uploader::fileUpload($file,$path,$thumb,$allowExtension);
                        if(!empty($getFile)){
                            if(!empty($getFile['error'])){
                                $filesArray[$fileKey][$FKey]['error'] = $getFile['error'];
                            }else{
                                $filesArray[$fileKey][$FKey]['orgName'] =  $getFile['orgName'];
                                $filesArray[$fileKey][$FKey]['mediaPath'] = $getFile['mediaPath'];
                            }
                        }
  
                    }     
                }
                else
                {
                    $getFile = Uploader::fileUpload($request->file($fileKey),$path,$thumb,$allowExtension);     
                    if(!empty($getFile)){   
                        if(!empty($getFile['error'])){
                            $filesArray[$fileKey]['error'] = $getFile['error'];
                        }else{
                            $filesArray[$fileKey]['orgName'] = $getFile['orgName'];
                            $filesArray[$fileKey]['mediaPath'] = $getFile['mediaPath'];
                        }
                    }
                }
            }
            else{
                return array('error'=>true,'message'=>$fileKey." file not found.");
            }
        }

        return array('success'=>true,'message'=>"File Uploaded Successfully.",'count'=>count($filesArray),'media_path'=>$filesArray);
    }

    public static function fileUpload($file,$path,$thumb,$allowExtension){
        
        $extension = $file->extension();
        $orgName = $file->getClientOriginalName();
        $newNameImage = rand().str_replace(' ','_',$orgName);
        $response = array();
        if(in_array($extension,$allowExtension)){
            $getFile = $file->getPathName();
            if(!in_array($extension,array('ico','svg'))){

                $img = Image::make($getFile)->save($path.$newNameImage);

                if(!empty($thumb)){
                    foreach ($thumb as $t => $ratio) {
                        if(!empty($ratio['w']) && !empty($ratio['h'])){
                            $savePath = $path.$ratio['w'].'X'.$ratio['h'];
                            if(!is_dir($savePath)){  mkdir($savePath); }
                            $imageDir = $savePath.'/'.$newNameImage;
                            Image::make($getFile)->resize($ratio['w'], $ratio['h'])->save($imageDir);
                        }
                    }
                }
            }
            else
            {
                $isUpload = $file->move($path,$newNameImage);
            }

            $response['orgName'] = $orgName;
            $response['mediaPath'] = $newNameImage;
            $response['error'] = '';
        }
        else{
            $response['error'] = 'Invalid extension file. Allowed extension is '.implode(',',$allowExtension); 
        }
        return $response;
    }

    public static function universalUnlink($imageName, $directory){
        
		if(empty($imageName)){
			return array('success'=>false,'error'=>true,'message'=>"Image Name is Empty",'files_unlink'=>0);
		}

        if(empty($directory)){
            return array('success'=>false,'error'=>true,'message'=>"Path is Empty",'files_unlink'=>0);
        }

        if(is_dir($directory)){
            if(file_exists($directory.$imageName)){
                @unlink($directory.$imageName);
            }
        }

        return array('success'=>true,'message'=>"Files Unlink Successfully.");
    }
}
