<?php



namespace App\Model\frontend;



use Illuminate\Database\Eloquent\Model;



class Contactus extends Model

{



	protected $table = 'contactus';



    //

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [

        'name', 'email', 'message',

    ];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [

        /*'password', 'remember_token',*/

    ];



}

