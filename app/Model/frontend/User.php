<?php
namespace App\Model\frontend;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Mail\MailSenders;
use Illuminate\Support\Facades\Mail;

use DB;

class User extends Authenticatable
{
	use Notifiable;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	'name','email','profile_image','address','phone_number','password', 'deleted_status','type','latitude','longitude','email_verified'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */

	protected $hidden = [

		// 'password', 'remember_token',
		'password'
	];

	

	public static function registeruser($input = array()) {
		$inputArr = [
			'email' 		=> $input['email'],
			'type' 			=> $input['type'],
			'signup_type'	=> $input['subtype'],
			'address'		=> $input['address'],
			'address1'		=> $input['address1'],
			'phone_number'	=> $input['phone_number'],
			'hear_source'	=> $input['hear_source'],
			'password'		=> bcrypt($input['password']),
			'reset_key' 	=> $input['reset_key'],
			'country'		=> $input['country'],
			'state'			=> $input['state'],
			'city'			=> $input['city'],
			'zipcode'		=> $input['zipcode']
		];

		if($inputArr['signup_type']=='individual'){
			$inputArr['dob'] = $input['dob'];
		}

		if(isset($input['subscribe'])){
			$inputArr['subscribe'] = '1';
		}
		else{
			$inputArr['subscribe'] = '0';
		}

		if(isset($input['profile_image'])){
			$inputArr['profile_image'] = $input['profile_image'];
		}

		if(isset($input['referral'])){
			$inputArr['referred_by'] = $input['referred_by'];
		}

		if($inputArr['signup_type']=='individual'){
			$inputArr['first_name']	= $input['first_name'];
			$inputArr['last_name']	= $input['last_name'];
			$inputArr['gender']		= $input['gender'];
			$inputArr['dob']		= date("Y-m-d",strtotime($input['dob']));
		}
		else{
			if($inputArr['type']=='provider'){
				if(isset($input['language'])){
					$inputArr['language']	= $input['language'];
				}
				if(isset($input['about'])){
					$inputArr['about']		= $input['about'];
				}
				if(isset($input['neq_number'])){
					$inputArr['neq_number']	= $input['neq_number'];
				}
				if(isset($input['tps_price'])){
					$inputArr['tps_price']	= $input['tps_price'];
				}
				if(isset($input['tvq_price'])){
					$inputArr['tvq_price']	= $input['tvq_price'];
				}
			}
			else{
				if($inputArr['signup_type']=='business'){
					$inputArr['business_name'] = $input['business_name'];
				}
				else{
					$inputArr['retirement_home'] = $input['retirement_home'];
				}
			}
		}

		if($inputArr['type']=='provider'){
			if(isset($input['language'])){
				$inputArr['language']	= $input['language'];
			}
			if(isset($input['about'])){
				$inputArr['about']		= $input['about'];
			}
			if(isset($input['neq_number'])){
				$inputArr['neq_number']	= $input['neq_number'];
			}
			if(isset($input['tps_price'])){
				$inputArr['tps_price']	= $input['tps_price'];
			}
			if(isset($input['tvq_price'])){
				$inputArr['tvq_price']	= $input['tvq_price'];
			}

			if(isset($input['service_taxable'])){
				$inputArr['service_taxable']	= $input['service_taxable'];
			}
		}
		
		//$insertUser = DB::table('users')->insertGetId($inputArr);
		$insertUser = User::create($inputArr);

		if($insertUser->id){
			$userData = DB::table('users')->where('users.id',$insertUser->id)->get()->first();
			
			/* Add referral amount if refer code is used */
	        $ownWalletData = [
	        	'uw_user_id'            =>  $userData->id,
	        	'uw_transaction_type'   =>  'credit',
	        	'uw_txn_amt'            =>  10,
	        	'uw_txn_reason'         =>  'Referral amount discount',
	        	'uw_txn_date'           =>  date('Y-m-d H:i:s'),
	        	'uw_ref_money'          =>  'no',
			];

	        $insertOwnWallet = DB::table('user_wallet')->insert($ownWalletData);

	        /*if($insertOwnWallet){
	        	$userSettings = getUserSettingsViaEmail($userData->id);
	            $sendMsg1 = '';
	            
	            if(isset($userSettings['ref_money_deposited']) && $userSettings['ref_money_deposited']['msg']=='active'){
	            	$sendMsg1 = true;
	            }

	            $clientName = getNameOnTypeBasis($userData,'full');

	            $oMailData = [
	            	'user_id'       => $userData->id,
	            	'user_name'     => $clientName,
	            	'user_email'    => $userData->email,
	            	'link'          => SITE_HTTP_URL.'/my-wallet',
	            	'subject'       => 'Booking Completed',
	            	'type'          => 'ref_money_deposited',
	            	'mail_template' => 'common_notification',
	            	'message'       => "Your wallet has been credited. Click below button to view your wallet."
	            ];

	            if($sendMsg1){
	                $oMailData['phone'] = $bookingData->phone_number;
	            }
	            
	            $sendRefMailOwn = Mail::send(new MailSenders($oMailData));
	        }*/

			$link=SITE_HTTP_URL.'/user/verify/'.$input['reset_key'];

	        if($input['subtype']=='individual'){
	        	$userName = $input['first_name'].' '.$input['last_name'];
	        }
	        else{
	            if($input['type']=='provider'){
	                $userName = 'Business Name';
	            }
	            else{
	                if($input['subtype']=='home'){
	                    $userName = $input['retirement_home'];
	                }
	                else{
	                    $userName = $input['business_name'];
	                }
	            }
	        }

	        if($inputArr['signup_type']=='business' || $inputArr['signup_type']=='home'){
				$managerData = [];
				foreach($input['first_name'] as $key => $name){
					$managerData[$key]['first_name']= $name;
					$managerData[$key]['last_name']	= $input['last_name'][$key];
					$managerData[$key]['dob']		= date("Y-m-d",strtotime($input['dob'][$key]));
					$managerData[$key]['gender']	= $input['gender'][$key];
					$managerData[$key]['user_id']	= $insertUser->id;
					$managerData[$key]['created_at']= date("Y-m-d H:i:s");
					$managerData[$key]['updated_at']= date("Y-m-d H:i:s");
				}
				$insertManager = DB::table('users_managers')->insert($managerData);
			}
		}

		if(isset($input['profile_image']) && !empty($input['profile_image'])){
			$inputArr['profile_image'] = $input['profile_image'];
		}
		
		return $insertUser;
	}
}