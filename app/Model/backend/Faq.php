<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'category',
		'question_en',
		'question_fr',
		'answer_en',
		'sort_order',
		'status'
    ];

   
}
