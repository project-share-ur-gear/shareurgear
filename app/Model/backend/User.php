<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'first_name',
		'last_name',
		'email',
		'password',
        'referral_code',
        'reset_key',
		'status',
		'email_verified',
        'account_suspended'
    ];

    
}
