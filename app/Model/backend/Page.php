<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title_en',
		'meta_title_en',
		'meta_keywords_en',
		'meta_desc_en',
		 
    ];

   
}
