<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'title_en',
		'title_fr',
		'subject_en',
		'subject_fr',
		'content_en',
		'content_fr',
    ];

    
}
