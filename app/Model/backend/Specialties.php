<?php

namespace App\Model\backend;

use Illuminate\Database\Eloquent\Model;

class Specialties extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

   	public $table = "master_specialities";
    protected $fillable = [
		'name',
		'content',
		'image',
		'features',
		'status',
		'featured',
		'name_slug',
		'sort_order',
		'meta_keywords',
		'meta_description',

	];

   
}
