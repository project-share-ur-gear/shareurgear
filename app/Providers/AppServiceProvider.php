<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;

use DB;
use App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
		$locale=App::getLocale();
        $siteConfigData = DB::table('config')->get();
        foreach($siteConfigData as $key=>$configs){
            $config_data[$configs->config_key] = $configs->value ;
            $config_groups[$configs->config_group][$configs->config_key] = $configs->value;
        }
		
		$keywordData = DB::table('keywords')->get();
		$keyword_info=array();
        foreach($keywordData as $key=>$allList){
			$name='name_'.$locale;
            $keyword_info[$allList->name_key] = trim($allList->$name);
        }
		
		$pagesData = DB::table('pages')->get();
		$pagesInfo=array();
		foreach($pagesData as $allVal){
			$title='title_'.$locale;
			$pagesInfo[$allVal->id]=$allVal->$title;
		}
		
        View::share('site_configs', $config_data);
		View::share('keyword_info', $keyword_info);
		View::share('pagesInfo', $pagesInfo);
		View::share('locale', $locale);
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
