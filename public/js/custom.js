$(document).ready(function() {
	$('input').attr('autocomplete','off');
	$('input[type="email"],input[type="password"]').attr('autocomplete','new');
	setTimeout(function(){
		$('input.required,textarea.required,select.required').prev('label').append('<span class="needed"> *</span>');
		$('input.required').parents('.common-input').find("label").append('<span class="needed"> *</span>');
		$('input[type="checkbox"].required').prev('span').append('<span class="needed" style="color:#F00"> *</span>');
		$('select.required').parent().prev().append('<span class="needed" style="color:#F00"> *</span>');

		$("label").removeClass("active");

	},500);
	
	
	$('.profileToolTip').tooltip({html:true});
	$("#bell-notification").click(function() {
		$("#notification-box").toggle();
	});
	$("#profile-btn").click(function() {
		$("#profile-dropdown-list").toggle();
	});

	$(".selectpicker").selectpicker();

	$("#basic-addon-btn").on("click",function(){
		var currentAttr = $(this).parents(".input-group").find("input[type='password']").attr('type');
		if(currentAttr=='password'){
			$(this).parents(".input-group").find("input[type='password']").attr('type',"text");
			$(this).children().attr("src", APPLICATION_URL+"/public/img/show.svg");
		}
		else{
			$(this).parents(".input-group").find("input[type='text']").attr('type',"password");
			$(this).children().attr("src", APPLICATION_URL+"/public/img/hide.svg");
		}
	})
	
	if($(window).innerWidth()<991){
		$('.company-data-two.collapse').removeClass('show');
	}
	
	$("#email").keyup(function() {
		var str = $("#email").val();
		var string = str.replace(/\s+/g, "");
		$('#email').val(string);
	});

	if(ROUTE_NAME=='user-signup/{type}/{subtype}'){
		$(".dobdatepicker").datepicker({
			endDate: '-16y',
			autoclose: true,
			format:'mm/dd/yyyy'
		});
	}

	if(ROUTE_NAME=='/'){
		var hash = window.location.hash;
        if (hash != '') {
            var type = window.location.hash.substr(1);
			if(type=='how-it-works'){
				showHowItWorks();
			}
		}
	}
	else if(ROUTE_NAME=='login'){
		$('#password').rules("remove","minlength maxlength");
	}
	else if(ROUTE_NAME=='contact-us'){
		$("#phone").mask("999-999-9999",{autoclear: true});
	}
	
	
	
});

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

//thanks: http://javascript.nwbox.com/cursor_position/
function getSelectionStart(o) {
	if (o.createTextRange) {
		var r = document.selection.createRange().duplicate()
		r.moveEnd('character', o.value.length)
		if (r.text == '') return o.value.length
		return o.value.lastIndexOf(r.text)
	} else return o.selectionStart
}