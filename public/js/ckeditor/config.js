
var fileCsrfToken = $('meta[name=csrf-token]').attr("content");
 
  
CKEDITOR.editorConfig = function( config ) {

	config.toolbar_tiny =

	[

		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },

		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },

		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },

		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo' ] },

		'/',

		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor' ] },

		{ name: 'tools', items: [ 'Maximize' ] }	

		

	];



	config.toolbar_medium =

	[

		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', 'Preview', 'Print' ] },

		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },

		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'Undo', 'Redo' ] },

		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },		

		'/',

		{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize', 'TextColor', 'BGColor' ] },

		{ name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule'] },

		{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] }		

	];	



	//var ck_base_path =APPLICATION_URL+'/';

	// var ck_base_path = 'https://www.example.com/js/kcfinder/';



	

	/*config.filebrowserBrowseUrl 		= 	ck_base_path + 'browse.php?type=files';

	config.filebrowserImageBrowseUrl 	= 	ck_base_path + 'browse.php?type=images';

	config.filebrowserFlashBrowseUrl	= 	ck_base_path + 'browse.php?type=flash';

	config.filebrowserUploadUrl 		= 	ck_base_path + 'upload.php?type=files';

	config.filebrowserImageUploadUrl 	= 	ck_base_path + 'upload.php?type=images';

	config.filebrowserFlashUploadUrl 	= 	ck_base_path + 'upload.php?type=flash';

	config.allowedContent = true;

	config.ignoreEmptyParagraph = false;

	config.extraAllowedContent = 'ul ol li';*/

	

	// Remove some buttons provided by the standard plugins, which are

	// not needed in the Standard(s) toolbar.

	config.removeButtons = 'Underline,Subscript,Superscript';



	// Set the most common block elements.

	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.

	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.contentsCss = [ APPLICATION_URL+'/public/css/bootstrap.min.css', APPLICATION_URL+'/public/css/custom.css',APPLICATION_URL+'/public/js/ckeditor/contents.css'];

	config.filebrowserImageBrowseUrl = ADMIN_APPLICATION_URL+'/browse';

	config.filebrowserUploadUrl = ADMIN_APPLICATION_URL+'/mediaupload?_token=' + fileCsrfToken;

	config.extraPlugins = 'youtube';

	config.filebrowserUploadMethod = 'form';

	config.autoParagraph = false;

	

};



CKEDITOR.config.allowedContent = true;

CKEDITOR.dtd.$removeEmpty.span = 0;

CKEDITOR.dtd.$removeEmpty.i = 0;

CKEDITOR.dtd.$removeEmpty.p = 0;

CKEDITOR.config.fillEmptyBlocks = false;