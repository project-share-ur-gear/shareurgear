$.validator.setDefaults({
    highlight: function(element) {
        $(element).closest('.form-group').addClass('has-error');
        if (!$(element).hasClass("LoginElements")) {
            $(element).closest('.form-group.has-error').css('padding-bottom', '1px');
        }
    },
    unhighlight: function(element) {
        $(element).closest('.form-group').removeClass('has-error');
        $(element).closest('.input-group').removeClass('has-error');
    },
    submitHandler: function(form) {
        $('button').attr('disabled', true);
        /*$.blockUI({

            css: {

                border: 'none',

                padding: '5px',

                width: '24%',

                left: '39%',

                backgroundColor: '#ca0001',

                opacity: .9,

                color: '#ffffff',

            },

            message: "<div class='Loader'><div class='Text' style='letter-spacing: 2px;'>Just a moment</div></div>",

        });*/

        form.submit();

    },

    errorElement: 'span',

    errorClass: 'help-block',

    errorPlacement: function(error, element) {

        console.log($(element).attr('name'));

        if (element.closest('.input-group').length) {

            error.insertAfter($(element).closest('.input-group'));

        } else if ($(element).hasClass("chosen-select") || $(element).hasClass("chosen-select1") || $(element).hasClass("chosen-select2")) {

            error.insertAfter($(element).next(".chosen-container"));

        } else if ($(element).attr('id') == "profile_image") {

            error.insertAfter($('.image-uploading'));

        } else if ($(element).attr('id') == "agree") {

            error.insertAfter($('.signup-check-box'));

        } else {

            error.insertAfter($(element));

        }

    },

    invalidHandler: function(form, validator) {

        //alert($('#userEditModal form').find('.help-block:first').length);

        if ($('#userEditModal').hasClass('show') && $('#userEditModal form').find('.help-block:first').length > 0) {

            $('#userEditModal').animate({

                scrollTop: $('#userEditModal .modal-body').find('.help-block:visible').offset().top

            }, 500);

        }



    }

});

$('.amount').on('keypress', function(event) {
    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
    var input = $(this).val();
    if ((input.indexOf('.') != -1) && (input.substring(input.indexOf('.')).length > 2)) {
        event.preventDefault();
    }
});


jQuery.validator.addClassRules("checkemail", {
    remote: APPLICATION_URL + "/checkemail"
});

jQuery.validator.addClassRules("checkemailexist", {
    remote: APPLICATION_URL + "/checkemailexist"
});

$.validator.setDefaults({
    ignore: ":hidden:not(.ckeditor,.chosen-select1,#profile_image.ignore,#type.ignore,#client_signup_type.ignore,#provider_signup_type.ignore)"
});

function uploadProfileImage() {
    $('#profile_image').trigger('click');
}

function openProfileImageModal() {
    $('#image-modal').modal('show');
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;

    return true;
}

function deleteAccount(num) {
    comfirmBox($(num).data('message'), num);
}

function deleteService(num) {
    comfirmBox($(num).data('message'), num);
}

$(document).on("click", ".floatwalletcloser", function() {
    location.reload(true);
})

$('.profile_form').validate({
    rules: {
        current_password: { remote: APPLICATION_URL + "/checkoldpassword" },
        password: { minlength: 8, maxlength: 30 },
        password_confirmation: { equalTo: '#password', minlength: 8, maxlength: 30 },
    },
    messages: {
        phone_number: {
            minlength: 'Please enter your valid 10 digit phone number'
        },
        email: {
            required: fieldRequired
        },
        password: {
            required: fieldRequired
        },
        first_name: {
            required: fieldRequired
        }
    },
    hiddenRecaptcha: {
        required: function() {
            if (grecaptcha.getResponse() == '') {
                return true;
            } else {
                return false;
            }
        }
    }
});

function share_linkedin(num) {
    var url = $(num).data('url');
    window.open(
        'http://www.linkedin.com/shareArticle?mini=true&url=' + encodeURIComponent(url),
        'facebook-share-dialog',
        'width=626,height=436');
}

function homeVideoPlay(e) {
    $('#videoImageDiv').addClass('d-none');
    $('#videoDiv').removeClass('d-none');
    $('video').trigger('play');
    // What you want to do after the event
}

function comfirmBox(text, num) {
    if (num != '') {
        $('#delete-modal .modal-title').html(text);
        $('#deleteForm').attr('action', $(num).data('action'));
        $('#deleteForm #value').val($(num).data('value'));
    }
    $('#delete-modal').modal('show');
}

function deleteRecords() {
    var fullPath = $('#deleteForm').attr('action');
    $.ajax({
        type: "POST",
        type: 'DELETE',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: $('#deleteForm').attr('action'),
        data: { data_ids: $('#deleteForm #value').val() },
        success: function(result) {
            if (result.success) {
                $('#delete-modal').modal('hide');
                showpopupmessage(result.success, 'success');
                $('[data-value="' + $('#deleteForm #value').val() + '"]').closest('.main').remove();
                if ($('#deleteForm #value').val() == '') { // delete account
                    location.reload();
                }
            } else {
                showpopupmessage(result.error, '');
            }
        },
        async: false
    });
}

function showpopupmessage(msg, alert_type) {
    var alert_type = alert_type.toLowerCase();
    var msg_html = '<div id="alert" class="alert alert-' + alert_type + '"><button type="button" class="close" data-dismiss="alert">&times;</button>' + msg + '&nbsp;&nbsp;</div>';
    $('.alert').remove();
    if ($(".content-header").html()) {
        //Admin
        // $(".content-header").after(msg_html);
    } else {
        //Front
        // $("#content").prepend(msg_html);
    }
    $(".content-wrapper").append(msg_html);
    //setTimeout("HideInformation();", 10000);
}



function HideInformation() {

    $(".alert, #authMessage").animate({

        opacity: 0.25,

        right: "-=1500",

    }, 3000, function() {

        // Animation complete.

    });

    //$( ".alert" ).fadeOut("slow");

}

jQuery.validator.addMethod("nameUS", function(value, element) {
    return this.optional(element) || /^[a-z\s.''‘]+$/i.test(value);
}, "Only alphabetical characters");

function ucwords(str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function($1) {
        return $1.toUpperCase();
    });
}

function historyPusher(page, title) {
    if (page == 'clientoffersent' || page == 'clientoffersposted') {
        urlPath = APPLICATION_URL + '/my-jobs?type=' + page;
    } else if (page == 'providerjobsapplied' || page == 'providerofferreceived') {
        urlPath = APPLICATION_URL + '/provider/jobs?type=' + page;
    } else {
        urlPath = APPLICATION_URL + '/user-profile?type=' + page;
    }

    $("title").text('Proxivie | ' + title);
    $(".side-sub-link").removeClass("active");
    $("a[data-id='" + page + "']").addClass("active");
    window.history.pushState({ "html": 'Hello' }, "", urlPath);
}

$("input[type='file']").change(function() {
    readURL(this);
});


function getServices(num, value, serviceVal) {
    $.ajax({
        type: "POST",
        type: 'DELETE',
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: APPLICATION_URL + '/service/getservices',
        data: { data_ids: value },
        success: function(result) {
            if (result.success) {

                var options = '';
                $(result.success).each(function(index, elem) {
                    var selectedV = '';
                    if (elem.id == num) {
                        selectedV = "selected='selected'";
                    }
                    options += '<option value = "' + elem.id + '" ' + selectedV + ' >' + elem.title + '</option>';
                })

                //console.log(num);
                //console.log(options);

                $("#us_service_id").append(options);
                $('.selectpicker').selectpicker('destroy');
                setTimeout(function() {
                    $('.selectpicker').selectpicker();
                }, 500);

            } else {
                showpopupmessage(result.error, '');
            }
        },
        async: false
    });
}


function readURL(input) {

    if (input.files && input.files[0]) {

        var file_size = input.files[0].size;
        var one_mb = 10485760;

        var get_mb = file_size / one_mb;

        if (get_mb >= IMAGE_VALID_SIZE) {

            showpopupmessage("Uploaded image should be less than " + IMAGE_VALID_SIZE + "MB in size.", 'danger');

            setTimeout(function a() {

                $(input).val('');

            }, 500);

        } else {

            var checkFlag = loadMime(input.files[0], input);

        }

    }

}


function saveProfileImage() {
    $('#imageForm').submit();
}


function loadMime(file, num) {



    var inputOne = num;

    //List of known mimes

    var mimes = [

        {

            mime: 'image/jpeg',

            pattern: [0xFF, 0xD8, 0xFF],

            mask: [0xFF, 0xFF, 0xFF],

        },

        {

            mime: 'image/png',

            pattern: [0x89, 0x50, 0x4E, 0x47],

            mask: [0xFF, 0xFF, 0xFF, 0xFF],

        }

        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern

    ];



    function check(bytes, mime) {

        for (var i = 0, l = mime.mask.length; i < l; ++i) {

            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {

                return false;

            }

        }

        return true;

    }



    var blob = file.slice(0, 4); //read the first 4 bytes of the file

    var reader = new FileReader();

    var errorFlag = 0;

    reader.onloadend = function(e) {

        var fileName = file.name;

        var extension = fileName.substr((fileName.lastIndexOf('.') + 1));

        if (e.target.readyState === FileReader.DONE) {

            var bytes = new Uint8Array(e.target.result);

            for (var i = 0, l = mimes.length; i < l; ++i) {

                if (check(bytes, mimes[i])) {

                    errorFlag = 0;

                    break;

                } else {

                    errorFlag++;

                }

            }



            if (errorFlag == 0 || extension == 'webp' || extension == 'WEBP' || extension == 'svg' || extension == 'SVG' || extension == 'gif') {

                var reader = new FileReader();

                reader.onload = function(num) {

                    var image = new Image();

                    image.src = num.target.result;

                    var width = 0;

                    image.onload = function() {

                        var width = image.naturalWidth;

                        var height = image.naturalHeight;

                        if ($(inputOne).attr('id') == 'profile_image') {

                            $('#profile_image_preview').css('background-image', 'url("' + num.target.result + '")');
                            $('#profile_image_preview').attr('src', num.target.result);
                            $(num).removeClass('required');
                            $(num).removeAttr('required');

                            /*if(width < 60 && extension!='webp' && extension!='WEBP' && extension!='svg' && extension!='SVG'){
                            	showpopupmessage("Please upload a bigger image", "danger");
                            }

                            else{

                            }*/

                        }

                    };

                }

                reader.readAsDataURL(file);



            } else if (extension != 'svg' && extension != 'SVG' && !$(inputOne).hasClass('videoValid')) {

                showpopupmessage(invalidImage, "danger");

                $(num).val('');

            }

        }

    };

    reader.readAsArrayBuffer(blob);



}

$(function() {
    $(document).on("click", ".reviewpopper", function() {
        var jobu = $(this).data('jobu');
        var clientId = '';
        $.ajax({
            url: APPLICATION_URL + '/view-client-review',
            type: "POST",
            data: { job_user: jobu },
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(response) {
                $("#bookingModalCenter").modal("hide");
                if (response) {
                    setTimeout(function() {
                        $("#clientReviewModal").html(response);
                        $("#clientReviewModal").modal('show');
                    }, 500);
                } else {
                    $.notify({ message: "No reviews given!" }, { type: 'danger' });
                }
            }
        })
    })
})


function loadMoreCities(num) {

    var startVal = parseInt($(num).data('start')) + 1;

    $.ajax({

        type: "POST",

        type: 'DELETE',

        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },

        url: APPLICATION_URL + '/get-more-cities',

        data: { start: startVal },

        success: function(result) {

            //console.log(result);

            //result=JSON.parse(result);

            var cityInfo = '';

            for (var i = 0; i < result.length; i++) {

                if (locale == 'en')

                    var nameVal = result[i].name_en;

                else

                    var nameVal = result[i].name_fr;

                cityInfo += '<li><a href="#" class="location-name">' + nameVal + '</a> </li>';

            }

            $('#cityBox').append(cityInfo);

            $(num).data('start', startVal);

            if (result.length == 0) {

                $('#loadMoreCity').remove();

            }



        },

        async: false

    });
}







$(".categoryselector").on("change", function() {
    var selVal = $(this).val();
    $.ajax({
        type: "GET",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: APPLICATION_URL + '/get-sub-categories',
        data: { selVal: selVal },
        success: function(result) {
            var html = '<option value="">' + ChooseTxt + '</option>';
            var resultData = result;

            $(resultData).each(function(index, element) {
                html += '<option value="' + element.id + '">' + element.name_en + '</option>';
            });
            $('.serviceselector').removeClass('custom-select');
            $('.serviceselector').selectpicker('destroy');
            $(".serviceselector").html(html);
            $(".serviceselector").selectpicker();
            // $(".serviceselector").find(".dropdown-menu").mCustomScrollbar({theme:"dark-3"});
        },
        async: false
    });
});

$(function() {
    // $(window).on("load",function(){
    // 	$('.dropdown-menu').mCustomScrollbar({theme:"dark-3"});
    // });

    if (ROUTE_NAME == 'user-profile') {
        let searchParams = new URLSearchParams(window.location.search)
        if (searchParams.has('type')) {
            let param = searchParams.get('type');
            if (param == 'edit' || param == 'password' || param == 'experience' || param == 'payout' || param == 'background') {
                $(".profile-navigatorrs").find("[data-type='" + param + "']").click();
            }
        }
    }
});

function ordinal_suffix_of(i) {
    var j = i % 10,
        k = i % 100;
    if (j == 1 && k != 11) {
        return i + "st";
    }
    if (j == 2 && k != 12) {
        return i + "nd";
    }
    if (j == 3 && k != 13) {
        return i + "rd";
    }
    return i + "th";
}


function showHowItWorks()

{

    if (ROUTE_NAME == '/') {
        $('#box,#btn').removeClass('active');
        $('html,body').animate({

            scrollTop: $('#howitWorks').position().top - 80

        }, 'slow');

    } else {

        window.location.href = APPLICATION_URL + '#how-it-works';

    }
}

function showMoreContent(num, id) {
    $('#moreReadModal').find('img').attr('src', $('#' + id + 'ConIcon').attr('src'));
    $('#moreReadModal').find('h2').html($('#' + id + 'ConTitle').html());
    $('#moreReadModal').find('.description').html(($('#' + id + 'ConText').html()));
    $('#moreReadModal').modal('show');
}

function nl2br(str, is_xhtml) {
    if (typeof str === 'undefined' || str === null) {
        return '';
    }
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function switchToFaq(num) {
    $('.ckfaq').removeClass("faq-active");
    $('#ck' + num).addClass("faq-active");
    $(".faq-data-group").addClass("d-none");
    $('#' + num + 'FaqBlock').removeClass("d-none");
    $('html,body').animate({
        scrollTop: $('#' + num + 'FaqBlock').offset().top
    }, 500);
}

function setSignUpType(num, value) {
    $('#firstBox').find('.client-round').removeClass('active');
    $('#firstBox').find('.help-block').remove();
    $(num).addClass('active');
    $('#type').val(value);
}

function setSignUpClientType(num, value) {
    $('#clientSecondBox').find('.client-round').removeClass('active');
    $(num).addClass('active');
    $('#client_signup_type').val($('#clientSecondBox').find('.client-round.active').data('value'));
    $('#clientSecondBox').find('.help-block').remove();
    $('#client_signup_type').val($('#clientSecondBox').find('.client-round.active').data('value'));
}

function setSignUpProviderType(num, value) {
    $('#providerSecondBox').find('.client-round').removeClass('active');
    $(num).addClass('active');
    $('#provider_signup_type').val($('#providerSecondBox').find('.client-round.active').data('value'));
    $('#providerSecondBox').find('.help-block').remove();
    $('#provider_signup_type').val($('#providerSecondBox').find('.client-round.active').data('value'));
}

function refreshErrorMsg() {
    window.onbeforeunload = function() {
        return "Are you sure you want to close the window?";
    }
}

function topslider() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
}

function showSignUpNextOpt() {
    if ($('#registerForm').valid()) {
        $('#firstBox').addClass('d-none');
        $('#' + $('#type').val() + 'SecondBox').removeClass('d-none');
        $('#' + $('#type').val() + '_signup_type').addClass('ignore');

        // refreshErrorMsg();
        topslider();
    }
}

function showClientSignUpDiv() {
    if ($('#registerForm').valid()) {
        // $('#clientSecondBox').addClass('d-none');
        // $('#'+$('#type').val()+$('#client_signup_type').val()+'Box').removeClass('d-none');
        var uType = $('#type').val();
        var uSubType = $('#client_signup_type').val();
        var refCode = $('#referral').val();
        if (refCode == '') {
            window.location.assign(APPLICATION_URL + '/user-signup/' + uType + '/' + uSubType);
        } else {
            window.location.assign(APPLICATION_URL + '/user-signup/' + uType + '/' + uSubType + '?referral=' + refCode);
        }
    }
}

function showProviderSignUpDiv() {
    if ($('#registerForm').valid()) {
        //$('#providerSecondBox').addClass('d-none');
        //$('#'+$('#type').val()+$('#provider_signup_type').val()+'Box').removeClass('d-none');
        var uType = $('#type').val();
        var uSubType = $('#provider_signup_type').val();
        var refCode = $('#referral').val();
        if (refCode == '') {
            window.location.assign(APPLICATION_URL + '/user-signup/' + uType + '/' + uSubType);
        } else {
            window.location.assign(APPLICATION_URL + '/user-signup/' + uType + '/' + uSubType + '?referral=' + refCode);
        }
    }
}

$(document).on("click", ".confirm-phone-twilio", function() {
    var phone = $(this).parents(".common-input").find("input[name='phone_number']").val();

    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    var str = phone;
    if (numberRegex.test(str)) {

    } else {
        alert('Invalid phone number');
        return false;
    }

    if (phone.length != 10) {
        alert('Invalid phone number');
        return false;
    }

    $(".pinbox").remove();

    var otpbox = '';
    otpbox += '<div class="input-group mt-2 pinbox">';
    otpbox += '<input placeholder="' + EnterPIN_placeholder + '" required="required" class="form-control digits required" maxlength="4" name="otp_number" type="text" aria-required="true" autocomplete="off">';
    otpbox += '<div class="input-group-append">';
    otpbox += '<span class="input-group-text input-confirm confirm-pin-twilio" id="">' + ConfirmPINTxt + '</span>';
    otpbox += '</div>';
    otpbox += '</div>';

    $(this).parent().after(otpbox);

    $(this).parent().attr('disabled', 'disabled');
    $(this).attr('disabled', 'disabled');
    $(this).attr('style', 'cursor:not-allowed');
    $(this).removeClass('confirm-phone-twilio');

    var that = $(this);
    var timeleft = 60;
    var downloadTimer = setInterval(function() {
        timeleft--;
        $("#retimer").text(ResendPinText + " " + timeleft)
        if (timeleft <= 0) {
            $("#retimer").text(clickConfirmResendPinText);

            $(that).parent().removeAttr('disabled');
            $(that).removeAttr('disabled');
            $(that).removeAttr('style');
            $(that).addClass('confirm-phone-twilio');

            clearInterval(downloadTimer);
        }
    }, 1000);

    $.ajax({
        type: "POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: APPLICATION_URL + '/confirm-phone',
        data: { phone: phone },
        success: function(result) {
            $(".confirm-pin-twilio").parents(".pinbox").find("input[name='otp_number']").val(result);
            showpopupmessage(Confirmation_PIN_senttext + ' ' + phone, 'success');


        }
    })
})

$(document).on("click", ".confirm-pin-twilio", function() {
    var pin = $(this).parents(".common-input").find("input[name='otp_number']").val();
    var phone = $(this).parents(".common-input").find("input[name='phone_number']").val();
    var that = $(this);

    $.ajax({
        type: "POST",
        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        url: APPLICATION_URL + '/confirm-pin',
        data: { pin: pin, phone: phone },
        success: function(result) {
            if (result == 'pin_verified') {
                showpopupmessage(pinAcceptedText, 'success');

                $(".pinbox").addClass('d-none');
                $("#basic-confirm-provider").text(confirmedText);
                $("#basic-confirm-provider").addClass('confirmator');
                $(that).parents(".common-input").find("input[name='phone_number']").attr("readonly", "readonly");
                $("#retimer").remove();
            }
        }
    })
})

function validateSignUpForm(num) {
    var currentStep = parseInt($(num).data('current'));
    var nextStep = currentStep + 1;
    var phoneConfirmed = 0;
    if ($('#registerForm').valid()) {
        if (currentStep == '1') {
            if ($("#basic-confirm-provider").hasClass('confirmator')) {
                $('.stepsDiv').addClass('d-none');
                $('#Step' + nextStep + 'Div').removeClass('d-none');
                $('.progressbar .round-one').removeClass('active');
                $('[data-step="' + nextStep + '"]').addClass('active');

                for (var i = 1; i <= nextStep; i++) {
                    $('[data-step="' + i + '"]').removeAttr('disabled');
                }

                topslider();
            } else {
                showpopupmessage(confirmPhoneToProceedText, "danger");
                return false;
            }
        }

        if (currentStep == '2') {
            $("#registerForm").submit();
        }
    }
}

function switchToSignUpSection(num) {
    var currentStep = parseInt($(num).data('step'));
    if (!$(num).attr('disabled')) {
        $('.stepsDiv').addClass('d-none');
        $('#Step' + currentStep + 'Div').removeClass('d-none');
        $('.progressbar .round-one').removeClass('active');
        $('[data-step="' + currentStep + '"]').addClass('active');
    }
}

function changeAccount() {
    $('#accountBox').removeClass('d-none');
    $('#change_account').remove();
    $('.border-lines').removeAttr("style");
    $('.bottom_box').removeClass('d-none');
}


jQuery.validator.addMethod("url", function(val, elem) {

    if (val.length == 0) {

        return true;

    }

    if (!/^(https?|ftp):\/\//i.test(val)) {

        val = 'https://' + val;

        $(elem).val(val);

    }

    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);

});




function getdiff(s) {
    var secs = Math.round(s / 1000);
    var modsecs = ((Math.round(s / 1000)) % 60); //remaining secs if not even
    var mins = Math.round(s / 1000 / 60);
    var modmins = ((Math.round(s / 1000 / 60)) % 60); //mins remaining if not even
    var modhrs = ((Math.round(s / 1000 / 60 / 60)) % 24); //mins remaining if not even

    var hrs = Math.round(s / 1000 / 60 / 60);
    if (modmins >= 30) {
        modhrs = modhrs - 1;
    }

    var enddiff = [
        modhrs
    ];
    var arr = jQuery.map(enddiff, function(modhrs) {
        return modhrs + ":" + modmins;
    });
    console.log(arr);
    //$('#offerModalLongTitle').text("Difference is " +arr);
}

jQuery.extend(jQuery.validator.messages, {
    required: fieldRequired,
    remote: "Please fix this field.",
    email: validEmailText,
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: validNumber,
    digits: digitsOnly,
    creditcard: "Please enter a valid credit card number.",
    equalTo: "Please enter the same value again.",
    accept: "Please enter a value with a valid extension.",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});


function isNumberKey(evt, element) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
        return false;
    else {
        var len = $(evt).val().length;
        var index = $(evt).val().indexOf('.');
        if (index > 0 && charCode == 46) {
            //return false;
        }

        if (index > 0) {
            var CharAfterdot = (len + 1) - index;
            if (CharAfterdot > 3) {
                return false;
            }
        }
    }
    return true;
}



function validateEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

jQuery.validator.addMethod("passcheck", function(e, t) {
    return this.optional(t) || /^(?=.*\d)(?=.*[a-zA-Z])[\w~@#$%^&*+=`|{}:;!.?\"()\[\]-]{8,1000}$/.test(e)
}, "Password must contain at least 8 characters including one letter and one number");

jQuery.validator.addMethod("email", function(value, element) {
    return this.optional(element) || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
}, validEmailText);

function freezeButton(t, e, o) {
    switch (o) {
        case "active":
            $(t).attr("disabled", !1), $(t).html(e);
            break;
        case "disabled":
            $(t).attr("disabled", "disabled"), $(t).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ' + e)
    }
}
// $('.nextmilestone').click(function() {
//     swal({
//         title: "",
//         text: "This is part of next milestone!",
//         icon: "success",


//     });
// });

$('.noalpha').keypress(function(e) {
    var arr = [];
    var kk = e.which;

    for (i = 48; i < 58; i++)
        arr.push(i);

    if (!(arr.indexOf(kk) >= 0))
        e.preventDefault();
});

function showAppAlert(title, msg, type) {
    /* NOT WORKING NOW */
    /*swal({   
      title: 'Error!',
      text: 'Do you want to continue',
      type: 'error',
      timer:3000,
      confirmButtonText: 'Cool',
      showConfirmButton:false     
    });*/

    swal({
        title: title,
        text: msg,
        type: type,
        /*timer:5000,*/
        confirmButtonText: 'Ok',
        showConfirmButton: true
    });
}


function multiUploader(primary_photo) {

    var acceptedfiless = '.jpg, .png, .jpeg';
    var numberoffiles = 10;

    if ($("#documentUploader").length == 1) {
        docMydropzone = $("#documentUploader").dropzone({
            init: function() {
                var thisDropzone = this;
            },

            //type: 'POST',
            type: 'POST',
            url: APPLICATION_URL + '/pro-documents',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            dataType: 'json',


            init: function() {
                var that = this;
            },
            accept: function(file, done) {
                var str1 = file.type;
                var str2 = "image";
                // console.log(file);


                if (str1.indexOf(str2) != -1) {
                    /*     if ($('.dz-preview').length == 10) {
                            $('#documentUploader').hide();
                        } */
                    if ($('.dz-preview').length >= 1) {
                        $('#onepicreq').hide();
                    } else {
                        $('#onepicreq').show();
                    }
                    if ($('.dz-preview').length > numberoffiles) {
                        //$('#documentUploader').hide();
                        this.removeFile(file);
                        done("check");
                        showAppAlert('You can upload maximum ' + numberoffiles + ' files only', "error");
                    } else {
                        var IsTrue = checkMimeFileType(file, "image", done);
                    }

                } else {
                    freezeButton('#startedbtn', $('#startedbtn').data('inprocess'), 'disabled');
                    done();
                }

            },
            success: function(file, response) {
                var result = response;
                /*console.log(file.previewElement); */
                var filetypee = file.type;
                var filesize = file.width;
                $('#documentPreview').removeClass('d-none');
                if (result == 'false' || !result) {
                    freezeButton('#startedbtn', $('#startedbtn').data('default'), 'active');
                    var _this = this;
                    file.previewElement.parentNode.removeChild(file.previewElement);

                    showAppAlert("Please Select Valid Image", "warning");
                    done("Please Select Valid Image");

                } else {



                    freezeButton('#startedbtn', $('#startedbtn').data('default'), 'active');
                    var image_name = result.name;
                    $('#documentPreview .dz-image-preview:last-child').attr("id", image_name);
                    $(file.previewTemplate).append('<input class="server_file" type="hidden" value="' + image_name + '" /><input class="server_file_type" type="hidden" value="0" /><input type="hidden" name="doc_org_name[' + image_name + ']" value="' + file.name + '" />');
                    if (primary_photo != '') {
                        $(file.previewTemplate).append("<div class='check-boxs'><div class='checks custom-checks'> <label class='checkbox'> Make it Primary<input type='radio' class='add' name='primary_image' value='" + image_name + "'  required > <span class='checkmark' data-id=" + image_name + "></span></label></div></div>");
                    } else {
                        $(file.previewTemplate).append("<div class='check-boxs'><div class='checks custom-checks'> <label class='checkbox'> Make it Primary<input type='radio' class='add' name='primary_image' value='" + image_name + "' required checked> <span class='checkmark'  data-id=" + image_name + "></span></label></div></div>");
                    }
                }
            },
            timeout: 30000000000,
            error: function(file, message) {
                var _this = this;
                file.previewElement.parentNode.removeChild(file.previewElement);
                _this.removeFile(file);
                showAppAlert('Please select the Valid File  or file is too large to accept.');
                freezeButton('#startedbtn', $('#startedbtn').data('default'), 'active');
            },
            canceled: function(file) {
                var _this = this;
                file.previewElement.parentNode.removeChild(file.previewElement);
                _this.removeFile(file);
            },

            acceptedFiles: "image/*",
            acceptedFiles: acceptedfiless,
            autoDiscover: false,
            addRemoveLinks: true,
            maxFilesize: 10,
            previewsContainer: "#documentPreview",
            removedfile: function(file) {
                var _this = file;
                var currentFile = $(file.previewTemplate).children('.server_file').val();

                var fileType = $(file.previewTemplate).children('.server_file_type').val();
                if (typeof currentFile != "undefined") {
                    $.ajax({
                        url: APPLICATION_URL + "/remove-files",
                        type: 'DELETE',
                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                        data: { file: currentFile, type: fileType },
                        success: function(result) {
                            $('#documentUploader').show();
                            $('#startedbtn').removeAttr("disabled");
                            if (result) {
                                _this.previewElement.parentNode.removeChild(file.previewElement);

                            } else {
                                showAppAlert('Something went wrong', "error");
                            }
                        }
                    });

                }
            }
        });
    }
}

function isEmpty(el) {
    return !$.trim(el.html())
}

function removeFileEdit(id, proid, page) {
    swal({
            title: "Are you sure?",
            text: "Are you sure you want to remove \n selected media?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            cancelButtonColor: "#DD6B55",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: APPLICATION_URL + "/remove-edit-files",
                    type: 'DELETE',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data: { id: id, proid: proid },
                    success: function(result) {
                        var c = document.getElementById(id);
                        if (result.trim() == 'true') {
                            $(c).remove();
                        }
                        if (result.trim() == 'false') {
                            $.notify({
                                message: "Invalid Request."
                            }, {
                                type: 'danger',
                                timer: 2000,
                            });
                        }
                    }
                });
            }
        }
    );

}

function checkMimeFileType(Obj, Type, done) { // for video and image
    let validFile = false;


    if (Type == 'image') {

        //return;
        if (1) {
            var Header = '';
            var _URL = window.URL || window.webkitURL;
            var file = Obj;
            var blob = file; // See step 1 above
            var fileReader = new FileReader();


            fileReader.onloadend = function(e) {
                var arr = (new Uint8Array(e.target.result)).subarray(0, 4);
                // console.log('arr');

                var header = "";
                for (var i = 0; i < arr.length; i++) {
                    header += arr[i].toString(16);
                }
                Header = getMimeType(header);

                if (Header != 'unknown') {
                    {
                        freezeButton('#startedbtn', $('#startedbtn').data('inprocess'), 'disabled');
                        done();
                    }

                } else {
                    showAppAlert("", "Please Select Valid Image", "warning");
                    done("Please Select Valid Image");
                }
            };

            fileReader.readAsArrayBuffer(blob);

        }
    }
}

function getMimeType(header) {

    var type = '';

    switch (header) {

        case "89504e47":

            type = "image/png";

            break;

        case "47494638":

            type = "image/gif";

            break;

        case "ffd8ffe0":

        case "ffd8ffe1":

        case "ffd8ffe2":

        case "ffd8ffe3":

        case "ffd8ffe8":

            type = "image/jpeg";

            break;

        default:

            type = "unknown"; // Or you can use the blob.type as fallback

            break;

    }

    return type;

}
jQuery.validator.addMethod("validurl", function(val, elem) {
    if (val.length == 0) {
        return true;
    }
    if (!/^(https?|ftp):\/\//i.test(val)) {
        val = 'http://' + val;
        $(elem).val(val);
    }
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);
}, "Please enter the valid url.");