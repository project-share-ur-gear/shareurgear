/*

Admin panel js

*/



$(function(){

  CKEDITOR.on( 'instanceReady', function( ev ) {

  $('iframe.cke_wysiwyg_frame', ev.editor.container.$).contents().on('click', function() {

  ev.editor.focus();

  });

  });

  })

  

$(document).ready(function(){



	$('input').attr('autocomplete','off');

	$('input[type="email"],input[type="password"]').attr('autocomplete','new');

	CKEDITOR.config.allowedContent = true;

	CKEDITOR.config.protectedSource.push(/<i[^>]*><\/i>/g);

	if($('.editor').length>0){

		$('.editor').each(function(index, element) {

			

		   CKEDITOR.replace( $(element).attr('name'), {

				on: {

					instanceReady: function() {

						this.document.appendStyleSheet( 'https://use.fontawesome.com/releases/v5.7.1/css/all.css' );

					},

				},

				toolbar :[
            ['Source'],['Bold','Italic','Underline'],['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],['TextColor'],['Format'],['FontSize'],['Youtube','Image','Link']],

				height: 250

			});



        });

	}

	

	CKEDITOR.on('instanceReady', function () {

		$.each(CKEDITOR.instances, function (instance) {

			CKEDITOR.instances[instance].document.on("keyup", CK_jQ);

			CKEDITOR.instances[instance].document.on("paste", CK_jQ);

			CKEDITOR.instances[instance].document.on("keypress", CK_jQ);

			CKEDITOR.instances[instance].document.on("blur", CK_jQ);

			CKEDITOR.instances[instance].document.on("change", CK_jQ);

		});

	});

	

	//Hide alert message box

	//setTimeout('$(".alert, .custom-alert").slideUp(1000);', 10000);

  setTimeout("HideInformation();", 10000);



	//Start security code

	$('#reset_captcha').click(function(){

    captcha_url = APPLICATION_URL + 'users/getcaptcha';

    if(PANEL_PREFIX == 'admin'){

      captcha_url = APPLICATION_URL + 'admin/users/getcaptcha';

    }

    

	  $('#reset_captcha').attr('src',APPLICATION_URL+'img/reload.gif?y='+Math.random()*500);

      $.ajax({ url: captcha_url,

        type: "POST",

        data: ({rand : (Math.random()*500)}),

        success: function(data){  

          $('#captcha').attr('src', APPLICATION_URL+'img/captcha.jpg'+'?y='+Math.random()*500);

          $('#reset_captcha').attr('src',APPLICATION_URL+'img/reload-static.jpg?y='+Math.random()*500);

      }});

    });

    $('#reset_captcha').trigger('click');

    //End security code





    /* Start characters count for textarea */

    if($('.char_count')){

      $('.char_count').each(function(){

        var this_char_count = $(this);  //$('.char_count');

        var cc_id = $(this).attr('id');

        var cc_maxlength = $(this).attr('maxlength');

        var cc_cur_val = $(this).val();

        cc_maxlength = parseInt(cc_maxlength);

        var new_ele_id = cc_id+'_count';

        $('#'+new_ele_id).remove();

        $(this).after('<small id="'+new_ele_id+'" class="char_count_box"><span>'+cc_maxlength+'</span> characters remain.</small>');

        $(this).keyup(function () {

          var char_len = $(this).val();

          char_len = (cc_maxlength - char_len.length);

          $('#'+new_ele_id+' span').html(char_len);

        });

        $(this).trigger('keyup');

      });

    }

    /* End characters count for textarea */



/*

* Start grid check all functionality

*/

    $('#selectAll').click(function(event){  

        if(this.checked) { 

            $('.grid_chk').each(function(){ 

                this.checked = true;  

            });

        }else{

            $('.grid_chk').each(function(){

                this.checked = false;

            });         

        }

    });



    $('.grid_chk').click(function(event){

      var chk_stat = false;

      $('.grid_chk').each(function(){

          if(this.checked == false){

            chk_stat = true;

          };  

      });

      if(chk_stat){

        $('#selectAll').prop('checked', false);

      } else {

        $('#selectAll').prop('checked', true);

      }  

    });



    //per page form submit

	$('.per-page-data').change(function(){

	  $('#perPageRecordsForm').submit();

	});



	//sorting records in data grid

	/*$('.data-grid').find('.asc').append("&nbsp;&nbsp;<i class='fa fa-fw fa-sort-alpha-asc floatr'></i>");

  $('.data-grid').find('.desc').append("&nbsp;&nbsp;<i class='fa fa-fw fa-sort-alpha-desc floatr'></i>");*/



  //correct are below 2 lines

  $('.data-grid').find('.asc').append("&nbsp;&nbsp;<i class='fa fa-fw fa-sort-alpha-asc'></i>");

  $('.data-grid').find('.desc').append("&nbsp;&nbsp;<i class='fa fa-fw fa-sort-alpha-desc'></i>");

  $('.fa-sort-alpha-asc, .fa-sort-alpha-desc').eq(1).remove();



  	//allow numbers only

	$(".allow_numbers").keyup(function () { 

	  this.value = this.value.replace(/[^0-9\.]/g,'');

	});



  //allow telephone numbers only

  $(".allow_tel_numbers").keyup(function () { 

    this.value = this.value.replace(/[^0-9-,\.]/g,'');

  });



	//remove Web HostingLinux Reseller Hosting

	/*if(APPLICATION_URL!='http://localhost/matri/'){

		$('a').last().remove();

		$('a').eq(-1).remove();	

	}*/



});

function validateFloatKeyPress(el, evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    var number = el.value.split('.');
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    //just one dot
    if(number.length>1 && charCode == 46){
         return false;
    }
    //get the carat position
    var caratPos = getSelectionStart(el);
    var dotPos = el.value.indexOf(".");
    if( caratPos > dotPos && dotPos>-1 && (number[1].length > 1)){
        return false;
    }
    return true;
}

//thanks: http://javascript.nwbox.com/cursor_position/
function getSelectionStart(o) {
  if (o.createTextRange) {
    var r = document.selection.createRange().duplicate()
    r.moveEnd('character', o.value.length)
    if (r.text == '') return o.value.length
    return o.value.lastIndexOf(r.text)
  } else return o.selectionStart
}