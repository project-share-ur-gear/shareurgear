/* General Js Functions */

$.validator.setDefaults({

	//onfocusout: false,

    invalidHandler: function(form, validator) {},

	highlight:function(element){$(element).addClass('is-invalid');

    },

    unhighlight:function(element){$(element).removeClass('is-invalid');

    },

	submitHandler: function(form){

		/* $.blockUI({

			message: '<p class="blockUitxt" style="margin-bottom:0">Just a moment...</p>'

			, css: {

				color: '#fff'

				, border: '1px solid #cc0001'

				, backgroundColor: '#cc0001'

			}

		});*/

		setTimeout(function(){form.submit();},1000); 

    },

    errorElement:'span',

    errorClass:'invalid-feedback',

    errorPlacement: function(error, element){ console.log($(element).attr('name'));

        if(element.parent('.input-group').length){

            error.insertAfter(element.parent());

        }

		else if(element.attr("id")=="description"){

			error.appendTo($('#cke_description').parent('div'));

		}

		else if(element.attr("id")=="answer"){

			error.appendTo($('#cke_answer').parent('div'));

		}

		else if(element.attr("id")=="content"){

			error.appendTo($('#cke_content').parent('div'));

		}

		else if(element.attr("type")=="radio"){

			error.appendTo($(element).closest('div')).wrap("<div class='new'></div>");

		}

		else if($(element).hasClass("chosen-select1") || $(element).hasClass("chosen-select2")){

			error.appendTo($(element).next('.chosen-container'));

		}

		else{

            error.insertAfter(element);

        }

}});



$.validator.addMethod("passcheck", function(e, t) {

    return this.optional(t) || /^(?=.*\d)(?=.*[a-zA-Z])[\w~@#$%^&*+=`|{}:;!.?\>"()\[\]-]{8,1000}$/.test(e)

}, "Password must contain at least 8 characters including one letter and one number");



jQuery.validator.addMethod("imageValidCheck", function(a, b, c) {

    return c = "string" == typeof c ? c.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(b) || a.match(new RegExp(".(" + c + ")$", "i"))

},"Only images are allowed");



jQuery.validator.addMethod("videoValidCheck", function(a, b, c) {

    return c = "string" == typeof c ? c.replace(/,/g, "|") : "mp4", this.optional(b) || a.match(new RegExp(".(" + c + ")$", "i"))

},"Only mp4 videos are allowed");



$.validator.addClassRules({

	imageValid: {

         imageValidCheck:"jpg|png|jpeg|png|bmp|svg|JPG|PNG|JPEG|BMP|SVG",

    },

	videoValid: {

         videoValidCheck:"mp4|MP4",

    },

});



jQuery.validator.addClassRules("checkemail", {

    remote: ADMIN_APPLICATION_URL  + "/checkemail"

});



jQuery.validator.addClassRules("checkemailexist", {

    remote: ADMIN_APPLICATION_URL  + "/checkemailexist"

});



function isNumberKey(evt, element){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
	return false;
	else{
		var len = $(evt).val().length;
		var index = $(evt).val().indexOf('.');
		if (index > 0 && charCode == 46) {
			//return false;
		}
				
		if(index > 0){
			var CharAfterdot = (len + 1) - index;
			if (CharAfterdot > 3){
				return false;
			}
		}
	}
	return true;
}



$.validator.setDefaults({ ignore: ':hidden:not(.ckeditor,.chosen,.chosen-select1)'});

$('.profile_form').validate({

	rules:{

		current_password:{remote:ADMIN_APPLICATION_URL+"/checkoldpassword"},	

		password:{minlength:8 , maxlength:30},

		confirm_password:{equalTo:'#password' , minlength:8, maxlength:30},

		password_confirmation:{equalTo:'#password' , minlength:8, maxlength:30},

		site_logo:{extension:"jpg|JPG|png|PNG|jpeg|JPEG|webp|WEBP|svg|SVG"},

		site_email:{email:true},

		site_contact_email:{email:true},

		site_fee:{min:1,max:100,digits:true},

		cancellation_fee:{min:1,max:100,digits:true},

		site_twitter_link:{url:true},

		site_linkedin_link:{url:true},

		site_facebook_link:{url:true},

		site_instagram_link:{url:true},

		facebook_link:{url:true},

		twitter_link:{url:true},

		linkedin_link:{url:true},

		googleplay_link:{url:true},

		apple_link:{url:true},

		image:{extension:"jpg|JPG|png|PNG|jpeg|JPEG|webp|WEBP|svg|SVG"},

		content:{	

			required: function() 

			{

				CKEDITOR.instances.content.updateElement();

			},

		},

		description:{	

			required: function() 

			{

				CKEDITOR.instances.description.updateElement();

			},

		},

		description_en:{	

			required: function() 

			{

				CKEDITOR.instances.description_en.updateElement();

			},

		},

		description_fr:{	

			required: function() 

			{

				CKEDITOR.instances.description_fr.updateElement();

			},

		},

		client_password: {

            minlength: 8,

            maxlength: 30,

        },

        client_rpassword: {

            equalTo: '#client_password',

            minlength: 8,

            maxlength: 30

        },

		transcription:{extension:'jpg|JPG|png|PNG|jpeg|JPEG|webp|WEBP|svg|SVG|pdf|doc|docx|txt|PDF|DOC|DOCX|TXT'},

 	},

	messages:{

     site_logo:{extension:"Only images are allowed"},

	 image:{extension:"Only images are allowed"},

	},

 });



function CK_jQ() { 

	for (instance in CKEDITOR.instances) {

		CKEDITOR.instances[instance].updateElement();

	}

}

 





function showpopupmessage(msg, alert_type){

	var alert_type = alert_type.toLowerCase();

	//alert_type = 'success, danger, warning, info';



	//var msg_html = '<div class="alert alert-'+alert_type+' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+msg+'</div>';



	var msg_html = '<div id="alert" class="alert alert-'+alert_type+'"><button type="button" class="close" data-dismiss="alert">&times;</button>'+msg+'&nbsp;&nbsp;</div>';



	$('.alert').remove();

	if($(".content-header").html()){

	//Admin

	// $(".content-header").after(msg_html);

	} else {

	//Front

	// $("#content").prepend(msg_html);

	}

	$(".content-header").append(msg_html);

	window.setTimeout(function() {

		$(".alert").hide("slow");

	}, 6000);

}



window.setTimeout(function() {

	$(".alert").hide("slow");

}, 6000);



/*function hideInformation(){

  $(".alert, #authMessage").animate({

    opacity: 0,

    right: "-1500",

  }, 3000, function() {

    // Animation complete.

  });

	//$( ".alert" ).fadeOut("slow");

}*/



function getCourses(num,value) {

	$.ajax({

		type: "POST",

		type: 'DELETE',

		headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

		url: APPLICATION_URL +'/course/getcourses',

		data: {data_ids:num},

		success: function(result) {

			if(result.success){

				

				$('#group_course option').remove();

				/*$('#add-current-course #title').append($("<option/>", {

					value: '',

					text: 'Search for your course'

				}));*/

				for (var i = 0; i < result.success.length; i++) {

					$('#group_course').append($("<option/>", {

						value: result.success[i].id,

						text: result.success[i].title

					}));

					if (value != '') {

						var allVal=value.split(',');

						var checkVal=result.success[i].id;

							checkVal=checkVal.toString();

						if(allVal.indexOf(checkVal)!=-1){

							$('#group_course option[value="' + result.success[i].id + '"]').prop('selected', true);

						}

						

					}

				}

				

				/*$('#add-current-course #title').append($("<option/>", {

					value: 'add',

					text: 'Other'

				}));*/

				

				

				setTimeout(function a() {

					var config = {

					 '.chosen-select1'          : {placeholder_text_multiple:'Search for courses'},

					 '.chosen-select2'          : {placeholder_text_multiple:'Current Courses'},

					 '.chosen-select-deselect'  : {allow_single_deselect:true},

					 '.chosen-select-no-single' : {disable_search_threshold:10},

					 '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},

					 '.chosen-select-width'     : {width:"95%"}

					 }

					 for (var selector in config) {

					  $(selector).chosen(config[selector]);

					 }

					$('#group_course').trigger("chosen:updated");

				},1000);

			}

			else{

				showpopupmessage(result.error,'');	

			}

		},

		async:false

	});   



}





//remove unwanted links

$('document').ready(function(){

    /*$('a').last().remove();

    $('a').eq(-1).remove();*/ 

 

});



$("input[type='file']").change(function() {

  readURL(this);

});





function readURL(input) {

  if (input.files && input.files[0]) {

	  var file_size=input.files[0].size;

	  var one_mb=1048576;

	  var get_mb=file_size/one_mb;

		if($(input).attr('name')!='upload_sheet'){
		/* if(get_mb>=IMAGE_VALID_SIZE){
		toastr.options.closeButton = true;
		toastr.error("Uploaded image should be less than "+IMAGE_VALID_SIZE+"MB in size.");
		setTimeout(function a() {
		$(input).val('');
		},500);
		}
		else{*/
		var checkFlag=loadMime(input.files[0],input);
		//}

	  }

  }

}





function loadMime(file,num) {

  

  var inputOne=num;

    //List of known mimes

    var mimes = [

        {

            mime: 'image/jpeg',

            pattern: [0xFF, 0xD8, 0xFF],

            mask: [0xFF, 0xFF, 0xFF],

        },

        {

            mime: 'image/png',

            pattern: [0x89, 0x50, 0x4E, 0x47],

            mask: [0xFF, 0xFF, 0xFF, 0xFF],

        }

        // you can expand this list @see https://mimesniff.spec.whatwg.org/#matching-an-image-type-pattern

    ];



    function check(bytes, mime) {

        for (var i = 0, l = mime.mask.length; i < l; ++i) {

            if ((bytes[i] & mime.mask[i]) - mime.pattern[i] !== 0) {

                return false;

            }

        }

        return true;

    }



    var blob = file.slice(0, 4); //read the first 4 bytes of the file

	var reader = new FileReader();

	var errorFlag=0;

    reader.onloadend = function(e) {

		var fileName=file.name;

		var extension = fileName.substr( (fileName.lastIndexOf('.') +1) );

        if (e.target.readyState === FileReader.DONE) {

            var bytes = new Uint8Array(e.target.result);

            for (var i=0, l = mimes.length; i<l; ++i) {

                if (check(bytes, mimes[i])){

					errorFlag=0;

					break;

				}

				else{

					errorFlag++;

				}

            }

			

			if(errorFlag==0 || extension=='webp' || extension=='WEBP' || extension=='svg' || extension=='SVG' || extension=='gif'){

				var reader = new FileReader();

				reader.onload = function(num) {

					var image = new Image();

					image.src = num.target.result;

					var width=0;

					image.onload = function() {

						var width = image.naturalWidth;

						var height = image.naturalHeight;

						if(num=='check'){

							/*if(width<60 && extension!='webp' && extension!='WEBP' && extension!='svg' && extension!='SVG'){

								toastr.options.closeButton = true;

								toastr.error("Image is too short.Minimum width required is 60px");

							}

							else{

								

							}*/

						}

					};

				}

				reader.readAsDataURL(file);

			

			}

			else if(extension!='svg' && extension!='SVG' && !$(inputOne).hasClass('videoValid') && $(inputOne).attr('id')!='transcription'){

				showpopupmessage("Uploaded image is not valid", "error");

				$(num).val('');

			}

        }

    };

    reader.readAsArrayBuffer(blob);

	

}



function removeTags(str) {

  if ((str===null) || (str===''))

       return false;

  else

   str = str.toString();

  return str.replace(/<[^>]*>/g, '');

}



jQuery.validator.addMethod("url", function(val, elem) {

    if (val.length == 0) {

        return true;

    }

    if (!/^(https?|ftp):\/\//i.test(val)) {

        val = 'https://' + val;

        $(elem).val(val);

    }

    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(val);

});

jQuery.validator.addMethod("email", function(value, element) {

    return this.optional(element) || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);

}, "Please enter a valid email address.");

