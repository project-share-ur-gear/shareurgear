<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

// uncomment below line to create symbolic link of 'storage' folder into public folder
// Artisan::call('storage:link');

Route::group(['middleware' => ['guest']], function () {

    //Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'frontend\LanguageController@switchLang']);

    //Auth::routes(['verify' => true]);
    /*Route::get('/', function () {
       return view('welcome');
    });*/
	
	Route::get('/', 'frontend\StaticController@index')->name('frontend.index');
	//Route::get('/home', 'frontend\StaticController@index')->name('frontend.index');

    
    // ADMIN
	Route::get('admin', 'backend\Auth\LoginController@getLoginForm');
    Route::get('admin/login', 'backend\Auth\LoginController@getLoginForm');
    Route::post('admin/authenticate', 'backend\Auth\LoginController@authenticate');
    
    Route::get('admin/register', 'backend\Auth\RegisterController@getRegisterForm');
    Route::post('admin/saveregister', 'backend\Auth\RegisterController@saveRegisterForm');

    // Password Reset Routes...
    Route::get('admin/password/reset', 'backend\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('admin/password/email', 'backend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('admin/password/reset/{token}', 'backend\Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('admin/password/resetpassword/{token}', 'backend\Auth\ResetPasswordController@resetpassword');
	Route::get('admin/checkemailexist', 'backend\AdminController@checkemailexist')->name('admin.checkemailexist');

    //================================================
    
    // USER 
    Route::get('login', 'frontend\Auth\LoginController@getLoginForm')->name('frontend.login');
    Route::post('user/authenticate', 'frontend\Auth\LoginController@authenticate');
    
    Route::get('signup','frontend\Auth\RegisterController@signup')->name('customer.signup');

    // Route::get('signup/{type?}', 'frontend\Auth\RegisterController@signup');
    Route::post('user/saveregister', 'frontend\Auth\RegisterController@saveRegisterForm');
    Route::get('user/verify/{token}', 'frontend\Auth\RegisterController@verifyUser');

    Route::get('user-signup/{type}/{subtype}', 'frontend\Auth\RegisterController@usersignup');

    // Route::get('user/password/reset', 'frontend\Auth\ForgotPasswordController@getForgotPasswordForm');
    
    //Password Reset Routes...
    Route::get('forgot-password', 'frontend\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('user/password/email', 'frontend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('user/password/reset/{token}', 'frontend\Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('user/password/resetpassword/{token}', 'frontend\Auth\ResetPasswordController@resetpassword');
	Route::get('set-password/{token}', 'frontend\Auth\ResetPasswordController@showNewPasswordForm')->name('password.setpassword');
	Route::post('save-password/{token}', 'frontend\Auth\ResetPasswordController@savepassword');
	
	Route::get('user-profile', 'frontend\UserController@userprofile')->name('user.userprofile');
	Route::get('checkoldpassword', 'frontend\UserController@checkoldpassword')->name('user.checkoldpassword');
	Route::get('checkemail', 'frontend\UserController@checkemail')->name('user.checkemail');
	Route::get('checkemailexist', 'frontend\UserController@checkemailexist')->name('user.checkemailexist');
        
    Route::post('confirm-phone', 'frontend\Auth\RegisterController@confirmphone')->name('user.confirmphone');
    Route::post('confirm-pin', 'frontend\Auth\RegisterController@confirmpin')->name('user.confirmpin');

    /* Page */
    Route::get('page/show/{id}', 'frontend\PageController@show')->name('page.show');

    /* Contact Us */
    Route::get('contact-us', 'frontend\ContactusController@contactus')->name('contactus.create');
    Route::post('contact-us/store', 'frontend\ContactusController@store')->name('contactus.store');

    /* Static Pages Routes */
    
    Route::get('about', 'frontend\StaticController@about')->name('static.about');
    // Route::get('contact-us', 'frontend\StaticController@contact')->name('static.contact');
    Route::post('post-contact', 'frontend\StaticController@postcontact')->name('static.contact');
    // Route::post('post-contact-us', 'frontend\StaticController@postcontactus');

    Route::get('terms-and-conditions', 'frontend\StaticController@termsconditions')->name('static.termsconditions');
    Route::get('faq', 'frontend\StaticController@faq')->name('static.faq');
    Route::get('getmessagetotalcount', 'frontend\StaticController@getmessagetotalcount')->name('static.getmessagetotalcount');

    Route::get('webhook-response', 'frontend\StaticController@webhookresponse')->name('static.webhookresponse');
    // Route::get('account-setting', 'frontend\StaticController@accountsetting')->name('static.accountsetting');

    Route::get('error-page', 'frontend\StaticController@errorpage')->name('static.errorpage');


    // 2nd milestone
     
    Route::get('add-products', 'frontend\StaticController@addproducts')->name('static.addproducts');
    // Route::get('manage-products', 'frontend\StaticController@manageproduct')->name('static.manageproducts');
    // Route::get('manage-availability', 'frontend\StaticController@manageavailability')->name('static.manageavailability');
    Route::get('browse-rental', 'frontend\StaticController@browserental')->name('static.browserental');
    Route::post('getbounddata', 'frontend\StaticController@getbounddata')->name('static.getbounddata');
    Route::post('getmoreproduct', 'frontend\StaticController@getmoreproduct')->name('static.getmoreproduct');

    // Route::get('payout-setting', 'frontend\StaticController@payoutsetting')->name('static.payoutsetting');

    // 3rd Milestone

    Route::get('user-profile/{id}', 'frontend\StaticController@userprofile')->name('static.userprofile');
    Route::post('getmoreuserprofilelist', 'frontend\StaticController@getmoreuserprofilelist')->name('static.getmoreuserprofilelist');
    
   
    Route::get('rental-detail-page/{id}', 'frontend\StaticController@rentaldetailpage')->name('static.rentaldetailpage');
    Route::post('getmorequestion', 'frontend\StaticController@getmorequestion')->name('static.getmorequestion');
 
    

    Route::get('add-remove-wishlist/{id}', 'frontend\StaticController@addremovewishlist')->name('static.addremovewishlist');
    Route::post('ask-question', 'frontend\StaticController@askquestion')->name('static.askquestion');

    // 4th Milestone

    Route::get('sales-transactions', 'frontend\StaticController@salestransactions')->name('static.salestransactions');

    Route::get('verify-email/{key}', 'frontend\UserController@verifiedemail');

    
    Route::post('sub-category', 'frontend\ProductController@subcategory')->name('product.subcategory');
 
    /** Get Reviews */
    Route::match(['get','post'],'get-reviews', 'frontend\BookingController@getreviews')->name('booking.getreviews');
    
    /** ----------  */
    Route::match(['get','post'],'get-unapporoved-bookings', 'frontend\CronController@checkunapprovedbookings')->name('booking.checkunapprovedbookings');
    Route::match(['get','post'],'check-checkinout', 'frontend\CronController@checkcheckinout')->name('booking.checkcheckinout');
    Route::match(['get','post'],'release-funds', 'frontend\CronController@releasefunds')->name('booking.releasefunds');
    
});

Route::group(['middleware' => ['user']], function () {
	//Route::auth();
    Route::post('user/logout', 'frontend\Auth\LoginController@getLogout');
    Route::get('user/dashboard', 'frontend\UserController@dashboard');
    
    Route::get('account-setting', 'frontend\ProfileController@accountsetting');
    Route::get('payout-setting', 'frontend\ProfileController@payoutsetting');
    Route::post('update-profile', 'frontend\ProfileController@updateprofile')->name('profile.updateprofile');
    Route::post('stripe-connect', 'frontend\ProfileController@stripeconnect')->name('profile.stripeconnect');

    Route::get('create-profile', 'frontend\UserController@createprofile')->name('user.createprofile');
	Route::patch('user/saveprofile', 'frontend\UserController@saveprofile')->name('user.saveprofile');
	Route::patch('user/saveprofileimage', 'frontend\UserController@saveprofileimage')->name('user.saveprofileimage');
    Route::patch('user/update', 'frontend\UserController@update')->name('user.update');
	
    
    Route::get('user/changepassword', 'frontend\UserController@changepassword')->name('user.changepassword');
    Route::patch('user/updatepassword', 'frontend\UserController@updatepassword')->name('user.updatepassword');
	Route::delete('user/deleteaccount', 'frontend\UserController@deleteaccount')->name('user.deleteaccount');
	
    Route::get('user/settings', 'frontend\UserController@settings')->name('user.settings');
    Route::post('user/savesettings', 'frontend\UserController@savesettings');
    Route::post('user/payout-settings','frontend\UserController@connectstripe');
    Route::post('user/background-check','frontend\UserController@checkbackground');
    Route::get('user/paypal-check','frontend\UserController@checkpaypal')->name('user.paypal');
    Route::post('user/upload-id-proof','frontend\UserController@uploadidproof');
    
 
 
    Route::get('client/receive-offer', 'frontend\UserController@clientreceiveoffer')->name('user.receiveoffer');
    Route::get('client/receive-offer-detail', 'frontend\UserController@clientreceiveofferdetail')->name('user.receiveofferdetail');
    
    Route::get('user/dashboard/', function () {
        return view('frontend.dashboard');
    });
 
    Route::get('manage-availability/{id}', 'frontend\ProductController@manageavailability')->name('product.manageavailability');
    Route::get('wish-list', 'frontend\ProductController@wishlist')->name('product.wishlist');
    Route::get('remove-wishlist/{id}', 'frontend\ProductController@removewishlist')->name('product.removewishlist');

    Route::post('getmorewishlist', 'frontend\ProductController@getmorewishlist')->name('product.getmorewishlist');
    
    Route::post('addanswer', 'frontend\ProductController@addanswer')->name('product.addanswer');
    Route::post('sorting-qa', 'frontend\ProductController@sortingqa')->name('product.sortingqa');
    Route::post('getmoreqalist', 'frontend\ProductController@getmoreqalist')->name('product.getmoreqalist');
    
    
    Route::get('messaging/{id?}', 'frontend\MessagingController@messaging')->name('messaging.messaging');
    
    Route::post('messagecontactslist/{id?}', 'frontend\MessagingController@messagecontactslist')->name('messaging.messagecontactslist');
    
    Route::post('getusermesssage/{id?}', 'frontend\MessagingController@getusermesssage')->name('messaging.getusermesssage');

    Route::post('postnewmsg/{id?}', 'frontend\MessagingController@postnewmsg')->name('messaging.postnewmsg');

    Route::get('get-reason/{id}', 'frontend\ProductController@getreason')->name('product.getreason');
    Route::post('add-event/{id}', 'frontend\ProductController@addevent')->name('product.addevent');
    Route::get('remove-event/{id}', 'frontend\ProductController@removeevent')->name('product.removeevent');

    
    Route::get('add-product/{id}', 'frontend\ProductController@addproduct')->name('product.addproduct');

    Route::get('deleteproduct/{id}', 'frontend\ProductController@deleteproduct')->name('product.deleteproduct');

    Route::get('edit-product/{id}', 'frontend\ProductController@addproduct')->name('product.addproduct');
    Route::patch('save-product/{id}', 'frontend\ProductController@saveproduct')->name('product.saveproduct');
    
    Route::get('manage-product', 'frontend\ProductController@manageproduct')->name('product.manageproduct');
    Route::get('manage-Q&a', 'frontend\ProductController@manageQa')->name('product.manageQa');
    Route::get('getquestion/{id}', 'frontend\ProductController@getquestion')->name('product.getquestion');
    Route::post('manage-searching', 'frontend\ProductController@managesearching')->name('product.managesearching');
    Route::post('getmoremanageproduct', 'frontend\ProductController@getmoremanageproduct')->name('product.getmoremanageproduct');

    Route::post('pro-documents', 'frontend\ProductController@prodocuments')->name('product.prodocuments');
    Route::delete('remove-files', 'frontend\ProductController@removefiles')->name('product.removefiles');
    Route::delete('remove-edit-files', 'frontend\ProductController@removefiles')->name('product.removefiles');

    /** Booking Section */
    Route::match(['get','post'],'get-booking-form', 'frontend\BookingController@getbookingform')->name('booking.getform');
    Route::match(['get','post'],'get-booking-data', 'frontend\BookingController@getbookingdata')->name('booking.getdata');
    Route::match(['get','post'],'add-booking', 'frontend\BookingController@addbooking')->name('booking.addbooking');

    /** My bookings */
    Route::get('my-bookings', 'frontend\BookingController@mybookings')->name('booking.mybookings');
    Route::match(['get','post'],'get-more-bookings', 'frontend\BookingController@getmorebookings')->name('booking.getmore');
    Route::match(['get','post'],'booking-approve', 'frontend\BookingController@approvestatus')->name('booking.approvestatus');
    Route::match(['get','post'],'give-review', 'frontend\BookingController@givereview')->name('booking.givereview');

    Route::get('sharer-bookings', 'frontend\BookingController@sharerbookings')->name('booking.sharerbookings');
    Route::match(['get','post'],'get-more-sharer-bookings', 'frontend\BookingController@getmoresharerbookings')->name('booking.getmoresharer');
    Route::match(['get','post'],'cancel-booking', 'frontend\BookingController@cancelbooking')->name('booking.cancelbooking');
    Route::match(['get','post'],'report-an-issued', 'frontend\BookingController@reportissue')->name('booking.reportissue');
    Route::match(['get','post'],'check-in-out', 'frontend\BookingController@checkinout')->name('booking.checkinout');



});


Route::group(['middleware' => ['admin']], function () {
    
    Route::get('admin/dashboard', 'backend\AdminController@dashboard')->name('admin.dashboard');
    Route::post('admin/logout', 'backend\Auth\LoginController@getLogout');

    /* User Pages */
    Route::get('admin/users/{type}', 'backend\UsersController@index')->name('admin.users.index');
    Route::get('admin/users/getdata/{type}', 'backend\UsersController@getdata')->name('admin.users.getdata');
    Route::get('admin/users/create', 'backend\UsersController@create')->name('admin.users.create');
    Route::post('admin/users/store', 'backend\UsersController@store')->name('admin.users.store');
    Route::get('admin/users/edit/{id}', 'backend\UsersController@edit')->name('admin.users.edit');
    Route::patch('admin/users/update/{id}', 'backend\UsersController@update')->name('admin.users.update');
    Route::delete('admin/users/destroy', 'backend\UsersController@destroy')->name('admin.users.destroy');
	Route::get('admin/users/show/{id}', 'backend\UsersController@show')->name('admin.users.show');
	Route::get('admin/users/changestatus/{id}/{type}', 'backend\UsersController@changestatus')->name('admin.users.changestatus');
    Route::get('admin/users/changebgstatus/{id}/{type}', 'backend\UsersController@changebgstatus')->name('admin.users.changebgstatus');
    Route::get('admin/users/changeidstatus/{id}/{type}', 'backend\UsersController@changeidstatus')->name('admin.users.changeidstatus');

	Route::get('admin/users/accessaccount/{id}', 'backend\UsersController@accessaccount')->name('admin.users.accessaccount');
    Route::post('admin/users/updatebgcheck/{id}', 'backend\UsersController@updatebgcheck')->name('admin.updatebgcheck');
    Route::post('admin/users/updateidcheck/{id}', 'backend\UsersController@updateidcheck')->name('admin.updateidcheck');

    Route::get('admin/edit', 'backend\AdminController@edit')->name('admin.edit');
    Route::patch('admin/update', 'backend\AdminController@update')->name('admin.update');
	Route::get('admin/checkemail', 'backend\AdminController@checkemail')->name('admin.checkemail');
	Route::get('admin/checkoldpassword', 'backend\AdminController@checkoldpassword')->name('admin.checkoldpassword');

    Route::get('admin/changepassword', 'backend\AdminController@changepassword')->name('admin.changepassword');
    Route::patch('admin/updatepassword', 'backend\AdminController@updatepassword')->name('admin.updatepassword');

	/* Admin Config */
    Route::get('admin/config', 'backend\ConfigController@index')->name('admin.config.index');
    Route::patch('admin/config/update', 'backend\ConfigController@update')->name('admin.config.update');
	
    /* Admin Pages */
    Route::get('admin/pages/index', 'backend\PagesController@index')->name('admin.pages.index');
    Route::get('admin/pages/getdata', 'backend\PagesController@getdata')->name('admin.pages.getdata');
    Route::get('admin/pages/create', 'backend\PagesController@create')->name('admin.pages.create');
    Route::post('admin/pages/store', 'backend\PagesController@store')->name('admin.pages.store');
    Route::get('admin/pages/edit/{id}', 'backend\PagesController@edit')->name('admin.pages.create');
    Route::patch('admin/pages/update/{id}', 'backend\PagesController@update')->name('admin.pages.update');

    // Route::post('admin/pages/update/{id}', 'backend\PagesController@update')->name('admin.pages.update');
    Route::delete('admin/pages/destroy', 'backend\PagesController@destroy')->name('admin.pages.destroy');
    Route::get('admin/pages/show/{id}', 'backend\PagesController@show')->name('admin.pages.show');
    Route::get('admin/pages/changestatus/{id}', 'backend\PagesController@changestatus')->name('admin.pages.changestatus');
    Route::get('admin/pages/editdesigned/{key}','backend\PagesController@editdesigned')->name('admin.pages.editdesigned');
    Route::post('admin/pages/edithomepage','backend\PagesController@edithomepage')->name('admin.pages.edithomepage');
    Route::post('admin/pages/editphysiopage','backend\PagesController@editphysiopage')->name('admin.pages.editphysiopage');

	/* Admin Templates */
    Route::get('admin/templates/index', 'backend\TemplatesController@index')->name('admin.templates.index');
    Route::get('admin/templates/getdata', 'backend\TemplatesController@getdata')->name('admin.templates.getdata');
    Route::get('admin/templates/edit/{id}', 'backend\TemplatesController@edit')->name('admin.templates.edit');
    Route::patch('admin/templates/update/{id}', 'backend\TemplatesController@update')->name('admin.templates.update');
    Route::get('admin/templates/show/{id}', 'backend\TemplatesController@show')->name('admin.templates.show');
	
	/* Admin FAQ */
    Route::get('admin/faq/index', 'backend\FaqController@index')->name('admin.faq.index');
    Route::get('admin/faq/getdata', 'backend\FaqController@getdata')->name('admin.faq.getdata');
    Route::get('admin/faq/create', 'backend\FaqController@create')->name('admin.faq.create');
    Route::post('admin/faq/store', 'backend\FaqController@store')->name('admin.faq.store');
    Route::get('admin/faq/edit/{id}', 'backend\FaqController@edit')->name('admin.faq.create');
    Route::patch('admin/faq/update/{id}', 'backend\FaqController@update')->name('admin.faq.update');
    Route::delete('admin/faq/destroy', 'backend\FaqController@destroy')->name('admin.faq.destroy');
    Route::get('admin/faq/show/{id}', 'backend\FaqController@show')->name('admin.faq.show');
    Route::get('admin/faq/changestatus/{id}', 'backend\FaqController@changestatus')->name('admin.faq.changestatus');

	/* Admin Specialties */
    Route::get('admin/specialties/index', 'backend\SpecialtiesController@index')->name('admin.specialties.index');
    Route::get('admin/specialties/getdata', 'backend\SpecialtiesController@getdata')->name('admin.specialties.getdata');
    Route::get('admin/specialties/create', 'backend\SpecialtiesController@create')->name('admin.specialties.create');
    Route::post('admin/specialties/store', 'backend\SpecialtiesController@store')->name('admin.specialties.store');
    Route::get('admin/specialties/edit/{id}', 'backend\SpecialtiesController@edit')->name('admin.specialties.create');
    Route::patch('admin/specialties/update/{id}', 'backend\SpecialtiesController@update')->name('admin.specialties.update');
    Route::delete('admin/specialties/destroy', 'backend\SpecialtiesController@destroy')->name('admin.specialties.destroy');
	Route::get('admin/specialties/changestatus/{id}', 'backend\SpecialtiesController@changestatus')->name('admin.specialties.changestatus');
	
	/* Admin Keywords */
    Route::get('admin/resource/keywords', 'backend\ResourceController@keywords')->name('admin.resource.keywords');
    Route::get('admin/resource/getkeywords', 'backend\ResourceController@getkeywords')->name('admin.resource.getkeywords');
    Route::get('admin/resource/editkeyword/{id}', 'backend\ResourceController@editkeyword')->name('admin.resource.createkeyword');
    Route::patch('admin/resource/updatekeyword/{id}', 'backend\ResourceController@updatekeyword')->name('admin.resource.updatekeyword');
    Route::post('admin/mediaupload', 'backend\AdminController@mediaupload')->name('admin.mediaupload');   

    /** Admin Products */ 
    Route::get('admin/product/add-product-categories/{id}', 'backend\ProductController@addproductcategories');
    Route::post('admin/product/create-product-categories', 'backend\ProductController@createproductcategories');
    Route::post('admin/product/update-product-categories/{id}', 'backend\ProductController@updateproductcategories');
    Route::get('admin/product/status-main-cat/{id}', 'backend\ProductController@statusmaincat');
    Route::get('admin/product/fetured-gear/{id}', 'backend\ProductController@feturedgear');

     
    Route::get('admin/product/show-catagory/{id}', 'backend\ProductController@showcatagory');
    Route::get('admin/product/show-product/{id}', 'backend\ProductController@showproduct');
    Route::delete('admin/product/approve-destroy', 'backend\ProductController@approvedestroy')->name('admin.product.approvedestroy');
    Route::delete('admin/product/destroy', 'backend\ProductController@destroy')->name('admin.product.destroy');
    Route::delete('admin/product/product-destroy', 'backend\ProductController@productdestroy')->name('admin.product.productdestroy');
    Route::get('admin/subproduct/sub-product', 'backend\SubProductController@subproduct');
    Route::get('admin/subproduct/subgetproductcategories', 'backend\SubProductController@subgetproductcategories');
    Route::get('admin/subproduct/create-sub-product/{id}', 'backend\SubProductController@createsubproduct');
    Route::get('admin/subproduct/show-sub-cat/{id}', 'backend\SubProductController@showsubcat');
    Route::delete('admin/subproduct/destroy', 'backend\SubProductController@destroy')->name('admin.subproduct.destroy');

    Route::get('admin/subproduct/change-status/{id}', 'backend\SubProductController@changestatus');
    Route::post('admin/subproduct/add-sub-product', 'backend\SubProductController@addsubproduct');
    Route::post('admin/subproduct/update-subcat/{id}', 'backend\SubProductController@updatesubcat');
    
    Route::get('admin/product/manage-request', 'backend\ProductController@managerequest')->name('admin.product.managerequest');
    Route::post('admin/product/decline-prduct', 'backend\ProductController@declineprduct')->name('admin.product.declineprduct');
    Route::get('admin/product/product-request', 'backend\ProductController@productrequest')->name('admin.product.productrequest');
    Route::get('admin/product/product-status/{id}', 'backend\ProductController@productstatus')->name('admin.product.productstatus');
    Route::get('admin/product/approve-prduct/{id}', 'backend\ProductController@approveprduct')->name('admin.product.approveprduct');
    Route::get('admin/product/get-managerequest', 'backend\ProductController@getmanagerequest')->name('admin.product.getmanagerequest');
    Route::get('admin/product/getproductrequest', 'backend\ProductController@getproductrequest')->name('admin.product.getproductrequest');
    Route::get('admin/product/product-categories', 'backend\ProductController@productcategories')->name('admin.product.productcategories');
    Route::get('admin/product/getproductcategories', 'backend\ProductController@getproductcategories')->name('admin.product.getproductcategories');

    /** Bookings and Reviews */
    Route::match(['get','post'],'bookings', 'backend\BookingController@bookings')->name('admin.booking.bookings');
    Route::match(['get','post'],'cancel-bookings', 'backend\BookingController@cancelbookings')->name('admin.booking.cancelbookings');
    Route::match(['get','post'],'get-bookings', 'backend\BookingController@getbookings')->name('admin.booking.getbookings');
    Route::match(['get','post'],'get-cancel-bookings', 'backend\BookingController@getcancelbookings')->name('admin.booking.getcancelbookings');
    Route::match(['get','post'],'view-bookings/{bkid?}', 'backend\BookingController@viewbookings')->name('admin.booking.viewbookings');
    Route::match(['get','post'],'reviews', 'backend\BookingController@reviews')->name('admin.booking.reviews');
    Route::match(['get','post'],'admin-get-reviews', 'backend\BookingController@getreviews')->name('admin.booking.getreviews');
    Route::match(['get','post'],'admin-view-review/{rvid}', 'backend\BookingController@viewreviews')->name('admin.booking.viewreviews');
    Route::match(['get','post'],'remove-reviews', 'backend\BookingController@removereviews')->name('admin.booking.removereviews');

    Route::get('/about-us',function($name){
        return view('aboutus',['name'=>$name]);
    });
 
});